/**
 * Description this is dashboard script
 **/

/**
 * Description POST LIKE
 **/

$('#poll-submit').click(function () {

    var url = $(this).attr('data-url');
    var this_name = $(this);
    var frm = $('#poll_from');
    $.ajax({
        type: 'post',
        url: url,
        dataType: 'json',
        data: frm.serialize(),
        beforeSend: function() {
            // before send code here
            $(this_name).html('<i class="fa fa-spinner  fa-spin"></i>');
        },
        complete: function() {
            // complete code here
        },
        success: function(data)
        {

            //alert(data['status']);
            // success code here
            if(data['status'] === 1)
            {
                $('#poll-success').addClass('hidden');
                $('#poll-warning').removeClass('hidden');

                var html = '';
                $.each(data['vote_option'],function (key, item) {
                    html += '<li>'+item+'</li>';
                });


                $('#voted_options').html(html);

                $(this_name).html('vote');

                $('#poll-chart').html(data['view']);
            }
            else {
                $(this_name).html('vote');
                $('#poll-success').removeClass('hidden');
                $('#poll-warning').addClass('hidden');

                $('#poll-chart').html(data['view']);
            }
        }
    });
});


/**
 * Description POST LIKE
 **/

$('.admin-view-results').click(function () {

    var url = $(this).attr('data-url');
    var this_name = $(this);
    $.ajax({
        type: 'get',
        url: url,
        dataType: 'json',
        beforeSend: function() {
            // before send code here
            $(this_name).html('<i class="fa fa-spinner  fa-spin"></i>');
        },
        complete: function() {
            // complete code here
        },
        success: function(data)
        {
            $('#admin-poll-view-results').html(data['view']);
            $(this_name).html('Results');
        }
    });
});




/**
 * Description USER FOLLOWS
 **/

$('.follow-btn').click(function () {

    var user_id = $(this).attr('data-user_id');
    var url = $(this).attr('data-url');
    var token =  $("input[name=_token]").val();
    var this_name = $(this);
    $.ajax({
        type: 'post',
        url: url,
        dataType: 'json',
        data: {'user_id': user_id, '_token': token},
        beforeSend: function() {
            // before send code here
            $(this_name).html('<i class="fa fa-spinner  fa-spin"></i>');
        },
        complete: function() {
            // complete code here
        },
        success: function(data)
        {
            // success code here
            if(data['status'] === 1)
            {
                $(this_name).html('<i class="fa fa-check"></i> Following');
            }
            else {
                $(this_name).html('<i class="fa fa-check"></i> Follow');
            }
        }
    });
});


/**
 * Description USER CONNECTIONS
 **/

$('.connection-btn').click(function () {

    var user_id = $(this).attr('data-user_id');
    var url = $(this).attr('data-url');
    var token =  $("input[name=_token]").val();
    var this_name = $(this);
    $.ajax({
        type: 'post',
        url: url,
        dataType: 'json',
        data: {'user_id': user_id, '_token': token},
        beforeSend: function() {
            // before send code here
            $(this_name).html('<i class="fa fa-spinner  fa-spin"></i>');
        },
        complete: function() {
            // complete code here
        },
        success: function(data)
        {
            // success code here
            if(data['status'] === 1)
            {
                $(this_name).html('<i class="fa fa-link"></i> Connecting');
            }
            else {
                $(this_name).html('<i class="fa fa-link"></i> Connect');
            }
        }
    });
});


/**
 * Description POST LIKE
 **/

$('.post-like-btn').click(function () {

    var post_id = $(this).attr('data-post_id');
    var url = $(this).attr('data-url');
    var token =  $("input[name=_token]").val();
    var this_name = $(this);
    var like_span = $('#post-like-'+post_id);
    $.ajax({
        type: 'post',
        url: url,
        dataType: 'json',
        data: {'post_id': post_id, '_token': token},
        beforeSend: function() {
            // before send code here
            $(this_name).html('<i class="fa fa-spinner  fa-spin"></i>');
        },
        complete: function() {
            // complete code here
        },
        success: function(data)
        {
            // success code here
            if(data['status'] === 1)
            {
                $(this_name).html('<i class="fa fa-thumbs-up"></i> liked');
                $(like_span).html(data['post_like_count']);
            }
            else {
                $(this_name).html('<i class="fa fa-thumbs-up"></i> like');
                $(like_span).html(data['post_like_count']);
            }
        }
    });
});

/**
 * Description Remove comments
 */
$('.comments').on('click', '.post-comment-remove-btn',function () {

    var url = $(this).attr('data-url');
    var post_id = $(this).attr('data-post_id');
    var post_comment_id = $(this).attr('data-post_comment_id');
    var token =  $("input[name=_token]").val();
    var comment_span = $('#post-comment-'+post_id);
    var li_comment_id = $('#li-comment-id-'+post_comment_id);
    var r = confirm("Are you sure, Do want to delete your comment?");
    if (r == true) {
        $.ajax({
            type: 'post',
            url: url,
            dataType: 'json',
            data: {'post_comment_id': post_comment_id, 'post_id': post_id, '_token': token},
            beforeSend: function() {
                // before send code here
                $(this).html('<i class="fa fa-spinner  fa-spin"></i>');
            },
            complete: function() {
                // complete code here
            },
            success: function(data)
            {
                $(comment_span).html(data['post_comment_count']);
                $(li_comment_id).remove();
            }
        });
    } else {
        // txt = "You pressed Cancel!";
    }




});

/**
 * Description ADD POST COMMENT
 */
$('.post-comment-btn').click(function (e) {

    var url = $(this).attr('data-url');
    var remove_url = $(this).attr('data-remove_url');
    var unique_id = $(this).attr('data-unique_id');
    var form_id = $(this).attr('data-form_id');
    var token =  $("input[name=_token]").val();

    var post_id = $('#post-id-'+unique_id).val();
    var description = $('#description-'+unique_id).val();
    var description_text = 'description-'+unique_id;
    var image = $('#comment-picture-'+unique_id).val();
    var video = $('#comment-video-'+unique_id).val();
    var level = $('#comment-level-'+unique_id).val();
    var parent_id = $('#parent-id-'+unique_id).val();
    var comment_span = $('#post-comment-'+unique_id);
    var comment_ul = $('#comments-'+unique_id+' li:last');
    var level_2_comment_ul  = $('#level-2-comment-box-'+unique_id+' li:last');

    $.ajax({
        type: 'post',
        url: url,
        dataType: 'json',
        data: {'post_id': post_id, 'description': description, 'image': image, 'video': video, 'level': level, '_token': token, 'parent_id' : parent_id},
        beforeSend: function() {
            // before send code here
            $(this).html('<i class="fa fa-spinner  fa-spin"></i>');
        },
        complete: function() {
            // complete code here
        },
        success: function(data)
        {
            $(comment_span).html(data['post_comment_count']);

            // console.log(data['image']);
            // alert(data['image']);

            var html = '';
            html += '<li id="li-comment-id-'+data['post_comment_id']+'">';
            html += '<img width="50px" src="'+data['profile_image']+'" alt="'+data['first_name']+'" class="online">';
            html += '<span class="name">'+data['first_name']+' '+data['last_name']+'</span>';
            html += '<button class="btn btn-sm btn-info pull-left post-comment-remove-btn" type="button" data-post_id="'+post_id+'" data-url="'+remove_url+'" data-post_comment_id="'+data['post_comment_id']+'" >';
            html += '<i class="fa fa-recycle" onclick=""></i>';
            html += '</button>';
            if(data['image_comment'] != 'no_image'){
                html += '<br/><img src="'+data['image_comment']+'" style="width: 100px; height: 50px; padding-left: 39px;"><br/><br/><br/><div class="clearfix"></div>';
            }
            if(data['description'] != 'no_description') {
                html += '<p style="margin-top: 13px;">'+data['description']+'</p>';
            }
            html += '</li>';

            if(data['image_comment'] != 'no_image' || data['description'] != 'no_description')
            {
                if(level == 1)
                {
                    $(comment_ul).before(html);
                }else{
                    $(level_2_comment_ul).before(html);
                }
            }

            // reset form
            $('#'+description_text).val('');
            $(image).val('');
            $(video).val('');

        }
    });
});


function showCommentBox(post_comment_id) {
    $('.level-2-comment-box').addClass('hidden');
    $('#level-2-comment-box-two-'+post_comment_id).removeClass('hidden');
}

/**
 * Description image upload and priviws
 */

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#profile_image_view').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}

$("#profile_image").change(function() {
    $('#profile_image_view').removeClass('hidden');
    readURL(this);
});

function readURLCover(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function(e) {
            $('#cover_image_view').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}

$("#cover_image").change(function() {
    $('#cover_image_view').removeClass('hidden');
    readURLCover(this);
});

//
// $('#profile-photo-btn').click(function (e) {
//
//     var url = $(this).attr('data-url');
//     var this_name = $(this);
//     e.preventDefault();
//     $.ajax({
//         type: 'post',
//         url: url,
//         dataType: 'json',
//         data: $('#profile-photo-form').serialize(),
//         beforeSend: function() {
//             // before send code here
//             $(this_name).html('<i class="fa fa-spinner  fa-spin"></i>');
//         },
//         complete: function() {
//             // complete code here
//         },
//         success: function(data)
//         {
//             // success code here
//             if(data['status'] === 1)
//             {
//                 $(this_name).html('<i class="fa fa-link"></i> Connecting');
//             }
//             else {
//                 $(this_name).html('<i class="fa fa-link"></i> Connect');
//             }
//         }
//     });
// });





/**
 * Description ALERT BOX for response
 */

// common ajax request handle

function ajaxRequest()
{
    $.ajax({
        type: 'post',
        url: "email_send.php",
        dataType: 'json',
        data: 'username='+name+'&useremail='+email+'&message='+message,
        beforeSend: function() {
            // before send code here
        },
        complete: function() {
            // complete code here
        },
        success: function(data)
        {
            // success code here
        }
    });
}