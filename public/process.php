<?php 

//print_r($_POST);
 
$action=$_GET['action'];

if($action=='create')
{
  /*  $signature_arr = array(
    'LK' => 'ezy_signature.jpg',
    'IN' => 'ezy_signature_india.jpg',
    'BD' => 'ezy_signature_bangladesh.jpg',
    'KH' => 'ezy_signature_cambodia.jpg',
    'MM' => 'ezy_signature_myanmar.jpg',
    'PK' => 'ezy_signature_pakistan.jpg',
    'PH' => 'ezy_signature_philippines.jpg',
    'SG' => 'ezy_signature_singapore.jpg',
    'LA' => 'ezy_signature_laos.jpg',
    'NP' => 'ezy_signature_nepal.jpg',
    'VN' => 'ezy_signature_vietnam.jpg'
);*/

$signature_arr = array(
    'LK' => 'ezy_signature',
    'IN' => 'ezy_signature_india',
    'BD' => 'ezy_signature_bangladesh',
    'KH' => 'ezy_signature_cambodia',
    'MM' => 'ezy_signature_myanmar',
    'PK' => 'ezy_signature_pakistan',
    'PH' => 'ezy_signature_philippines',
    'SG' => 'ezy_signature_singapore',
    'LA' => 'ezy_signature_laos',
    'NP' => 'ezy_signature_nepal',
    'VN' => 'ezy_signature_vietnam'
);

$companyArr = array(
  ['EZY INFOTECH PVT LTD','EZY DISTRIBUTION PVT LTD','EZY INTELLECT'], //BAN 0
  ['EZY DISTRIBUTION PVT LTD','EZY INFOTECH PVT LTD','EZY  TECHNOCRAFT PVT LTD', 'EZY  INTELLECT PVT LTD'], //  LK 1
  ['EZY INTELLECT PTE LTD','EZY SEA TECHNOLOGIES LTD'], //CAMBODIA 2
  ['EZY INFOTECH SOLUTIONS INDIA PVT LTD'],//india 3
  ['EZY INTELLECT PTE LTD'],//laos 4
  ['EZY INFOTECH ME FZE'], //middle east 5
  ['EZY INFOTECH COMPANY LIMITED','EZY INTELLECT COMPANY LIMITED','CITOO SOLUTIONS COMPANY LIMITED'],//MYANMAR 6
  ['EZY INFOTECH NEPAL PVT LTD'],//NEPAL 7
  ['EZY TECHNOLOGIES PVT LTD','EZY INFOTECH PVT LTD','EZY INTELLECT PVT LTD'], //PAKISTAN 8
  ['EZY INFOTECH INC'],//PHILIPPINES 9
  ['EZY INFOTECH PTE LTD','CITOO SOLUTIONS PTE LTD','EZY INTELLECT PTE LTD'], //SINGAPORE 10
  ['ELITE TECHNOLOGY JSC'], //VIETNAM 11
);

	$country =	trim($_POST['country']);
	$userName = trim($_POST['fullname']);
	$userPosition = trim($_POST['position']);
	$userSection =	trim($_POST['section']);
    $userMobile =	trim($_POST['mobile']);
	$fax =	trim($_POST['fax']);
	$company_name =	trim($_POST['company_name']);

    //$email = trim($_POST['email']);

function imagettftextSp($image, $size, $angle, $x, $y, $color, $font, $text, $spacing = 0)
{        
    if ($spacing == 0)
    {
        imagettftext($image, $size, $angle, $x, $y, $color, $font, $text);
    }
    else
    {
        $temp_x = $x;
        for ($i = 0; $i < strlen($text); $i++)
        {
            $bbox = imagettftext($image, $size, $angle, $temp_x, $y, $color, $font, $text[$i]);
            $temp_x += $spacing + ($bbox[2] - $bbox[0]);
        }
    }
}		
	// Set Path to Font File
      $font_path = 'fonts/EK03Plain-M02.ttf';
      $font_path2 = 'fonts/EK03Plain-B02.ttf';
      $font_path3 = 'fonts/EK03Serif-B01.ttf';

//unlink(getcwd().'/signature_final_'.$country.'.jpg');

if($country=='LK') {

         $countryImg = $signature_arr[$country];
        //echo $countryImg.'ffffffff';
		
		if($company_name === trim($companyArr[1][0])){
			$jpg_image = imagecreatefromjpeg(getcwd().'/images/'.$country.'/LK_01.jpg');
		}
		else if($company_name === trim($companyArr[1][1])){
			 $jpg_image = imagecreatefromjpeg(getcwd().'/images/'.$country.'/LK_02.jpg');			
		}
		else if($company_name === trim($companyArr[1][2])){
			 $jpg_image = imagecreatefromjpeg(getcwd().'/images/'.$country.'/LK_03.jpg');			
		}
		else{
			 $jpg_image = imagecreatefromjpeg(getcwd().'/images/'.$country.'/LK_04.jpg');			
		}
     
	  // Allocate A Color For The Text
      $font_color = imagecolorallocate($jpg_image, 55, 52, 52);
      $font_color2 = imagecolorallocate($jpg_image, 127, 128, 132);
      $font_color3 = imagecolorallocate($jpg_image, 102, 102, 102);

        $leftpost=5;
        $leftpost2=177;
        $leftpost3=334;
        $leftpost4=308;        
        
        // name
       // imagettftext(image src, font size, alignmemnt, from left, from top, $font_color, $font_path3, $userName);
 		imagettftext($jpg_image, 13, 0, 10, 25, $font_color, $font_path3, $userName);
        
        // position
        imagettftext($jpg_image, 9, 0, 10,42, $font_color2, $font_path2, $userPosition);

        // Section
        imagettftextSp($jpg_image, 9, 0, 10,57, $font_color2, $font_path2, $userSection, 0);
         //imagettftext($jpg_image, 10, 0, $leftpost, 56, $font_color2, $font_path2, $userSection);
        
        // userMobile
        imagettftext($jpg_image, 8, 0,178, 125, $font_color3, $font_path3, 'Mobile:');
        imagettftext($jpg_image, 8, 0, 220, 125, $font_color2, $font_path3, $userMobile);
		
		imagettftext($jpg_image, 8, 0,178, 137, $font_color3, $font_path3, 'Fax:');
        imagettftext($jpg_image, 8, 0, 198, 137, $font_color2, $font_path3, $fax);
		
		//imagettftext($jpg_image, 8, 0, 262, 180, $font_color2, $font_path3, $company_name);
        
        // userTp
       /* imagettftext($jpg_image, 8, 0, $leftpost4, 123, $font_color3, $font_path3, '| Tel:');
        imagettftext($jpg_image, 8, 0, $leftpost3, 123, $font_color2, $font_path3, $userTp);
     */
}
   
    
// 2**************************************************************** india ****************************************************************
if($country=='IN') {

         $countryImg = $signature_arr[$country];
       //echo $countryImg.'ffffffff';
	   
	   
	   /*if($company_name === trim($companyArr[3][0])){
			$jpg_image = imagecreatefromjpeg(getcwd().'/images/'.$countryImg);
		}
		else if($company_name === trim($companyArr[3][1])){
			 //$jpg_image = imagecreatefromjpeg(getcwd().'/images/'.$countryImg);			
		}
		else if($company_name === trim($companyArr[3][2])){
			 //$jpg_image = imagecreatefromjpeg(getcwd().'/images/'.$countryImg);			
		}
		else{
			 //$jpg_image = imagecreatefromjpeg(getcwd().'/images/'.$countryImg);			
		}*/
	   
      $jpg_image = imagecreatefromjpeg(getcwd().'/images/'.$country.'/IN_01.jpg');

      // Allocate A Color For The Text
      $font_color = imagecolorallocate($jpg_image, 55, 52, 52);
      $font_color2 = imagecolorallocate($jpg_image, 127, 128, 132);
      $font_color3 = imagecolorallocate($jpg_image, 102, 102, 102);

   
      
        $leftpost=61;
        $leftpost2=236;
        $leftpost3=334;
        $leftpost4=308;
        
        
        // name
 		imagettftext($jpg_image, 13, 0, 10, 25, $font_color, $font_path3, $userName);
        
        // position
        imagettftext($jpg_image, 9, 0, 10,42, $font_color2, $font_path2, $userPosition);

        // Section
        imagettftextSp($jpg_image, 9, 0, 10,57, $font_color2, $font_path2, $userSection, 0);
         //imagettftext($jpg_image, 10, 0, $leftpost, 56, $font_color2, $font_path2, $userSection);
        
        // userMobile
        imagettftext($jpg_image, 8, 0,178, 125, $font_color3, $font_path3, 'Mobile:');
        imagettftext($jpg_image, 8, 0, 215, 125, $font_color2, $font_path3, $userMobile);
		
		imagettftext($jpg_image, 8, 0,178, 137, $font_color3, $font_path3, 'Fax:');
        imagettftext($jpg_image, 8, 0, 198, 137, $font_color2, $font_path3, $fax);
        // userTp
       /* imagettftext($jpg_image, 8, 0, $leftpost4, 123, $font_color3, $font_path3, '| Tel:');
        imagettftext($jpg_image, 8, 0, $leftpost3, 123, $font_color2, $font_path3, $userTp);*/
     
}



// 3**************************************************************** Bangladesh ****************************************************************
if($country=='BD') {

         $countryImg = $signature_arr[$country];
       //echo $countryImg.'ffffffff';
	   
	   if($company_name === trim($companyArr[0][0])){
			$jpg_image = imagecreatefromjpeg(getcwd().'/images/'.$country.'/BD_01.jpg');
		}
		else if($company_name === trim($companyArr[0][1])){
			 $jpg_image = imagecreatefromjpeg(getcwd().'/images/'.$country.'/BD_02.jpg');			
		}
		else{
			 $jpg_image = imagecreatefromjpeg(getcwd().'/images/'.$country.'/BD_03.jpg');
		}
	   

      // Allocate A Color For The Text
      $font_color = imagecolorallocate($jpg_image, 55, 52, 52);
      $font_color2 = imagecolorallocate($jpg_image, 127, 128, 132);
      $font_color3 = imagecolorallocate($jpg_image, 102, 102, 102);

   
      
        $leftpost=70;
        $leftpost2=233;
        $leftpost3=220;
        $leftpost4=308;
        
        
        // name
 		imagettftext($jpg_image, 13, 0, 10, 25, $font_color, $font_path3, $userName);
        
        // position
        imagettftext($jpg_image, 9, 0, 10,42, $font_color2, $font_path2, $userPosition);

        // Section
        imagettftextSp($jpg_image, 9, 0, 10,57, $font_color2, $font_path2, $userSection, 0);
         //imagettftext($jpg_image, 10, 0, $leftpost, 56, $font_color2, $font_path2, $userSection);
        
        // userMobile
        imagettftext($jpg_image, 8, 0,178, 124, $font_color3, $font_path3, 'Mobile:');
        imagettftext($jpg_image, 8, 0, 220, 124, $font_color2, $font_path3, $userMobile);
		
		imagettftext($jpg_image, 8, 0,178, 137, $font_color3, $font_path3, 'Fax:');
        imagettftext($jpg_image, 8, 0, 198, 137, $font_color2, $font_path3, $fax);
        
        // userTp
        /*if(isset($userTp)) {
        imagettftext($jpg_image, 7, 0, 270, 95, $font_color3, $font_path3, '| Tel:');
        imagettftext($jpg_image, 7, 0, 295, 95, $font_color2, $font_path3, $userTp);
        }*/
     
}




// 4**************************************************************** Cambodia ****************************************************************
if($country=='KH') {

         $countryImg = $signature_arr[$country];
       //echo $countryImg.'ffffffff';
	   
	    if($company_name === trim($companyArr[2][0])){
			 $jpg_image = imagecreatefromjpeg(getcwd().'/images/'.$country.'/KH_01.jpg');
		}
		else {
			 $jpg_image = imagecreatefromjpeg(getcwd().'/images/'.$country.'/KH_02.jpg');		
		}

      // Allocate A Color For The Text
      $font_color = imagecolorallocate($jpg_image, 55, 52, 52);
      $font_color2 = imagecolorallocate($jpg_image, 127, 128, 132);
      $font_color3 = imagecolorallocate($jpg_image, 102, 102, 102);

   
      
        $leftpost=70;
        $leftpost2=233;
        $leftpost3=220;
        $leftpost4=308;
        
        
        imagettftext($jpg_image, 13, 0, 10, 25, $font_color, $font_path3, $userName);
        
        // position
        imagettftext($jpg_image, 9, 0, 10,42, $font_color2, $font_path2, $userPosition);

        // Section
        imagettftextSp($jpg_image, 9, 0, 10,57, $font_color2, $font_path2, $userSection, 0);
         //imagettftext($jpg_image, 10, 0, $leftpost, 56, $font_color2, $font_path2, $userSection);
        
        // userMobile
        imagettftext($jpg_image, 8, 0,178, 123, $font_color3, $font_path3, 'Mobile:');
        imagettftext($jpg_image, 8, 0, 220, 123, $font_color2, $font_path3, $userMobile);
        
        // userTp
        /*if(isset($userTp) && $userTp !='') {
            imagettftext($jpg_image, 7, 0, $leftpost4, 123, $font_color3, $font_path3, '| Tel:');
            imagettftext($jpg_image, 7, 0, $leftpost3, 123, $font_color2, $font_path3, $userTp);
        }*/
     
}


// 5**************************************************************** Myanmar ****************************************************************
if($country=='MM') {

         $countryImg = $signature_arr[$country];
        //echo $countryImg.'ffffffff';
		
		if($company_name === trim($companyArr[6][0])){
			 $jpg_image = imagecreatefromjpeg(getcwd().'/images/'.$country.'/MM_01.jpg');
		}
		else if($company_name === trim($companyArr[6][0])){
			$jpg_image = imagecreatefromjpeg(getcwd().'/images/'.$country.'/MM_02.jpg');			
		}
		else {
			$jpg_image = imagecreatefromjpeg(getcwd().'/images/'.$country.'/MM_03.jpg');
		}
		
      // Allocate A Color For The Text
      $font_color = imagecolorallocate($jpg_image, 55, 52, 52);
      $font_color2 = imagecolorallocate($jpg_image, 127, 128, 132);
      $font_color3 = imagecolorallocate($jpg_image, 102, 102, 102);
      
        $leftpost=65;
        $leftpost2=200;
        $leftpost3=334;
        $leftpost4=308;
        
        
        // name
 		imagettftext($jpg_image, 13, 0, 10, 25, $font_color, $font_path3, $userName);
        
        // position
        imagettftext($jpg_image, 9, 0, 10,42, $font_color2, $font_path2, $userPosition);

        // Section
        imagettftextSp($jpg_image, 9, 0, 10,57, $font_color2, $font_path2, $userSection, 0);
         //imagettftext($jpg_image, 10, 0, $leftpost, 56, $font_color2, $font_path2, $userSection);
        
        // userMobile
        imagettftext($jpg_image, 8, 0,177, 124, $font_color3, $font_path3, 'Mobile:');
        imagettftext($jpg_image, 8, 0, 219, 124, $font_color2, $font_path3, $userMobile);
}

// 6**************************************************************** Pakistan ****************************************************************
if($country=='PK') {

         $countryImg = $signature_arr[$country];
       //echo $countryImg.'ffffffff';
	   
	   if($company_name === trim($companyArr[8][0])){
			 $jpg_image = imagecreatefromjpeg(getcwd().'/images/'.$country.'/PK_01.jpg');
		}
		else if($company_name === trim($companyArr[8][0])){
			$jpg_image = imagecreatefromjpeg(getcwd().'/images/'.$country.'/PK_02.jpg');			
		}
		else {
			$jpg_image = imagecreatefromjpeg(getcwd().'/images/'.$country.'/PK_03.jpg');
		}

      // Allocate A Color For The Text
      $font_color = imagecolorallocate($jpg_image, 55, 52, 52);
      $font_color2 = imagecolorallocate($jpg_image, 127, 128, 132);
      $font_color3 = imagecolorallocate($jpg_image, 102, 102, 102);

   
      
        $leftpost=65;
        $leftpost2=230;
        $leftpost3=334;
        $leftpost4=308;
        
        
        // name
    	imagettftext($jpg_image, 13, 0, 10, 25, $font_color, $font_path3, $userName);
        
        // position
        imagettftext($jpg_image, 9, 0, 10,42, $font_color2, $font_path2, $userPosition);

        // Section
        imagettftextSp($jpg_image, 9, 0, 10,57, $font_color2, $font_path2, $userSection, 0);
         //imagettftext($jpg_image, 10, 0, $leftpost, 56, $font_color2, $font_path2, $userSection);
        
        // userMobile
        imagettftext($jpg_image, 8, 0,178, 124, $font_color3, $font_path3, 'Mobile:');
        imagettftext($jpg_image, 8, 0, 220, 124, $font_color2, $font_path3, $userMobile);
        
        // userTp
        /*imagettftext($jpg_image, 8, 0, $leftpost4, 123, $font_color3, $font_path3, '| Tel:');
        imagettftext($jpg_image, 8, 0, $leftpost3, 123, $font_color2, $font_path3, $userTp);*/
     
}

// 7**************************************************************** Philippines ****************************************************************
if($country=='PH') {

         $countryImg = $signature_arr[$country];
       //echo $countryImg.'ffffffff';
	   
			 $jpg_image = imagecreatefromjpeg(getcwd().'/images/'.$country.'/PH_01.jpg');

      // Allocate A Color For The Text
      $font_color = imagecolorallocate($jpg_image, 55, 52, 52);
      $font_color2 = imagecolorallocate($jpg_image, 127, 128, 132);
      $font_color3 = imagecolorallocate($jpg_image, 102, 102, 102);

   
      
        $leftpost=125;
        $leftpost2=270;
        $leftpost3=334;
        $leftpost4=308;
        
        
        // name
 		imagettftext($jpg_image, 13, 0, 10, 15, $font_color, $font_path3, $userName);
        
        // position
        imagettftext($jpg_image, 9, 0, 10,35, $font_color2, $font_path2, $userPosition);

        // Section
        imagettftextSp($jpg_image, 9, 0, 10,55, $font_color2, $font_path2, $userSection, 0);
         //imagettftext($jpg_image, 10, 0, $leftpost, 56, $font_color2, $font_path2, $userSection);
        
        // userMobile
        imagettftext($jpg_image, 8, 0,178, 124, $font_color2, $font_path3, 'Mobile:');
        imagettftext($jpg_image, 8, 0, 220, 124, $font_color3, $font_path3, $userMobile);
        
        // userTp
        /*imagettftext($jpg_image, 8, 0, $leftpost4, 123, $font_color3, $font_path3, '| Tel:');
        imagettftext($jpg_image, 8, 0, $leftpost3, 123, $font_color2, $font_path3, $userTp);*/
     
}



// 8**************************************************************** Singapore ****************************************************************
if($country=='SG') {

         $countryImg = $signature_arr[$country];
       //echo $countryImg.'ffffffff';
	   
	   if($company_name === trim($companyArr[10][0])){
			 $jpg_image = imagecreatefromjpeg(getcwd().'/images/'.$country.'/SG_01.jpg');
		}
		else if($company_name === trim($companyArr[10][1])){
			$jpg_image = imagecreatefromjpeg(getcwd().'/images/'.$country.'/SG_01.jpg');			
		}
		else {
			$jpg_image = imagecreatefromjpeg(getcwd().'/images/'.$country.'/SG_01.jpg');
		}
	  

      // Allocate A Color For The Text
      $font_color = imagecolorallocate($jpg_image, 55, 52, 52);
      $font_color2 = imagecolorallocate($jpg_image, 127, 128, 132);
      $font_color3 = imagecolorallocate($jpg_image, 102, 102, 102);

   
      
        $leftpost=125;
        $leftpost2=280;
        $leftpost3=334;
        $leftpost4=308;
        
        
        // name
 		imagettftext($jpg_image, 13, 0, 10, 25, $font_color, $font_path3, $userName);
        
        // position
        imagettftext($jpg_image, 9, 0, 10,42, $font_color2, $font_path2, $userPosition);

        // Section
        imagettftextSp($jpg_image, 9, 0, 10,57, $font_color2, $font_path2, $userSection, 0);
         //imagettftext($jpg_image, 10, 0, $leftpost, 56, $font_color2, $font_path2, $userSection);
        
        // userMobile
        imagettftext($jpg_image, 8, 0,178, 125, $font_color3, $font_path3, 'Mobile:');
        imagettftext($jpg_image, 8, 0, 220, 125, $font_color2, $font_path3, $userMobile);
		
		imagettftext($jpg_image, 8, 0,178, 137, $font_color3, $font_path3, 'Fax:');
        imagettftext($jpg_image, 8, 0, 198, 137, $font_color2, $font_path3, $fax);
        // userTp
        /*imagettftext($jpg_image, 8, 0, $leftpost4, 123, $font_color3, $font_path3, '| Tel:');
        imagettftext($jpg_image, 8, 0, $leftpost3, 123, $font_color2, $font_path3, $userTp);*/
     
}

// 9**************************************************************** Laos ****************************************************************
if($country=='LA') {

         $countryImg = $signature_arr[$country];
       //echo $countryImg.'ffffffff';
	   
      $jpg_image = imagecreatefromjpeg(getcwd().'/images/'.$countryImg);

      // Allocate A Color For The Text
     	$font_color = imagecolorallocate($jpg_image, 55, 52, 52);
      	$font_color2 = imagecolorallocate($jpg_image, 127, 128, 132);
      	$font_color3 = imagecolorallocate($jpg_image, 102, 102, 102);

   
      
        $leftpost=65;
        $leftpost2=238;
        $leftpost3=334;
        $leftpost4=308;
        
        
        // name
 		imagettftext($jpg_image, 13, 0, 65, 50, $font_color, $font_path3, $userName);
        
        // position
        imagettftext($jpg_image, 10, 0, $leftpost, 65, $font_color2, $font_path2, $userPosition);

        // Section
        imagettftextSp($jpg_image, 10, 0, $leftpost, 80, $font_color2, $font_path2, $userSection, 0);
         //imagettftext($jpg_image, 10, 0, $leftpost, 56, $font_color2, $font_path2, $userSection);
        
        // userMobile
        imagettftext($jpg_image, 8, 0, $leftpost2, 130, $font_color3, $font_path3, 'Mobile:');
        imagettftext($jpg_image, 8, 0, 280, 130, $font_color2, $font_path3, $userMobile);
		
		imagettftext($jpg_image, 8, 0,234, 170, $font_color3, $font_path3, 'Fax:');
        imagettftext($jpg_image, 8, 0, 262, 170, $font_color2, $font_path3, $fax);
        
        // userTp
        /*imagettftext($jpg_image, 8, 0, $leftpost4, 123, $font_color3, $font_path3, '| Tel:');
        imagettftext($jpg_image, 8, 0, $leftpost3, 123, $font_color2, $font_path3, $userTp);*/
     
}

// 10**************************************************************** Nepal ****************************************************************
if($country=='NP') {

         $countryImg = $signature_arr[$country];
       //echo $countryImg.'ffffffff';
      $jpg_image = imagecreatefromjpeg(getcwd().'/images/'.$country.'/NP_01.jpg');

      // Allocate A Color For The Text
      $font_color = imagecolorallocate($jpg_image, 55, 52, 52);
      $font_color2 = imagecolorallocate($jpg_image, 127, 128, 132);
      $font_color3 = imagecolorallocate($jpg_image, 102, 102, 102);

   
      
        $leftpost=65;
        $leftpost2=237;
        $leftpost3=334;
        $leftpost4=308;
        
        
        // name
 		imagettftext($jpg_image, 13, 0, 10, 25, $font_color, $font_path3, $userName);
        
        // position
       imagettftext($jpg_image, 9, 0, 10,42, $font_color2, $font_path2, $userPosition);

        // Section
        imagettftextSp($jpg_image, 9, 0, 10,57, $font_color2, $font_path2, $userSection, 0);
         //imagettftext($jpg_image, 10, 0, $leftpost, 56, $font_color2, $font_path2, $userSection);
        
        // userMobile
        imagettftext($jpg_image, 8, 0,184, 130, $font_color3, $font_path3, 'Mobile:');
        imagettftext($jpg_image, 8, 0, 220, 130, $font_color2, $font_path3, $userMobile);
		
		imagettftext($jpg_image, 8, 0,184, 143, $font_color3, $font_path3, 'Fax:');
        imagettftext($jpg_image, 8, 0, 210, 143, $font_color2, $font_path3, $fax);
        // userTp
        /*imagettftext($jpg_image, 8, 0, $leftpost4, 123, $font_color3, $font_path3, '| Tel:');
        imagettftext($jpg_image, 8, 0, $leftpost3, 123, $font_color2, $font_path3, $userTp);*/
     
}

// 11**************************************************************** Vietnam ****************************************************************
if($country=='VN') {

         $countryImg = $signature_arr[$country];
       //echo $countryImg.'ffffffff';
      $jpg_image = imagecreatefromjpeg(getcwd().'/images/'.$countryImg);

      // Allocate A Color For The Text
      $font_color = imagecolorallocate($jpg_image, 55, 52, 52);
      $font_color2 = imagecolorallocate($jpg_image, 127, 128, 132);
      $font_color3 = imagecolorallocate($jpg_image, 102, 102, 102);

   
      
        $leftpost=65;
        $leftpost2=233;
        $leftpost3=334;
        $leftpost4=308;
        
        
        // name
 		imagettftext($jpg_image, 13, 0, 65, 60, $font_color, $font_path3, $userName);
        
        // position
        imagettftext($jpg_image, 10, 0, $leftpost, 80, $font_color2, $font_path2, $userPosition);

        // Section
        imagettftextSp($jpg_image, 10, 0, $leftpost, 95, $font_color2, $font_path2, $userSection, 0);
         //imagettftext($jpg_image, 10, 0, $leftpost, 56, $font_color2, $font_path2, $userSection);
        
        // userMobile
        imagettftext($jpg_image, 8, 0, $leftpost2, 145, $font_color3, $font_path3, 'Mobile:');
        imagettftext($jpg_image, 8, 0, 270, 145, $font_color2, $font_path3, $userMobile);
		
		imagettftext($jpg_image, 8, 0,234, 170, $font_color3, $font_path3, 'Fax:');
        imagettftext($jpg_image, 8, 0, 262, 170, $font_color2, $font_path3, $fax);
        
        // userTp
        /*imagettftext($jpg_image, 8, 0, $leftpost4, 123, $font_color3, $font_path3, '| Tel:');
        imagettftext($jpg_image, 8, 0, $leftpost3, 123, $font_color2, $font_path3, $userTp);*/
     
}


 //exit();   
    
  //echo   
    
    
     // working imagettftext($jpg_image, 16, 0, 280, 180, $white, $font_path, $text);

       // Send Image to Browser
       //imagejpeg($jpg_image);
        //$finaloutput = '/signature_final_'.$country.'.jpg?t='.time();
        $finaloutput = 'signature_final_'.$country.'.jpg';
        
		imagejpeg($jpg_image,getcwd().'/'.$finaloutput);

        // Clear Memory
        imagedestroy($jpg_image); 
        
        
        echo $finaloutput;
 

}

 
    
    
    
    
    
    
 


?>