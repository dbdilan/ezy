<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class EventModel extends Model
{
    /**
     * Description add new event to table
     * @param $data
     */
    public function addEvent($data)
    {
       return  DB::table('events')->insertGetId($data);
    }

    /**
     * Description Get All events status 1
     * @return mixed
     */
    public function getAllEvents()
    {
        $results = DB::table('events')
            ->where('status', '=', 1)
            ->get();

        return $results;
    }

    /**
     * Description Get All Events
     * @return mixed
     */
    public function allEvents()
    {
        $results = DB::table('events')
            ->select('events.*', 'users.first_name', 'users.last_name')
            ->join('users', 'users.id', '=', 'events.created_by')
            ->get();

        return $results;
    }


    /**
     * Description Get events limit
     * @param int $limit
     * @return mixed
     */
    public function getEventsByLimit($limit = 10)
    {
        $results = DB::table('events')
            ->select('events.*', 'users.first_name', 'users.last_name', 'users.profile_image')
            ->join('users', 'users.id', '=', 'events.created_by')
            ->where('events.status', '=', 1)
            ->limit($limit)
            ->get();

        return $results;
    }


    /**
     * Description get all events by privacy code
     * @param $privacy_code
     * @return array
     */
    public function getAllEventsByPrivacyCode($privacy_code)
    {
        $logged_user = Auth::user()->id;
        $logged_user_office = Auth::user()->office_branch_id;

        $results = array();
        // get only me privacy
        if($privacy_code == 'only_me')
        {
            $results = DB::table('events')
                ->where('privacy', '=', $privacy_code)
                ->where('created_by', '=', $logged_user )
                ->where('status', '=', 1)
                ->get();

        }

        // get public privacy
        if($privacy_code == 'public')
        {
            $results = DB::table('events')
                ->where('privacy', '=', $privacy_code)
                ->where('status', '=', 1)
                ->get();
        }

        // get location privacy
        if($privacy_code == 'office')
        {
            $results = DB::table('events')
                ->where('privacy', '=', $privacy_code)
                ->where('status', '=', 1)
                ->get();

            foreach ($results as $key=>$result) {
                $offices = json_decode($result->location);
                if(in_array($logged_user_office, $offices)){

                }else{
                    unset($results[$key]);
                }
            }
        }

        // get user group
        if($privacy_code == 'group')
        {
            $results = DB::table('events')
                ->where('privacy', '=', $privacy_code)
                ->where('status', '=', 1)
                ->get();

            // get user group
            $user_group = DB::table('user_groups')
                ->where('user_groups.user_id', '=', $logged_user)
                ->get();

            $user_group_array = array();
            if(count($user_group)> 0)
            {
                foreach ($user_group as $item) {
                    $user_group_array[] = $item->group_id;
                }
            }

            if(count($results) > 0)
            {
                foreach ($results as $key=>$result) {
                    $group= json_decode($result->group);
                    $check_results = $this->in_array_any( $user_group_array, $group );
                    if($check_results == false)
                    {
                        unset($results[$key]);
                    }
                }
            }
        }

        return $results;

    }

    // php - in_array multiple values
    function in_array_any($needles, $haystack) {
        return (bool)array_intersect($needles, $haystack);
    }

    /**
     * Description Get Event By ID
     * @param $id
     * @return mixed
     */
    public function getEventByID($id)
    {
        $results = DB::table('events')
            ->select('events.*', 'users.first_name', 'users.last_name')
            ->join('users', 'users.id', '=', 'events.created_by')
            ->where('events.event_id', '=', $id)
            ->first();
        return $results;
    }

    /**
     * Description Update Event
     * @param $data
     * @param $id
     */
    public function updateEvent($data,  $id)
    {
       DB::table('events')
       ->where('events.event_id', '=', $id)
       ->update($data);
    }

    public function deleteEvent($event_id)
    {
        DB::table('events')
            ->where('events.event_id', '=', $event_id)
            ->delete();

    }
}
