<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasRoles;
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'first_name', 'last_name', 'birthday', 'office_phone', 'mobile_phone', 'emp_number', 'profession', 'address', 'profile_image', 'ip_address', 'ip_location', 'country_id', 'office_branch_id', 'facebook_link', 'instagram_link', 'google_link', 'twitter_link', 'description', 'gender', 'cover_photo', 'email_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function setPasswordAttribute($password)
    {   
        $this->attributes['password'] = bcrypt($password);
    }


    public function getUsers()
    {
        $sql = "SELECT
                users.*,
                (SELECT count(user_connections.user_connection_id) FROM user_connections WHERE user_connections.user_id = users.id) as user_connetions,
                (SELECT count(user_follows.user_id) FROM user_follows WHERE user_follows.user_id = users.id) as user_follows,
                country.`name` as country_name
                FROM
                users
                INNER JOIN country ON users.country_id = country.country_id
                WHERE users.`status` = 1";
        $results = DB::select($sql);
        return $results;
    }


    /**
     * Description Get user by user ID
     * @param $user_id
     * @return mixed
     */
    public function getUserByID($user_id)
    {
        $results = DB::table('users')
            ->select('users.*', 'country.name as country_name', 'offices.name as office_name')
            ->leftJoin('country', 'users.country_id', 'country.country_id')
            ->leftJoin('offices', 'users.office_branch_id', 'offices.office_id')
            ->where('users.id', '=', $user_id)
            ->first();
        return $results;
    }

    /**
     * Description get user connections
     * @param $id
     * @return mixed
     */
    public function getUserConnection($id)
    {
        $results = DB::table('user_connections')
            ->select('users.profile_image','users.id', 'users.first_name')
            ->join('users' , 'users.id', '=',  'user_connections.user_connection_id')
            ->where('user_connections.user_id', '=', $id)
            ->get();
        return $results;
    }

    /**
     * Description get user follows
     * @param $id
     * @return mixed
     */
    public function getUserFollows($id)
    {
        $results = DB::table('user_follows')
            ->where('user_id', '=', $id)
            ->get();

        return $results;
    }

    /**
     * Description get user following status
     * @param $id
     * @return mixed
     */
    public function getUserFollowingByUserID($id)
    {
        $logged_user_id = Auth::user()->id;
        $results = DB::table('user_follows')
            ->where('user_id', '=', $logged_user_id)
            ->where('user_connection_id', '=', $id)
            ->get();

        return $results;
    }

    /**
     * Description get user Connection status
     * @param $id
     * @return mixed
     */
    public function getUserConnectionByUserID($id)
    {
        $logged_user_id = Auth::user()->id;
        $results = DB::table('user_connections')
            ->where('user_id', '=', $logged_user_id)
            ->where('user_connection_id', '=', $id)
            ->get();

        return $results;
    }

    /**
     * Description Add User Follows
     * @param $data
     * @return int
     */
    public function addUserFollows($data)
    {
        // check user already added
        $logged_user_id = Auth::user()->id;
        $results = DB::table('user_follows')
            ->where('user_id', '=', $logged_user_id)
            ->where('user_connection_id', '=', $data['user_id'])
            ->get();

        $return_status = 0;
        if(count($results) === 0)
        {
            // active user follow
            DB::table('user_follows')->insert([
                'user_id'           => $logged_user_id,
                'user_connection_id' => $data['user_id']
            ]);
            $return_status = 1;
        }
        else
        {
            // delete user follow
            DB::table('user_follows')
                ->where('user_id', '=', $logged_user_id)
                ->where('user_connection_id', '=', $data['user_id'])
                ->delete();
        }

        return $return_status;
    }
    /**
     * Description Add User Connection
     * @param $data
     * @return int
     */
    public function addUserConnections($data)
    {
        // check user already added
        $logged_user_id = Auth::user()->id;
        $results = DB::table('user_connections')
            ->where('user_id', '=', $logged_user_id)
            ->where('user_connection_id', '=', $data['user_id'])
            ->get();

        $return_status = 0;
        if(count($results) === 0)
        {
            // active user follow
            DB::table('user_connections')->insert([
                'user_id'           => $logged_user_id,
                'user_connection_id' => $data['user_id']
            ]);
            $return_status = 1;
        }
        else
        {
            // delete user follow
            DB::table('user_connections')
                ->where('user_id', '=', $logged_user_id)
                ->where('user_connection_id', '=', $data['user_id'])
                ->delete();
        }

        return $return_status;
    }

    /**
     * Description Get users by limit
     * @param $limit
     * @return mixed
     */
    public function getUsersByLimit($limit)
    {
        $results = DB::table('users')
            ->where('users.status', '=', '1')
            ->orderBy('users.created_at', 'ASC')
            ->paginate($limit);
        return $results;
    }

    /**
     * Description get Upcoming Birthdays
     * @return mixed
     */
    public function getUpcomingBirthday()
    {
        $sql = "SELECT * 
                FROM  users 
                WHERE  DATE_ADD(birthday, 
                                INTERVAL YEAR(CURDATE())-YEAR(birthday)
                                         + IF(DAYOFYEAR(CURDATE()) > DAYOFYEAR(birthday),1,0)
                                YEAR)  
                            BETWEEN CURDATE() AND DATE_ADD(CURDATE(), INTERVAL 200 DAY)";

        $results = DB::select($sql);
        return $results;
    }

    /**
     * Description get post by post ID
     * @param $post_id
     * @return mixed
     */
    public function getPostByID($post_id)
    {
        $results = DB::table('posts')
            ->where('posts.id', '=', $post_id)
            ->first();
        return $results;
    }

    /**
     * Description Update post data
     * @param $data
     * @param $post_id
     */
    public function updatePost($data, $post_id)
    {
        DB::table('posts')
            ->where('posts.id', '=', $post_id)
            ->update($data);
    }

    /**
     * Description pagination with post
     * @return mixed
     */
    public function getPostByLimit($limit)
    {
        $results = DB::table('posts')
            ->select('posts.*', 'users.first_name', 'users.last_name')
            ->join('users', 'users.id', '=', 'posts.created_by')
            ->where('posts.privacy', '=', 'only_me')
            ->orWhere('posts.privacy', '=', 'public')
            ->orderby('publish_date')
            ->paginate($limit);
        return $results;
    }
    /**
     * Description pagination with post
     * @return mixed
     */
    public function getPostByLimittest()
    {
        $results = DB::table('posts')
            ->select('posts.*', 'users.first_name', 'users.last_name')
            ->join('users', 'users.id', '=', 'posts.created_by')
            ->where('posts.privacy', '=', 'only_me')
            ->orWhere('posts.privacy', '=', 'public')
            ->orderby('publish_date')
            ->get();
        return $results;
    }

    /**
     * Description pagination with post
     * @return mixed
     */
    public function getPostByIDObj($id)
    {
        $results = DB::table('posts')
            ->select('posts.*', 'users.first_name', 'users.last_name')
            ->join('users', 'users.id', '=', 'posts.created_by')
            ->where('posts.id', '=', $id)
            ->get();

        return $results;
    }

    /**
     * Description get recent post
     * @param $limit
     * @return mixed
     */
    public function getRecentPost($limit)
    {
        $results = DB::table('posts')
            ->select('posts.*', 'users.first_name', 'users.last_name')
            ->join('users', 'users.id', '=', 'posts.created_by')
            ->orderby('publish_date', 'ASE')
            ->limit($limit)
            ->get();

        return $results;
    }

    /**
     * Description  add user post like button
     * @param $post_id
     * @return int
     */
    public function addUserPost($post_id)
    {

        $logged_user = Auth::user()->id;
        $results = DB::table('user_post')
            ->where('user_post.user_id', '=', $logged_user)
            ->where('user_post.post_id', '=', $post_id)
            ->first();

        $return_status = 0;
        if(count($results) === 0)
        {
            DB::table('user_post')
                ->insert([
                    'user_id' => $logged_user,
                    'post_id' => $post_id
                ]);
            $return_status = 1;
        }else{
            // delete user follow
            DB::table('user_post')
                ->where('user_id', '=', $logged_user)
                ->where('post_id', '=', $post_id)
                ->delete();
        }
        return $return_status;
    }

    /**
     * Description get user  post liked
     * @return mixed
     */
    public function getUserLikePostId()
    {

        $logged_user = Auth::user()->id;
        $results = DB::table('user_post')
            ->select('user_post.post_id')
            ->where('user_post.user_id', '=', $logged_user)
            ->get();
        return $results;
    }

    /**
     * Description get post like count
     * @param $post_id
     * @return mixed
     */
    public function getPostLikeByPostID($post_id)
    {
        $results = DB::table('user_post')
            ->where('user_post.post_id', '=', $post_id)
            ->count();

        return $results;
    }

    /**
     * Description add post comments
     * @param $data
     * @return mixed post comment count
     */
    public function addComment($data)
    {
        $results = DB::table('user_profile_comment')
            ->insertGetId($data);

        return $results;
    }

    /**
     * Description get post comment count
     * @param $post_id
     * @return mixed
     */
    public function getCommentByUserID($post_id)
    {
        $results = DB::table('post_comment')
            ->where('post_comment.post_id', '=', $post_id)
            ->count();
        return $results;
    }

    /**
     * Description get post comments count
     * @param $post_id
     * @return mixed
     */
    public function getCommentsByUserID($id, $parent_id = 0)
    {
        $results = DB::table('user_profile_comment')
            ->select('user_profile_comment.*', 'users.first_name', 'users.last_name', 'users.profile_image')
            ->join('users', 'users.id', '=', 'user_profile_comment.user_id')
            ->where('user_profile_comment.post_id', '=', $id)
            ->where('user_profile_comment.parent_id', '=', $parent_id)
            ->orderBy('created_at', 'ASC')
            ->get();
        return $results;
    }

    /**
     * Description Remove post comment
     * @param $post_comment_id
     */
    public function removeComment($post_comment_id){
        DB::table('user_profile_comment')
            ->where('user_profile_comment.user_profile_comment_id', '=', $post_comment_id)
            ->delete();
    }

    public function getAllPostByPrivacyCode($privacy_code)
    {
        $logged_user = Auth::user()->id;
        $logged_user_office = Auth::user()->office_branch_id;

        $results = array();
        // get only me privacy
        if($privacy_code == 'only_me')
        {
            $results = DB::table('posts')
                ->select('posts.*', 'users.first_name', 'users.last_name', 'users.profile_image')
                ->join('users', 'users.id', '=', 'posts.created_by')
                ->where('posts.created_by', '=', $logged_user )
                ->where('posts.privacy', '=', $privacy_code)
                ->where('posts.status', '=', 1)
                ->get();

        }

        // get public privacy
        if($privacy_code == 'public')
        {
            $results = DB::table('posts')
                ->select('posts.*', 'users.first_name', 'users.last_name', 'users.profile_image')
                ->join('users', 'users.id', '=', 'posts.created_by')
                ->where('posts.privacy', '=', $privacy_code)
                ->where('posts.status', '=', 1)
                ->get();
        }

        // get location privacy
        if($privacy_code == 'office')
        {
            $results = DB::table('posts')
                ->select('posts.*', 'users.first_name', 'users.last_name', 'users.profile_image')
                ->join('users', 'users.id', '=', 'posts.created_by')
                ->where('posts.privacy', '=', $privacy_code)
                ->where('posts.status', '=', 1)
                ->get();

            foreach ($results as $key=>$result) {
                $offices = json_decode($result->location);
                if(in_array($logged_user_office, $offices)){

                }else{
                    unset($results[$key]);
                }
            }
        }

        // get user group
        if($privacy_code == 'group')
        {
            $results = DB::table('posts')
                ->select('posts.*', 'users.first_name', 'users.last_name', 'users.profile_image')
                ->join('users', 'users.id', '=', 'posts.created_by')
                ->where('posts.privacy', '=', $privacy_code)
                ->where('posts.status', '=', 1)
                ->get();

            // get user group
            $user_group = DB::table('user_groups')
                ->where('user_groups.user_id', '=', $logged_user)
                ->get();

            $user_group_array = array();
            if(count($user_group)> 0)
            {
                foreach ($user_group as $item) {
                    $user_group_array[] = $item->group_id;
                }
            }

            if(count($results) > 0)
            {
                foreach ($results as $key=>$result) {
                    $group= json_decode($result->group);
                    $check_results = $this->in_array_any( $user_group_array, $group );
                    if($check_results == false)
                    {
                        unset($results[$key]);
                    }
                }
            }
        }

        return $results;

    }

    // php - in_array multiple values
    function in_array_any($needles, $haystack) {
        return (bool)array_intersect($needles, $haystack);
    }

}
