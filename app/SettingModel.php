<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class SettingModel extends Model
{

    /**
     * Description Get All Countries
     * @return mixed
     */
    public function getAllCountries()
    {
        $results = DB::table('country')->orderBy('name', 'ASC')->get();
        return $results;
    }

    /**
     * Description Get All Offices
     * @return mixed
     */
    public function getAllOffices()
    {
        $results = DB::table('offices')->orderBy('name', 'ASC')->get();
        return $results;
    }

    /**
     * Description Get All Privacy
     * @return mixed
     */
    public function getAllPrivacy()
    {
        $results = DB::table('privacy')
            ->where('status', '=', 1)
            ->get();
        return $results;
    }
    /**
     * Description Get All Users Groups
     * @return mixed
     */
    public function getAllUserGroup()
    {
        if(Auth::user()->hasRole('admina'))
        {
            $results = DB::table('groups')
                ->where('status', '=', 1)
                ->get();
        }else{
            $logged_user = Auth::user()->id;
            $results = DB::table('user_groups')
                ->select('groups.name', 'groups.group_id')
                ->join('groups', 'user_groups.group_id', '=', 'groups.group_id')
                ->where('status', '=', 1)
                ->where('user_groups.user_id', '=', $logged_user)
                ->get();
        }

        return $results;
    }

    /**
     * Description create time ago
     * @param $time_ago
     * @return string
     */
    function timeAgo($time_ago)
    {
        $time_ago = strtotime($time_ago);
        $cur_time   = time();
        $time_elapsed   = $cur_time - $time_ago;
        $seconds    = $time_elapsed ;
        $minutes    = round($time_elapsed / 60 );
        $hours      = round($time_elapsed / 3600);
        $days       = round($time_elapsed / 86400 );
        $weeks      = round($time_elapsed / 604800);
        $months     = round($time_elapsed / 2600640 );
        $years      = round($time_elapsed / 31207680 );
        // Seconds
        if($seconds <= 60){
            return "just now";
        }
        //Minutes
        else if($minutes <=60){
            if($minutes==1){
                return "one minute ago";
            }
            else{
                return "$minutes minutes ago";
            }
        }
        //Hours
        else if($hours <=24){
            if($hours==1){
                return "an hour ago";
            }else{
                return "$hours hrs ago";
            }
        }
        //Days
        else if($days <= 7){
            if($days==1){
                return "yesterday";
            }else{
                return "$days days ago";
            }
        }
        //Weeks
        else if($weeks <= 4.3){
            if($weeks==1){
                return "a week ago";
            }else{
                return "$weeks weeks ago";
            }
        }
        //Months
        else if($months <=12){
            if($months==1){
                return "a month ago";
            }else{
                return "$months months ago";
            }
        }
        //Years
        else{
            if($years==1){
                return "one year ago";
            }else{
                return "$years years ago";
            }
        }
    }

    /**
     * Description insert Setting
     * @param $data
     */
    function insertSetting($data)
    {
        // delete setting by code
        DB::table('setting')
            ->where('setting.code', '=', $data['code'])
            ->delete();
        // insert data
        DB::table('setting')
            ->insert($data);
    }

    /**
     * Description Get Setting
     * @param $code
     * @return array
     */
    public function getSetting($code)
    {
        $results = DB::table('setting')
            ->select('setting.*', 'users.id', 'users.first_name', 'users.last_name', 'users.profile_image')
            ->join('users', 'users.id' , '=', 'setting.user_id')
            ->where('setting.code', '=', 'dashboard_main_slider')
            ->first();

        $value = $results->value;
        // if serialized 1
        if($results->serialized == 1 )
        {
            $value = json_decode($results->value);
        }

        $data =  array(
            'setting_value' => $value,
            'user_name' => $results->first_name.' '.$results->last_name,
            'user_profile_image' => $results->profile_image,
            'created_at' => $results->created_at
        );

        return $data;
    }

    /**
     * Description Get Events Slider
     * @return mixed
     */
    public function getEventsSlider()
    {
        $results = DB::table('events')
            ->select('events.*','users.id', 'users.first_name', 'users.last_name', 'users.profile_image')
            ->join('users', 'users.id' , '=', 'events.created_by')
            ->where('events.attach_to_main_slider', '=', 1)
            ->get();

       return $results;
    }

    /**
     * Description Get News Slider
     * @return mixed
     */
    public function getNewsSlider()
    {
        $results = DB::table('posts')
            ->select('posts.*','users.id', 'users.first_name', 'users.last_name', 'users.profile_image')
            ->join('users', 'users.id' , '=', 'posts.created_by')
            ->where('posts.attach_to_main_slider', '=', 1)
            ->get();

       return $results;
    }

    /**
     * Description user activities
     * @param $data
     */
    public function userAction($data)
    {
        DB::table('user_activities')
            ->insert($data);
    }

    /**
     * Description Add user role
     */
    public function addUserRole($data)
    {
        DB::table('user_has_roles')
            ->insert($data);
    }

    /**
     * Description Add Notification
     * @param $notification
     */

    public function addNotification($notification)
    {
        $notification_id = DB::table('notification')->insertGetId([
            'notify_user_id'    => $notification['notify_user_id'],
            'notified_by'       => $notification['notified_by'],
            'description'       => $notification['description'],
            'notify_type'       => $notification['notify_type'],
            'privacy'           => isset($notification['privacy'])? $notification['privacy'] : 1,
            'location'          => isset($notification['location'])? $notification['location'] : 0,
            'group'             => isset($notification['group'])? $notification['group'] : 0,
            'notify_date'       => isset($notification['notify_date'])? $notification['notify_date'] : date('Y-m-d H:i:s'),
            'link'              => isset($notification['link'])? $notification['link'] : ''
        ]);



        if(isset($notification['privacy']))
        {
            if($notification['privacy'] == 'only_me')
            {
                DB::table('notification_user')->insert([
                    'notification_id'   => $notification_id,
                    'user_id'           => $notification['notified_by'],
                    'created_at'        => date('Y-m-d H:i:s'),
                    'notify_date'       => isset($notification['notify_date'])? $notification['notify_date'] : date('Y-m-d H:i:s'),
                    'post_id'           => isset($notification['post_id'])? $notification['post_id'] : null,
                ]);
            }
            elseif ($notification['privacy'] == 'office')
            {
                $office_ids = $notification['location'];
                $office_array = json_decode($office_ids, true);
                $in_array_office = array();
                foreach ($office_array as $item)
                {
                    $in_array_office[] = $item;
                }
                // Get user list
                $users = DB::select("SELECT users.id FROM users WHERE users.office_branch_id IN (".implode(',',$in_array_office).")");
                foreach ($users as $user)
                {
                    DB::table('notification_user')->insert([
                        'notification_id'   => $notification_id,
                        'user_id'           => $user->id,
                        'created_at'        => date('Y-m-d H'),
                        'notify_date'       => isset($notification['notify_date'])? $notification['notify_date'] : date('Y-m-d'),
                        'post_id'           => isset($notification['post_id'])? $notification['post_id'] : null,
                    ]);
                }

            }
            elseif ($notification['privacy'] == 'group')
            {
                $group_ids = $notification['group'];
                $group_array = json_decode($group_ids, true);
                $in_array_group = array();
                foreach ($group_array as $item)
                {
                    $in_array_group[] = $item;
                }
                // get users
                $users = DB::select("SELECT user_groups.user_id FROM user_groups WHERE user_groups.group_id IN (".implode(',', $in_array_group).")");
                foreach ($users as $user)
                {
                    DB::table('notification_user')->insert([
                        'notification_id'   => $notification_id,
                        'user_id'           => $user->user_id,
                        'created_at'        => date('Y-m-d H'),
                        'notify_date'       => isset($notification['notify_date'])? $notification['notify_date'] : date('Y-m-d'),
                        'post_id'           => isset($notification['post_id'])? $notification['post_id'] : null,
                    ]);
                }

            }
            elseif ($notification['privacy'] == 'public')
            {
                // Get user list
                $users = DB::select("SELECT users.id FROM users");
                foreach ($users as $user)
                {
                    DB::table('notification_user')->insert([
                        'notification_id'   => $notification_id,
                        'user_id'           => $user->id,
                        'created_at'        => date('Y-m-d H'),
                        'notify_date'       => isset($notification['notify_date'])? $notification['notify_date'] : date('Y-m-d'),
                        'post_id'           => isset($notification['post_id'])? $notification['post_id'] : null,
                    ]);
                }
            }
        }else{
            DB::table('notification_user')->insert([
                'notification_id'   => $notification_id,
                'user_id'           => $notification['notify_user_id'],
                'created_at'        => date('Y-m-d H'),
                'notify_date'       => isset($notification['notify_date'])? $notification['notify_date'] : date('Y-m-d'),
                'post_id'           => isset($notification['post_id'])? $notification['post_id'] : null,
            ]);
        }

    }


    public function getUserNotification($user_id)
    {

        $sql = "SELECT
                    notification_user.notification_user_id,
                    notification_user.notification_id,
                    notification_user.`status`,
                    notification_user.notify_date,  
                    notification_user.created_at,
                    notification.description,
                    notification.link,
                    notification.notify_type
                FROM
                    notification_user
                INNER JOIN notification ON notification_user.notification_id = notification.notification_id
                WHERE
                    notification_user.user_id = $user_id
                AND notification_user.`status` = 1
                AND notification_user.notify_date <= NOW()";

        $results = DB::select($sql);
        $new_array = array();
        foreach ($results as $result)
        {
            $time_ago = strtotime($result->notify_date);
            $cur_time   = time();
            $time_elapsed   = $cur_time - $time_ago;
            $new_array[] = array(
                'notification_user_id'  => $result->notification_user_id,
                'notification_id'       => $result->notification_id,
                'status'                => $result->status,
                'notify_date'           => $result->notify_date,
                'created_at'            => $result->created_at,
                'time_ago'              => $this->timeAgo($result->notify_date),
                'description'           => $result->description,
                'time_elapsed'          => $time_elapsed,
                'link'                  => $result->link,
                'notify_type'           => $result->notify_type
            );
        }

        return $new_array;

    }
}
