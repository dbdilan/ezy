<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class AnnouncementModel extends Model
{
    /*
     * Description add new announcement
     */
    public function addNewAnnouncement($data)
    {
        return DB::table('announcements')->insertGetId($data);
    }

    /**
     * Description get Announcement by id
     * @param $id
     * @return mixed
     */
    public function getAnnouncementByID($id)
    {
        $results = DB::table('announcements')
            ->select('announcements.*', 'users.first_name', 'users.last_name')
            ->join('users', 'users.id', '=', 'announcements.created_by')
            ->where('announcement_id', '=', $id)
            ->first();
        return $results;
    }

    /**
     * Description update announcement
     * @param $data
     * @param $id
     */
    public function updateAnnouncement($data, $id)
    {
        DB::table('announcements')
            ->where('announcement_id', '=', $id)
            ->update($data);
    }

    /**
     * Description get all announcement
     * @return mixed
     */
    public function getAll()
    {
        $results = DB::table('announcements')
            ->select('announcements.*', 'users.first_name', 'users.last_name')
            ->join('users', 'users.id', '=', 'announcements.created_by')
            ->where('announcements.status', '=', 1)
            ->get();
        return $results;
    }

    /**
     * Description get all announcement by status
     * @return mixed
     */
    public function getAllByStatus($status = 1, $limit = 10)
    {
        $results = DB::table('announcements')
            ->select('announcements.*', 'users.first_name', 'users.last_name')
            ->join('users', 'users.id', '=', 'announcements.created_by')
            ->where('announcements.status', '=', $status)
            ->limit($limit)
            ->get();
        return $results;
    }

    public function deleteAnnouncementByID($id)
    {
        DB::table('announcements')
            ->where('announcement_id', '=', $id)
            ->delete();
    }

}
