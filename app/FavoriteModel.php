<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class FavoriteModel extends Model
{
    /**
     * Description Create favorite
     * @param $data
     */
    public  function createFavorite($data)
    {
        DB::table('favorites')
            ->insert($data);

    }

    /**
     * Description get List
     * @param $user_id
     * @return mixed
     */
    public function getList($user_id)
    {

        $results = DB::table('favorites')
            ->where('favorites.user_id', '=', $user_id)
            ->get();

        return $results;
    }

    /**
     * Description Delete favorite
     * @param $id
     * @param $user_id
     */
    public function deleteFavorite($id, $user_id)
    {
        DB::table('favorites')
            ->where('favorites.user_id', '=', $user_id)
            ->where('favorites.favorite_id', '=', $id)
            ->delete();
    }

    /**
     * Description get Favorite By ID
     * @param $id
     * @return mixed
     */
    public function getFavoriteByID($id)
    {
        $results = DB::table('favorites')
            ->where('favorites.favorite_id', '=', $id)
            ->first();

        return $results;
    }

    public function updateFavoriteByID($data, $id, $user_id)
    {
        DB::table('favorites')
            ->where('favorites.user_id', '=', $user_id)
            ->where('favorites.favorite_id', '=', $id)
            ->update($data);
    }
}
