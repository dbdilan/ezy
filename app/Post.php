<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Post extends Model
{
	/**
	 * Description table fillable variable
	 * @var array
	 */
    protected $fillable = [
		'title', 'body', 'publish_date', 'created_by', 'post_type', 'post_content', 'privacy', 'location', 'group', 'thumb_image', 'attach_to_main_slider'
	];


	/**
	 * Description get post by post ID
	 * @param $post_id
	 * @return mixed
	 */
	public function getPostByID($post_id)
	{
		$results = DB::table('posts')
			->where('posts.id', '=', $post_id)
			->first();
		return $results;
	}

	/**
	 * Description Update post data
	 * @param $data
	 * @param $post_id
	 */
	public function updatePost($data, $post_id)
	{
		DB::table('posts')
			->where('posts.id', '=', $post_id)
			->update($data);
	}

	/**
	 * Description pagination with post
	 * @return mixed
	 */
	public function getPostByLimit($limit)
	{
		$results = DB::table('posts')
			->select('posts.*', 'users.first_name', 'users.last_name')
			->join('users', 'users.id', '=', 'posts.created_by')
			->where('posts.privacy', '=', 'only_me')
			->orWhere('posts.privacy', '=', 'public')
			->orderby('publish_date')
			->paginate($limit);
		return $results;
	}
	/**
	 * Description pagination with post
	 * @return mixed
	 */
	public function getPostByLimittest()
	{
		$results = DB::table('posts')
			->select('posts.*', 'users.first_name', 'users.last_name')
			->join('users', 'users.id', '=', 'posts.created_by')
			->where('posts.privacy', '=', 'only_me')
			->orWhere('posts.privacy', '=', 'public')
			->orderby('publish_date')
			->get();
		return $results;
	}

	/**
	 * Description pagination with post
	 * @return mixed
	 */
	public function getPostByIDObj($id)
	{
		$results = DB::table('posts')
			->select('posts.*', 'users.first_name', 'users.last_name')
			->join('users', 'users.id', '=', 'posts.created_by')
			->where('posts.id', '=', $id)
			->get();

		return $results;
	}

	/**
	 * Description get recent post
	 * @param $limit
	 * @return mixed
	 */
	public function getRecentPost($limit)
	{
		$results = DB::table('posts')
			->select('posts.*', 'users.first_name', 'users.last_name')
			->join('users', 'users.id', '=', 'posts.created_by')
			->orderby('publish_date', 'ASE')
			->limit($limit)
			->get();

		return $results;
	}

	/**
	 * Description  add user post like button
	 * @param $post_id
	 * @return int
	 */
	public function addUserPost($post_id)
	{

		$logged_user = Auth::user()->id;
		$results = DB::table('user_post')
			->where('user_post.user_id', '=', $logged_user)
			->where('user_post.post_id', '=', $post_id)
			->first();

		$return_status = 0;
		if(count($results) === 0)
		{
			DB::table('user_post')
				->insert([
					'user_id' => $logged_user,
					'post_id' => $post_id
				]);
			$return_status = 1;
		}else{
			// delete user follow
			DB::table('user_post')
				->where('user_id', '=', $logged_user)
				->where('post_id', '=', $post_id)
				->delete();
		}
		return $return_status;
	}

	/**
	 * Description get user  post liked
	 * @return mixed
	 */
	public function getUserLikePostId()
	{

		$logged_user = Auth::user()->id;
		$results = DB::table('user_post')
			->select('user_post.post_id')
			->where('user_post.user_id', '=', $logged_user)
			->get();
		return $results;
	}

	/**
	 * Description get post like count
	 * @param $post_id
	 * @return mixed
	 */
	public function getPostLikeByPostID($post_id)
	{
		$results = DB::table('user_post')
			->where('user_post.post_id', '=', $post_id)
			->count();

		return $results;
	}

	/**
	 * Description add post comments
	 * @param $data
	 * @return mixed post comment count
	 */
	public function addPostComment($data)
	{
		$results = DB::table('post_comment')
		->insertGetId($data);

		return $results;
	}

	/**
	 * Description get post comment count
	 * @param $post_id
	 * @return mixed
	 */
	public function getPostCommentByPostID($post_id)
	{
		$results = DB::table('post_comment')
			->where('post_comment.post_id', '=', $post_id)
			->count();
		return $results;
	}

	/**
	 * Description get post comments count
	 * @param $post_id
	 * @return mixed
	 */
	public function getPostCommentsByPostID($post_id, $parent_id = 0)
	{
		$results = DB::table('post_comment')
			->select('post_comment.*', 'users.first_name', 'users.last_name', 'users.profile_image')
			->join('users', 'users.id', '=', 'post_comment.user_id')
			->where('post_comment.post_id', '=', $post_id)
			->where('post_comment.parent_id', '=', $parent_id)
			->orderBy('created_at', 'ASC')
			->get();
		return $results;
	}

	/**
	 * Description Remove post comment
	 * @param $post_comment_id
	 */
	public function removePostComment($post_comment_id){
		DB::table('post_comment')
			->where('post_comment.post_comment_id', '=', $post_comment_id)
			->delete();
	}

	public function getAllPostByPrivacyCode($privacy_code)
	{
		$logged_user = Auth::user()->id;
		$logged_user_office = Auth::user()->office_branch_id;

		$results = array();
		// get only me privacy
		if($privacy_code == 'only_me')
		{
			$results = DB::table('posts')
				->select('posts.*', 'users.first_name', 'users.last_name', 'users.profile_image')
				->join('users', 'users.id', '=', 'posts.created_by')
				->where('posts.created_by', '=', $logged_user )
				->where('posts.privacy', '=', $privacy_code)
				->where('posts.status', '=', 1)
				->get();

		}

		// get public privacy
		if($privacy_code == 'public')
		{
			$results = DB::table('posts')
				->select('posts.*', 'users.first_name', 'users.last_name', 'users.profile_image')
				->join('users', 'users.id', '=', 'posts.created_by')
				->where('posts.privacy', '=', $privacy_code)
				->where('posts.status', '=', 1)
				->get();
		}

		// get location privacy
		if($privacy_code == 'office')
		{
			$results = DB::table('posts')
				->select('posts.*', 'users.first_name', 'users.last_name', 'users.profile_image')
				->join('users', 'users.id', '=', 'posts.created_by')
				->where('posts.privacy', '=', $privacy_code)
				->where('posts.status', '=', 1)
				->get();

			foreach ($results as $key=>$result) {
				$offices = json_decode($result->location);
				if(in_array($logged_user_office, $offices)){

				}else{
					unset($results[$key]);
				}
			}
		}

		// get user group
		if($privacy_code == 'group')
		{
			$results = DB::table('posts')
				->select('posts.*', 'users.first_name', 'users.last_name', 'users.profile_image')
				->join('users', 'users.id', '=', 'posts.created_by')
				->where('posts.privacy', '=', $privacy_code)
				->where('posts.status', '=', 1)
				->get();

			// get user group
			$user_group = DB::table('user_groups')
				->where('user_groups.user_id', '=', $logged_user)
				->get();

			$user_group_array = array();
			if(count($user_group)> 0)
			{
				foreach ($user_group as $item) {
					$user_group_array[] = $item->group_id;
				}
			}

			if(count($results) > 0)
			{
				foreach ($results as $key=>$result) {
					$group= json_decode($result->group);
					$check_results = $this->in_array_any( $user_group_array, $group );
					if($check_results == false)
					{
						unset($results[$key]);
					}
				}
			}
		}

		return $results;

	}

	// php - in_array multiple values
	function in_array_any($needles, $haystack) {
		return (bool)array_intersect($needles, $haystack);
	}

	public function sync()
	{

		$this->load->model('setting/setting');
		$this->load->model('catalog/category');
		$this->load->model('catalog/product');
		//$products = $this->model_setting_setting->getAllProducts();

		$apiKey = '3295c76acbf4caaed33c36b1b5fc2cb1';
		$url = "http://myhotelina.com/pos/Api/GetStock";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POSTFIELDS, array("apiKey" => $apiKey));
		$result = curl_exec($ch);
		curl_close($ch);


		$results_array = json_decode($result);
		foreach ($results_array as $item)
		{

			$name = ltrim($item->name);
			$sku = $item->barcode;
			$reorder_level = $item->reorder_level;
			$is_active = $item->isactive;
			$category = ltrim($item->Category);
			$sub_category = ltrim($item->SubCategory);
			$unit = $item->Unit;
			$stock = $item->Stock;
			$price = '0.0';
			if(isset($item->Price)){
				$price = $item->Price;
			}

			//$cost = $this->Cost;

			$category_id = 0;
			$sub_cat_id  = 0;

			//check DB sku product
			$product = $this->model_setting_setting->getProductBySKU($sku);
			$product_id = '';
			if($product) //check product have
			{
				//check qty different
				$web_qty = $product['quantity'];
				$product_id = $product['product_id'];
				$product_price = $product['price'];
				$product_name = $product['name'];
				$product_description = $product['description'];
				$product_model = $product['model'];
				//update price
				$this->model_setting_setting->updatePriceProduct($price, $product_id);
				if($stock > $web_qty)
				{
					//add stock
					$this->model_setting_setting->updateQtyProduct($stock, $product_id);
				}
				elseif ($stock < $web_qty)
				{
					$total = $product_price * ($web_qty - $stock);
					$tqy =$web_qty - $stock;

					///place order
					$insert_data = array(
						'invoice_prefix' => 'INV-2017-0',
						'store_id' => '0',
						'store_name' => 'elc',
						'store_url' => 'elc.com',
						'customer_id' => '0',
						'customer_group_id' => 1,
						'firstname' => 'POS',
						'lastname' => '',
						'email' => 'pos@elc.com',
						'telephone' => '',
						'fax' => '',
						'custom_field' => '',
						'total' => $total,
						'affiliate_id' => 0,
						'product_name' => $product_name,
						'product_model' => $product_model,
						'product_price' => $product_price,
						'product_id' => $product_id,
						'product_qty' => $tqy
					);
					$this->model_setting_setting->addOrder($insert_data);
					$this->model_setting_setting->updateQtyProduct($stock, $product_id);

					// update category

					//$category_array['product_category'] = array($category_id, $sub_cat_id);
					// var_dump($category_array);
//                    if($product_id != 1877)
//                    {
//                        $this->model_catalog_product->addCategory($category_array, $product_id);
//                    }

				}

			}
			else // if product have not
			{


				//check options have sku
				$product_options = $this->model_setting_setting->getOptionsProductBySKU($sku);
				if($product_options) //check product have in product option
				{
					//update option
					$this->model_setting_setting->updateQtyProductOptions($stock, $sku, $price);
					//echo 'update qty<br/>';
				}
				else // if not have
				{
					//echo 'add new product<br/>';
					echo $name.'--'.$sku.'--'.$category.'--'.$sub_category.'<br/>';

				}


			}

		}

		//var_dump($results_array);
		die;
		//$this->index();
	}


}
