<?php
class ControllerCommonDashboard extends Controller {
    public function index() {
        $this->load->language('common/dashboard');

        $this->document->setTitle($this->language->get('heading_title'));

        $data['heading_title'] = $this->language->get('heading_title');

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
        );

        // Check install directory exists
        if (is_dir(dirname(DIR_APPLICATION) . '/install')) {
            $data['error_install'] = $this->language->get('error_install');
        } else {
            $data['error_install'] = '';
        }

        $data['sync_url'] = $this->url->link('common/dashboard/sync');
        $data['token'] = $this->session->data['token'];

        // Dashboard Extensions
        $dashboards = array();

        $this->load->model('extension/extension');

        // Get a list of installed modules
        $extensions = $this->model_extension_extension->getInstalled('dashboard');

        // Add all the modules which have multiple settings for each module
        foreach ($extensions as $code) {
            if ($this->config->get('dashboard_' . $code . '_status') && $this->user->hasPermission('access', 'extension/dashboard/' . $code)) {
                $output = $this->load->controller('extension/dashboard/' . $code . '/dashboard');

                if ($output) {
                    $dashboards[] = array(
                        'code'       => $code,
                        'width'      => $this->config->get('dashboard_' . $code . '_width'),
                        'sort_order' => $this->config->get('dashboard_' . $code . '_sort_order'),
                        'output'     => $output
                    );
                }
            }
        }

        $sort_order = array();

        foreach ($dashboards as $key => $value) {
            $sort_order[$key] = $value['sort_order'];
        }

        array_multisort($sort_order, SORT_ASC, $dashboards);

        // Split the array so the columns width is not more than 12 on each row.
        $width = 0;
        $column = array();
        $data['rows'] = array();

        foreach ($dashboards as $dashboard) {
            $column[] = $dashboard;

            $width = ($width + $dashboard['width']);

            if ($width >= 12) {
                $data['rows'][] = $column;

                $width = 0;
                $column = array();
            }
        }

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        // Run currency update
        if ($this->config->get('config_currency_auto')) {
            $this->load->model('localisation/currency');

            $this->model_localisation_currency->refresh();
        }

        $this->response->setOutput($this->load->view('common/dashboard', $data));
    }

    public function sync2()
    {

        $this->load->model('setting/setting');
        $this->load->model('catalog/category');
        $this->load->model('catalog/product');
        //$products = $this->model_setting_setting->getAllProducts();

        $apiKey = '3295c76acbf4caaed33c36b1b5fc2cb1';
        $url = "http://myhotelina.com/pos/Api/GetStock";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, array("apiKey" => $apiKey));
        $result = curl_exec($ch);
        curl_close($ch);


        $results_array = json_decode($result);
        foreach ($results_array as $item)
        {

            $name = $item->name;
            $sku = $item->barcode;
            $reorder_level = $item->reorder_level;
            $is_active = $item->isactive;
            $category = $item->Category;
            $sub_category = $item->SubCategory;
            $unit = $item->Unit;
            $stock = $item->Stock;
            $price = '00.00';
            $cost = '00.00';

            $category_id = 0;
            $sub_cat_id  = 0;

            //check DB sku product
            $product = $this->model_setting_setting->getProductBySKU($sku);
            if($product)
            {

                $this->model_setting_setting->updatePriceProduct($stock, $product['product_id']);

//				$category_array['product_category'] = array($category_id, $sub_cat_id);
//				if($product['product_id'] != 1877)
//				{
//					$this->model_catalog_product->addCategory($category_array, $product['product_id']);
//				}
//				var_dump($category_id,'---', $sub_cat_id, '--', $sku);
//				echo '---------------------------<br/>';

            }else
            {
                $product_options = $this->model_setting_setting->getOptionsProductBySKU($sku);
                if($product_options) //check product have in product option
                {
                    //update option
                    //$this->model_setting_setting->updateQtyProductOptions($stock, $sku);
                    //echo 'update qty<br/>';
                }
                else // if not have
                {
                    echo 'hot<br/>';
                }
            }

        }

        //var_dump($results_array);
        die;
        $this->index();
    }

    public function sync()
    {

        $this->load->model('setting/setting');
        $this->load->model('catalog/category');
        $this->load->model('catalog/product');
        //$products = $this->model_setting_setting->getAllProducts();

        $apiKey = '3295c76acbf4caaed33c36b1b5fc2cb1';
        $url = "http://myhotelina.com/pos/Api/GetStock";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, array("apiKey" => $apiKey));
        $result = curl_exec($ch);
        curl_close($ch);


        $results_array = json_decode($result);
        foreach ($results_array as $item)
        {

            $name = $item->name;
            $sku = $item->barcode;
            $reorder_level = $item->reorder_level;
            $is_active = $item->isactive;
            $category = $item->Category;
            $sub_category = $item->SubCategory;
            $unit = $item->Unit;
            $stock = $item->Stock;
            $price = '0.0';
            if(isset($item->Price)){
                $price = $item->Price;
            }

            //$cost = $this->Cost;

            $category_id = 0;
            $sub_cat_id  = 0;

            //check DB sku product
            $product = $this->model_setting_setting->getProductBySKU($sku);
            $product_id = '';
            if($product) //check product have
            {
                //check qty different
                $web_qty = $product['quantity'];
                $product_id = $product['product_id'];
                $product_price = $product['price'];
                $product_name = $product['name'];
                $product_description = $product['description'];
                $product_model = $product['model'];
                //update price
                $this->model_setting_setting->updatePriceProduct($price, $product_id);
                if($stock > $web_qty)
                {
                    //add stock
                    $this->model_setting_setting->updateQtyProduct($stock, $product_id);
                }
                elseif ($stock < $web_qty)
                {
                    $total = $product_price * ($web_qty - $stock);
                    $tqy =$web_qty - $stock;

                    ///place order
                    $insert_data = array(
                        'invoice_prefix' => 'INV-2017-0',
                        'store_id' => '0',
                        'store_name' => 'elc',
                        'store_url' => 'elc.com',
                        'customer_id' => '0',
                        'customer_group_id' => 1,
                        'firstname' => 'POS',
                        'lastname' => '',
                        'email' => 'pos@elc.com',
                        'telephone' => '',
                        'fax' => '',
                        'custom_field' => '',
                        'total' => $total,
                        'affiliate_id' => 0,
                        'product_name' => $product_name,
                        'product_model' => $product_model,
                        'product_price' => $product_price,
                        'product_id' => $product_id,
                        'product_qty' => $tqy
                    );
                    $this->model_setting_setting->addOrder($insert_data);
                    $this->model_setting_setting->updateQtyProduct($stock, $product_id);

                    // update category

                    //$category_array['product_category'] = array($category_id, $sub_cat_id);
                    // var_dump($category_array);
//                    if($product_id != 1877)
//                    {
//                        $this->model_catalog_product->addCategory($category_array, $product_id);
//                    }

                }

            }
            else // if product have not
            {

                //check options have sku
                $product_options = $this->model_setting_setting->getOptionsProductBySKU($sku);
                if($product_options) //check product have in product option
                {
                    //update option
                    $this->model_setting_setting->updateQtyProductOptions($stock, $sku, $price);
                    //echo 'update qty<br/>';
                }
                else // if not have
                {

                    if( $category != '' || $category != null)
                    {
                        $category_data = $this->model_setting_setting->getCategoryParent($category);
                        if(!$category_data)
                        {
                            //have not category
                            //insert category
                            $data = array(
                                'category_description' => array('1' => array(
                                    'name' => $category,
                                    'description' => '',
                                    'meta_title' => $category,
                                    'meta_description' => '',
                                    'meta_keyword' => ''
                                )),
                                'path' 				=> '',
                                'parent_id' 		=> 0,
                                'filter' 			=> '',
                                'category_store' 	=> array(0),
                                'keyword' 			=> '',
                                'image' 			=> '',
                                'column' 			=> 1,
                                'sort_order' 		=> 0,
                                'status' 			=> 1,
                                'category_layout' 	=> array('')
                            );

                            $category_id = $this->model_catalog_category->addCategory($data);
                            //check sub category
                            if( $sub_category != '' || $sub_category != null)
                            {
                                //have not sub cat
                                $data = array(
                                    'category_description' => array('1' => array(
                                        'name' => $sub_category,
                                        'description' => '',
                                        'meta_title' => $sub_category,
                                        'meta_description' => '',
                                        'meta_keyword' => ''
                                    )),
                                    'path' 				=> '',
                                    'parent_id' 		=> $category_id,
                                    'filter' 			=> '',
                                    'category_store' 	=> array(0),
                                    'keyword' 			=> '',
                                    'image' 			=> '',
                                    'column' 			=> 1,
                                    'sort_order' 		=> 0,
                                    'status' 			=> 1,
                                    'category_layout' 	=> array('')
                                );
                                $sub_cat_id = $this->model_catalog_category->addCategory($data);

                            }

                            //echo 'add new cat<br/>';

                        }
                        else
                        {
                            $category_id = $category_data['category_id'];
                            //check sub category
                            if( $sub_category != '' || $sub_category != null)
                            {
                                $sub_cat_data = $this->model_setting_setting->getCategorySub($sub_category, $category_id);
                                if(!$sub_cat_data)
                                {
                                    //have not sub cat
                                    $data = array(
                                        'category_description' => array('1' => array(
                                            'name' => $sub_category,
                                            'description' => '',
                                            'meta_title' => $sub_category,
                                            'meta_description' => '',
                                            'meta_keyword' => ''
                                        )),
                                        'path' 				=> '',
                                        'parent_id' 		=> $category_id,
                                        'filter' 			=> '',
                                        'category_store' 	=> array(0),
                                        'keyword' 			=> '',
                                        'image' 			=> '',
                                        'column' 			=> 1,
                                        'sort_order' 		=> 0,
                                        'status' 			=> 1,
                                        'category_layout' 	=> array('')
                                    );

                                    $sub_cat_id = $this->model_catalog_category->addCategory($data);
                                    //echo 'add new sub cat<br/>';
                                }else{
                                    $sub_cat_id = $sub_cat_data['category_id'];
                                }
                            }
                        }
                    }

                    $product_data = array(
                        'product_description' => array( '1' =>array(
                            'name' => $name,
                            'description' => '',
                            'meta_title' => $name,
                            'meta_description' => '',
                            'meta_keyword' => '',
                            'tag' => ''),
                        ),
                        'model' => $sku,
                        'sku' => $sku,
                        'upc' => '',
                        'ean' => '',
                        'jan' => '',
                        'isbn' => '',
                        'mpn' => '',
                        'location' => '',
                        'price' => $price,
                        'tax_class_id' => '0',
                        'quantity' => $stock,
                        'minimum' => 1,
                        'subtract' => 1,
                        'stock_status_id' => 5,
                        'shipping' => 1,
                        'keyword' => '',
                        'date_available' => date('Y-m-d'),
                        'length' => '',
                        'width' => '',
                        'height' => '',
                        'length_class_id' => 2,
                        'weight' => '',
                        'weight_class_id' => 1,
                        'status' => 3,
                        'sort_order' => 1,
                        'manufacturer' => '',
                        'manufacturer_id' => 0,
                        'category' => '',
                        'product_category' => array($category_id, $sub_cat_id),
                        'filter' => '',
                        'product_store' => array(0),
                        'download' => '',
                        'related' => '',
                        'option' => '',
                        'image' => '',
                        'points' => '',
                        'product_reward' => array(),
                        'product_layout' => array()
                    );
                    $product_id = $this->model_catalog_product->addProduct2($product_data);

                    //echo 'add new product<br/>';
                    echo $name.'--'.$sku.'--'.$category.'--'.$sub_category.'<br/>';
                }

            }

        }

        //var_dump($results_array);
        //die;
        $this->index();
    }


}
