<?php

namespace App\Http\Controllers;

use App\SettingModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SettingController extends Controller
{
    //
    public $setting_model;

    public function __construct()
    {
        $this->setting_model =  new SettingModel();
    }


    /**
     * Description dashboard
     * @return mixed
     */
    public function dashboard()
    {

        //get slider details
        $sliders = $this->setting_model->getSetting('dashboard_main_slider');
        $slider_images = array();

        if( $sliders['setting_value'] != null && $sliders['setting_value'] != 'null' && $sliders['setting_value'] != '')
        {
            if(isset($sliders['setting_value']->slider_image))
            {
                foreach ($sliders['setting_value']->slider_image as $key=>$slider_image)
                {
                    //var_dump($sliders['setting_value']->title); die;
                    $slider_images[] = array(
                        'image'                 => $slider_image,
                        'title'                 => $sliders['setting_value']->title[$key],
                        'description'           => $sliders['setting_value']->description[$key],
                        'user_name'             => $sliders['user_name'],
                        'user_profile_image'    => $sliders['user_profile_image'],
                        'created_at'            => $sliders['created_at']
                    );
                }
            }
        }

        return view('setting.dashboard',[
            'row_id' => count($slider_images) +1,
            'slider_images' => $slider_images
        ]);
    }

    /**
     * Description Dashboard Save
     * @param Request $request
     */
    public function dashboardSave(Request $request)
    {
        $values = json_encode($request->all());

        $data = array(
            'code'          => 'dashboard_main_slider',
            'value'         => $values,
            'serialized'    => 1,
            'created_at'    => date('Y-m-d H:i:s'),
            'user_id'       => Auth::user()->id
        );

        $this->setting_model->insertSetting($data);

        // user activity track
        $data = array(
            'user_id'       => Auth::user()->id,
            'type'          => 'Dashboard main slider',
            'code'          => 'dashboard_main_slider',
            'description'   => 'Add dashboard main slider',
            'created_at'    => date('Y-m-d H:i:s')
        );
        $this->setting_model->userAction($data);
        // End user activity track


        return redirect()->route('slider-list')
            ->with('success_message', 'Slider, Updated successful');

    }
    
   
    public function getUserNotification()
    {
        
    }

}
