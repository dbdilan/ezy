<?php

namespace App\Http\Controllers;

use App\PollModel;
use App\SettingModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PollController extends Controller
{
    public $poll_model;
    public $setting_model;

    public function __construct()
    {
        $this->poll_model =  New PollModel();
        $this->setting_model = New SettingModel();
    }

    /**
     * Description Create new poll
     * @return mixed
     */
    public function create()
    {

        $privacy_list = $this->setting_model->getAllPrivacy();
        $user_groups =  $this->setting_model->getAllUserGroup();
        $offices =  $this->setting_model->getAllOffices();
        return view('polls.create',[
            'privacy'       => $privacy_list,
            'user_groups'   => $user_groups,
            'offices'       => $offices,
            'row_id'        => 1
        ]);

    }

    /**
     * Description Add new poll
     * @param Request $request
     */
    public function addNewPoll(Request $request)
    {

        $this->validate($request,[
            'title' => 'required',
            'publish_date' => 'required',
            'close_date' => 'required',
            'body' => 'required'
        ]);


        $post_data = $request->all();

        $data = array(
            'title' => $post_data['title'],
            'question' => $post_data['body'],
            'option_type' => $post_data['option_type'],
            'publish_date' => $post_data['publish_date'],
            'close_date' => $post_data['close_date'],
            'created_by' => $post_data['created_by'],
            'created_at' => date('Y-m-d H:i:s'),
            'status' => 1,
            'privacy' => $post_data['privacy'],
            'location' => isset($post_data['office']) ?  json_encode($post_data['office']) : null,
            'group' => isset($post_data['group']) ?  json_encode($post_data['group']) : null,
        );

        $options = $post_data['options'];

        $this->poll_model->addNewPoll($data, $options);

        // user activity track
        $data = array(
            'user_id'       =>  Auth::user()->id,
            'type'          => 'Poll',
            'code'          => $post_data['title'],
            'description'   => 'Add New Poll',
            'created_at'    => date('Y-m-d H:i:s')
        );
        $this->setting_model->userAction($data);
        // End user activity track

        return redirect()->route('poll-list')
            ->with('flash_message', 'POll, New poll created successful');
    }

    /**
     * Description poll Edit view
     * @param $id
     * @return mixed
     */
    public function pollEdit($id)
    {
        // get poll details by 
        $poll = $this->poll_model->getPollByID($id);
        $poll_options = $this->poll_model->getPollOptionsByPollID($id);

        //setting
        $privacy_list = $this->setting_model->getAllPrivacy();
        $user_groups =  $this->setting_model->getAllUserGroup();
        $offices =  $this->setting_model->getAllOffices();

        //if office or group
        $office_array =  array();
        $group_array = array();
        if($poll->group)
        {
            $group_array =json_decode($poll->group);
        }
        if($poll->location)
        {
            $office_array =json_decode($poll->location);
        }
        
        return view('polls.edit',[
            'privacy'       => $privacy_list,
            'user_groups'   => $user_groups,
            'offices'       => $offices,
            'row_id'        => count($poll_options) + 1,
            'poll'          => $poll,
            'options'       => $poll_options,
            'office_array'  => $office_array,
            'group_array'   => $group_array
        ]);
    }

    /**
     * Description poll view
     * @param $id
     * @return mixed
     */
    public function pollView($id)
    {
        // get poll details by
        $poll = $this->poll_model->getPollByID($id);
        $poll_options = $this->poll_model->getPollOptionsByPollID($id);

        //setting
        $privacy_list = $this->setting_model->getAllPrivacy();
        $user_groups =  $this->setting_model->getAllUserGroup();
        $offices =  $this->setting_model->getAllOffices();

        //if office or group
        $office_array =  array();
        $group_array = array();
        if($poll->group)
        {
            $group_array =json_decode($poll->group);
        }
        if($poll->location)
        {
            $office_array =json_decode($poll->location);
        }

        return view('polls.edit',[
            'privacy'       => $privacy_list,
            'user_groups'   => $user_groups,
            'offices'       => $offices,
            'row_id'        => count($poll_options) + 1,
            'poll'          => $poll,
            'options'       => $poll_options,
            'office_array'  => $office_array,
            'group_array'   => $group_array
        ]);
    }

    /**
     * Description poll edit save 
     * @param Request $request
     */
    public function pollEditSave(Request $request)
    {

        $this->validate($request,[
            'title' => 'required',
            'publish_date' => 'required',
            'close_date' => 'required',
            'body' => 'required'
        ]);



        $post_data = $request->all();
        $post_id = $post_data['poll_id'];

        $data = array(
            'title' => $post_data['title'],
            'question' => $post_data['body'],
            'option_type' => $post_data['option_type'],
            'publish_date' => $post_data['publish_date'],
            'close_date' => $post_data['close_date'],
            'created_by' => $post_data['created_by'],
            'created_at' => date('Y-m-d H:i:s'),
            'status' => 1,
            'privacy' => $post_data['privacy'],
            'location' => isset($post_data['office']) ?  json_encode($post_data['office']) : null,
            'group' => isset($post_data['group']) ?  json_encode($post_data['group']) : null,
        );

        $options = $post_data['options'];
        $this->poll_model->editPoll($data, $options, $post_id);

    }

    /**
     * Description poll edit post
     * @param Request $request
     */
    public function pollEditPost(Request $request)
    {
        $post_data = $request->all();
        $poll_id = $post_data['poll_id'];
        $data = array(
            'title' => $post_data['title'],
            'question' => $post_data['body'],
            'option_type' => $post_data['option_type'],
            'publish_date' => $post_data['publish_date'],
            'close_date' => $post_data['close_date'],
            'created_by' => $post_data['created_by'],
            'created_at' => date('Y-m-d H:i:s'),
            'status' => $post_data['status'],
            'privacy' => $post_data['privacy'],
            'location' => isset($post_data['office']) ?  json_encode($post_data['office']) : null,
            'group' => isset($post_data['group']) ?  json_encode($post_data['group']) : null,
        );
        
        $options = $post_data['options'];

        $this->poll_model->updatePoll($data, $options, $poll_id);
        return redirect()->route('poll-list')
            ->with('flash_message', 'Poll, Update poll successful');
    }

    /**
     * Description get list
     * @return mixed
     */
    public function getList()
    {
        $polls = $this->poll_model->getAll();

        $poll_vote_count = array();
        foreach ($polls as $poll)
        {
            $poll_vote_count[$poll->poll_id] = count($this->poll_model->getVoteCountByPollID($poll->poll_id));
        }

        return view('polls.list', [
            'polls' => $polls,
            'polls_vote_count' => $poll_vote_count
        ]);
    }

    /**
     * Description add poll vote
     * @param Request $request
     */
    public function addPollVote(Request $request)
    {

        $post_data = $request->all();
        $post_type = $post_data['vote_type'];
        
        $voted = 0;
        $message = "Thank You for voting";
        $user_id = $post_data['user_id'];
        $poll_id = $post_data['poll_id'];
        // find user voted
        $results = $this->poll_model->checkUserVote($poll_id, $user_id);
        $voted_option = array();
        if(count($results) > 0)
        {

            foreach ($results as $result)
            {
                $voted_option[] = $result->name;
            }
            $voted = 1;
            $message = "You  are already voted this poll thank you";
        }
        else
        {
            if($post_type == 'radio')
            {
                // insert poll vote
                $data = array(
                    'user_id'       => $post_data['user_id'],
                    'option_id'     => $post_data['poll_radio'],
                    'poll_id'       => $post_data['poll_id'],
                    'created_at'    => date('Y-m-d H:i:s')
                );
                $this->poll_model->insertVote($data);
            }
            else
            {
                foreach ($post_data['poll_checkbox'] as $checkbox)
                {
                    $data = array(
                        'user_id'       => $post_data['user_id'],
                        'option_id'     => $checkbox,
                        'poll_id'       => $post_data['poll_id'],
                        'created_at'    => date('Y-m-d H:i:s')
                    );
                    $this->poll_model->insertVote($data);
                }

            }

        }

        // get poll option vote
        $results = $this->poll_model->getVoteCountByPollID($poll_id);
        $votes_array = array();
        foreach ($results as $votes)
        {
            $votes_array[] = array(
                'option_id' => $votes->option_id,
                'votes_count' => $votes->count_value,
                'name' => $this->poll_model->getOptionByOptionID($votes->option_id)->name
            );
        }

        $view =  view('polls.chart',[
                    'votes_array'     => $votes_array,
                ])->render();


        // user activity track
        $data = array(
            'user_id'       => $user_id,
            'type'          => 'Poll',
            'code'          => $poll_id,
            'description'   => 'Vote Poll',
            'created_at'    => date('Y-m-d H:i:s')
        );
        $this->setting_model->userAction($data);
        // End user activity track

        return response()->json([
            'status'        => $voted,
            'message'       => $message,
            'view'          => $view,
            'vote_option'   => $voted_option
        ]);
        


    }

    /** 
     * Description get poll results
     * @param $poll_id
     * @return mixed
     */
    public function viewPollResults($poll_id)
    {

        // get poll option vote
        $results = $this->poll_model->getVoteCountByPollID($poll_id);
        $votes_array = array();
        foreach ($results as $votes)
        {
            $votes_array[] = array(
                'option_id' => $votes->option_id,
                'votes_count' => $votes->count_value,
                'name' => $this->poll_model->getOptionByOptionID($votes->option_id)->name
            );
        }

        $view =  view('polls.chart',[
            'votes_array'     => $votes_array,
        ])->render();


        return response()->json([
            'status'        => 1,
            'message'       => 'Admin view results',
            'view'          => $view
        ]);
    }

    /**
     * Description check poll has vote
     * @param $id
     * @return int
     */
    public function checkPollHasVote($id)
    {
        $results = $this->poll_model->getVoteCountByPollID($id);
        if(count($results) > 0)
        {
            return 1;
        }
        else
        {
            return 2;
        }
    }
    
    /**
     * Description Poll destroy
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
       $this->poll_model->deletePollByID($id);
        return redirect()->route('poll-list')
            ->with('flash_message', 'Poll, Delete successful');
    }
}
