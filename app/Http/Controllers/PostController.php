<?php

namespace App\Http\Controllers;

use App\SettingModel;
use App\User;
use Illuminate\Http\Request;

use App\Post;
use Auth;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Session;
use Symfony\Component\HttpFoundation\File\File;

class PostController extends Controller
{
    public $setting_model;
    public $post_model;

    public function __construct()
    {
        $this->middleware(['auth', 'clearance'])
            ->except('index', 'show');

        $this->setting_model = New SettingModel();
        $this->post_model = New Post();
    }

    
    public function testUser(Request $request)
    {
        var_dump($request->all()); die;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Description create custom pagination
     * @param $collection
     * @param $per_page
     * @return LengthAwarePaginator
     */
    function collection_paginate($collection, $per_page)
    {
        $page   =  1;
        if(isset($_GET['page'])){
            $page = $_GET['page'];
        }

        // $items->forPage($page, $per_page)->values(),
        return new LengthAwarePaginator(
            $collection->forPage($page, $per_page)->values(),
            $collection->count(),
            $per_page,
            Paginator::resolveCurrentPage(),
            ['path' => Paginator::resolveCurrentPath()]
        );

    }


    /**
     * Description all post show with pagination
     * @return mixed
     */
    public function index()
    {

        // Get all posts by view for user
        $privacy_list = $this->setting_model->getAllPrivacy();
        $final_result = array();
        foreach ($privacy_list as $privacy_item)
        {
            $posts = $this->post_model->getAllPostByPrivacyCode($privacy_item->code);
            if(count($posts) > 0)
            {
                foreach ($posts as $post)
                {
                    $final_result[] = $post;
                }
            }
        }

        // array short by publish date
        foreach($final_result as $key => $value){
            $short_date[$key] = strtotime($value->publish_date);
        }
        array_multisort($short_date, SORT_DESC, $final_result);

        // Collection create for pagination
        $collection = collect($final_result);
        $posts = $this->collection_paginate($collection, config('app.pagination_limit_per_page'));

        $recent_post = $this->post_model->getRecentPost(5);

        $content = array();
        $content_other = '';
        $post_like_count = array();
        $post_comment_count = array();
        $post_comment = array();
        $album_images = array();
        foreach ($posts  as $post)
        {
            // get content
            $post_type = $post->post_type;
            if($post_type == 'gallery')
            {
                $content[$post->id] = json_decode($post->post_content);
            }elseif ($post_type == 'album'){
                $album = json_decode($post->post_content);
                //get all photo using path
                $path = base_path().'/public'.$album;
                $files = scandir($path);
                foreach ($files as $file)
                {
                    $file_array = explode('.', $file);
                    if(isset($file_array[1]))
                    {
                        if($file_array[1] == 'jpg' || $file_array[1] == 'jpeg' || $file_array[1] == 'png'){
                            $album_images[] = 'public'.$album.'/'.$file;
                        }
                    }
                }
                //$content[$post->id] = json_decode($post->post_content);
            }else{
                $content_other[$post->id] = json_decode($post->post_content);
            }

            //post like counts
            $post_like_count[$post->id] = $this->post_model->getPostLikeByPostID($post->id);
            $post_comment_count[$post->id] = $this->post_model->getPostCommentByPostID($post->id);
            $post_comment[$post->id] = $this->post_model->getPostCommentsByPostID($post->id);

        }


        // level 2 post
        $level_2_post_comment = array();
        foreach ($posts as $post)
        {
            foreach ($post_comment[$post->id]  as $post_comment_level)
            {
                $level_2_post_comment[$post->id][$post_comment_level->post_comment_id] = $this->post_model->getPostCommentsByPostID($post->id, $post_comment_level->post_comment_id );
            }
        }

        // user liked post
        $user_liked_posts = $this->post_model->getUserLikePostId();
        $user_post_liked_ids = array();
        if(count($user_liked_posts) > 0)
        {
            foreach ($user_liked_posts as $user_like)
            {
                $user_post_liked_ids[] = $user_like->post_id;
            }
        }
        return view('posts.index', [
            'posts'                 => $posts,
            'content'               => $content,
            'content_other'         => $content_other,
            'recent_posts'          => $recent_post,
            'user_liked_post_ids'   => $user_post_liked_ids,
            'post_like_count'       => $post_like_count,
            'post_comment_count'    => $post_comment_count,
            'post_comment'          => $post_comment,
            'level_2_post_comment'  => $level_2_post_comment,
            'album'                 => $album_images
        ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $privacy_list = $this->setting_model->getAllPrivacy();
        $user_groups =  $this->setting_model->getAllUserGroup();
        $offices =  $this->setting_model->getAllOffices();
        return view('posts.create',[
            'privacy'       => $privacy_list,
            'user_groups'   => $user_groups,
            'offices'       => $offices,
            'row_id'        => 2
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request,[
            'title' => 'required|max:255',
            'description' => 'required',
            'publish_date' => 'required',
            'post_type' => 'required',
            'post_content' => 'required',
            'thumb_image' => 'required'
        ]);

        $post_data = $request->all();

        $post_content = '';
        if($post_data['post_type'] == 'gallery')
        {
            $post_content = isset($post_data['gallery_image']) || $post_data['gallery_image'] != '' && $post_data['gallery_image'] != null ?  json_encode($post_data['gallery_image']) : null;
        }
        if($post_data['post_type'] == 'video')
        {
            $post_content = isset($post_data['video']) || $post_data['video'] != '' && $post_data['video'] != null ?  json_encode($post_data['video']) : null;
        }
        if($post_data['post_type'] == 'audio')
        {
            $post_content = isset($post_data['audio']) || $post_data['audio'] != '' && $post_data['audio'] != null ?  json_encode($post_data['audio']) : null;
        }
        if($post_data['post_type'] == 'other')
        {
            $post_content = isset($post_data['other']) || $post_data['other'] != '' && $post_data['other'] != null ?  json_encode($post_data['other']) : null;
        }


        if($post_data['post_type'] == 'album')
        {
            $post_content =  null;
            if(isset($post_data['album']) || $post_data['album'] != '' && $post_data['album'] != null)
            {
                $post_content_array = explode('/', $post_data['album']);
                $last_content = $post_content_array[count($post_content_array) - 1];
                $post_content = str_replace('/'.$last_content, '', $post_data['album']) ;
            }
            $post_content =  json_encode($post_content);
        }

        $attach_to_main_slider = 0;
        if(isset($post_data['attach_to_main_slider']))
        {
            $attach_to_main_slider = $post_data['attach_to_main_slider'];
        }

        $data = array(
            'title'                 => $post_data['title'],
            'body'                  => $post_data['description'],
            'publish_date'          => $post_data['publish_date'],
            'created_by'            => $post_data['created_by'],
            'post_type'             => $post_data['post_type'],
            'post_content'          => $post_content,
            'attach_to_main_slider' => $attach_to_main_slider,
            'privacy'               => $post_data['privacy'],
            'thumb_image'           => $post_data['thumb_image'],
            'location'              => isset($post_data['office']) ?  json_encode($post_data['office']) : null,
            'group'                 => isset($post_data['group']) ?  json_encode($post_data['group']) : null,
        );

        $post_id = DB::table('posts')->insertGetId($data);


        // user activity track
        $data = array(
            'user_id'       => Auth::user()->id,
            'type'          => 'News',
            'code'          => $post_data['title'],
            'description'   => 'Add new News',
            'created_at'    => date('Y-m-d H:i:s')
        );
        $this->setting_model->userAction($data);
        // End user activity track

        // notification add
        $description =  Auth::user()->first_name. " added ".$post_data['title']." as News.";
        $notification = array(
            'notify_user_id' => '',
            'notified_by' => Auth::user()->id,
            'description' => $description,
            'notify_type' => 'news',
            'privacy' => $post_data['privacy'],
            'location' => isset($post_data['office']) ?  json_encode($post_data['office']) : null,
            'group' => isset($post_data['group']) ?  json_encode($post_data['group']) : null,
            'notify_date' => date('Y-m-d', strtotime($post_data['publish_date'])),
            'link' => 'posts/'.$post_id
        );
        $this->setting_model->addNotification($notification);
        // notification

        return redirect()->route('posts.index')
            ->with('flash_message', 'Article,
             '. $post_data['title'].' created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::findOrFail($id);

        $content = array();
        $content_other = '';
        $album_images =  array();

        // get content
        $post_type = $post->post_type;
        if($post_type == 'gallery')
        {
            $content[$post->id] = json_decode($post->post_content);

        }elseif ($post_type == 'album'){
            $album = json_decode($post->post_content);
            //get all photo using path
            $path = base_path().'/public'.$album;
            $files = scandir($path);
            foreach ($files as $file)
            {
                $file_array = explode('.', $file);
                if(isset($file_array[1]))
                {
                    if($file_array[1] == 'jpg' || $file_array[1] == 'jpeg' || $file_array[1] == 'png'){
                        $album_images[$post->id][] = 'public'.$album.'/'.$file;
                    }
                }
            }
            //$content[$post->id] = json_decode($post->post_content);
        }else{
            $content_other[$post->id] = json_decode($post->post_content);
        }
        
        $posts = $this->post_model->getPostByIDObj($id);

        $content = array();
        $content_other = '';
        $post_like_count = array();
        $post_comment_count = array();
        $post_comment = array();
        foreach ($posts  as $post)
        {
            // get content
            $post_type = $post->post_type;
            if($post_type == 'gallery') {
                $content[$post->id] = json_decode($post->post_content);
            }elseif ($post_type == 'album'){

            }else{
                $content_other[$post->id] = json_decode($post->post_content);
            }



            //post like counts
            $post_like_count[$post->id] = $this->post_model->getPostLikeByPostID($post->id);
            $post_comment_count[$post->id] = $this->post_model->getPostCommentByPostID($post->id);
            $post_comment[$post->id] = $this->post_model->getPostCommentsByPostID($post->id);

        }

        // level 2 post
        $level_2_post_comment = array();
        foreach ($posts as $post)
        {
            foreach ($post_comment[$post->id]  as $post_comment_level)
            {
                $level_2_post_comment[$post->id][$post_comment_level->post_comment_id] = $this->post_model->getPostCommentsByPostID($post->id, $post_comment_level->post_comment_id );
            }
        }

        // user liked post
        $user_liked_posts = $this->post_model->getUserLikePostId();
        $user_post_liked_ids = array();
        if(count($user_liked_posts) > 0)
        {
            foreach ($user_liked_posts as $user_like)
            {
                $user_post_liked_ids[] = $user_like->post_id;
            }
        }

        return view ('posts.show', [
            'post'                  => $post,
            'content'               => $content,
            'content_other'         => $content_other,
            'user_liked_post_ids'   => $user_post_liked_ids,
            'post_like_count'       => $post_like_count,
            'post_comment_count'    => $post_comment_count,
            'post_comment'          => $post_comment,
            'level_2_post_comment'  => $level_2_post_comment,
            'album'                 => $album_images
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = $this->post_model->getPostByID($id);

        // get content
        $post_type = $post->post_type;
        $content = array();
        $content_other = '';
        if($post_type == 'gallery')
        {
            $content = json_decode($post->post_content);
        }else{
            $content_other = json_decode($post->post_content);
        }

        //get events for user related
        $privacy =  $this->setting_model->getAllPrivacy();
        $user_groups =  $this->setting_model->getAllUserGroup();
        $offices =  $this->setting_model->getAllOffices();

        //if office or group
        $office_array =  array();
        $group_array = array();
        if($post->group)
        {
            $group_array =json_decode($post->group);
        }
        if($post->location)
        {
            $office_array =json_decode($post->location);
        }

        //return view('posts.edit', compact('post'));
        return view('posts.edit', [
            'post' => $post,
            'row_id' => 2,
            'content_array' => $content,
            'content_other' => $content_other,
            'privacy'       => $privacy,
            'user_groups'   => $user_groups,
            'offices'       => $offices,
            'office_array'  => $office_array,
            'group_array'   => $group_array
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request,[
            'title' => 'required|max:255',
            'description' => 'required',
            'publish_date' => 'required',
            'post_type' => 'required',
            'post_content' => 'required',
            'thumb_image' => 'required'
        ]);
        
        $post_data = $request->all();

        $post_content = '';
        if($post_data['post_type'] == 'gallery')
        {
            $post_content = isset($post_data['gallery_image']) || $post_data['gallery_image'] != '' && $post_data['gallery_image'] != null ?  json_encode($post_data['gallery_image']) : null;
        }
        if($post_data['post_type'] == 'video')
        {
            $post_content = isset($post_data['video']) || $post_data['video'] != '' && $post_data['video'] != null ?  json_encode($post_data['video']) : null;
        }
        if($post_data['post_type'] == 'audio')
        {
            $post_content = isset($post_data['audio']) || $post_data['audio'] != '' && $post_data['audio'] != null ?  json_encode($post_data['audio']) : null;
        }
        if($post_data['post_type'] == 'other')
        {
            $post_content = isset($post_data['other']) || $post_data['other'] != '' && $post_data['other'] != null ?  json_encode($post_data['other']) : null;
        }

        if($post_data['post_type'] == 'album')
        {
            $post_content =  null;
            if(isset($post_data['album']) || $post_data['album'] != '' && $post_data['album'] != null)
            {
                $post_content_array = explode('/', $post_data['album']);
                $last_content = $post_content_array[count($post_content_array) - 1];
                $post_content = str_replace('/'.$last_content, '', $post_data['album']) ;
            }
            $post_content =  json_encode($post_content);
        }
        
        $attach_to_main_slider = 0;
        if(isset($post_data['attach_to_main_slider']))
        {
            $attach_to_main_slider = $post_data['attach_to_main_slider'];
        }

        $data = array(
            'title' => $post_data['title'],
            'body' => $post_data['description'],
            'publish_date' => $post_data['publish_date'],
            'created_by' => $post_data['created_by'],
            'post_type' => $post_data['post_type'],
            'post_content' => $post_content,
            'attach_to_main_slider' => $attach_to_main_slider,
            'privacy' => $post_data['privacy'],
            'thumb_image' => $post_data['thumb_image'],
            'location' => isset($post_data['office']) ?  json_encode($post_data['office']) : null,
            'group' => isset($post_data['group']) ?  json_encode($post_data['group']) : null,
        );

        $this->post_model->updatePost($data, $id);

        // user activity track
        $data = array(
            'user_id'       => Auth::user()->id,
            'type'          => 'News',
            'code'          => $post_data['title'],
            'description'   => 'Add new News',
            'created_at'    => date('Y-m-d H:i:s')
        );
        $this->setting_model->userAction($data);
        // End user activity track

        return redirect()->route('posts.show', 
            $id)->with('flash_message',
            'Article, '. $post_data['title'].' updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::findOrFail($id);
        $post->delete();

        // user activity track
        $data = array(
            'user_id'       => Auth::user()->id,
            'type'          => 'News',
            'code'          => $id,
            'description'   => 'Delete News',
            'created_at'    => date('Y-m-d H:i:s')
        );
        $this->setting_model->userAction($data);
        // End user activity track

        return redirect()->route('posts.index')
            ->with('flash_message',
             'Article successfully deleted');
    }

    /**
     * Description add post like ajax
     * @param Request $request
     * @return mixed
     */
    public function addPostLike(Request $request)
    {
        $post_data = $request->all();
        $post_id = $post_data['post_id'];

        $results = $this->post_model->addUserPost($post_id);

        $message = "post like successfully!";
        $response_code = 200;
        if($results === 0){
            $message = "post like removed successfully!";
            $response_code = 400;
        }

        $post_like_count = $this->post_model->getPostLikeByPostID($post_id);

        // user activity track
        $data = array(
            'user_id'       => Auth::user()->id,
            'type'          => 'News',
            'code'          => $post_id,
            'description'   => 'Add news like',
            'created_at'    => date('Y-m-d H:i:s')
        );
        $this->setting_model->userAction($data);
        // End user activity track

        return response()->json([
            'status'         => $results,
            'message'        => $message,
            'response_code'  => $response_code,
            'post_like_count'=> $post_like_count
        ]);

    }

    /**
     * Description Add comment
     * @param Request $request
     * @return mixed
     */
    public function comments(Request $request)
    {
        $post_data = $request->all();

        $logged_user_id = Auth::user()->id;
        $logged_user_image = Auth::user()->profile_image;
        $logged_user_first_name = Auth::user()->first_name;
        $logged_user_last_name = Auth::user()->last_name;
        $data = array(
            'description' => $post_data['description'],
            'image' => $post_data['image'],
            'video' => $post_data['video'],
            'level' => $post_data['level'],
            'parent_id' => $post_data['parent_id'],
            'post_id' => $post_data['post_id'],
            'user_id' => $logged_user_id,
            'created_at' => date('Y-m-d H:i:s')
        );


        if($data['description'] != '' ||  $data['image'] != '')
        {
            $post_comment_id = $this->post_model->addPostComment($data);
            $count = $this->post_model->getPostCommentByPostID($post_data['post_id']);

            // user activity track
            $data = array(
                'user_id'       => Auth::user()->id,
                'type'          => 'News',
                'code'          => $post_data['post_id'],
                'description'   => 'Add news Comment',
                'created_at'    => date('Y-m-d H:i:s')
            );
            $this->setting_model->userAction($data);
            // End user activity track

            // notification add
            $description =  Auth::user()->first_name. " commented on news";
            if($post_data['level'] == 2 )
            {
                $description =  Auth::user()->first_name. " replay your comment";
            }

            // find posted user
            $post_create_user = DB::table('posts')->where('posts.id', $post_data['post_id'])->first()->created_by;

            $sql = "SELECT DISTINCT notification_user.user_id FROM notification_user WHERE notification_user.post_id = ". $post_data['post_id'];
            $post_comment_users = DB::select($sql);


            $notification = array(
                'notify_user_id'    => $post_create_user,
                'post_id'           => $post_data['post_id'],
                'notified_by'       => Auth::user()->id,
                'description'       => $description,
                'notify_type'       => 'news_comment',
                'link'              => 'news/'.$post_data['post_id'].'#'.$post_comment_id
            );
            $this->setting_model->addNotification($notification);

            if(count($post_comment_users) > 0)
            {
                foreach ($post_comment_users as $user_id)
                {
                    $notification = array(
                        'notify_user_id'    => $user_id->user_id,
                        'post_id'           => $post_data['post_id'],
                        'notified_by'       => Auth::user()->id,
                        'description'       => $description,
                        'notify_type'       => 'news_comment',
                        'link'              => 'news/'.$post_data['post_id'].'#'.$post_comment_id
                    );
                    $this->setting_model->addNotification($notification);
                }
            }

            // notification

            return response()->json([
                'post_comment_count' => $count,
                'description'        => $post_data['description'] == '' ? 'no_description' : $post_data['description'],
                'image_comment'      => $post_data['image'] == '' ? 'no_image' : $post_data['image'],
                'profile_image'      => $logged_user_image,
                'first_name' => $logged_user_first_name,
                'last_name' => $logged_user_last_name,
                'post_comment_id' => $post_comment_id
            ]);
        }
    }

    /**
     * Description Comment Remove
     * @param Request $request
     * @return mixed
     */
    public function commentsRemove(Request $request)
    {
        $post_data = $request->all();
        $post_comment_id = $post_data['post_comment_id'];
        $post_id = $post_data['post_id'];

        //remove comment
        $this->post_model->removePostComment($post_comment_id);
        $count = $this->post_model->getPostCommentByPostID($post_id);

        // user activity track
        $data = array(
            'user_id'       => Auth::user()->id,
            'type'          => 'News',
            'code'          => $post_data['post_id'],
            'description'   => 'Remove news Comment',
            'created_at'    => date('Y-m-d H:i:s')
        );
        $this->setting_model->userAction($data);
        // End user activity track

        return response()->json([
            'post_comment_count' => $count,
            'post_comment_id' => $post_comment_id,
            'post_id'   => $post_id
        ]);
    }

    /**
     * Description Login
     * @return mixed
     */
    public function login()
    {
        return redirect('/login');
    }
}
