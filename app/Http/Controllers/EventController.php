<?php

namespace App\Http\Controllers;

use App\EventModel;
use App\SettingModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use MaddHatter\LaravelFullcalendar\Calendar;
use Illuminate\Support\Facades\Validator;

class EventController extends Controller
{
    //
    public $event_model;
    public $setting_model;

    public function __construct()
    {
        $this->event_model = New EventModel();
        $this->setting_model =  New SettingModel();
    }

    /**
     * Description Show Events Calender
     * @return mixed
     */
    public function index()
    {

        //get events for user related
        $privacy_list = $this->setting_model->getAllPrivacy();
        $events = array();
        foreach ($privacy_list as $privacy_item)
        {
            $events[] = $this->event_model->getAllEventsByPrivacyCode($privacy_item->code);
        }

        $user_groups =  $this->setting_model->getAllUserGroup();
        $offices =  $this->setting_model->getAllOffices();

        return view('event.index',[
            'events'        => $events,
            'privacy'       => $privacy_list,
            'user_groups'   => $user_groups,
            'offices'       => $offices
        ]);
    }

    /**
     * Description Add New Events Post
     * @param Request $request
     * @return mixed
     */
    public function addNewEvent(Request $request)
    {
        $this->validate($request,[
            'title' => 'required|max:255',
            'event_image' => 'required|max:255',
            'description' => 'required',
            'start_date' => 'required',
            'end_date' => 'required'
        ]);


        $logged_user_id = Auth::user()->id;
        $post_data = $request->all();


        $attach_to_main_slider = 0;
        if(isset($post_data['attach_to_main_slider']))
        {
            $attach_to_main_slider = $post_data['attach_to_main_slider'];
        }

        $data = array(
            'name'                  => $post_data['title'],
            'description'           => $post_data['description'],
            'icon'                  => $post_data['iconselect'],
            'color'                 => $post_data['priority'],
            'status'                => 1,
            'start_date'            => date('Y-m-d H:i:s', strtotime($post_data['start_date'])),
            'end_date'              => date('Y-m-d H:i:s', strtotime($post_data['end_date'])),
            'created_by'            => $logged_user_id,
            'attach_to_main_slider' => $attach_to_main_slider,
            'privacy'               => $post_data['privacy'],
            'event_image'           => $post_data['event_image'],
            'location'              => isset($post_data['office']) ?  json_encode($post_data['office']) : null,
            'group'                 => isset($post_data['group']) ?  json_encode($post_data['group']) : null,
            'created_at'            => date('Y-m-d H:i:s')
        );

        $event_id = $this->event_model->addEvent($data);

        // user activity track
        $data = array(
            'user_id'       => Auth::user()->id,
            'type'          => 'Event',
            'code'          => $post_data['title'],
            'description'   => 'Add new Event',
            'created_at'    => date('Y-m-d H:i:s')
        );
        $this->setting_model->userAction($data);
        // End user activity track

        // notification add
        $description =  Auth::user()->first_name. " added ".$post_data['title']." as Event.";
        $notification = array(
            'notify_user_id' => '',
            'notified_by' => Auth::user()->id,
            'description' => $description,
            'notify_type' => 'event',
            'privacy' => $post_data['privacy'],
            'location' => isset($post_data['office']) ?  json_encode($post_data['office']) : null,
            'group' => isset($post_data['group']) ?  json_encode($post_data['group']) : null,
            'notify_date' => date('Y-m-d', strtotime($post_data['start_date'])),
            'link' => 'events/view/'.$event_id
        );
        $this->setting_model->addNotification($notification);
        // notification

        session()->flash('success_message', 'New Event added successful!');
        return redirect('/events');
    }

    /**
     * Description get list of events
     * @return mixed
     */
    public function getList()
    {
        $events = $this->event_model->allEvents();
        return view('event.list',[
            'events' => $events
        ]);

    }

    /**
     * Description Events Edit form view+
     * @param $id
     * @return mixed
     */
    public function editEvent($id)
    {
        $event = $this->event_model->getEventByID($id);
        //get events for user related
        $privacy =  $this->setting_model->getAllPrivacy();
        $user_groups =  $this->setting_model->getAllUserGroup();
        $offices =  $this->setting_model->getAllOffices();

        //if office or group
        $office_array =  array();
        $group_array = array();
        if($event->group)
        {
            $group_array =json_decode($event->group);
        }
        if($event->location)
        {
            $office_array =json_decode($event->location);
        }

        return view('event.edit',[
            'event'         => $event,
            'privacy'       => $privacy,
            'user_groups'   => $user_groups,
            'offices'       => $offices,
            'office_array'  => $office_array,
            'group_array'   => $group_array
        ]);
        
    }

    /**
     * Description edit form submit
     * @param Request $request
     * @return mixed
     */
    public function editPostEvent(Request $request)
    {

        $this->validate($request,[
            'title' => 'required|max:255',
            'event_image' => 'required|max:255',
            'description' => 'required',
            'start_date' => 'required',
            'end_date' => 'required'
        ]);


        $logged_user_id = Auth::user()->id;
        $post_data = $request->all();

        $attach_to_main_slider = 0;
        if(isset($post_data['attach_to_main_slider']))
        {
            $attach_to_main_slider = $post_data['attach_to_main_slider'];
        }

        $data = array(
            'name' => $post_data['title'],
            'description' => $post_data['description'],
            'icon' => $post_data['iconselect'],
            'color' => $post_data['priority'],
            'status' => $post_data['status'],
            'start_date' => date('Y-m-d H:i:s', strtotime($post_data['start_date'])),
            'end_date' => date('Y-m-d H:i:s', strtotime($post_data['end_date'])),
            'created_by' => $logged_user_id,
            'attach_to_main_slider' => $attach_to_main_slider,
            'privacy' => $post_data['privacy'],
            'event_image' => $post_data['event_image'],
            'location' => isset($post_data['office']) ?  json_encode($post_data['office']) : null,
            'group' => isset($post_data['group']) ?  json_encode($post_data['group']) : null,
            'created_at' => date('Y-m-d H:i:s')
        );

        $this->event_model->updateEvent($data, $post_data['event_id']);

        // user activity track
        $data = array(
            'user_id'       => Auth::user()->id,
            'type'          => 'Event',
            'code'          => $post_data['title'],
            'description'   => 'Edit Event',
            'created_at'    => date('Y-m-d H:i:s')
        );
        $this->setting_model->userAction($data);
        // End user activity track

        session()->flash('success_message', 'Event update successful!');
        return redirect('/events');
    }


    public function eventsView($id)
    {
        $event = $this->event_model->getEventByID($id);
        //get events for user related
        $privacy =  $this->setting_model->getAllPrivacy();
        $user_groups =  $this->setting_model->getAllUserGroup();
        $offices =  $this->setting_model->getAllOffices();

        //if office or group
        $office_array =  array();
        $group_array = array();
        if($event->group)
        {
            $group_array =json_decode($event->group);
        }
        if($event->location)
        {
            $office_array =json_decode($event->location);
        }

        return view('event.view',[
            'event'         => $event,
            'privacy'       => $privacy,
            'user_groups'   => $user_groups,
            'offices'       => $offices,
            'office_array'  => $office_array,
            'group_array'   => $group_array
        ]);
    }

    /**
     * Description Delete post
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        $this->event_model->deleteEvent($id);
        session()->flash('success_message', 'Delete successful!');
        return redirect('events/list-event');
    }

}
