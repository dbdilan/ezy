<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DocumentationController extends Controller
{
    //
    public function listOfDocumentation()
    {
        return view('documentation.list');
    }
}
