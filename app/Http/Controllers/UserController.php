<?php

namespace App\Http\Controllers;

use App\SettingModel;
use Illuminate\Http\Request;

use App\User;
use Auth;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Session;

class UserController extends Controller
{
    public $setting_model;
    public $user_model;
    public function __construct() 
    {
        $this->middleware(['auth', 'isAdmin']);
        $this->setting_model =  new SettingModel();
        $this->user_model = new User();
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_model = new User();
        $users = $user_model->getUsers();
        return view('users.index')->with('users', $users);
    }

    /**
     * @return mixed
     */
    public function members()
    {
        //get users with paginate
        $user_model = new User();
        $users = $user_model->getUsersByLimit(config('app.pagination_limit_per_page'));

        return view('users.members',[
            'users' => $users
        ]);
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::get();
        return view('users.create', ['roles'=>$roles]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'=>'required|max:120',
            'email'=>'required|email|unique:users',
            'password'=>'required|min:6|confirmed'
        ]);

        $user = User::create($request->only('email', 'name', 'password'));

        $roles = $request['roles'];

        if (isset($roles)) {

            foreach ($roles as $role) {
            $role_r = Role::where('id', '=', $role)->firstOrFail();            
            $user->assignRole($role_r);
            }
        }        

        return redirect()->route('users.index')
            ->with('flash_message',
             'User successfully added.');
    }


    public function profile(){


        $user = Auth::user();
        $this->show($user->id);


        return redirect()->action('UserController@show', $user->id);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        // echo "dfdf";die;
        // get user details
        $user_model =  new User();
        $setting_model = new SettingModel();
        $user = $user_model->getUserByID($id);

        // creating activate date ago
        $active_string = $setting_model->timeAgo($user->created_at);

        // get user connection and follows
        $user_connections   = $user_model->getUserConnection($id);
        $user_follows       = $user_model->getUserFollows($id);

        // selected user following status
        $user_following       = $user_model->getUserFollowingByUserID($id);
        $user_connecting       = $user_model->getUserConnectionByUserID($id);

        $countries = $setting_model->getAllCountries();
        $offices = $setting_model->getAllOffices();

        $post_comment = $this->user_model->getCommentsByUserID($id);
        $level_2_post_comment = array();
        foreach ($post_comment  as $post_comment_level)
        {
            $level_2_post_comment[$post_comment_level->user_profile_comment_id] = $this->user_model->getCommentsByUserID($id, $post_comment_level->user_profile_comment_id );
        }


        return view('users.view',[
            'user'                  => $user,
            'activate_ago'          => $active_string,
            'connections'           => $user_connections,
            'follows'               => $user_follows,
            'user_following'        => count($user_following),
            'user_connecting'       => count($user_connecting),
            'countries'             => $countries,
            'offices'               => $offices,
            'post_comment'          => $post_comment,
            'level_2_post_comment'  => $level_2_post_comment,
        ]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        $roles = Role::get();

        return view('users.edit', compact('user', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $this->validate($request, [
            'name'=>'required|max:120',
            'email'=>'required|email|unique:users,email,'.$id,
            'password'=>'required|min:6|confirmed'
        ]);

        $input = $request->only(['name', 'email', 'password']);
        $roles = $request['roles'];
        $user->fill($input)->save();

        if (isset($roles)) {        
            $user->roles()->sync($roles);            
        }        
        else {
            $user->roles()->detach();
        }
        return redirect()->route('users.index')
            ->with('flash_message',
             'User successfully edited.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();

        return redirect()->route('users.index')
            ->with('delete_success_message',
             'User successfully deleted.');
    }

    /**
     * Description adda user follow by ajax call
     * @param Request $request
     * @return mixed
     */
    public function userAddFollows(Request $request)
    {
        $user_model = new User();
        $post_data = $request->all();
        $results = $user_model->addUserFollows($post_data);

        $message = "Follow added successfully!";
        $response_code = 200;
        if($results === 0){
            $message = "Follow removed successfully!";
            $response_code = 400;

            // user activity track
            $data = array(
                'user_id'       => Auth::user()->id,
                'type'          => 'Follows',
                'code'          => 'user_follow',
                'description'   => Auth::user()->first_name. " un followed you.",
                'created_at'    => date('Y-m-d H:i:s')
            );
            $this->setting_model->userAction($data);
            // End user activity track

        }else{

            // user activity track
            $data = array(
                'user_id'       => Auth::user()->id,
                'type'          => 'Follows',
                'code'          => 'user_follow',
                'description'   => Auth::user()->first_name. " followed you.",
                'created_at'    => date('Y-m-d H:i:s')
            );
            $this->setting_model->userAction($data);
            // End user activity track

            // notification add
            $description =  Auth::user()->first_name. " followed you.";
            $notification = array(
                'notify_user_id' => $post_data['user_id'],
                'notified_by' => Auth::user()->id,
                'description' => $description,
                'notify_type' => 'follow'
            );
            $this->setting_model->addNotification($notification);
            // notification

        }

        return response()->json([
            'status'         => $results,
            'message'        => $message,
            'response_code'  => $response_code
        ]);

    }

    /**
     * Description adda user connection by ajax call
     * @param Request $request
     * @return mixed
     */
    public function userAddConnections(Request $request)
    {
        $user_model = new User();
        $post_data = $request->all();
        $results = $user_model->addUserConnections($post_data);

        $message = "Connection added successfully!";
        $response_code = 200;
        if($results === 0){
            $message = "Connection removed successfully!";
            $response_code = 400;

            // user activity track
            $data = array(
                'user_id'       => Auth::user()->id,
                'type'          => 'Connection',
                'code'          => 'user_connection',
                'description'   => Auth::user()->first_name. " removed you for the connections",
                'created_at'    => date('Y-m-d H:i:s')
            );
            $this->setting_model->userAction($data);
            // End user activity track

        }else{

            // user activity track
            $data = array(
                'user_id'       => Auth::user()->id,
                'type'          => 'Connection',
                'code'          => 'user_connection',
                'description'   => Auth::user()->first_name. " added you for the connections",
                'created_at'    => date('Y-m-d H:i:s')
            );
            $this->setting_model->userAction($data);
            // End user activity track

            // notification add
            $description =  Auth::user()->first_name. " added you for the connections";
            $notification = array(
                'notify_user_id' => $post_data['user_id'],
                'notified_by' => Auth::user()->id,
                'description' => $description,
                'notify_type' => 'connection',
            );
            $this->setting_model->addNotification($notification);
            // notification
        }

        return response()->json([
            'status'         => $results,
            'message'        => $message,
            'response_code'  => $response_code
        ]);

    }

    /**
     * Description upload profile photo
     * @param Request $request
     * @return mixed
     */
    public function uploadProfilePhoto(Request $request)
    {
        $data = $request->all();

        $logged_user_id = Auth::user()->id;
        DB::table('users')
            ->where('users.id', '=', $logged_user_id)
            ->update([
                'profile_image' => $data['profile_image']
            ]);

        // user activity track
        $data = array(
            'user_id'       => Auth::user()->id,
            'type'          => 'user',
            'code'          => 'update',
            'description'   => 'User profile update',
            'created_at'    => date('Y-m-d H:i:s')
        );
        $this->setting_model->userAction($data);
        // End user activity track

        session()->flash('success_message', 'Profile image uploaded was successful!');
        return redirect('users/'.$logged_user_id);
    }

    /**
     * Description upload cover photo
     * @param Request $request
     * @return mixed
     */
    public function uploadCoverPhoto(Request $request)
    {
        $data = $request->all();
        //profile image upload
        
        $logged_user_id = Auth::user()->id;
        DB::table('users')
            ->where('users.id', '=', $logged_user_id)
            ->update([
                'cover_photo' =>  $data['cover_image']
            ]);

        // user activity track
        $data = array(
            'user_id'       => Auth::user()->id,
            'type'          => 'user',
            'code'          => 'update',
            'description'   => 'User cover update',
            'created_at'    => date('Y-m-d H:i:s')
        );
        $this->setting_model->userAction($data);

        session()->flash('success_message', 'Cover image uploaded was successful!');
        return redirect('users/'.$logged_user_id);
    }

    /**
     * Description change user password
     * @param Request $request
     */
    public function changePassword(Request $request)
    {
        $data = $request->all();
        $logged_user_id = Auth::user()->id;
        DB::table('users')
            ->where('users.id', '=', $logged_user_id)
            ->update([
                'password' => bcrypt($data['password'])
            ]);

        // user activity track
        $data = array(
            'user_id'       => Auth::user()->id,
            'type'          => 'user',
            'code'          => 'update',
            'description'   => 'User password update',
            'created_at'    => date('Y-m-d H:i:s')
        );
        $this->setting_model->userAction($data);
        // End user activity track

        session()->flash('success_message', 'Password changed was successful!');
        return redirect('users/'.$logged_user_id);
    }

    /**
     * Description Update User profile
     * @param Request $request
     * @return mixed
     */
    public function updateUserProfile(Request $request)
    {
        $data = $request->all();

        // post data
        $post_data = array(
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'birthday' => date('Y-m-d', strtotime($data['birthday'])),
            'office_phone' => $data['office_phone'],
            'mobile_phone' => $data['mobile_phone'],
            'emp_number' => $data['emp_number'],
            'address' => $data['address'],
            'country_id' => $data['country_id'],
            'office_branch_id' => $data['office_branch_id'],
            'gender' => $data['gender'],
            'profession' => $data['profession'],
            'facebook_link' => $data['facebook_link'],
            'instagram_link' => $data['instagram_link'],
            'google_link' => $data['google_link'],
            'twitter_link' => $data['twitter_link'],
            'description' => $data['description']
        );

        $logged_user_id = Auth::user()->id;

        DB::table('users')
           ->where('users.id', '=', $logged_user_id)
           ->update($post_data);

        // user activity track
        $data = array(
            'user_id'       => Auth::user()->id,
            'type'          => 'user',
            'code'          => 'update',
            'description'   => 'User setting update',
            'created_at'    => date('Y-m-d H:i:s')
        );
        $this->setting_model->userAction($data);
        // End user activity track

        session()->flash('success_message', 'Profile updated successful!');
        return redirect('users/'.$logged_user_id);

    }

    /**
     * Description Add comment user profile
     * @param Request $request
     * @return mixed
     */
    public function comments(Request $request)
    {
        $post_data = $request->all();

        $logged_user_id         = Auth::user()->id;
        $logged_user_image      = Auth::user()->profile_image;
        $logged_user_first_name = Auth::user()->first_name;
        $logged_user_last_name  = Auth::user()->last_name;
        $data = array(
            'description'   => $post_data['description'],
            'image'         => $post_data['image'],
            'video'         => $post_data['video'],
            'level'         => $post_data['level'],
            'parent_id'     => $post_data['parent_id'],
            'post_id'       => $post_data['post_id'],
            'user_id'       => $logged_user_id,
            'created_at'    => date('Y-m-d H:i:s')
        );


        if($data['description'] != '' ||  $data['image'] != '')
        {
            $post_comment_id = $this->user_model->addComment($data);

            $count = $this->user_model->getCommentByUserID($post_data['post_id']);

            // user activity track
            $data = array(
                'user_id'       => Auth::user()->id,
                'type'          => 'News',
                'code'          => $post_data['post_id'],
                'description'   => 'Add news Comment',
                'created_at'    => date('Y-m-d H:i:s')
            );
            $this->setting_model->userAction($data);
            // End user activity track

            // notification add
            $description =  Auth::user()->first_name. " added commented on your wall";
            if($post_data['level'] == 2 )
            {
                $description =  Auth::user()->first_name. " replay your comment";
            }

            $notification = array(
                'notify_user_id' => $post_data['post_id'],
                'notified_by' => Auth::user()->id,
                'description' => $description,
                'notify_type' => 'profile_comment',
                'link' => 'users/'.$post_data['post_id'].'#'.$post_comment_id
            );
            $this->setting_model->addNotification($notification);
            // notification

            return response()->json([
                'post_comment_count'    => $count,
                'description'           => $post_data['description'] == '' ? 'no_description' : $post_data['description'],
                'image_comment'         => $post_data['image'] == '' ? 'no_image' : $post_data['image'],
                'profile_image'         => $logged_user_image,
                'first_name'            => $logged_user_first_name,
                'last_name'             => $logged_user_last_name,
                'post_comment_id'       => $post_comment_id
            ]);
        }
    }

    /**
     * Description Comment Remove
     * @param Request $request
     * @return mixed
     */
    public function commentsRemove(Request $request)
    {
        $post_data = $request->all();
        $post_comment_id = $post_data['post_comment_id'];
        $post_id = $post_data['post_id'];

        //remove comment
        $this->user_model->removeComment($post_comment_id);
        $count = $this->user_model->getCommentByUserID($post_id);

        // user activity track
        $data = array(
            'user_id'       => Auth::user()->id,
            'type'          => 'News',
            'code'          => $post_data['post_id'],
            'description'   => 'Remove news Comment',
            'created_at'    => date('Y-m-d H:i:s')
        );
        $this->setting_model->userAction($data);
        // End user activity track

        return response()->json([
            'post_comment_count' => $count,
            'post_comment_id' => $post_comment_id,
            'post_id'   => $post_id
        ]);
    }

    public function emailSignature()
    {
        return view('users/email_signature');
    }


}
