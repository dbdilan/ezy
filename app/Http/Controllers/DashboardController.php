<?php

namespace App\Http\Controllers;

use App\AnnouncementModel;
use App\EventModel;
use App\PollModel;
use App\Post;
use App\SettingModel;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Session;


class DashboardController extends Controller
{

    public $setting_model;
    public $user_model;
    public $event_model;
    public $post_model;
    public $announcement_model;
    public $poll_model;

    public function __construct()
    {

        // Load model to public variable
        $this->setting_model = New SettingModel();
        $this->user_model = New User();
        $this->event_model = New EventModel();
        $this->post_model = New Post();
        $this->announcement_model = New AnnouncementModel();
        $this->poll_model = New PollModel();

    }

    //
    public function index()
    {

        // Hameedia Query Row
//        $sql_1 = "SELECT
//                        entity_id,
//                        GROUP_CONCAT(category_id) AS category_ids
//                    FROM
//                        (
//                            SELECT
//                                `e`.entity_id,
//                                `at_category_id`.`category_id`
//                            FROM
//                                `catalog_product_entity` AS `e`
//                            LEFT JOIN `catalog_category_product` AS `at_category_id` ON (
//                                at_category_id.`product_id` = e.entity_id
//                            )
//                        ) sub_query
//
//                    GROUP BY
//                        entity_id";
//
//
//        $row_one =  DB::select($sql_1);
//        $category = array();
//        $html = "<table>";
//        foreach ($row_one as $item_one)
//        {
//            $cat_ids = explode(',', $item_one->category_ids);
//            $string = '';
//            foreach ($cat_ids as $id)
//            {
//                $SQL = "SELECT
//                    catalog_category_entity_varchar.`value`
//                    FROM
//                    catalog_category_entity_varchar
//                    WHERE
//                    catalog_category_entity_varchar.attribute_id = 41 AND
//                    catalog_category_entity_varchar.entity_id = $id";
//                $cat_query = DB::select($SQL);
//                $string .= $cat_query[0]->value .', ';
//               // $category[$item_one->entity_id] = $cat_query[0]->value;
//            }
//            $category[$item_one->entity_id] = $string;
//            //find category
//
//            $html .= "<tr><td>".$item_one->entity_id."</td><td>".$string."</td></tr>";


//            // other details
//            $sql_two = "SELECT
//                    `e`.`entity_id`,
//                `e`.sku,
//                 `price_index`.`price`,
//                    `price_index`.`tax_class_id`,
//                    `price_index`.`final_price`,
//                    `price_index`.`min_price`,
//                    `price_index`.`max_price`,
//                    `price_index`.`tier_price`,
//                   b.value AS thumbnail
//                FROM
//                    `catalog_product_entity` AS `e`
//
//                INNER JOIN `catalog_product_index_price` AS `price_index` ON price_index.entity_id = e.entity_id
//                LEFT JOIN `catalog_product_entity_media_gallery` AS b ON e.entity_id = b.entity_id
//                INNER JOIN `catalog_product_entity_media_gallery_value` AS c ON c.value_id = b.value_id
//                INNER JOIN `catalog_category_product` AS ccp ON ccp.product_id = e.entity_id
//                AND price_index.website_id = '1'
//                AND price_index.customer_group_id = 0
//                WHERE e.entity_id = $item_one->entity_id
//                GROUP BY
//                    `e`.`entity_id`";
//
//            $cat_query = DB::select($sql_two);

//        }

//        $html .= "</table>";
//        echo $html;
//        die;

        $privacy_list = $this->setting_model->getAllPrivacy();

        // get news limit 10
        $news_data = $this->getDashboardNews(6);

        // get events limit
        $events = $this->getDashboardEvents(6);

        // Announcement
        $announcements = $this->getDashboardAnnouncement(10);

        // get user details
        $user = $this->user_model->getUserByID(Auth::user()->id);

        // get photos
        $photo_files = $this->getFiles('photos', array('jpg', 'jpeg', 'png', 'gif', 'pjpeg' ));
        $documents_files = $this->getFiles('documents', array('pdf', 'txt', 'html', 'dox', 'xlsx' ));
        $media_files = $this->getFiles('media', array('mp4', 'mpeg', 'mp4', 'x-flv', 'quicktime', '3gpp' ));

        // get Upcoming Birthdays
        $upcoming_birthday = $this->getUpcomingBirthday();

        // slider images
        $slider_images = $this->getSliders();
        // get polls
        $polls_data =  $this->getPoll();
        
        // get user notification by user_id
        $notifications = $this->setting_model->getUserNotification(Auth::user()->id);
        foreach ($notifications as $key => $value) {
            $short_date[$key] = $value['time_elapsed'];
        }

        if(count($notifications) > 0)
        {
            array_multisort($short_date, SORT_ASC, $notifications);
        }
        session()->put('notifications', $notifications);


        return view('dashboard.new', [
            'news_posts'                => $news_data['posts'],
            'news_content'              => $news_data['content'],
            'news_content_other'        => $news_data['content_other'],
            'news_user_liked_post_ids'  => $news_data['user_liked_post_ids'],
            'news_post_like_count'      => $news_data['post_like_count'],
            'news_post_comment_count'   => $news_data['post_comment_count'],
            'events'                    => $events,
            'announcements'             => $announcements,
            'user'                      => $user,
            'photo_files'               => $photo_files,
            'documents_files'           => $documents_files,
            'media_files'               => $media_files,
            'upcoming_birthday'         => $upcoming_birthday,
            'polls'                     => $polls_data['polls'],
            'poll_options'              => $polls_data['poll_options'],
            'slider_images'             => $slider_images
        ]);

    }

    //
    public function indexOld()
    {
        $privacy_list = $this->setting_model->getAllPrivacy();

        // get news limit 10
        $news_data = $this->getDashboardNews(6);

        // get events limit
        $events = $this->getDashboardEvents(6);

        // Announcement
        $announcements = $this->getDashboardAnnouncement(10);

        // get user details
        $user = $this->user_model->getUserByID(Auth::user()->id);

        // get photos
        $photo_files = $this->getFiles('photos', array('jpg', 'jpeg', 'png', 'gif', 'pjpeg' ));
        $documents_files = $this->getFiles('documents', array('pdf', 'txt', 'html', 'dox', 'xlsx' ));
        $media_files = $this->getFiles('media', array('mp4', 'mpeg', 'mp4', 'x-flv', 'quicktime', '3gpp' ));

        // get Upcoming Birthdays
        $upcoming_birthday = $this->getUpcomingBirthday();

        // slider images
        $slider_images = $this->getSliders();

        // get polls
        $polls_data =  $this->getPoll();

        return view('dashboard.index', [
            'news_posts'                => $news_data['posts'],
            'news_content'              => $news_data['content'],
            'news_content_other'        => $news_data['content_other'],
            'news_user_liked_post_ids'  => $news_data['user_liked_post_ids'],
            'news_post_like_count'      => $news_data['post_like_count'],
            'news_post_comment_count'   => $news_data['post_comment_count'],
            'events'                    => $events,
            'announcements'             => $announcements,
            'user'                      => $user,
            'photo_files'               => $photo_files,
            'documents_files'           => $documents_files,
            'media_files'               => $media_files,
            'upcoming_birthday'         => $upcoming_birthday,
            'polls'                     => $polls_data['polls'],
            'poll_options'              => $polls_data['poll_options'],
            'slider_images'             => $slider_images
        ]);

    }

    /**
     * Description Get Dashboard News by Limit
     * @param int $limit
     * @return array
     */
    public function getDashboardNews($limit = 0)
    {

        $privacy_list = $this->setting_model->getAllPrivacy();

        $results = array(
            'posts'                 => array(),
            'content'               => array(),
            'content_other'         => array(),
            'user_liked_post_ids'   => array(),
            'post_like_count'       => 0,
            'post_comment_count'    => 0,
            'post_comment'          => array(),
            'level_2_post_comment'  => 0
        );
        // Get all posts by view for user
        $final_result = array();
        foreach ($privacy_list as $privacy_item) {
            $posts = $this->post_model->getAllPostByPrivacyCode($privacy_item->code);
            if (count($posts) > 0) {
                foreach ($posts as $post) {
                    $final_result[] = $post;
                }
            }
        }

        if(count($final_result) > 0)
        {
            // array short by publish date
            foreach ($final_result as $key => $value) {
                $short_date[$key] = strtotime($value->publish_date);
            }

            array_multisort($short_date, SORT_DESC, $final_result);

            // Collection create for pagination
            $collection = collect($final_result);
            $posts = $this->collection_paginate($collection, $limit);

            $content = array();
            $content_other = '';
            $post_like_count = array();
            $post_comment_count = array();
            $post_comment = array();
            foreach ($posts as $post) {
                // get content
                $post_type = $post->post_type;
                if ($post_type == 'gallery') {
                    $content[$post->id] = json_decode($post->post_content);
                } else {
                    $content_other[$post->id] = json_decode($post->post_content);
                }

                //post like counts
                $post_like_count[$post->id] = $this->post_model->getPostLikeByPostID($post->id);
                $post_comment_count[$post->id] = $this->post_model->getPostCommentByPostID($post->id);
                $post_comment[$post->id] = $this->post_model->getPostCommentsByPostID($post->id);

            }

            // level 2 post
            $level_2_post_comment = array();
            foreach ($posts as $post) {
                foreach ($post_comment[$post->id] as $post_comment_level) {
                    $level_2_post_comment[$post->id][$post_comment_level->post_comment_id] = $this->post_model->getPostCommentsByPostID($post->id, $post_comment_level->post_comment_id);
                }
            }

            // user liked post
            $user_liked_posts = $this->post_model->getUserLikePostId();
            $user_post_liked_ids = array();
            if (count($user_liked_posts) > 0) {
                foreach ($user_liked_posts as $user_like) {
                    $user_post_liked_ids[] = $user_like->post_id;
                }
            }

            $posts_array = array();
            foreach ($posts as $post) {
                $posts_array[] = array(
                    'id'            => $post->id,
                    'first_name'    => $post->first_name,
                    'thumb_image'   => $post->thumb_image,
                    'last_name'     => $post->last_name,
                    'body'          => $post->body,
                    'profile_image' => $post->profile_image,
                    'publish_date'  => $post->publish_date,
                    'title'         => $post->title,
                    'active_time'   => $this->setting_model->timeAgo($post->publish_date)
                );
            }

            $results = array(
                'posts'                 => $posts_array,
                'content'               => $content,
                'content_other'         => $content_other,
                'user_liked_post_ids'   => $user_post_liked_ids,
                'post_like_count'       => $post_like_count,
                'post_comment_count'    => $post_comment_count,
                'post_comment'          => $post_comment,
                'level_2_post_comment'  => $level_2_post_comment
            );
        }


        return $results;
    }

    /**
     * Description get list of events
     * @return mixed
     */
    public function getDashboardEvents($limit = 0)
    {
        $events = $this->event_model->getEventsByLimit($limit);

        $events_array = array();
        foreach ($events as $event) {
            $events_array[] = array(
                'name' => $event->name,
                'event_image' => $event->event_image,
                'description' => $event->description,
                'start_date' => $event->start_date,
                'end_date' => $event->end_date,
                'profile_image' => $event->profile_image,
                'first_name' => $event->first_name,
                'last_name' => $event->last_name,
                'active_time' => $this->setting_model->timeAgo($event->created_at)
            );
        }

        return $events_array;

    }


    /**
     * Description get dashboard announcement by status
     * @return mixed
     */
    public function getDashboardAnnouncement($limit)
    {
        $announcements = $this->announcement_model->getAllByStatus(1, $limit);
        return $announcements;
    }


    /**
     * Description Get files by folder vise
     * @param $path
     * @return array
     */
    public function getFiles($path, $extensions)
    {
        // files
        $dir = base_path('public/files/shares/'.$path);
        $files2 = scandir($dir, 1);
        $_files = array();
        foreach ($files2 as $file) {
            if (file_exists($dir . '/' . $file)) {
                $file_data = explode('.', $file);
                $file_data_value = explode('.', $file);
                end($file_data);
                $file_array_count = key($file_data);
                if ($file_array_count > 0) {
                    $file_type = $file_data_value[$file_array_count];
                    if (in_array($file_type, $extensions)) {
                        $_files[] = array(
                            'name' => $file,
                            'modify_date' => date("Y-m-d H:i:s", filectime($dir . '/' . $file)),
                            'date' => date("F d Y H:i:s.", filectime($dir . '/' . $file)),
                            'file_path' => '/shares/'.$path
                        );
                    }
                }
            }
        }

        // array short by modify date
        if(!empty($_files))
        {
            foreach ($_files as $key => $value) {
                $short_date[$key] = strtotime($value['modify_date']);
            }
            array_multisort($short_date, SORT_DESC, $_files);
        }

        return $_files;
    }


    /**
     * Description Get Upcoming Birthday
     * @return array
     */
    public function getUpcomingBirthday()
    {
        $_birthday =  $this->user_model->getUpcomingBirthday();
        $upcoming_birthday =  array();
        foreach ($_birthday  as $birthday)
        {
            $upcoming_birthday[] = array(
                'name' => $birthday->first_name. ' ' . $birthday->last_name,
                'birthday' => date("F j, Y", strtotime($birthday->birthday)) ,
                'profile_image' => $birthday->profile_image,
                'age' => $this->getAge($birthday->birthday)
            );
        }

        // array short by modify date
        if(!empty($upcoming_birthday))
        {
            foreach ($upcoming_birthday as $key => $value) {
                $short_date[$key] = strtotime($value['age']);
            }
            array_multisort($short_date, SORT_DESC, $upcoming_birthday);
        }

        return $upcoming_birthday;
    }

    /**
     * Description Get age
     * @param $date
     * @return mixed
     */
    function getAge($date) {
        return intval(date('Y', time() - strtotime($date))) - 1970;
    }


    /**
     * Description Get Poll
     * @return array
     */
    public function getPoll()
    {
        // get active polls 
        $polls = $this->poll_model->getActivePoll();
        $poll_options = array();
        foreach ($polls as $poll)
        {
            $poll_options[$poll->poll_id] = $this->poll_model->getPollOptionsByPollID($poll->poll_id);
        }

        return array(
            'polls' => $polls,
            'poll_options' => $poll_options
        );
        
    }

    /**
     * Description get Sliders
     * @return array
     */
    public function getSliders()
    {
        $sliders = $this->setting_model->getSetting('dashboard_main_slider');
        $slider_images = array();
        if(count($sliders) > 0 )
        {
            if( $sliders['setting_value'] != null && $sliders['setting_value'] != 'null' && $sliders['setting_value'] != '')
            {
                foreach ($sliders['setting_value']->slider_image as $key=>$slider_image)
                {
                    $slider_images[] = array(
                        'image'                 => $slider_image,
                        'title'                 => $sliders['setting_value']->title[$key],
                        'description'           => $sliders['setting_value']->description[$key],
                        'user_name'             => $sliders['user_name'],
                        'user_profile_image'    => $sliders['user_profile_image'],
                        'created_at'            => $sliders['created_at']
                    );
                }
            }

        }


        // get event slider images
        $slider_events_images = $this->setting_model->getEventsSlider();
        if(count($slider_events_images) > 0)
        {
            foreach ($slider_events_images as $slider_image)
            {
                $slider_images[] = array(
                    'image'                 => $slider_image->event_image,
                    'title'                 => $slider_image->name,
                    'description'           => $slider_image->description,
                    'user_name'             => $slider_image->first_name.' '.$slider_image->last_name,
                    'user_profile_image'    => $slider_image->profile_image,
                    'created_at'            => $slider_image->created_at
                );
            }
        }

        // get news slider images
        $slider_news_images = $this->setting_model->getNewsSlider();
        if(count($slider_news_images) > 0)
        {
            foreach ($slider_news_images as $slider_image)
            {
                $slider_images[] = array(
                    'image'                 => $slider_image->thumb_image,
                    'title'                 => $slider_image->title,
                    'description'           => $slider_image->body,
                    'user_name'             => $slider_image->first_name.' '.$slider_image->last_name,
                    'user_profile_image'    => $slider_image->profile_image,
                    'created_at'            => $slider_image->created_at,
                    'like_count'            => $this->post_model->getPostLikeByPostID($slider_image->id),
                    'comment_count'         => $this->post_model->getPostCommentByPostID($slider_image->id)
                );
            }
        }

        return $slider_images;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * Description create custom pagination
     * @param $collection
     * @param $per_page
     * @return LengthAwarePaginator
     */
    function collection_paginate($collection, $per_page)
    {
        $page   =  1;
        if(isset($_GET['page'])){
            $page = $_GET['page'];
        }

        // $items->forPage($page, $per_page)->values(),
        return new LengthAwarePaginator(
            $collection->forPage($page, $per_page)->values(),
            $collection->count(),
            $per_page,
            Paginator::resolveCurrentPage(),
            ['path' => Paginator::resolveCurrentPath()]
        );

    }


}
