<?php

namespace App\Http\Controllers;

use App\FavoriteModel;
use App\SettingModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FavoriteController extends Controller
{
    public $setting_model;
    public $favorite_model;

    public function __construct()
    {
        $this->setting_model =  new SettingModel();
        $this->favorite_model = new FavoriteModel();
    }

    /**
     * Description Index
     * @return mixed
     */
    public function index()
    {

        $privacy_list = $this->setting_model->getAllPrivacy();
        $user_groups =  $this->setting_model->getAllUserGroup();
        $offices =  $this->setting_model->getAllOffices();
        return view('favorites.create',[
            'privacy'       => $privacy_list,
            'user_groups'   => $user_groups,
            'offices'       => $offices,
            'row_id'        => 2
        ]);
    }

    /**
     * Description Create
     * @param Request $request
     * @return mixed
     */
    public function create(Request $request)
    {

        $this->validate($request,[
            'site_link' => 'required',
            'site' => 'required'
        ]);


        $post_data = $request->all();

        $data = array(
            'site' => $post_data['site'],
            'site_link' => $post_data['site_link'],
            'created_at' => date('Y-m-d H:i:s'),
            'status' => 1,
            'user_id' => Auth::user()->id
        );

        $this->favorite_model->createFavorite($data);
        return redirect()->route('favorite-list')
            ->with('flash_message', 'Favorite Site,
             '. $post_data['site'].' created');


    }

    /**
     * Description favorite List
     * @return mixed
     */
    public function favoriteList()
    {
        $user_id = Auth::user()->id;
        $results = $this->favorite_model->getList($user_id);

        return view('favorites.list',[
            'favorites' => $results
        ]);

    }

    /**
     * Description Destroy
     * @param $id
     * @return mixed
     */
    public function destroy($id)
    {
        $user_id = Auth::user()->id;
        $this->favorite_model->deleteFavorite($id, $user_id);
        return redirect()->route('favorite-list')
            ->with('flash_message', 'Favorite Site Delete successful');

    }
    
    public function favoriteEdit($id)
    {
        $results = $this->favorite_model->getFavoriteByID($id);

        $privacy_list = $this->setting_model->getAllPrivacy();
        $user_groups =  $this->setting_model->getAllUserGroup();
        $offices =  $this->setting_model->getAllOffices();
        return view('favorites.edit',[
            'privacy'       => $privacy_list,
            'user_groups'   => $user_groups,
            'offices'       => $offices,
            'row_id'        => 2,
            'favorite'      => $results
        ]);
    }

    /**
     * Description Edit favorite
     * @param Request $request
     */
    public function editFavorite(Request $request)
    {

        $this->validate($request,[
            'site_link' => 'required',
            'site' => 'required'
        ]);

        $post_data = $request->all();
        $user_id = Auth::user()->id;
        $favorite_id = $post_data['favorite_id'];
        $data = array(
            'site' => $post_data['site'],
            'site_link' => $post_data['site_link'],
            'status' => 1
        );

        $this->favorite_model->updateFavoriteByID($data, $favorite_id, $user_id);
        return redirect()->route('favorite-list')
            ->with('flash_message', 'Favorite Site Edit successful');

    }
}
