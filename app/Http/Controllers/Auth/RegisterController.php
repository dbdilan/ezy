<?php

namespace App\Http\Controllers\Auth;

use App\SettingModel;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use App\Jobs\SendVerificationEmail;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        // echo "string";die;
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users|domain:gmail.com',
            'password' => 'required|min:6|confirmed',
            'first_name' => 'required',
            'last_name' => 'required',
            'birthday' => 'required',
            'office_phone' => 'required',
            'mobile_phone' => 'required',
            'emp_number' => 'required',
            'address' => 'required',
            'profile_image' => 'required',
            'country_id' => 'required',
            'office_branch_id' => 'required',
            'gender' => 'required'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $setting_model = new SettingModel();

        //Identify user ip address
        $client  = @$_SERVER['HTTP_CLIENT_IP'];
        $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
        $remote  = $_SERVER['REMOTE_ADDR'];
        $country  = "Unknown";

        if(filter_var($client, FILTER_VALIDATE_IP))
        {
            $ip = $client;
        }
        elseif(filter_var($forward, FILTER_VALIDATE_IP))
        {
            $ip = $forward;
        }
        else
        {
            $ip = $remote;
        }

        $country_code = $this->ip_visitor_country($ip);

        //profile image upload
//        $file = $data['profile_image'];
//        $file_name = date('Ymdhis').'_'.$file->getClientOriginalName(); // md5(uniqid());
//        $image_name = $file_name;// . "." . $file->getClientOriginalExtension();
//        $file->move('upload/profile', $image_name);

        // post data
        $post_data = array(
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => $data['password'],
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'birthday' => date('Y-m-d', strtotime($data['birthday'])),
            'office_phone' => $data['office_phone'],
            'mobile_phone' => $data['mobile_phone'],
            'emp_number' => $data['emp_number'],
            'address' => $data['address'],
            'profile_image' => 'upload/profile/temp.JPG',
            'country_id' => $data['country_id'],
            'office_branch_id' => $data['office_branch_id'],
            'gender' => $data['gender'],
            'profession' => $data['profession'],
            'facebook_link' => $data['facebook_link'],
            'instagram_link' => $data['instagram_link'],
            'google_link' => $data['google_link'],
            'twitter_link' => $data['twitter_link'],
            'description' => $data['description'],
            'ip_address' => $ip,
            'ip_location' => '',
            'cover_photo' => 'upload/cover/temp_cover.jpg',
            'created_at' => date('Y-m-d H:i:s'),
            'email_token' => base64_encode($data['email']),
        );


        // return User::create($post_data);

       // user id
        $user_id =  User::create($post_data);

        // add role to new user
        $data_role = array(
            'user_id' => $user_id->id,
            'role_id' => 2 // guest user
        );
        $setting_model->addUserRole($data_role);
        // End add role to new user

        // user activity track
        $data_action = array(
            'user_id'       => $user_id->id,
            'type'          => 'Registration',
            'code'          => 'Registration',
            'description'   => 'User Registration',
            'created_at'    => date('Y-m-d H:i:s')
        );
        $setting_model->userAction($data_action);
        // End user activity track

        return $user_id;
    }

    /**
     * Description detected IP Address
     * @param $ip
     * @return string
     */
    function ip_visitor_country($ip)
    {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://www.geoplugin.net/json.gp?ip=".$ip);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        $ip_data_in = curl_exec($ch); // string
        curl_close($ch);

        $ip_data = json_decode($ip_data_in,true);
        $ip_data = str_replace('&quot;', '"', $ip_data); // for PHP 5.2 see stackoverflow.com/questions/3110487/

        $country = "LK";
        if($ip_data && $ip_data['geoplugin_countryCode'] != null) {
            $country = $ip_data['geoplugin_countryCode'];
        }

        return $country;
    }


    /**
     * Description IP Address detected
     * @param null $ip
     * @param string $purpose
     * @param bool $deep_detect
     * @return array|null
     */
    public function ipInfo($ip = NULL, $purpose = "location", $deep_detect = TRUE) {
        $output = NULL;
        if (filter_var($ip, FILTER_VALIDATE_IP) === FALSE) {
            $ip = $_SERVER["REMOTE_ADDR"];
            if ($deep_detect) {
                if (filter_var(@$_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP))
                    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
                if (filter_var(@$_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP))
                    $ip = $_SERVER['HTTP_CLIENT_IP'];
            }
        }
        $purpose    = str_replace(array("name", "\n", "\t", " ", "-", "_"), NULL, strtolower(trim($purpose)));
        $support    = array("country", "countrycode", "state", "region", "city", "location", "address");
        $continents = array(
            "AF" => "Africa",
            "AN" => "Antarctica",
            "AS" => "Asia",
            "EU" => "Europe",
            "OC" => "Australia (Oceania)",
            "NA" => "North America",
            "SA" => "South America"
        );

        if (filter_var($ip, FILTER_VALIDATE_IP) && in_array($purpose, $support)) {
            $ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));
            if (@strlen(trim($ipdat->geoplugin_countryCode)) == 2) {
                switch ($purpose) {
                    case "location":
                    $output = array(
                        "city"           => @$ipdat->geoplugin_city,
                        "state"          => @$ipdat->geoplugin_regionName,
                        "country"        => @$ipdat->geoplugin_countryName,
                        "country_code"   => @$ipdat->geoplugin_countryCode,
                        "continent"      => @$continents[strtoupper($ipdat->geoplugin_continentCode)],
                        "continent_code" => @$ipdat->geoplugin_continentCode
                    );
                    break;
                    case "address":
                    $address = array($ipdat->geoplugin_countryName);
                    if (@strlen($ipdat->geoplugin_regionName) >= 1)
                        $address[] = $ipdat->geoplugin_regionName;
                    if (@strlen($ipdat->geoplugin_city) >= 1)
                        $address[] = $ipdat->geoplugin_city;
                    $output = implode(", ", array_reverse($address));
                    break;
                    case "city":
                    $output = @$ipdat->geoplugin_city;
                    break;
                    case "state":
                    $output = @$ipdat->geoplugin_regionName;
                    break;
                    case "region":
                    $output = @$ipdat->geoplugin_regionName;
                    break;
                    case "country":
                    $output = @$ipdat->geoplugin_countryName;
                    break;
                    case "countrycode":
                    $output = @$ipdat->geoplugin_countryCode;
                    break;
                }
            }
        }
        return $output;

        //output usage
        // echo ipInfo("Visitor", "Country"); // India
        // echo ipInfo("Visitor", "Country Code"); // IN
        // echo ipInfo("Visitor", "State"); // Andhra Pradesh
        // echo ipInfo("Visitor", "City"); // Proddatur
        // echo ipInfo("Visitor", "Address"); // Proddatur, Andhra Pradesh, India
        //print_r(ipInfo("Visitor", "Location")); // Array ( [city] => Proddatur [state] => Andhra Pradesh [country] => India [country_code] => IN [continent] => Asia [continent_code] => AS )
    }

    /**
* Handle a registration request for the application.
*
* @param \Illuminate\Http\Request $request
* @return \Illuminate\Http\Response
*/
public function register(Request $request)
{
    $this->validator($request->all())->validate();
    event(new Registered($user = $this->create($request->all())));
    dispatch(new SendVerificationEmail($user));
    return view('verification');
}
/**
* Handle a registration request for the application.
*
* @param $token
* @return \Illuminate\Http\Response
*/
public function verify($token)
{
    $user = User::where('email_token',$token)->first();
    $user->verified = 1;
    if($user->save()){
        return view('emailconfirm',['user'=>$user]);
    }
}
}
