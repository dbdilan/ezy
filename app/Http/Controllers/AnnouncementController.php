<?php

namespace App\Http\Controllers;

use App\AnnouncementModel;
use App\SettingModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AnnouncementController extends Controller
{
    public $announcement_model;
    public $setting_model;

    public function __construct()
    {
        $this->announcement_model = New AnnouncementModel();
        $this->setting_model =  New SettingModel();
    }

    /**
     * Description create new announcement
     * @return mixed
     */
    public function create()
    {
        $privacy_list = $this->setting_model->getAllPrivacy();
        $user_groups =  $this->setting_model->getAllUserGroup();
        $offices =  $this->setting_model->getAllOffices();
        return view('announcement.create',[
            'privacy'       => $privacy_list,
            'user_groups'   => $user_groups,
            'offices'       => $offices,
            'row_id'        => 1
        ]);
    }

    /**
     * Description Add new announcement
     * @param Request $request
     */
    public function addNewAnnouncement(Request $request)
    {

        $this->validate($request,[
            'subject' => 'required|max:255',
            'description' => 'required|max:255',
            'publish_date' => 'required',
            'status' => 'required'
        ]);


        $post_data = $request->all();

        $data = array(
            'subject' => $post_data['subject'],
            'description' => $post_data['description'],
            'publish_date' => $post_data['publish_date'],
            'created_by' => $post_data['created_by'],
            'created_at' => date('Y-m-d H:i:s'),
            'status' => $post_data['status'],
            'privacy' => $post_data['privacy'],
            'location' => isset($post_data['office']) ?  json_encode($post_data['office']) : null,
            'group' => isset($post_data['group']) ?  json_encode($post_data['group']) : null,
        );

        $announcement_id = $this->announcement_model->addNewAnnouncement($data);

        // user activity track
        $data = array(
            'user_id'       => Auth::user()->id,
            'type'          => 'Announcement',
            'code'          => $post_data['subject'],
            'description'   => 'Add new Announcement',
            'created_at'    => date('Y-m-d H:i:s')
        );
        $this->setting_model->userAction($data);
        // End user activity track

        // notification add
        $description =  Auth::user()->first_name. " added ".$post_data['subject']." as announcement.";
        $notification = array(
            'notify_user_id' => '',
            'notified_by' => Auth::user()->id,
            'description' => $description,
            'notify_type' => 'announcement',
            'privacy' => $post_data['privacy'],
            'location' => isset($post_data['office']) ?  json_encode($post_data['office']) : null,
            'group' => isset($post_data['group']) ?  json_encode($post_data['group']) : null,
            'notify_date' => $post_data['publish_date'],
            'link' => 'announcement/list/'.$announcement_id
        );
        $this->setting_model->addNotification($notification);
        // notification

        session()->flash('success_message', 'Announcement created successful!');
        return redirect('announcement/list');
    }

    /**
     * Description Gwt List
     * @return mixed
     */
    public function getList()
    {
        $announcements = $this->announcement_model->getAll();

        return view('announcement.list',[
            'announcements' => $announcements
        ]);
    }


    /**
     * Descriptoin Edit announcement
     * @param $id
     * @return mixed
     */
    public function editAnnouncement($id)
    {

        $announcement = $this->announcement_model->getAnnouncementByID($id);
        //setting
        $privacy_list = $this->setting_model->getAllPrivacy();
        $user_groups =  $this->setting_model->getAllUserGroup();
        $offices =  $this->setting_model->getAllOffices();

        //if office or group
        $office_array =  array();
        $group_array = array();
        if($announcement->group)
        {
            $group_array =json_decode($announcement->group);
        }
        if($announcement->location)
        {
            $office_array =json_decode($announcement->location);
        }

        return view('announcement.edit',[
            'announcement' => $announcement,
            'privacy'       => $privacy_list,
            'user_groups'   => $user_groups,
            'offices'       => $offices,
            'office_array'  => $office_array,
            'group_array'   => $group_array
        ]);
    }

    /**
     * Description Edit announcement 
     * @param Request $request
     * @return mixed
     */
    public function editPostAnnouncement(Request $request)
    {
        $post_data = $request->all();
        $announcement_id = $post_data['announcement_id'];
        $data = array(
            'subject' => $post_data['subject'],
            'description' => $post_data['description'],
            'publish_date' => $post_data['publish_date'],
            'created_by' => $post_data['created_by'],
            'created_at' => date('Y-m-d H:i:s'),
            'status' => $post_data['status'],
            'privacy' => $post_data['privacy'],
            'location' => isset($post_data['office']) ?  json_encode($post_data['office']) : null,
            'group' => isset($post_data['group']) ?  json_encode($post_data['group']) : null,
        );

        $this->announcement_model->updateAnnouncement($data, $announcement_id);

        // user activity track
        $data = array(
            'user_id'       => Auth::user()->id,
            'type'          => 'Announcement',
            'code'          => $post_data['subject'],
            'description'   => 'Edit Announcement',
            'created_at'    => date('Y-m-d H:i:s')
        );
        $this->setting_model->userAction($data);
        // End user activity track

        session()->flash('success_message', 'Announcement update successful!');
        return redirect('announcement/list');
    }
    
    
    public function destroy($id)
    {
        $this->announcement_model->deleteAnnouncementByID($id);
        session()->flash('success_message', 'Announcement Delete successful!');
        return redirect('announcement/list');
    }
}
