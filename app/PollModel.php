<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PollModel extends Model
{
    /**
     * Description add new poll
     * @param $data
     * @param $options
     */
    public function addNewPoll($data, $options)
    {
        $results = DB::table('polls')->insertGetId($data);

        if($options)
        {
            foreach ($options as $option)
            {
                DB::table('poll_options')
                    ->insert([
                        'poll_id' => $results,
                        'name' => $option
                    ]);
            }
        }
    }

    /**
     * @param $data
     * @param $options
     * @param $poll_id
     */
    public function updatePoll($data, $options, $poll_id)
    {
        $results = DB::table('polls')
            ->where('polls.poll_id', '=', $poll_id)
            ->update($data);

        if($options)
        {
            foreach ($options as $key=>$option)
            {
                DB::table('poll_options')
                    ->where('poll_options.id', '=', $key)
                    ->update([
                        'name' => $option
                    ]);
            }
        }
    }

    /**
     * Description get poll by id
     * @param $id
     * @return mixed
     */
    public function getPollByID($id)
    {
        $results = DB::table('polls')
            ->where('polls.poll_id', '=', $id)
            ->first();
        return $results;
    }


    /**
     * Description get Poll Option by poll ID
     * @param $id
     * @return mixed
     */
    public function getPollOptionsByPollID($id)
    {
        $results = DB::table('poll_options')
            ->where('poll_options.poll_id', '=', $id)
            ->get();
        return $results;
    }

    /**
     * Description edit poll
     * @param $data
     * @param $options
     */
    public function editPoll($data, $options, $poll_id)
    {
        DB::table('polls')
            ->where('polls.poll_id', '=', $poll_id)
            ->update($data);

        // delete poll

//        if($options)
//        {
//            foreach ($options as $option)
//            {
//                DB::table('poll_options')
//                    ->insert([
//                        'poll_id' => $results,
//                        'name' => $option
//                    ]);
//            }
//        }
    }


    /**
     * Description get all polls
     * @return mixed
     */
    public function getAll()
    {
        $results = DB::table('polls')
            ->select('polls.*', 'users.first_name', 'users.last_name')
            ->join('users', 'users.id' , '=', 'polls.created_by')
            ->get();
        return $results;
    }

    /**
     * Description get active poll
     * @return mixed
     */
    public function getActivePoll()
    {
        $results = DB::table('polls')
            ->select('polls.*', 'users.first_name', 'users.last_name')
            ->join('users', 'users.id' , '=', 'polls.created_by')
            ->where('polls.publish_date', '<=' , date('Y-m-d'))
            ->where('polls.close_date', '>=' , date('Y-m-d'))
            ->where('polls.status', '=', 1)
            ->get();
        return $results;
    }

    /**
     * Description insert poll data
     * @param $data
     */
    public function insertVote($data)
    {
        DB::table('poll_votes')
            ->insert($data);
    }

    /**
     * Description check user vote
     * @param $poll_id
     * @param $user_id
     * @return mixed
     */
    public function checkUserVote($poll_id, $user_id)
    {
        $results = DB::table('poll_votes')
            ->select('poll_votes.*', 'poll_options.name')
            ->join('poll_options', 'poll_options.id', '=', 'poll_votes.option_id')
            ->where('poll_votes.poll_id', '=', $poll_id)
            ->where('poll_votes.user_id', '=', $user_id)
            ->get();

        return $results;
    }

    /**
     * Description get vote count by poll id
     * @param $poll_id
     * @return mixed
     */
    public function getVoteCountByPollID($poll_id)
    {
        $results = DB::table('poll_votes')
            ->select('poll_votes.option_id', DB::raw('count(*) as count_value'))
            ->where('poll_votes.poll_id', '=', $poll_id)
            ->groupBy('poll_votes.option_id')
            ->get();

        return $results;
    }

    /**
     * Description get option by option id
     * @param $option_id
     * @return mixed
     */
    public function getOptionByOptionID($option_id)
    {
        $results = DB::table('poll_options')
            ->where('poll_options.id', '=', $option_id)
            ->first();
        return $results;
    }

    /**
     * Description Delete Poll by ID
     * @param $id
     */
    public function deletePollByID($id)
    {
        DB::table('polls')
            ->where('polls.poll_id', '=', $id)
            ->delete();
    }
    
}

