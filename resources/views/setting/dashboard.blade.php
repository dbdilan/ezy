@extends('layouts.dashboard_app')

@section('title', '| Create New News')

@section('content')


        <!-- RIBBON -->
<div id="ribbon">

    <span class="ribbon-button-alignment">
        <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
            <i class="fa fa-refresh"></i>
        </span>
    </span>

    <!-- breadcrumb -->
    <ol class="breadcrumb">
        <li>Home</li>
        <li>News</li>
        <li>Create</li>
    </ol>
    <!-- end breadcrumb -->

</div>
<!-- END RIBBON -->

<!-- MAIN CONTENT -->
<div id="content">

    <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
            <h1 class="page-title txt-color-blueDark">
                <i class="fa fa-users fa-fw "></i>
                Setting <span>> Update </span>
            </h1>
        </div>
        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
            <button type="button" class="btn btn-primary pull-right" onclick="window.history.back()">< Back</button>
        </div>
    </div>

    <!-- widget grid -->
    <section id="widget-grid" class="">

        <!-- row -->
        <div class="row">

            <article class="col-sm-12 col-md-12 col-lg-9">

                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget" id="wid-id-1" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                        <h2>Setting </h2>

                    </header>

                    <!-- widget div-->
                    <div>

                        <!-- widget edit box -->
                        <div class="jarviswidget-editbox">
                            <!-- This area used as dropdown edit box -->

                        </div>
                        <!-- end widget edit box -->

                        <!-- widget content -->
                        <div class="widget-body">

                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <form action="{{ url('setting/dashboard/save') }}" method="post" id="post-form" class="form" >
                                {{ csrf_field() }}

                                <!-- Slider Image Div -->
                                <div class="form-group " id="gallery-div-box">
                                    <label>Slider Image</label>
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>File</th>
                                            <th>Image</th>
                                            <th>description</th>
                                            <th>Remove</th>
                                        </tr>
                                        </thead>
                                        <tfoot>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td>
                                                <button type="button" id="add-new-gallery-btn" class="btn btn-primary hidden" onclick="addNewGallery()"><i class="fa fa-plus"></i></button>
                                            </td>
                                        </tr>
                                        </tfoot>
                                        <tbody class="gallery-div">
                                        @php $i = $row_id; @endphp
                                        @if(count($slider_images) > 0)
                                            @php $i = 1; @endphp
                                            @foreach($slider_images as $slider)
                                            <tr id="image-row{{ $i }}">
                                                <td style="width:40%">
                                                    <div class="input-group">
                                                    <span class="input-group-btn">
                                                        <a id="lfm" data-input="thumbnail-1" data-preview="holder-{{ $i }}"  class="btn btn-primary file-manager">
                                                            <i class="fa fa-picture-o"></i> Choose
                                                        </a>
                                                    </span>
                                                        <input id="thumbnail-{{ $i }}" value="{{ $slider['image'] }}" class="form-control" type="text" name="slider_image[]">
                                                    </div>
                                                </td>
                                                <td class="gallery-thmb-img">
                                                    <img id="holder-{{ $i }}" src="{{ $slider['image'] }}" style="max-height:50px;">
                                                </td>
                                                <td>
                                                    <input class="form-control" type="text" name="title[]" value="{{ $slider['title'] }}" placeholder="Title">
                                                </td>
                                                <td>
                                                    <textarea class="form-control" name="description[]" placeholder="description">{{ $slider['description'] }}</textarea>
                                                </td>
                                                <td>
                                                    <button type="button" onclick="$('#image-row{{ $i }}, .tooltip').remove();" data-toggle="tooltip" title="Remove" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button>
                                                </td>
                                            </tr>
                                            @php $i++; @endphp
                                       @endforeach
                                            @endif
                                            <tr id="image-row{{ $i }}">
                                                <td style="width:40%">
                                                    <div class="input-group">
                                                    <span class="input-group-btn">
                                                        <a id="lfm" data-input="thumbnail-{{ $i }}" data-preview="holder-{{ $i }}"  class="btn btn-primary file-manager">
                                                            <i class="fa fa-picture-o"></i> Choose
                                                        </a>
                                                    </span>
                                                        <input id="thumbnail-{{ $i }}" class="form-control" type="text" name="slider_image[]">
                                                    </div>
                                                </td>
                                                <td class="gallery-thmb-img">
                                                    <img id="holder-{{ $i }}" style="max-height:50px;">
                                                </td>
                                                <td>
                                                    <input class="form-control" type="text" name="title[]" value="" placeholder="Title">
                                                </td>
                                                <td>
                                                    <textarea class="form-control" name="description[]" placeholder="description"></textarea>
                                                </td>
                                                <td>
                                                    <button type="button" onclick="$('#image-row{{ $i }}, .tooltip').remove();" data-toggle="tooltip" title="Remove" class="btn btn-danger hidden row-delete-btn-{{ $i }}"><i class="fa fa-minus-circle"></i></button>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- // Slider image Div -->

                                <div class="form-group">
                                    {{ Form::submit('Create Post', array('class' => 'btn btn-success btn-lg btn-block')) }}
                                </div>

                            </form>
                        </div>
                        <!-- end widget content -->

                    </div>
                    <!-- end widget div -->

                </div>
                <!-- end widget -->

            </article>
            <!-- END COL -->
        </div>

    </section>
    <!-- end widget grid -->

</div>
<!-- END MAIN CONTENT -->



@endsection

@section('page-js')

    <script>


        //======================================== Gallery Add =============================================================
        thumbChange();
        $("#thumbnail-{{ $i }}").bind("change paste keyup", function() {
            thumbChange();
        });

        function thumbChange() {
            var str_value = $('#thumbnail-{{ $i }}').val();
            var add_new_btn = $('#add-new-gallery-btn');
            var row_delete = $('.row-delete-btn-{{ $i }}');
            add_new_btn.addClass('hidden');
            if(str_value != '')
            {
                add_new_btn.removeClass('hidden');
                row_delete.removeClass('hidden');
            }
        }
        //======================================== End Gallery Add =========================================================
        @php $i++; @endphp

        var row_id = '{{ $i }}';
        function addNewGallery() {

            var html = '';
            html += '<tr id="image-row' + row_id  + '">';
            html += '<td style="width:40%">';
            html += '<div class="input-group">';
            html += '<span class="input-group-btn">';
            html += '<a id="lfm'+row_id+'" data-input="thumbnail-'+row_id+'" data-preview="holder-'+row_id+'"  class="btn btn-primary file-manager">';
            html += '<i class="fa fa-picture-o"></i> Choose';
            html += '</a>';
            html += '</span>';
            html += '<input id="thumbnail-'+row_id+'" class="form-control" type="text" name="slider_image[]">';
            html += '</div>';
            html += '</td>';
            html += '<td class="gallery-thmb-img">';
            html += '<img id="holder-'+row_id+'" style="max-height:50px;">';
            html += '</td>';
            html += '<td>';
            html += '<input  class="form-control"  type="text" name="title[]" value="" placeholder="Title">';
            html += '</td>';
            html += '<td>';
            html += '<textarea  class="form-control"  name="description[]" placeholder="description"></textarea>';
            html += '</td>';
            html += '<td style="padding-left: 10px; " class="text-left"><button type="button" onclick="$(\'#image-row' + row_id  + ', .tooltip\').remove();" data-toggle="tooltip" title="Remove" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
            html += '</tr>';

            row_id++;

            $('.gallery-div').append(html);

        }

        $("#post-form").validate();

        // DO NOT REMOVE : GLOBAL FUNCTIONS!
        $(document).ready(function() {
            pageSetUp();
        })

    </script>
@endsection