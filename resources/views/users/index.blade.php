@extends('layouts.new_dashboard_app')

@section('title', '| Users')

@section('content')
    <style>
        .pagination li {
            display:inline-block;
            padding:0px;
        }
    </style>

    <!--CONTENT CONTAINER-->
    <!--===================================================-->
    <div id="content-container">

        <!--Page content-->
        <!--===================================================-->
        <div id="page-content">


            <div id="page-head">
                <!--Page Title-->
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <div style="padding-left:0px;" id="page-title">
                    <h1 style="color:#4d627b;" class="page-header text-overflow">Users</h1>
                </div>
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <!--End page title-->

                <!--Breadcrumb-->
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <ol style="padding-left:0px;" class="breadcrumb">
                    <li><a href="#"><i style="color:#4d627b;" class="demo-pli-home"></i></a></li>
                    <li><a style="color:#4d627b;" href="#">Users</a></li>
                </ol>
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <!--End breadcrumb-->
            </div>

            <div id="users">
                <div class="filter-group row">

                    <!-- Contact Toolbar -->
                    <!---------------------------------->
                    <div style="margin-top: 20px;" class=" pad-btm">
                        <div class="col-sm-6 toolbar-left">
                            @can('Role View')<a href="{{ route('roles.index') }}" id="demo-btn-addrow" class="btn btn-purple">Roles</a>@endcan
                            @can('Permission View')<a href="{{ route('permissions.index') }}" id="demo-btn-addrow" class="btn btn-purple">Permissions</a>@endcan
                        <!--@can('User Create')<a href="{{ route('users.create') }}" id="demo-btn-addrow" class="btn btn-purple">Add User</a>@endcan -->
                        </div>
                        <div class="col-sm-6 toolbar-right">
                            <form class="form-inline">
                                <div class="form-group">
                                    <input type="text" class="search form-control" placeholder="Search" />
                                </div>

                            </form>
                        </div>
                    </div>
                    <!---------------------------------->
                        </div>

                        <div id="content"  class="list row demo">
                            @php
                            $id = 0;
                            @endphp
                            @foreach ($users as $user)
                                @php
                                $id++;
                                @endphp
                                <!-- Contact Widget -->
                                <div  class="col-sm-3 col-md-3 col-xs-6 list--list-item">
                                    <div class="panel pos-rel">
                                        <div class="pad-all text-center">
                                            <div class="widget-control">
                                                <div class="btn-group dropdown">
                                                    <a href="#" class="dropdown-toggle btn btn-trans" data-toggle="dropdown" aria-expanded="false"><i class="demo-psi-dot-vertical icon-lg"></i></a>
                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                        @if(auth()->user()->can('User Edit'))
                                                            @can('User Edit')
                                                            <li><a href="{{ route('users.edit', $user->id) }}"><i class="icon-lg icon-fw demo-psi-pen-5"></i> Edit</a></li>
                                                            @endcan
                                                            @endif

                                                            @if(auth()->user()->can('User Delete'))
                                                            @can('User Delete')
                                                                    <!--{!! Form::open(['method' => 'DELETE', 'route' => ['users.destroy', $user->id], 'id' => 'form-user-'.$user->id, 'class' => 'delete-form' ]) !!}
                                                                    <button type="button" data-form="form-user-{{ $user->id }}" class="btn delete-btn"><i class="icon-lg icon-fw demo-pli-recycling"></i> Delete</button>
                                                            {!! Form::close() !!}-->
                                                            <li><a href="{{ route('users.edit', $user->id) }}"><i class="icon-lg icon-fw demo-pli-recycling"></i> Remove</a></li>
                                                            @endcan
                                                        @endif
                                                    </ul>
                                                </div>
                                            </div>
                                            <a>
                                                <img alt="Profile Picture" class="img-lg img-circle mar-btm" src="{{ asset($user->profile_image) }}">
                                                <p class="text-lg text-semibold mar-no text-main name">{{ $user->name }}</p>
                                                <p class="text-sm profession">{{ $user->profession }}</p>
                                                <p class="text-sm country">{{ $user->country_name }}</p>

                                                @if(auth()->user()->can('User View'))
                                                    @can('User View')
                                                    <a href="{{ route('users.show', $user->id) }}" class="btn btn-primary mar-ver"><i class="demo-pli-male icon-fw"></i>View Profile</a>
                                                    @endcan
                                                @endif

                                                <ul class="list-unstyled text-center bord-top pad-top mar-no row">
                                                    <li class="col-xs-6">
                                                        <span class="text-lg text-semibold text-main">{{ $user->user_follows }}</span>
                                                        <p class="text-muted mar-no">Following</p>
                                                    </li>
                                                    <li class="col-xs-6">
                                                        <span class="text-lg text-semibold text-main">{{ $user->user_connetions }}</span>
                                                        <p class="text-muted mar-no">Connection</p>
                                                    </li>
                                                </ul>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                        <ul id="page-selection" class="pagination demo"></ul>
                        <!--<div class="no-result">No Results</div>-->
                    </div>
                </div>
            </div>
            <!--===================================================-->
            <!--End page content-->
        </div>
        <!--===================================================-->
        <!--END CONTENT CONTAINER-->

        @endsection

        @section('page-js')
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/list.js/1.5.0/list.min.js"></script>
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

            <script>
                var options = {
                    valueNames: [
                        'name',
                            'profession','country',
                        { data: ['gender']}
                    ],
                    page: 8,
                    pagination: true
                };
                var userList = new List('users', options);

                function resetList(){
                    userList.search();
                    userList.filter();
                    userList.update();
                    $(".filter-all").prop('checked', true);
                    $('.filter').prop('checked', false);
                    $('.search').val('');
                    //console.log('Reset Successfully!');
                };

                function updateList(){
                    var values_gender = $("input[name=gender]:checked").val();
                    var values_address = $("input[name=address]:checked").val();
                    console.log(values_gender, values_address);

                    userList.filter(function (item) {
                        var genderFilter = false;
                        var addressFilter = false;

                        if(values_gender == "all")
                        {
                            genderFilter = true;
                        } else {
                            genderFilter = item.values().gender == values_gender;

                        }
                        if(values_address == null)
                        {
                            addressFilter = true;
                        } else {
                            addressFilter = item.values().address.indexOf(values_address) >= 0;
                        }
                        return addressFilter && genderFilter
                    });
                    userList.update();
                    //console.log('Filtered: ' + values_gender);
                }
            </script>

            <!-- PAGE RELATED PLUGIN(S) -->
            <script src="{{ asset('asset/js/plugin/datatables/jquery.dataTables.min.js') }}"></script>
            <script src="{{ asset('asset/js/plugin/datatables/dataTables.colVis.min.js') }}"></script>
            <script src="{{ asset('asset/js/plugin/datatables/dataTables.tableTools.min.js') }}"></script>
            <script src="{{ asset('asset/js/plugin/datatables/dataTables.bootstrap.min.js') }}"></script>
            <script src="{{ asset('asset/js/plugin/datatable-responsive/datatables.responsive.min.js') }}"></script>

            <script>
                // DO NOT REMOVE : GLOBAL FUNCTIONS!
                $(document).ready(function() {
                    pageSetUp();

                    /* // DOM Position key index //
                     l - Length changing (dropdown)
                     f - Filtering input (search)
                     t - The Table! (datatable)
                     i - Information (records)
                     p - Pagination (paging)
                     r - pRocessing
                     < and > - div elements
                     <"#id" and > - div with an id
                     <"class" and > - div with a class
                     <"#id.class" and > - div with an id and class
                     Also see: http://legacy.datatables.net/usage/features
                     */

                    /* BASIC ;*/
                    var responsiveHelper_dt_basic = undefined;

                    var breakpointDefinition = {
                        tablet: 1024,
                        phone: 480
                    };

                    $('#dt_basic').dataTable({
                        "sDom": "<'dt-toolbar'<'col-xs-12 col-sm-6'f><'col-sm-6 col-xs-12 hidden-xs'l>r>" +
                        "t" +
                        "<'dt-toolbar-footer'<'col-sm-6 col-xs-12 hidden-xs'i><'col-xs-12 col-sm-6'p>>",
                        "autoWidth": true,
                        "oLanguage": {
                            "sSearch": '<span class="input-group-addon"><i class="glyphicon glyphicon-search"></i></span>'
                        },
                        "preDrawCallback": function () {
                            // Initialize the responsive datatables helper once.
                            if (!responsiveHelper_dt_basic) {
                                responsiveHelper_dt_basic = new ResponsiveDatatablesHelper($('#dt_basic'), breakpointDefinition);
                            }
                        },
                        "rowCallback": function (nRow) {
                            responsiveHelper_dt_basic.createExpandIcon(nRow);
                        },
                        "drawCallback": function (oSettings) {
                            responsiveHelper_dt_basic.respond();
                        }
                    });
                });
            </script>
@endsection