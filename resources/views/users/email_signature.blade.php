@extends('layouts.new_dashboard_app')

@section('title', '| Create New News')

@section('content')

    <link href="{{ asset('css/smart_wizard.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/smart_wizard_theme_circles.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/jquery.toast.min.css') }}" rel="stylesheet" type="text/css" />

    <!-- Main Style -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/main.css') }}"/>


    <div id="content-container">
        <!--Page content-->
        <!--===================================================-->
        <div id="page-content">
            <div style="margin-top:20px;" class="row">
                <div class="col-md-12">

                    <div class="panel">
                        <!--Page Title-->
                        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->

                        <div id="page-title">
                            <h1 class="page-header text-overflow">Email Signature</h1>
                        </div>
                        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                        <!--End page title-->


                        <!--Breadcrumb-->
                        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                        <ol class="breadcrumb">
                            <li><a href="#"><i class="demo-pli-home"></i></a></li>
                            <li><a href="#">User</a></li>
                            <li class="active">Email Signature</li>
                        </ol>
                        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                        <!--End breadcrumb-->

                        <!--Block Styled Form -->
                        <!--===================================================-->

                            <div class="panel-body" style="background-color: red;">

                                <!-- multistep form -->
                                <legend id="logo-legend"><img src="{{ asset('images/logo.png') }}" id="logoimg" /> <span id="appname">Email Signature Wizard</span></legend>

                                <form action="index.php" method="post" class="form" role="form" id="msform">

                                    <h2 class="fs-main-title">Follow the steps below and create your own Email Signature within minutes!</h2>

                                    <!-- progressbar -->
                                    <ul id="progressbar">
                                        <li class="active">Select Country</li>
                                        <li>Enter Information</li>
                                        <li>Confirm</li>
                                        <li>Download</li>
                                    </ul>
                                    <!-- fieldsets -->
                                    <fieldset class="country-panel">

                                        <select class="form-control" name="country" id="country" onChange="countryDone()">
                                            <option value="">Select your country</option>
                                            <option value="LK" <?php if(isset($country) && $country=='LK') echo 'selected="selected"'?>>Sri Lanka</option>
                                            <option value="IN" <?php if(isset($country) && $country=='IN') echo 'selected="selected"'?>>India</option>
                                            <option value="BD" <?php if(isset($country) && $country=='BD') echo 'selected="selected"'?>>Bangladesh</option>
                                            <option value="KH" <?php if(isset($country) && $country=='KH') echo 'selected="selected"'?>>Cambodia</option>
                                            <option value="MM" <?php if(isset($country) && $country=='MM') echo 'selected="selected"'?>>Myanmar</option>
                                            <option value="PK" <?php if(isset($country) && $country=='PK') echo 'selected="selected"'?>>Pakistan</option>
                                            <option value="PH" <?php if(isset($country) && $country=='PH') echo 'selected="selected"'?>>Philippines</option>
                                            <option value="SG" <?php if(isset($country) && $country=='SG') echo 'selected="selected"'?>>Singapore</option>
                                            {{--<option value="LA" <?php if(isset($country) && $country=='LA') echo 'selected="selected"'?>>Laos</option>                   --}}
                                            <option value="NP" <?php if(isset($country) && $country=='NP') echo 'selected="selected"'?>>Nepal</option>
                                            <option value="VN" <?php if(isset($country) && $country=='VN') echo 'selected="selected"'?>>Vietnam</option>
                                        </select>

                                        <select class="form-control companySelectBox" name="company_name" id="company_name"   onChange="companyDone()" style="display:none;">
                                        </select>

                                        <input type="button" name="next" class="next action-button" value="Next" id="countryOK"/>
                                    </fieldset>

                                    <fieldset>
                                        <div class="fieldWrap">
                                            <div class="input-inner">
                                                <span class="span1 required-field"><i class="fa fa-asterisk"></i></span>
                                                <input type="text" name="fullname" placeholder="Enter Your Name"  id="txt_fname" value="<?php if(isset($userName)) echo $userName?>" required/>
                                                <label>Full Name</label>
                                            </div>

                                            <div class="input-inner">
                                                <span class="span2 required-field"><i class="fa fa-asterisk"></i></span>
                                                <input type="text" name="position" placeholder="Designation (eg: Marketing Manager)" id="txt_position" value="<?php if(isset($userPosition)) echo $userPosition?>" required/>
                                                <label>Designation</label>
                                            </div>

                                            <div class="input-inner">
                                                <input type="text" name="section" placeholder="Group/Division (eg: Enterprise Solutions Group)" id="section" value="<?php if(isset($userSection)) echo $userSection?>"/>
                                                <label>Group/Division</label>
                                            </div>

                                            <div class="input-inner">
                                                <span class="span3 required-field"><i class="fa fa-asterisk"></i></span>
                                                <input type="text" name="mobile" placeholder="Mobile Number" id="txt_mobile" value="<?php if(isset($userMobile)) echo $userMobile?>" required/>
                                                <label>Mobile Number</label>
                                            </div>

                                            <div id="fax_wrap" class="input-inner">
                                                <span class="span3 required-field"><i class="fa fa-asterisk"></i></span>
                                                <input type="text" name="fax" placeholder="Fax Number" id="txt_fax" value="" required/>
                                                <label>Fax Number</label>
                                            </div>
                                        </div>

                                        <input type="button" name="previous" class="previous action-button" value="&#xf104; &nbsp; Go Back" />
                                        <input type="button" name="next" class="next-validate action-button " value="Continue &nbsp; &#xf105;" onClick="fillConfirm()" id="btn_continue" /> <!-- remove next class and and next-validate class -->

                                    </fieldset>


                                    <fieldset id="confirm-info">
                                        <div id="confirm-info-prev">
                                            <ul>
                                                <li><label>Country</label><span id="selected_country" name="selected_country"></span></li>
                                                <li><label>Name</label><span id="selected_name" name="selected_name"></span></li>
                                                <li><label>Designation</label><span id="selected_designation" name="selected_designation"></span></li>
                                                <li><label>Group/Division</label><span id="selected_division" name="selected_division"></span></li>
                                                <li><label>Mobile Number</label><span id="selected_mobile" name="selected_mobile"></span></li>
                                                <li class="fax_element"><label>Fax</label><span id="selected_fax" name="selected_fax"></span></li>
                                            </ul>
                                        </div>

                                        <input type="button" name="previous" class="previous action-button" value="&#xf104; &nbsp;Go Back/Edit" />
                                        <input type="button" name="confirm" id="confirmInfobtn-hidden" class="next action-button" value="Confirm-hidden"  style="display:none;"/>
                                        <input type="button" name="next-hidden" id="confirmInfobtn" class="next-hidden action-button" value="Confirm &nbsp; &#xf105;"/>


                                    </fieldset>

                                    <fieldset>
                                        <h2 class="fs-main-title-final" style="background-color: blue;">Nice! Your Signature is ready to download.</h2>

                                        <div id="preview">
                                            <img src="" alt="Preview"/>
                                        </div>

                                        <input type="button" name="previous" class="previous action-button" value="&#xf104;  &nbsp;Go Back/Edit"/>
                                        <!--<input type="submit" name="submit" class="submit action-button" value="Download" />-->
                                        <a href="#" id="downloadFinal" class="submit action-button">Download<span><i class="fa fa-download" aria-hidden="true"></i></span>
                                        </a>
                                    </fieldset>
                                </form>

                            </div>
                        <div class="mid-canves"></div>
                            </div>

                            <div class="panel-footer text-right">
                            </div>

                        <!--===================================================-->
                        <!--End Block Styled Form -->
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('page-js')

    <script src="{{ asset('js/jquery.easing-1.3.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/jquery.toast.min.js') }}" type="text/javascript"></script>

    <script>

        var companyArr = [
            ['EZY INFOTECH PVT LTD ', 'EZY DISTRIBUTION PVT LTD ', 'EZY INTELLECT '], //BAN 0
            ['EZY DISTRIBUTION PVT LTD ', 'EZY INFOTECH PVT LTD ', 'EZY  TECHNOCRAFT PVT LTD ', 'EZY  INTELLECT PVT LTD '], //  LK 1
            ['EZY INTELLECT PTE LTD ', 'EZY SEA TECHNOLOGIES LTD'], //CAMBODIA 2
            ['EZY INFOTECH SOLUTIONS INDIA PVT LTD'],//india 3
            ['EZY INTELLECT PTE LTD'],//laos 4
            ['EZY INFOTECH ME FZE'], //middle east 5
            ['EZY INFOTECH COMPANY LIMITED', 'EZY INTELLECT COMPANY LIMITED', 'CITOO SOLUTIONS COMPANY LIMITED'],//MYANMAR 6
            ['EZY INFOTECH NEPAL PVT LTD'],//NEPAL 7
            ['EZY TECHNOLOGIES PVT LTD', 'EZY INFOTECH PVT LTD', 'EZY INTELLECT PVT LTD'], //PAKISTAN 8
            ['EZY INFOTECH INC'],//PHILIPPINES 9
            ['EZY INFOTECH PTE LTD', 'CITOO SOLUTIONS PTE LTD', 'EZY INTELLECT PTE LTD'], //SINGAPORE 10
            ['ELITE TECHNOLOGY JSC'], //VIETNAM 11
        ];



        //var testPattern = new RegExp("^(\\+\s)?(\\d+)$");
        var testPattern = new RegExp("[0-9 ]");

        var selectCountryVali;

        $('[name="mobile"]').on('keyup', function(){
            if($(this).val().length == 1)$(this).val('+');
            else{
                var res = chkInput();
                if(!res)$(this).val($(this).val().slice(0, -1));
            }
        });
        function chkInput(){
            var v = $('[name="mobile"]').val().charAt($('[name="mobile"]').val().length-1);
            return testPattern.test(v);
        }


        // bathiya added this codes for validations
        $(".next-validate").click(function(){
            var validate_fax = $('[name="fax"]').val();

            var mob_num = $.isNumeric($('[name="mobile"]').val());
            if( !$('#txt_fname').val() ) {
                //$('#txt_fname').addClass('placeholder_err');
                $.toast({
                    heading: 'Error',
                    text: 'Please Enter Your Full Name',
                    showHideTransition: 'fade',
                    position: 'mid-center',
                    icon: 'error'
                });
                $('#txt_fname').focus();
            }
            else if ($('[name="position"]').val()  === '') {
                //$('#txt_position').addClass('placeholder_err');
                $.toast({
                    heading: 'Error',
                    text: 'Please Enter Your Designation',
                    showHideTransition: 'fade',
                    position: 'mid-center',
                    icon: 'error'
                });
            }
            else if ( $('[name="mobile"]').val() === '') {
                //$('#txt_mobile').addClass('placeholder_err');
                $.toast({
                    heading: 'Error',
                    text: 'Please Enter Your Mobile Number',
                    showHideTransition: 'fade',
                    position: 'mid-center',
                    icon: 'error'
                });
            }
            else if ( validate_fax === '' && selectCountryVali == 1 ) {
                //$('#txt_mobile').addClass('placeholder_err');
                $.toast({
                    heading: 'Error',
                    text: 'Please Enter Your Fax Number',
                    showHideTransition: 'fade',
                    position: 'mid-center',
                    icon: 'error'
                });
            }
            else {

                if(animating) return false;
                animating = true;

                current_fs = $(this).parent();
                next_fs = $(this).parent().next();

                //activate next step on progressbar using the index of next_fs
                $('#progressbar li.active:last').removeClass("current"); /* bathiya added this code*/

                $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active current");

                //show the next fieldset
                next_fs.show();
                //hide the current fieldset with style
                current_fs.animate({opacity: 0}, {
                    step: function(now, mx) {
                        //as the opacity of current_fs reduces to 0 - stored in "now"
                        //1. scale current_fs down to 80%
                        scale = 1 - (1 - now) * 0.2;
                        //2. bring next_fs from the right(50%)
                        left = (now * 50)+"%";
                        //3. increase opacity of next_fs to 1 as it moves in
                        opacity = 1 - now;
                        current_fs.css({'transform': 'scale('+scale+')'});
                        next_fs.css({'left': left, 'opacity': opacity});
                    },
                    duration: 800,
                    complete: function(){
                        current_fs.hide();
                        animating = false;
                    },
                    //this comes from the custom easing plugin
                    easing: 'easeInOutBack'
                });

            }
        });

        // End bathiya added codes for validations

        function countryDone(){

            $('#company_name').css("display","block");

            countryVal = $( "select#country option:checked" ).val();

            var selectCountry = ["LK","VN","SG","NP","BD","LA","IN"];

            if(jQuery.inArray(countryVal, selectCountry) !== -1){
                var selectCountryVali = 1;
            }
            else{
                var selectCountryVali = 0;
            }

            $('#company_name').find('option').remove().end();

            switch (countryVal)
            {
                case "BD":
                    var optionStr = '';
                    var selArr = companyArr[0];
                    var arrayLength = selArr.length;
                    $('#company_name').append('<option value="">Select Company</option>');
                    for (var i = 0; i < arrayLength; i++) {
                        var arrvl = selArr[i];
                        $('#company_name').append('<option value="'+arrvl+'">' + arrvl + '</option>');
                    }
                    break;
                case "LK":
                    var optionStr = '';
                    var selArr = companyArr[1];
                    var arrayLength = selArr.length;
                    $('#company_name').append('<option value="">Select Company</option>');
                    for (var i = 0; i < arrayLength; i++) {
                        var arrvl = selArr[i];
                        $('#company_name').append('<option value="'+arrvl+'">' + arrvl + '</option>');

                    }
                    break;
                case "IN":
                    var optionStr = '';
                    var selArr = companyArr[3];
                    var arrayLength = selArr.length;
                    $('#company_name').append('<option value="">Select Company</option>');
                    for (var i = 0; i < arrayLength; i++) {
                        var arrvl = selArr[i];
                        $('#company_name').append('<option value="'+arrvl+'">' + arrvl + '</option>');

                    }
                    break;
                case "KH":
                    var optionStr = '';
                    var selArr = companyArr[2];
                    var arrayLength = selArr.length;
                    $('#company_name').append('<option value="">Select Company</option>');
                    for (var i = 0; i < arrayLength; i++) {
                        var arrvl = selArr[i];
                        $('#company_name').append('<option value="'+arrvl+'">' + arrvl + '</option>');

                    }
                    break;
                case "MM":
                    var optionStr = '';
                    var selArr = companyArr[6];
                    var arrayLength = selArr.length;
                    $('#company_name').append('<option value="">Select Company</option>');
                    for (var i = 0; i < arrayLength; i++) {
                        var arrvl = selArr[i];
                        $('#company_name').append('<option value="'+arrvl+'">' + arrvl + '</option>');

                    }
                    break;
                case "PK":
                    var optionStr = '';
                    var selArr = companyArr[8];
                    var arrayLength = selArr.length;
                    $('#company_name').append('<option value="">Select Company</option>');
                    for (var i = 0; i < arrayLength; i++) {
                        var arrvl = selArr[i];
                        $('#company_name').append('<option value="'+arrvl+'">' + arrvl + '</option>');

                    }
                    break;
                case "PH":
                    var optionStr = '';
                    var selArr = companyArr[9];
                    var arrayLength = selArr.length;
                    $('#company_name').append('<option value="">Select Company</option>');
                    for (var i = 0; i < arrayLength; i++) {
                        var arrvl = selArr[i];
                        $('#company_name').append('<option value="'+arrvl+'">' + arrvl + '</option>');

                    }
                    break;
                case "SG":
                    var optionStr = '';
                    var selArr = companyArr[10];
                    var arrayLength = selArr.length;
                    $('#company_name').append('<option value="">Select Company</option>');
                    for (var i = 0; i < arrayLength; i++) {
                        var arrvl = selArr[i];
                        $('#company_name').append('<option value="'+arrvl+'">' + arrvl + '</option>');

                    }
                    break;
                case "NP":
                    var optionStr = '';
                    var selArr = companyArr[7];
                    var arrayLength = selArr.length;
                    $('#company_name').append('<option value="">Select Company</option>');
                    for (var i = 0; i < arrayLength; i++) {
                        var arrvl = selArr[i];
                        $('#company_name').append('<option value="'+arrvl+'">' + arrvl + '</option>');

                    }

                    break;

                case "LA":
                    var optionStr = '';
                    var selArr = companyArr[4];
                    var arrayLength = selArr.length;
                    $('#company_name').append('<option value="">Select Company</option>');
                    for (var i = 0; i < arrayLength; i++) {
                        var arrvl = selArr[i];
                        $('#company_name').append('<option value="'+arrvl+'">' + arrvl + '</option>');

                    }
                    break;

                default:
                    var optionStr = '';
                    var selArr = companyArr[11];
                    var arrayLength = selArr.length;
                    $('#company_name').append('<option value="">Select Company</option>');
                    for (var i = 0; i < arrayLength; i++) {
                        var arrvl = selArr[i];
                        $('#company_name').append('<option value="'+arrvl+'">' + arrvl + '</option>');

                    }
                    break;
            }

        }

        function companyDone() {

            //companyName = $("select#company_name option:checked").val();

            companyName = $("select#company_name option:checked").text();
            //var currVal = obj.value;
            //alert(companyName);

            if(companyName !='' && companyName != 'Select Company') {
                //alert("ok");
                $('#countryOK').trigger('click');
                $('.fs-main-title').hide();
            }

            if(countryVal == 'LK'){
                $('#fax_wrap').css("display","block");
                $('#confirm-info-prev .fax_element').css("display","block");
                $('[name="fax"]').val("+94 117 666 910");
                var faxVl = $('[name="fax"]').val();
                $('#selected_fax').text(faxVl);
            }
            else if (countryVal == 'VN ') {
                $('#fax_wrap').css("display","block");
                $('#confirm-info-prev .fax_element').css("display","block");
                $$('[name="fax"]').val("VN 66666");
                var faxVl = $('[name="fax"]').val();
                $('#selected_fax').text(faxVl);
            }
            else if (countryVal == 'SG'){
                $('#fax_wrap').css("display","block");
                $('#confirm-info-prev .fax_element').css("display","block");
                $('[name="fax"]').val("+65 6708 8002");
                var faxVl = $('[name="fax"]').val();
                $('#selected_fax').text(faxVl);
            }
            else if (countryVal == 'NP') {
                $('#fax_wrap').css("display","block");
                $('#confirm-info-prev .fax_element').css("display","block");
                $('[name="fax"]').val("+94 117 666 910");
                var faxVl = $('[name="fax"]').val();
                $('#selected_fax').text(faxVl);
            }
            else if (countryVal == 'BD'){
                $('#fax_wrap').css("display","block");
                $('#confirm-info-prev .fax_element').css("display","block");
                $('[name="fax"]').val("+8802 985 1285");
                var faxVl = $('[name="fax"]').val();
                $('#selected_fax').text(faxVl);
            }
            else if (countryVal == 'LA'){
                $('#fax_wrap').css("display","block");
                $('#confirm-info-prev .fax_element').css("display","block");
                $('[name="fax"]').val("LA 33333");
                var faxVl = $('[name="fax"]').val();
                $('#selected_fax').text(faxVl);
            }
            else if (countryVal == 'IN'){
                $('#fax_wrap').css("display","block");
                $('#confirm-info-prev .fax_element').css("display","block");
                $('[name="fax"]').val("+94 117 666 910");
                var faxVl = $('[name="fax"]').val();
                $('#selected_fax').text(faxVl);
            }
            else{
                $('#fax_wrap').css("display","none");
                $('#confirm-info-prev .fax_element').css("display","none");
            }


        }
        //$('#selected_fax').text($('[name="fax"]').val("hello"));

        function fillConfirm() {
            // var typedVal = obj.value;

            /*alert(companyName);
             alert(countryVal);*/

            $('#selected_country').text($('#country option:selected').text());
            $('#selected_name').text($('[name="fullname"]').val());
            $('#selected_designation').text($('[name="position"]').val());
            $('#selected_division').text($('[name="section"]').val());
            $('#selected_mobile').text($('[name="mobile"]').val());
            $('#selected_fax').text($('[name="fax"]').val());

//alert(companyName);
        }


        //jQuery time
        var current_fs, next_fs, previous_fs; //fieldsets
        var left, opacity, scale; //fieldset properties which we will animate
        var animating; //flag to prevent quick multi-click glitches

        $(".next").click(function(){

            /*$('input,textarea,select').filter('[required]:visible').each(function() {
             if ( $(this).val() === '' )
             return false;
             });*/


            if(animating) return false;
            animating = true;

            current_fs = $(this).parent();
            next_fs = $(this).parent().next();

            //activate next step on progressbar using the index of next_fs
            $('#progressbar li.active:last').removeClass("current"); /* bathiya added this code*/

            $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active current");

            //show the next fieldset
            next_fs.show();
            //hide the current fieldset with style
            current_fs.animate({opacity: 0}, {
                step: function(now, mx) {
                    //as the opacity of current_fs reduces to 0 - stored in "now"
                    //1. scale current_fs down to 80%
                    scale = 1 - (1 - now) * 0.2;
                    //2. bring next_fs from the right(50%)
                    left = (now * 50)+"%";
                    //3. increase opacity of next_fs to 1 as it moves in
                    opacity = 1 - now;
                    current_fs.css({'transform': 'scale('+scale+')'});
                    next_fs.css({'left': left, 'opacity': opacity});
                },
                duration: 800,
                complete: function(){
                    current_fs.hide();
                    animating = false;
                },
                //this comes from the custom easing plugin
                easing: 'easeInOutBack'
            });
        });

        $(".previous").click(function(){
            if(animating) return false;
            animating = true;

            current_fs = $(this).parent();
            previous_fs = $(this).parent().prev();

            $('#progressbar li.active:last').removeClass("current"); /* bathiya added this code*/
            $('#progressbar li.active:last').prev().addClass("current"); /* bathiya added this code*/
            //de-activate current step on progressbar
            $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");



            //show the previous fieldset
            previous_fs.show();
            //hide the current fieldset with style
            current_fs.animate({opacity: 0}, {
                step: function(now, mx) {
                    //as the opacity of current_fs reduces to 0 - stored in "now"
                    //1. scale previous_fs from 80% to 100%
                    scale = 0.8 + (1 - now) * 0.2;
                    //2. take current_fs to the right(50%) - from 0%
                    left = ((1-now) * 50)+"%";
                    //3. increase opacity of previous_fs to 1 as it moves in
                    opacity = 1 - now;
                    current_fs.css({'left': left});
                    previous_fs.css({'transform': 'scale('+scale+')', 'opacity': opacity});
                },
                duration: 800,
                complete: function(){
                    current_fs.hide();
                    animating = false;
                },
                //this comes from the custom easing plugin
                easing: 'easeInOutBack'
            });
        });


        //************************************************************************
        $('#confirmInfobtn').bind('click', function() {


            var country = encodeURIComponent($('#country').val());
            var fullname = encodeURIComponent($('[name="fullname"]').val());
            var position = encodeURIComponent($('[name="position"]').val());
            var section = encodeURIComponent($('[name="section"]').val());
            var mobile = encodeURIComponent($('[name="mobile"]').val());
            var fax = encodeURIComponent($('[name="fax"]').val());
            //var company_name = encodeURIComponent($('#company_name').val());
            var company_name = ($('#company_name').val());

            //alert(company_name);
            //alert(country);

            var currentURL = window.location.href;

            //alert(currentURL);

            var saveData = $.ajax({
                type: 'POST',
                url: "{{ asset('process.php?action=create') }}",
                data: 'country='+country+'&fullname='+fullname+'&position='+position+'&section='+section+'&mobile='+mobile+'&fax='+fax+'&company_name='+company_name,
                dataType: "text",
                success: function(resultData) {
                    //alert(resultData);
                    if(resultData) {
                        const now = new Date();
                       // var finalimgname = currentURL+resultData+'?t='+now.getTime();   //http://signatureapp.yesdigitalspace.com
                        //var finalimgname = 'http://localhost/ezy/new/'+resultData+'?t='+now.getTime();
                        ///alert(resultData);
                        $('#preview img').attr('src',resultData);
                        $('#downloadFinal').attr('href','/download.php?f='+resultData);
                        //$('#downloadFinal').attr('href','http://localhost/ezy/new/download.php?f='+resultData);

                        $('#confirmInfobtn-hidden').trigger('click');

                    }
                }
            });




            //return false;
        });


    </script>
@endsection