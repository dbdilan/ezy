@extends('layouts.dashboard_app')

@section('content')
{{ csrf_field() }}
        <!-- RIBBON -->
<div id="ribbon">

    <span class="ribbon-button-alignment">
        <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
            <i class="fa fa-refresh"></i>
        </span>
    </span>

    <!-- breadcrumb -->
    <ol class="breadcrumb">
        <li>Home</li><li>User</li><li>Profile</li>
    </ol>


</div>
<!-- END RIBBON -->

<!-- MAIN CONTENT -->
<div id="content">

    <!-- Bread crumb is created dynamically -->
    <!-- row -->
    <div class="row">

        <!-- col -->
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
            <h1 class="page-title txt-color-blueDark"><!-- PAGE HEADER --><i class="fa-fw fa fa-puzzle-piece"></i> User <span>>
                    Profile </span></h1>
        </div>
        <!-- end col -->

        <!-- right side of the page with the sparkline graphs -->
        <!-- col -->
        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
            <!-- sparks -->

            <!-- end sparks -->
        </div>
        <!-- end col -->

    </div>
    <!-- end row -->

    <!-- row -->

    <div class="row">

        <div class="col-sm-12">


            <div class="well well-sm">

                <div class="row">

                    <div class="col-sm-12 col-md-12 col-lg-6">
                        <div class="well well-light well-sm no-margin no-padding">

                            <div class="row">

                                <div class="col-sm-12">
                                    <div id="myCarousel" class="carousel fade profile-carousel">
                                        @if($user->id != Auth::user()->id)
                                            <div class="air air-bottom-right padding-10">
                                                <button data-user_id="{{ $user->id }}" data-url="{{ url("/user/add-follows") }}"  id="follow-btn" class="btn txt-color-white bg-color-teal btn-sm follow-btn"><i class="fa fa-check"></i> @if($user_following > 0) Following @else Follow @endif</button>&nbsp;
                                                <button data-user_id="{{ $user->id }}" data-url="{{ url("/user/add-connections") }}"  id="connection-btn" class="btn txt-color-white bg-color-pinkDark btn-sm connection-btn"><i class="fa fa-link"></i> @if($user_connecting > 0) Connecting @else Connect @endif</button>
                                            </div>
                                        @else
                                            <div class="air air-bottom-right padding-10">
                                                <div class="widget-toolbar">
                                                    <!-- add: non-hidden - to disable auto hide -->

                                                    <div class="btn-group">
                                                        <button class="btn dropdown-toggle btn-xs btn-success" data-toggle="dropdown">
                                                            <i class="fa fa-ellipsis-h"></i>
                                                        </button>
                                                        <ul class="dropdown-menu pull-right js-status-update user-profile-more" style="min-width: 180px;">
                                                            <li>
                                                                <button data-toggle="modal" data-target="#profile-photo-model" class="change-profile-photo"><i class="fa fa-user"></i> Change Profile photo</button>
                                                            </li>
                                                            <li>
                                                                <button data-toggle="modal" data-target="#cover-photo-model" class="change-cover-photo"><i class="fa fa-image"></i> Change Cover photo</button>
                                                            </li>
                                                            <li>
                                                                <button data-toggle="modal" data-target="#change-password-model"><i class="fa fa-expeditedssl"></i> Change Password</button>
                                                            </li>
                                                            <li>
                                                                <button data-toggle="modal" data-target="#change-other-model"><i class="fa fa-eye"></i> Change Profile</button>
                                                            </li>
                                                            <li class="divider"></li>
                                                            <li>
                                                                <button><i class="fa fa-power-off"></i> Log Off</button>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                        <div class="air air-top-left padding-10">
                                            <h4 class="txt-color-white font-md">Scene {{ date("d-M-Y", strtotime($user->created_at)) }}</h4>
                                        </div>

                                            <div class="carousel-inner">
                                                <!-- Slide 1 -->
                                                <div class="item active">
                                                    <img src="{{ asset($user->cover_photo) }}" alt="{{ $user->first_name }}">
                                                </div>
                                            </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">

                                    <div class="row">

                                        <div class="col-sm-3 profile-pic">
                                            <img src="{{ asset($user->profile_image) }}" alt="{{ $user->first_name }}">
                                            <div class="padding-10">
                                                <h4 class="font-md"><strong>{{ count($follows) }}</strong>
                                                    <br>
                                                    <small>Followers</small></h4>
                                                <br>
                                                <h4 class="font-md"><strong>{{ count($connections) }}</strong>
                                                    <br>
                                                    <small>Connections</small></h4>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <h1>{{ $user->first_name }} <span class="semi-bold">{{ $user->last_name }}</span>
                                                <br>
                                                <small> {{ $user->profession }}, {{ $user->office_name }}</small></h1>

                                            <ul class="list-unstyled">
                                                <li>
                                                    <p class="text-muted">
                                                        <i class="fa fa-phone"></i>&nbsp;&nbsp; <span class="txt-color-darken">{{ $user->office_phone }} - (<i class="fa fa-mobile"></i>) {{ $user->mobile_phone }}</span>
                                                    </p>
                                                </li>
                                                <li>
                                                    <p class="text-muted">
                                                        <i class="fa fa-envelope"></i>&nbsp;&nbsp;<a href="mailto:{{ $user->email }}">{{ $user->email }}</a>
                                                    </p>
                                                </li>
                                                @if($user->facebook_link != '' || $user->facebook_link != null)
                                                    <li>
                                                        <p class="text-muted">
                                                            <i class="fa fa-facebook"></i>&nbsp;&nbsp;<span class="txt-color-darken">{{ $user->facebook_link }}</span>
                                                        </p>
                                                    </li>
                                                @endif
                                                @if($user->instagram_link != '' || $user->instagram_link != null)
                                                    <li>
                                                        <p class="text-muted">
                                                            <i class="fa fa-instagram"></i>&nbsp;&nbsp;<span class="txt-color-darken">{{ $user->instagram_link }}</span>
                                                        </p>
                                                    </li>
                                                @endif
                                                @if($user->google_link != '' || $user->google_link != null)
                                                    <li>
                                                        <p class="text-muted">
                                                            <i class="fa fa-google-plus"></i>&nbsp;&nbsp;<span class="txt-color-darken">{{ $user->google_link }}</span>
                                                        </p>
                                                    </li>
                                                @endif
                                                @if($user->twitter_link != '' || $user->twitter_link != null)
                                                    <li>
                                                        <p class="text-muted">
                                                            <i class="fa fa-twitter"></i>&nbsp;&nbsp;<span class="txt-color-darken">{{ $user->twitter_link }}</span>
                                                        </p>
                                                    </li>
                                                @endif

                                                <li>
                                                    <p class="text-muted">
                                                        <i class="fa fa-calendar"></i>&nbsp;&nbsp;<span class="txt-color-darken">active <a href="javascript:void(0);" rel="tooltip" title="" data-placement="top" data-original-title="Create an Appointment">{{ $activate_ago }}</a></span>
                                                    </p>
                                                </li>
                                            </ul>
                                            <br>
                                            <p class="font-md">
                                                <i>A little about me...</i>
                                            </p>
                                            <p>{{ $user->description }}</p>
                                            <br>
                                            <a href="javascript:void(0);" class="btn btn-default btn-xs"><i class="fa fa-envelope-o"></i> Send Message</a>
                                            <br>
                                            <br>

                                        </div>
                                        <div class="col-sm-3">
                                            <h1><small>Connections</small></h1>
                                            @if(count($connections) > 0)
                                                <ul class="list-inline friends-list">
                                                    @foreach($connections as $key=>$connection)
                                                        @if($key <= 5)
                                                            <li><a href="{{ url('users/'.$connection->id) }}"><img src="{{ asset($connection->profile_image) }}" alt="{{ $connection->first_name }}"></a></li>
                                                        @endif
                                                    @endforeach
                                                    @if(count($connections) > 5)
                                                            <li><a href="javascript:void(0);">@php (count($connection) - 6 ) @endphp more</a></li>
                                                        @endif
                                                </ul>
                                            @else
                                                <ul class="list-inline friends-list">
                                                    <li><a href="javascript:void(0);">No Connections</a></li>
                                                </ul>
                                            @endif
                                            {{--<h1><small>Recent visitors</small></h1>--}}
                                            {{--<ul class="list-inline friends-list">--}}
                                            {{--<li><img src="{{ asset('asset/img/avatars/male.png') }}" alt="friend-1">--}}
                                            {{--</li>--}}
                                            {{--<li><img src="{{ asset('asset/img/avatars/female.png') }}" alt="friend-2">--}}
                                            {{--</li>--}}
                                            {{--<li><img src="{{ asset('asset/img/avatars/female.png') }}" alt="friend-3">--}}
                                            {{--</li>--}}
                                            {{--</ul>--}}

                                        </div>

                                    </div>

                                </div>
                            </div>
                            <div class="row">

                                <div class="col-sm-12">

                                    <hr>

                                    <div class="padding-10">

                                        <ul class="nav nav-tabs tabs-pull-right">
                                            <li class="active">
                                                <a href="#a1" data-toggle="tab">Recent Articles</a>
                                            </li>
                                            <li>
                                                <a href="#a2" data-toggle="tab">New Members</a>
                                            </li>
                                            <li class="pull-left">
                                                <span class="margin-top-10 display-inline"><i class="fa fa-rss text-success"></i> Activity</span>
                                            </li>
                                        </ul>

                                        <div class="tab-content padding-top-10">
                                            <div class="tab-pane fade in active" id="a1">

                                                <div class="row">

                                                    <div class="col-xs-2 col-sm-1">
                                                        <time datetime="2014-09-20" class="icon">
                                                            <strong>Jan</strong>
                                                            <span>10</span>
                                                        </time>
                                                    </div>

                                                    <div class="col-xs-10 col-sm-11">
                                                        <h6 class="no-margin"><a href="javascript:void(0);">Allice in Wonderland</a></h6>
                                                        <p>
                                                            Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi Nam eget dui.
                                                            Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero,
                                                            sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel.
                                                        </p>
                                                    </div>

                                                    <div class="col-sm-12">

                                                        <hr>

                                                    </div>

                                                    <div class="col-xs-2 col-sm-1">
                                                        <time datetime="2014-09-20" class="icon">
                                                            <strong>Jan</strong>
                                                            <span>10</span>
                                                        </time>
                                                    </div>

                                                    <div class="col-xs-10 col-sm-11">
                                                        <h6 class="no-margin"><a href="javascript:void(0);">World Report</a></h6>
                                                        <p>
                                                            Morning our be dry. Life also third land after first beginning to evening cattle created let subdue you'll winged don't Face firmament.
                                                            You winged you're was Fruit divided signs lights i living cattle yielding over light life life sea, so deep.
                                                            Abundantly given years bring were after. Greater you're meat beast creeping behold he unto She'd doesn't. Replenish brought kind gathering Meat.
                                                        </p>
                                                    </div>

                                                    <div class="col-sm-12">

                                                        <br>

                                                    </div>

                                                </div>

                                            </div>
                                            <div class="tab-pane fade" id="a2">

                                                <div class="alert alert-info fade in">
                                                    <button class="close" data-dismiss="alert">
                                                        ×
                                                    </button>
                                                    <i class="fa-fw fa fa-info"></i>
                                                    <strong>51 new members </strong>joined today!
                                                </div>

                                                <div class="user" title="email@company.com">
                                                    <img src="{{ asset('asset/img/avatars/female.png') }}" alt="demo user"><a href="javascript:void(0);">Jenn Wilson</a>
                                                    <div class="email">
                                                        travis@company.com
                                                    </div>
                                                </div>
                                                <div class="user" title="email@company.com">
                                                    <img src="{{ asset('asset/img/avatars/male.png') }}" alt="demo user"><a href="javascript:void(0);">Marshall Hitt</a>
                                                    <div class="email">
                                                        marshall@company.com
                                                    </div>
                                                </div>
                                                <div class="user" title="email@company.com">
                                                    <img src="{{ asset('asset/img/avatars/male.png') }}" alt="demo user"><a href="javascript:void(0);">Joe Cadena</a>
                                                    <div class="email">
                                                        joe@company.com
                                                    </div>
                                                </div>
                                                <div class="user" title="email@company.com">
                                                    <img src="{{ asset('asset/img/avatars/male.png') }}" alt="demo user"><a href="javascript:void(0);">Mike McBride</a>
                                                    <div class="email">
                                                        mike@company.com
                                                    </div>
                                                </div>
                                                <div class="user" title="email@company.com">
                                                    <img src="{{ asset('asset/img/avatars/male.png') }}" alt="demo user"><a href="javascript:void(0);">Travis Wilson</a>
                                                    <div class="email">
                                                        travis@company.com
                                                    </div>
                                                </div>
                                                <div class="user" title="email@company.com">
                                                    <img src="{{ asset('asset/img/avatars/male.png') }}" alt="demo user"><a href="javascript:void(0);">Marshall Hitt</a>
                                                    <div class="email">
                                                        marshall@company.com
                                                    </div>
                                                </div>
                                                <div class="user" title="Joe Cadena joe@company.com">
                                                    <img src="{{ asset('asset/img/avatars/male.png') }}" alt="demo user"><a href="javascript:void(0);">Joe Cadena</a>
                                                    <div class="email">
                                                        joe@company.com
                                                    </div>
                                                </div>
                                                <div class="user" title="email@company.com">
                                                    <img src="{{ asset('asset/img/avatars/male.png') }}" alt="demo user"><a href="javascript:void(0);">Mike McBride</a>
                                                    <div class="email">
                                                        mike@company.com
                                                    </div>
                                                </div>
                                                <div class="user" title="email@company.com">
                                                    <img src="{{ asset('asset/img/avatars/male.png') }}" alt="demo user"><a href="javascript:void(0);">Marshall Hitt</a>
                                                    <div class="email">
                                                        marshall@company.com
                                                    </div>
                                                </div>
                                                <div class="user" title="email@company.com">
                                                    <img src="{{ asset('asset/img/avatars/male.png') }}" alt="demo user"><a href="javascript:void(0);">Joe Cadena</a>
                                                    <div class="email">
                                                        joe@company.com
                                                    </div>
                                                </div>
                                                <div class="user" title="email@company.com">
                                                    <img src="{{ asset('asset/img/avatars/male.png') }}" alt="demo user"><a href="javascript:void(0);"> Mike McBride</a>
                                                    <div class="email">
                                                        mike@company.com
                                                    </div>
                                                </div>

                                                <div class="text-center">
                                                    <ul class="pagination pagination-sm">
                                                        <li class="disabled">
                                                            <a href="javascript:void(0);">Prev</a>
                                                        </li>
                                                        <li class="active">
                                                            <a href="javascript:void(0);">1</a>
                                                        </li>
                                                        <li>
                                                            <a href="javascript:void(0);">2</a>
                                                        </li>
                                                        <li>
                                                            <a href="javascript:void(0);">3</a>
                                                        </li>
                                                        <li>
                                                            <a href="javascript:void(0);">...</a>
                                                        </li>
                                                        <li>
                                                            <a href="javascript:void(0);">99</a>
                                                        </li>
                                                        <li>
                                                            <a href="javascript:void(0);">Next</a>
                                                        </li>
                                                    </ul>
                                                </div>

                                            </div><!-- end tab -->
                                        </div>

                                    </div>

                                </div>

                            </div>
                            <!-- end row -->
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-6">

                        <form id="post-comment-form-{{ $user->id }}" method="post" class="well padding-bottom-10" onsubmit="return false;">
                            {{ csrf_field() }}
                            {{--<img src="{{ Auth::user()->profile_image }}" style="width: 50px; height: 50px;;"  alt="img" class="online">--}}
                            <textarea rows="2" class="form-control"  name="description" id="description-one-{{ $user->id }}"  placeholder="What are you thinking?"></textarea>
                            <div class="margin-top-10">
                                <button data-target-description-id="description-one-{{ $user->id }}" type="button" data-unique_id="one-{{ $user->id }}" data-form_id="post-comment-form-{{ $user->id }}" data-url="{{ url('user-profile/comments') }}" data-remove_url="{{ url('/user-profile/comment/remove') }}" class="btn btn-info btn-sm post-comment-btn" >
                                    post
                                </button>
                            </div>
                            <input id="comment-picture-one-{{ $user->id }}"  class="form-control" type="hidden" name="comment_picture">
                            <input id="comment-video-one-{{ $user->id }}" class="form-control" type="hidden" name="comment_video">
                            <input id="comment-level-one-{{ $user->id }}" value="1" name="level_id" type="hidden">
                            <input id="post-id-one-{{ $user->id }}" value="{{ $user->id }}" name="post_id" type="hidden">
                            <input type="hidden" id="parent-id-one-{{ $user->id }}" name="parent_id" value="0">
                        </form>

                        @foreach($post_comment as $comment)
                            <div class="" id="li-comment-id-{{ $comment->user_profile_comment_id }}">
                            <div class="timeline-seperator text-center"> <span>10:30PM January 1st, 2013</span>
                                <div class="btn-group pull-right">
                                    <a href="javascript:void(0);" data-toggle="dropdown" class="btn btn-default btn-xs dropdown-toggle"><span class="caret single"></span></a>
                                    <ul class="dropdown-menu text-left">
                                        <li>
                                            <a href="javascript:void(0);">Hide this post</a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0);">Hide future posts from this user</a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0);">Mark as spam</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <div class="chat-body no-padding profile-message comments">
                                <ul id="comments-one-{{ $user->id }}">
                                    <li class="message" >
                                        <img width="50px" src="{{ $comment->profile_image }}" class="online" alt="sunny">
                                        <span class="message-text">
                                            <a href="javascript:void(0);" class="username">{{ $comment->first_name.' '.$comment->last_name }}
                                                <small class="text-muted pull-right ultra-light"> 2 Minutes ago </small>
                                            </a>
                                            @if($comment->description != '')
                                                {{ $comment->description }}
                                            @endif
                                        </span>
                                        <ul class="list-inline font-xs">
                                            <li>
                                                <a href="javascript:void(0);" onclick="showCommentBox('{{ $comment->user_profile_comment_id }}')" data-user_profile_comment_id="{{ $comment->user_profile_comment_id }}">
                                                    <i class="fa fa-reply"></i>
                                                    @if(isset($level_2_post_comment[$user->id][$comment->user_profile_comment_id]))
                                                        @if($level_2_post_comment[$user->id][$comment->user_profile_comment_id])
                                                            {{ count($level_2_post_comment[$user->id][$comment->user_profile_comment_id]) }}
                                                        @endif
                                                    @endif
                                                    Reply
                                                </a>
                                            </li>
                                            <li>
                                                <a href="javascript:void(0);" class="text-danger"><i class="fa fa-thumbs-up"></i> Like</a>
                                            </li>
                                            <li>
                                                <a href="javascript:void(0);" class="text-muted">Show All Comments (14)</a>
                                            </li>
                                            <li>
                                                <a href="javascript:void(0);" class="text-primary">Edit</a>
                                            </li>
                                            <li>
                                                <a href="javascript:void(0);" class="text-danger post-comment-remove-btn" data-post_id="{{ $user->id }}" data-url="{{ url('user-profile/comment/remove') }}" data-post_comment_id="{{ $comment->user_profile_comment_id }}" >Delete</a>
                                            </li>
                                        </ul>
                                    </li>

                                    @if(isset($level_2_post_comment[$comment->user_profile_comment_id]))
                                        @if($level_2_post_comment[$comment->user_profile_comment_id])
                                            @foreach($level_2_post_comment[$comment->user_profile_comment_id] as $comment_level_2)
                                                <li class="message message-reply"  id="li-comment-id-{{ $comment_level_2->user_profile_comment_id }}">
                                                    <img src="{{ $comment_level_2->profile_image }}" class="online" alt="user">
                                                    <span class="message-text"> <a href="javascript:void(0);" class="username">{{ $comment_level_2->first_name.' '.$comment_level_2->last_name }}</a>
                                                        @if($comment_level_2->description != '')
                                                            {{ $comment_level_2->description }}
                                                        @endif
                                                    </span>

                                                    <ul class="list-inline font-xs">
                                                        <li>
                                                            <a href="javascript:void(0);" class="text-muted">1 minute ago </a>
                                                        </li>
                                                        <li>
                                                            <a href="javascript:void(0);" class="text-danger"><i class="fa fa-thumbs-up"></i> Like</a>
                                                        </li>
                                                        @if(Auth::user()->id == $comment_level_2->user_id)
                                                        <li>
                                                            <a href="javascript:void(0);" class="text-danger post-comment-remove-btn" data-post_id="{{ $user->id }}" data-url="{{ url('user-profile/comment/remove') }}" data-post_comment_id="{{ $comment_level_2->user_profile_comment_id }}">Delete</a>
                                                        </li>
                                                        @endif
                                                    </ul>

                                                </li>
                                            @endforeach
                                        @endif
                                    @endif
                                    <li>
                                        <div class="input-group wall-comment-reply">
                                            <form id="post-comment-form-two-{{ $comment->user_profile_comment_id }}" method="post">
                                                {{ csrf_field() }}
                                                <input type="text" name="description" id="description-two-{{ $comment->user_profile_comment_id }}" class="form-control" placeholder="Post your comment...">
                                            <span class="input-group-btn">
                                                <button type="button" data-unique_id="two-{{ $comment->user_profile_comment_id }}" data-form_id="post-comment-form-two-{{ $comment->user_profile_comment_id }}" data-url="{{ url('user-profile/comments') }}" data-remove_url="{{ url('/user-profile/comment/remove') }}" class="btn btn-primary post-comment-btn" id="btn-chat">
                                                    <i class="fa fa-reply"></i> Reply
                                                </button> </span>

                                                <input id="comment-picture-two-{{ $comment->user_profile_comment_id }}" class="form-control" type="hidden" name="comment_picture">
                                                <input id="comment-video-two-{{ $comment->user_profile_comment_id }}" class="form-control" type="hidden" name="comment_video">
                                                <input id="comment-level-two-{{ $comment->user_profile_comment_id }}" value="2" name="level_id" type="hidden">
                                                <input id="post-id-two-{{ $comment->user_profile_comment_id }}" value="{{ $user->id }}" name="post_id" type="hidden">
                                                <input type="hidden" id="parent-id-two-{{ $comment->user_profile_comment_id }}" name="parent_id" value="{{ $comment->user_profile_comment_id }}">

                                            </form>
                                        </div>
                                    </li>

                                </ul>

                            </div>
                            </div>
                        @endforeach

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end row -->
</div>
<!-- END MAIN CONTENT -->

<!-- MODEL PROFILE PHOTO MODEL -->
<div class="modal fade" id="profile-photo-model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="myModalLabel">Upload profile photo</h4>
            </div>
            <form id="profile-photo-form" method="post" action="{{ url('user/upload-profile-photo') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="input-group">
                                        <span class="input-group-btn">
                                            <a data-input="profile-image"  data-preview="holder" class="btn btn-primary file-manager">
                                                <i class="fa fa-picture-o"></i> Choose
                                            </a>
                                        </span>
                                <input id="profile-image" class="form-control" type="text" name="profile_image">
                            </div>
                            <img id="holder" style="margin-top:15px;max-height:100px;">
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        Cancel
                    </button>
                    <button type="submit" data-url="{{ url("user/upload-profile-photo") }}" id="profile-photo-btn" class="btn btn-primary">
                        Save change
                    </button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- MODEL COVER PHOTO MODEL -->
<div class="modal fade" id="cover-photo-model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="myModalLabel">Upload profile photo</h4>
            </div>
            <form id="cover-photo-form" method="post" action="{{ url('user/upload-cover-photo') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="input-group">
                                        <span class="input-group-btn">
                                            <a data-input="cover-image"  data-preview="cover-holder" class="btn btn-primary file-manager">
                                                <i class="fa fa-picture-o"></i> Choose
                                            </a>
                                        </span>
                                <input id="cover-image" class="form-control" type="text" name="cover_image">
                            </div>
                            <img id="cover-holder" style="margin-top:15px;max-height:100px;">
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        Cancel
                    </button>
                    <button type="submit" data-url="{{ url("user/upload-cover-photo") }}" id="cover-photo-btn" class="btn btn-primary">
                        Save change
                    </button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- MODEL CHANGE PASSWORD MODEL -->
<div class="modal fade" id="change-password-model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="myModalLabel">Upload profile photo</h4>
            </div>
            <form id="change-password-form" method="post" action="{{ url('user/change-password') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="modal-body">

                    <div class="row">
                        <div class="col-md-12">
                            <label>New Password</label>
                            <div class="form-group">
                                <input type="password" class="form-control" name="password" placeholder="Password" id="password" required />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label>Confirm Password</label>
                            <div class="form-group">
                                <input type="password" class="form-control" id="password-confirm" name="password_confirmation" placeholder="Confirm password" required />
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        Cancel
                    </button>
                    <button type="submit" data-url="{{ url("user/change-password") }}" id="change-password-btn" class="btn btn-primary">
                        Save change
                    </button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- MODEL CHANGE OTHER MODEL -->
<div class="modal fade" id="change-other-model" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    &times;
                </button>
                <h4 class="modal-title" id="myModalLabel">Upload profile photo</h4>
            </div>
            <form id="change-other-form" method="post" action="{{ url('user/update-user-profile') }}" class="smart-form client-form"  enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="modal-body">

                    <fieldset>
                        <div class="row">
                            <section class="col col-6">
                                <label class="input">
                                    First Name
                                    <input type="text" name="first_name" value="{{ $user->first_name }}" placeholder="First name" required>
                                    @if ($errors->has('first_name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('first_name') }}</strong>
                                        </span>
                                    @endif
                                </label>
                            </section>
                            <section class="col col-6">
                                <label class="input">
                                    Last Name
                                    <input type="text" name="last_name" value="{{ $user->last_name }}" placeholder="Last name" required>
                                    @if ($errors->has('last_name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('last_name') }}</strong>
                                        </span>
                                    @endif
                                </label>
                            </section>
                        </div>

                        <div class="row">
                            <section class="col col-6">
                                <label class="select">
                                    Gender
                                    <select name="gender">
                                        <option value="0" selected="" disabled="">Gender</option>
                                        <option @if($user->gender == 'Male') selected @endif  value="Male">Male</option>
                                        <option @if($user->gender == 'Female') selected @endif value="Female">Female</option>
                                        <option @if($user->gender == 'Prefer not to answer') selected @endif value="Prefer not to answer">Prefer not to answer</option>
                                    </select> <i></i>
                                    @if ($errors->has('gender'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('gender') }}</strong>
                                        </span>
                                    @endif
                                </label>
                            </section>
                            <section class="col col-6">
                                <label class="input">
                                    Birthday<br/>
                                    <i class="icon-append fa fa-calendar" style="padding-top: 21px;"></i>
                                    <input type="text" name="birthday" placeholder="Date of Birth" value="{{ $user->birthday }}" class="datepicker" id="birthday" data-dateformat='dd/mm/yy' required>
                                    @if ($errors->has('birthday'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('birthday') }}</strong>
                                        </span>
                                    @endif
                                </label>
                            </section>
                        </div>

                        <div class="row">
                            <section class="col col-6">
                                <label class="input">
                                    Office Phone
                                    <input type="text" name="office_phone" value="{{ $user->office_phone }}" placeholder="Office Phone" required>
                                    @if ($errors->has('office_phone'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('office_phone') }}</strong>
                                        </span>
                                    @endif
                                </label>
                            </section>
                            <section class="col col-6">
                                <label class="input">
                                    Mobile Phone
                                    <input type="text" name="mobile_phone" value="{{ $user->mobile_phone }}" placeholder="Mobile Phone" required>
                                    @if ($errors->has('mobile_phone'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('mobile_phone') }}</strong>
                                        </span>
                                    @endif
                                </label>
                            </section>
                        </div>

                        <div class="row">
                            <section class="col col-6">
                                <label class="input">
                                    EMP Number
                                    <input type="text" name="emp_number" value="{{ $user->emp_number }}" placeholder="EMP Number" required>
                                    @if ($errors->has('emp_number'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('emp_number') }}</strong>
                                        </span>
                                    @endif
                                </label>
                            </section>
                            <section class="col col-6">
                                <label class="input">
                                    Profession
                                    <input type="text" name="profession" value="{{ $user->profession }}" placeholder="Profession" required>
                                    @if ($errors->has('profession'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('profession') }}</strong>
                                        </span>
                                    @endif
                                </label>
                            </section>
                        </div>

                        <div class="row">
                            <section class="col col-6">
                                <label class="select">
                                    Office
                                    <select name="office_branch_id" required>
                                        <option value="0" selected="" disabled="">Office</option>
                                        @foreach($offices as $office)
                                            <option @if($user->office_branch_id == $office->office_id) selected @endif value="{{ $office->office_id }}">{{ $office->name }}</option>
                                        @endforeach

                                    </select> <i></i>
                                    @if ($errors->has('office_branch_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('office_branch_id') }}</strong>
                                        </span>
                                    @endif
                                </label>
                            </section>
                            <section class="col col-6">
                                <label class="select">
                                    Country
                                    <select name="country_id" required>
                                        <option value="0" selected="" disabled="">Country</option>
                                        @foreach($countries as $country)
                                            <option @if($user->country_id == $country->country_id) selected @endif value="{{ $country->country_id }}">{{ $country->name }}</option>
                                        @endforeach
                                    </select> <i></i>
                                    @if ($errors->has('country_id'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('country_id') }}</strong>
                                        </span>
                                    @endif
                                </label>
                            </section>
                        </div>

                        <div class="row">
                            <section class="col col-6">
                                <label class="input">
                                    Facebook
                                    <input type="text" name="facebook_link" value="{{ $user->facebook_link }}" placeholder="Facebook Link">
                                </label>
                            </section>
                            <section class="col col-6">
                                <label class="input">
                                    Instagram
                                    <input type="text" name="instagram_link" value="{{ $user->instagram_link }}" placeholder="Instagram Link">
                                </label>
                            </section>
                        </div>

                        <div class="row">
                            <section class="col col-6">
                                <label class="input">
                                    Google
                                    <input type="text" name="google_link" value="{{ $user->google_link }}" placeholder="Google Link">
                                </label>
                            </section>
                            <section class="col col-6">
                                <label class="input">
                                    Twitter
                                    <input type="text" name="twitter_link" value="{{ $user->twitter_link }}" placeholder="Twitter Link">
                                </label>
                            </section>
                        </div>

                        <div class="row">
                            <section class="col col-6">
                                <label class="input">
                                    Your Address
                                    <textarea style="padding-left: 7px;" class="form-control" placeholder="Your Address" name="address" required>{{ $user->description }}</textarea>
                                    @if ($errors->has('address'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('address') }}</strong>
                                        </span>
                                    @endif
                                </label>
                            </section>
                            <section class="col col-6">
                                <label class="input">
                                    About me
                                    <textarea style="padding-left: 7px;" class="form-control" placeholder="About you" name="description" required>{{ $user->description }}</textarea>
                                    @if ($errors->has('description'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('description') }}</strong>
                                        </span>
                                    @endif
                                </label>
                            </section>

                        </div>

                    </fieldset>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">
                        Cancel
                    </button>
                    <button type="submit"  data-url="{{ url("user/change-password") }}" id="change-password-btn" class="btn btn-primary form-control">
                        Save change
                    </button>
                </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

@endsection

@section('page-js')
    <script>
        $("#change-password-form").validate({
            // Rules for form validation
            rules : {
                password : {
                    required : true,
                    minlength : 3,
                    maxlength : 20
                },
                password_confirmation : {
                    required : true,
                    minlength : 3,
                    maxlength : 20,
                    equalTo : '#password'
                }
            },

            // Messages for form validation
            messages : {
                password : {
                    required : 'Please enter your password'
                }
            },

            // Do not change code below
            errorPlacement : function(error, element) {
                error.insertAfter(element.parent());
            }
        });
    </script>
@endsection