@extends('layouts.dashboard_app')

@section('title', '| Create New News')

@section('content')


        <!-- RIBBON -->
<div id="ribbon">

    <span class="ribbon-button-alignment">
        <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
            <i class="fa fa-refresh"></i>
        </span>
    </span>

    <!-- breadcrumb -->
    <ol class="breadcrumb">
        <li>Home</li>
        <li>User</li>
        <li>Favorite</li>
    </ol>
    <!-- end breadcrumb -->

</div>
<!-- END RIBBON -->

<!-- MAIN CONTENT -->
<div id="content">

    <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
            <h1 class="page-title txt-color-blueDark">
                <i class="fa fa-users fa-fw "></i>
                User <span>> Favorite </span>
            </h1>
        </div>
        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
            <button type="button" class="btn btn-primary pull-right" onclick="window.history.back()">< Back</button>
        </div>
    </div>

    <!-- widget grid -->
    <section id="widget-grid" class="">

        <!-- row -->
        <div class="row">

            <article class="col-sm-12 col-md-12 col-lg-6">

                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget" id="wid-id-1" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                        <h2>Edit a favorite </h2>

                    </header>

                    <!-- widget div-->
                    <div>

                        <!-- widget edit box -->
                        <div class="jarviswidget-editbox">
                            <!-- This area used as dropdown edit box -->

                        </div>
                        <!-- end widget edit box -->

                        <!-- widget content -->
                        <div class="widget-body">

                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <form action="{{ route('favorite-create-post') }}" method="post" id="favorite-form" class="form" >
                                {{ csrf_field() }}

                                <!-- Title -->
                                <div class="form-group">
                                    <label>Web site name</label>
                                    <input type="text" name="site" class="form-control" required>
                                    @if ($errors->has('site'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('site') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <!-- // Title -->


                                <!-- Title -->
                                <div class="form-group">
                                    <label>Web Site Link</label>
                                    <input type="text" name="site_link" class="form-control" required>
                                    @if ($errors->has('site_link'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('site_link') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <!-- // Title -->



                                <!-- Description -->
                                {{--<div class="form-group">--}}
                                    {{--<label>Description</label>--}}
                                    {{--<textarea name="description" class="form-control my-editor"></textarea>--}}
                                   {{----}}
                                {{--</div>--}}
                                <!-- Description -->


                                <div class="form-group">
                                    {{ Form::submit('Create favorite', array('class' => 'btn btn-success btn-lg btn-block')) }}
                                </div>

                            </form>
                        </div>
                        <!-- end widget content -->

                    </div>
                    <!-- end widget div -->

                </div>
                <!-- end widget -->

            </article>
            <!-- END COL -->
        </div>

    </section>
    <!-- end widget grid -->

</div>
<!-- END MAIN CONTENT -->

@endsection

@section('page-js')

<script>


    $("#favorite-form").validate();

    // DO NOT REMOVE : GLOBAL FUNCTIONS!
    $(document).ready(function() {
        pageSetUp();
    });

    function choose(item_name) {
        performLfmRequest('jsonitems', {working_dir: $('#working_dir').val() + '/' + item_name, type: 'Images'}, 'html')
                .done(function (data) {
                    var response = JSON.parse(data);
                    var result_html = $(response.html);
                    var urls = [];
                    result_html.find('[data-url]').each(function(imageDiv){
                        urls.push($(this).attr('data-url'));
                    });
                    if (window.opener) {
                        window.opener.SetUrlsArray(urls);
                        window.close();
                    }
                });
    }

</script>
@endsection