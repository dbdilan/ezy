@extends('layouts.new_dashboard_app')

@section('content')

        <!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">
    <!--Page content-->
    <!--===================================================-->
    <div style="padding-bottom: 30px;" id="page-content">
        <div class="">
            <div class="row">
                <!--Network Line Chart-->
                <!--===================================================-->
                <div id="demo-panel-network">
                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                        <!-- Wrapper for slides -->
                        <div style="overflow:visible;" class="carousel-inner">

                            @foreach($slider_images as $key=>$slider)
                                @php
                                $body = preg_replace("/<img[^>]+>/", "", $slider['description']);
                                $body = preg_replace("/<video [^>]+>/", "", $body);
                                    $body = preg_replace("/<iframe [^>]+>/", "", $body);
                                        @endphp
                                        <div class="item @if($key == 0) active @endif">
                                            <div id="des" class="col-sm-12 col-md-4 hidden-xs hidden-sm holder">
                                                <div  class="carousel-caption">
                                                    <h3 id="caro1">{{ $slider['title'] }}</h3>
                                                    <h4 id="caro2" style="font-weight: 100; margin-top: 10%; margin-bottom:10%;">{!! substr(htmlspecialchars_decode($body), 0, 350) !!}</h4>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4 col-sm-3">
                                                        <div id="imgwrpr1" style="border-radius:100px; overflow: hidden;" class="wrapper">
                                                            <img style="width:250%;" src="{{ $slider['user_profile_image'] }}" alt="Profile Picture">
                                                        </div>
                                                    </div>
                                                    <div id="ava" class="col-md-8">
                                                        <h4 id="caro2">By {{ $slider['user_name'] }}</h4>
                                                        <h4 id="caro2" style="font-weight: 100;">{{ $slider['created_at'] }}</h4>
                                                        @if(isset($slider['like_count']))
                                                        <i class="demo-pli-heart-2" aria-hidden="true"> {{ $slider['like_count'] }}</i>
                                                        @endif
                                                        @if(isset($slider['comment_count']))
                                                        <i class="demo-pli-mail" aria-hidden="true"> {{ $slider['comment_count'] }}</i>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="dessm" class="col-sm-12 col-xs-12 col-md-4 hidden-lg hidden-md ">
                                                <div  class="carousel-caption">
                                                    <h4>{{ $slider['title'] }}</h4>
                                                    <h5 style="font-weight: 100; margin-top: 10%; margin-bottom:10%;">{!! htmlspecialchars_decode($body) !!}</h5>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4 col-sm-3 col-xs-3">
                                                        <div id="imgwrpr" style="border-radius:100px; overflow: hidden;" class="wrapper">
                                                            <img style="width:250%;" class="img-responsive" src="{{ $slider['user_profile_image'] }}" alt="Profile Picture">
                                                        </div>
                                                    </div>
                                                    <div id="ava" class="col-md-8 col-sm-9 col-xs-9">
                                                        <h5>By {{ $slider['user_name'] }}</h5>
                                                        <h6 style="font-weight: 100;">{{ $slider['created_at'] }}</h6>
                                                    </div>
                                                </div>
                                            </div>

                                            <div style="margin-bottom: 10px;" class="col-sm-12 col-md-9 col-md-offset-3 hidden-xs hidden-sm">
                                                <div id="imgwrpr2" style="border-radius:10px; overflow: hidden;" class="wrapper">
                                                    <img id="slideimg" class="img-responsive" src="{{ $slider['image'] }}" alt="...">
                                                </div>
                                            </div>

                                            <div style="margin-bottom: 10px" class="col-sm-12 col-md-8 hidden-lg hidden-md">
                                                    <center>
                                                        <img id="slideimg" class="img-responsive" src="{{ $slider['image'] }}" alt="...">
                                                    </center>
                                            </div>
                                        </div>
                            @endforeach

                        </div>

                        <div class=" col-sm-8 col-sm-offset-4">
                            <ol class="carousel-indicators">
                                @foreach($slider_images as $key=>$slider)
                                    <li id="indi" data-target="#carousel-example-generic" data-slide-to="{{ $key }}" class="@if($key == 0) active @endif"></li>
                                @endforeach
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">


            <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">

                <!-- news Content -->
                @if(count($news_posts) > 0)
                    <div class="panel">

                        <!--Panel heading-->
                        <div style="padding-top: 10px; padding-bottom: 10px;" class="panel-heading">
                            <h3 class="panel-title"><i style="background-color: #8c8c8c; padding: 10px; border-radius: 20px; color: #fff;" class="fa fa-file-text-o" aria-hidden="true"></i> News</h3>
                        </div>

                        <hr style="width: 80%;">

                        <!--Panel body-->
                        <div class="pad-btm">
                            <div class="nano" style="max-height: 500px">
                                <div class="nano-content">
                                    <div class="panel-body">
                                        @foreach ($news_posts as $post)
                                            <div class="row">
                                                <div class="col-md-3 col-sm-3 col-xs-3">
                                                    <div id="imgwrpr" style="border-radius:100px; overflow: hidden;" class="wrapper">
                                                        <img style="width:250%;" src="{{ $post['thumb_image'] }}" alt="Profile Picture">
                                                    </div>
                                                </div>
                                                <div class="col-md-9 col-sm-9 col-xs-9">
                                                    <h4 style="font-weight: 100;">{{ $post['title'] }}</h4>
                                                    <div class="row">
                                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                                            <p><i class="fa fa-globe" aria-hidden="true"></i> {{ $post['first_name'].' '.$post['last_name'] }}</p>
                                                        </div>
                                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                                            <p><i class="fa fa-clock-o" aria-hidden="true"></i> {{ $post['active_time'] }}</p>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-4 col-md-4 col-sm-4 col-xs-4">
                                                            <p><i class="fa fa-heart" aria-hidden="true"></i> {{ $news_post_like_count[$post['id']] }}</p>
                                                        </div>
                                                        <div class="col-md-4 col-md-4 col-sm-4 col-xs-4 ">
                                                            <p><i class="fa fa-commenting-o" aria-hidden="true"></i> {{ $news_post_comment_count[$post['id']] }}</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- // News Content -->
                    @endif

                    <!-- favorite Site -->
                    <div class="panel">
                        <div style="padding-top: 10px; padding-bottom: 10px;" class="panel-heading">
                            <div class="col-md-8 col-sm-8 col-xs-8">
                                <h3 class="panel-title"><i style="background-color: #8c8c8c; padding: 10px; border-radius: 20px; color: #fff;" class="fa fa-clock-o" aria-hidden="true"></i> Favorite Sites</h3>
                            </div>

                            <div class=" col-md-4 col-sm-4 col-xs-4">
                                <h3 class="panel-title"><a class="pull-right" href="#"><i style="background-color: #03a9f4; padding: 10px; border-radius: 20px; color: #fff;" class="ti-plus" aria-hidden="true"></i></a></h3>
                            </div>
                        </div>

                        <hr style="width: 80%;">

                        <!--Panel body-->
                        <div class="pad-btm">
                            <div>
                                <div>
                                    <div style="padding-top: 0px;" class="panel-body">
                                        <div style="margin-bottom: 10px;" class="row">
                                            <section id="carousel">
                                                <div class="carousel slide" id="fade-quote-carousel" data-ride="carousel" data-interval="3000">
                                                    <!-- Carousel indicators -->
                                                    <ol class="carousel-indicators">
                                                        <li data-target="#fade-quote-carousel" data-slide-to="0"></li>
                                                        <li data-target="#fade-quote-carousel" data-slide-to="1"></li>
                                                        <li data-target="#fade-quote-carousel" data-slide-to="2" class="active"></li>
                                                    </ol>
                                                    <!-- Carousel items -->
                                                    <div class="carousel-inner">
                                                        <div class="item">
                                                            <div style="padding-top: 0px;" class="panel-body">
                                                                <div style="margin-bottom: 10px;" class="row">
                                                                    <div class="col-md-3 col-sm-3 col-xs-3">
                                                                        <img class="img-xs" src="{{ asset('template_assets/img/profile-photos/1.png') }}" alt="Profile Picture">
                                                                    </div>
                                                                    <div class="col-md-9 col-sm-9 col-xs-9">
                                                                        <h4 style="font-weight: 100;">MSN Singapore</h4>
                                                                    </div>
                                                                </div>

                                                                <div style="margin-bottom: 10px;" class="row">
                                                                    <div class="col-md-3 col-sm-3 col-xs-3">
                                                                        <img class="img-xs" src="{{ asset('template_assets/img/profile-photos/1.png') }}" alt="Profile Picture">
                                                                    </div>
                                                                    <div class="col-md-9 col-sm-9 col-xs-9">
                                                                        <h4 style="font-weight: 100;">Travel Corp</h4>
                                                                    </div>
                                                                </div>

                                                                <div style="margin-bottom: 10px;" class="row">
                                                                    <div class="col-md-3 col-sm-3 col-xs-3">
                                                                        <img class="img-xs" src="{{ asset('template_assets/img/profile-photos/1.png') }}" alt="Profile Picture">
                                                                    </div>
                                                                    <div class="col-md-9 col-sm-9 col-xs-9">
                                                                        <h4 style="font-weight: 100;">Tech Solution</h4>
                                                                    </div>
                                                                </div>

                                                                <div style="margin-bottom: 10px;" class="row">
                                                                    <div class="col-md-3 col-sm-3 col-xs-3">
                                                                        <img class="img-xs" src="{{ asset('template_assets/img/profile-photos/1.png') }}" alt="Profile Picture">
                                                                    </div>
                                                                    <div class="col-md-9 col-sm-9 col-xs-9">
                                                                        <h4 style="font-weight: 100;">Hotel.com</h4>
                                                                    </div>
                                                                </div>

                                                                <div style="margin-bottom: 10px;" class="row">
                                                                    <div class="col-md-3 col-sm-3 col-xs-3">
                                                                        <img class="img-xs" src="{{ asset('template_assets/img/profile-photos/1.png') }}" alt="Profile Picture">
                                                                    </div>
                                                                    <div class="col-md-9 col-sm-9 col-xs-9">
                                                                        <h4 style="font-weight: 100;">Thai Airlines</h4>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="item">
                                                            <div style="padding-top: 0px;" class="panel-body">
                                                                <div style="margin-bottom: 10px;" class="row">
                                                                    <div class="col-md-3 col-sm-3 col-xs-3">
                                                                        <img class="img-xs" src="{{ asset('template_assets/img/profile-photos/1.png') }}" alt="Profile Picture">
                                                                    </div>
                                                                    <div class="col-md-9 col-sm-9 col-xs-9">
                                                                        <h4 style="font-weight: 100;">MSN Singapore</h4>
                                                                    </div>
                                                                </div>

                                                                <div style="margin-bottom: 10px;" class="row">
                                                                    <div class="col-md-3 col-sm-3 col-xs-3">
                                                                        <img class="img-xs" src="{{ asset('template_assets/img/profile-photos/1.png') }}" alt="Profile Picture">
                                                                    </div>
                                                                    <div class="col-md-9 col-sm-9 col-xs-9">
                                                                        <h4 style="font-weight: 100;">Travel Corp</h4>
                                                                    </div>
                                                                </div>

                                                                <div style="margin-bottom: 10px;" class="row">
                                                                    <div class="col-md-3 col-sm-3 col-xs-3">
                                                                        <img class="img-xs" src="{{ asset('template_assets/img/profile-photos/1.png') }}" alt="Profile Picture">
                                                                    </div>
                                                                    <div class="col-md-9 col-sm-9 col-xs-9">
                                                                        <h4 style="font-weight: 100;">Tech Solution</h4>
                                                                    </div>
                                                                </div>

                                                                <div style="margin-bottom: 10px;" class="row">
                                                                    <div class="col-md-3 col-sm-3 col-xs-3">
                                                                        <img class="img-xs" src="{{ asset('template_assets/img/profile-photos/1.png') }}" alt="Profile Picture">
                                                                    </div>
                                                                    <div class="col-md-9 col-sm-9 col-xs-9">
                                                                        <h4 style="font-weight: 100;">Hotel.com</h4>
                                                                    </div>
                                                                </div>

                                                                <div style="margin-bottom: 10px;" class="row">
                                                                    <div class="col-md-3 col-sm-3 col-xs-3">
                                                                        <img class="img-xs" src="{{ asset('template_assets/img/profile-photos/1.png') }}" alt="Profile Picture">
                                                                    </div>
                                                                    <div class="col-md-9 col-sm-9 col-xs-9">
                                                                        <h4 style="font-weight: 100;">Thai Airlines</h4>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="active item">
                                                            <div style="padding-top: 0px;" class="panel-body">
                                                                <div style="margin-bottom: 10px;" class="row">
                                                                    <div class="col-md-3 col-sm-3 col-xs-3">
                                                                        <img class="img-xs" src="{{ asset('template_assets/img/profile-photos/1.png') }}" alt="Profile Picture">
                                                                    </div>
                                                                    <div class="col-md-9 col-sm-9 col-xs-9">
                                                                        <h4 style="font-weight: 100;">MSN Singapore</h4>
                                                                    </div>
                                                                </div>

                                                                <div style="margin-bottom: 10px;" class="row">
                                                                    <div class="col-md-3 col-sm-3 col-xs-3">
                                                                        <img class="img-xs" src="{{ asset('template_assets/img/profile-photos/1.png') }}" alt="Profile Picture">
                                                                    </div>
                                                                    <div class="col-md-9 col-sm-9 col-xs-9">
                                                                        <h4 style="font-weight: 100;">Travel Corp</h4>
                                                                    </div>
                                                                </div>

                                                                <div style="margin-bottom: 10px;" class="row">
                                                                    <div class="col-md-3 col-sm-3 col-xs-3">
                                                                        <img class="img-xs" src="{{ asset('template_assets/img/profile-photos/1.png') }}" alt="Profile Picture">
                                                                    </div>
                                                                    <div class="col-md-9 col-sm-9 col-xs-9">
                                                                        <h4 style="font-weight: 100;">Tech Solution</h4>
                                                                    </div>
                                                                </div>

                                                                <div style="margin-bottom: 10px;" class="row">
                                                                    <div class="col-md-3 col-sm-3 col-xs-3">
                                                                        <img class="img-xs" src="{{ asset('template_assets/img/profile-photos/1.png') }}" alt="Profile Picture">
                                                                    </div>
                                                                    <div class="col-md-9 col-sm-9 col-xs-9">
                                                                        <h4 style="font-weight: 100;">Hotel.com</h4>
                                                                    </div>
                                                                </div>

                                                                <div style="margin-bottom: 10px;" class="row">
                                                                    <div class="col-md-3 col-sm-3 col-xs-3">
                                                                        <img class="img-xs" src="{{ asset('template_assets/img/profile-photos/1.png') }}" alt="Profile Picture">
                                                                    </div>
                                                                    <div class="col-md-9 col-sm-9 col-xs-9">
                                                                        <h4 style="font-weight: 100;">Thai Airlines</h4>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </section>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- // favorite Site -->

                    <!-- Media -->
                    @if(count($media_files) > 0)
                        <div class="panel">
                            <div style="padding-top: 10px; padding-bottom: 10px;" class="panel-heading">
                                <h3 class="panel-title"><i style="background-color: #8c8c8c; padding: 10px; border-radius: 20px; color: #fff;" class="fa fa-play-circle-o" aria-hidden="true"></i> Media</h3>
                            </div>

                            <hr style="width: 80%;">

                            <!--Panel body-->
                            <div class="pad-btm">
                                <div class="nano" style="max-height: 230px">
                                    <div class="nano-content">
                                        <div class="panel-body">
                                            @foreach($media_files  as $media)
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <em><video controls="controls" width="100%"><source src="http://ezy.local/files{{ $media['file_path'].'/'.$media['name'] }}" type="video/mp4"></video></em>
                                                    </div>
                                                    <div class="col-md-8">
                                                        <a class="file-item clickable" data-id="{{ config('app.url').'/files/'.$media['file_path'].'/'.$media['name'] }}" title="{{ $media['name'] }}"><h4 style="font-weight: 100;">
                                                                {{ $media['name'] }}</h4>
                                                        </a>
                                                        <p>{{ $media['date'] }}</p>
                                                    </div>
                                                </div>
                                            @endforeach


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                    <!-- // Media -->

            </div>


            <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">

                <!-- Event Content -->
                @if(count($events) > 0)
                <div class="panel">
                    <!--Panel heading-->
                    <div style="padding-top: 10px; padding-bottom: 10px;" class="panel-heading">
                        <h3 class="panel-title"><i style="background-color: #8c8c8c; padding: 10px; border-radius: 20px; color: #fff;" class="fa fa-calendar" aria-hidden="true"></i> Event</h3>
                    </div>

                    <hr style="width: 80%;">

                    <!--Panel body-->
                    <div class="pad-btm">
                        <div class="nano" style="max-height: 500px">
                            <div class="nano-content">
                                <div class="panel-body">
                                    @foreach ($events as $event)
                                        <div class="row">
                                            <div class="col-md-3 col-sm-3 col-xs-3">
                                                <div id="imgwrpr" style="border-radius:100px; overflow: hidden;" class="wrapper">
                                                    <img style="width: 250px" src="{{ $event['event_image'] }}" alt="Profile Picture">
                                                </div>
                                            </div>
                                            <div class="col-md-9 col-sm-9 col-xs-9">
                                                <h4 style="font-weight: 100;">{{ $event['name'] }}</h4>
                                                <div class="row">
                                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                                        <p><i class="fa fa-globe" aria-hidden="true"></i> {{ $event['first_name'].' '.$event['last_name'] }}</p>
                                                    </div>
                                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                                        <p><i class="fa fa-clock-o" aria-hidden="true"></i> {{ $event['active_time'] }}</p>
                                                    </div>
                                                </div>
                                                {{--<div class="row">--}}
                                                {{--<div class="col-md-4 col-sm-4 col-xs-4">--}}
                                                {{--<p><i class="fa fa-heart" aria-hidden="true"></i> 609</p>--}}
                                                {{--</div>--}}
                                                {{--</div>--}}
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                <!-- // Event Content -->

                <!-- Top Documents -->
                @if(count($documents_files) > 0)
                    <div class="panel">

                        <!--Panel heading-->
                        <div style="padding-top: 8px; padding-bottom: 10px;" class="panel-heading">
                            <h3 class="panel-title"><i style="background-color: #8c8c8c; padding: 10px; border-radius: 20px; color: #fff;" class="fa fa-folder-o" aria-hidden="true"></i> Top Documents</h3>
                        </div>

                        <hr style="width: 80%;">

                        <!--Panel body-->
                        <div class="pad-btm">
                            <div>
                                <div>
                                    <div class="panel-body">
                                        @foreach($documents_files  as $document)
                                            <div class="row">
                                                <div class="col-md-2 col-sm-2 col-xs-2">
                                                    <i style="margin-top: 10px;" class="fa fa-folder-open-o fa-2x pull-right img-responsive" aria-hidden="true"></i>
                                                </div>
                                                <div class="col-md-8 col-sm-8 col-xs-8">
                                                    <h4 style="font-weight:100;">{{ $document['name'] }}</h4>
                                                    <!--<p>Date By : {{ $document['date'] }}</p>-->
                                                </div>
                                                <div style="margin-top: 12px;" class="col-md-2 col-sm-2 col-xs-2 zoom">
                                                    <a  href="javascript:downloadPhoto('{{ $document['name'] }}', '{{ $document['file_path'] }}')" $document="Download"><i style="background-color: #fff; border-radius: 20px;  color: #4a4c4c; padding: 8px; border: 1px solid #4a4c4c;" class="ti-arrow-down" aria-hidden="true"></i></a>
                                                </div>
                                            </div>
                                            <hr style="width: 80%;">
                                        @endforeach
                                    </div>
                                    <div style="text-align: center;"><button class="btn btn-dark btn-lg">Browse From File Manager</button></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                 <!-- //Top Documents -->

            </div>


            <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">

                <!-- Weather Widget -->
                <a href="https://www.accuweather.com/en/lk/colombo/311399/weather-forecast/311399" class="aw-widget-legal">
                </a><div id="awcc1524733624723" class="aw-widget-current"  data-locationkey="" data-unit="c" data-language="en-us" data-useip="true" data-uid="awcc1524733624723"></div>
                <script type="text/javascript" src="https://oap.accuweather.com/launch.js"></script>
                <!--// Weather Widget -->

                <!-- Announcements -->
                @if(count($announcements) > 0)
                    <div class="panel">

                        <!--Panel heading-->
                        <div style="padding-top: 10px; padding-bottom: 10px;" class="panel-heading">
                            <div class="col-md-7 col-sm-8 col-xs-8">
                                <h3 class="panel-title"><i style="background-color: #8c8c8c; padding: 10px; border-radius: 20px; color: #fff;" class="fa fa-volume-up" aria-hidden="true"></i> Announcements</h3>
                            </div>
                            <div class="col-md-5 col-sm-4 col-xs-4">
                                <div class="pull-right" style="margin-top:10px;">
                                    <!-- Carousel Buttons Next/Prev -->
                                    <a style="position: inherit;" data-slide="prev" href="#quote-carousel" class="left carousel-control hiddeen-xs hidden-sm"><i class="fa fa-chevron-left"></i></a>
                                    <a style="position: inherit;" data-slide="next" href="#quote-carousel" class="right carousel-control hiddeen-xs hidden-sm"><i class="fa fa-chevron-right"></i></a>
                                </div>
                            </div>
                        </div>

                        <hr style="width: 80%;">

                        <!--Panel body-->
                        <div >
                            <div >
                                <div >
                                    <div class="panel-body">
                                        <div style="background-color: red; border-radius: 10px; color: #fff;" class="row">
                                            <div class='col-md-12'>
                                                <div class="carousel slide" data-ride="carousel" id="quote-carousel">

                                                    <!-- Carousel Slides / Quotes -->
                                                    <div class="carousel-inner">

                                                        @foreach($announcements as $key=>$announcement)
                                                                <!-- Quote 1 -->
                                                        <div class="item @if($key == 0) active @endif">
                                                            <div class="row">
                                                                <div class="col-sm-12">
                                                                    <h4>{{ $announcement->subject }}</h4>
                                                                    <p>{!! htmlspecialchars_decode($announcement->description) !!}</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @endforeach

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    @endif
                            <!-- //Announcements -->

                    <!-- Upcoming Birthday -->
                    @if(count($upcoming_birthday) > 0)
                        <div class="panel">
                            <!--Panel heading-->
                            <div style="padding-top: 10px; padding-bottom: 10px;" class="panel-heading">
                                <h3 class="panel-title"><i style="background-color: #8c8c8c; padding: 10px; border-radius: 20px; color: #fff;" class="fa fa-gift" aria-hidden="true"></i> Upcoming Birthdays</h3>
                            </div>

                            <hr style="width: 80%;">

                            <!--Panel body-->
                            <div class="pad-btm">
                                <div class="nano" style="max-height: 170px">
                                    <div class="nano-content">
                                        <div class="panel-body">
                                            @foreach ($upcoming_birthday as $birthday)
                                                <div style="margin-bottom: 10px;" class="row">
                                                    <div class="col-md-3 col-sm-3 col-xs-3">
                                                        <div id="imgwrpr" style="border-radius:100px; overflow: hidden;" class="wrapper">
                                                            <img style="width:250%;" class="img-circle img-responsive" src="{{ $birthday['profile_image'] }}" alt="Profile Picture">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-9 col-sm-6 col-xs-6">
                                                        <h4 style="font-weight: 100;">{{ $birthday['name']}}</h4>
                                                        <div class="row">
                                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                                <p>on {{ $birthday['birthday'] }}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                                <!-- // Upcoming Birthday -->

                        <!-- Poll -->
                        @if(count($polls) > 0)
                            <div  class="panel">
                                <!--Panel heading-->
                                <div style="padding-top: 10px; padding-bottom: 10px;" class="panel-heading">
                                    <h3 class="panel-title"><i style="background-color: #8c8c8c; padding: 10px; border-radius: 20px; color: #fff; padding-left: 12px; padding-right: 12px;" class="fa fa-lightbulb-o" aria-hidden="true"></i> Poll</h3>
                                </div>

                                <hr style="width: 80%;">

                                <!--Panel body-->
                                <div class="pad-btm">

                                    <div>
                                        <div>
                                            @foreach($polls  as $poll)
                                                <form action="" id="poll_from" method="post" >
                                                    <div class="panel-body">
                                                        <div style="margin-bottom: 10px;">
                                                            <h4 style="font-weight: 100;">{{ $poll->title }}</h4>
                                                            <p>{{ $poll->question }}</p>
                                                        </div>

                                                        <div id="poll-success" class="alert alert-success hidden">
                                                            <strong>Success!</strong> Thank you for voting.
                                                        </div>
                                                        <div id="poll-warning" class="alert alert-warning hidden">
                                                            <strong>Warning!</strong> Already voted this poll.
                                                            <ul id="voted_options">
                                                                <li></li>
                                                            </ul>
                                                        </div>

                                                        <div class="form-group pad-ver">

                                                            @if($poll->option_type == 'checkbox')
                                                                @foreach($poll_options[$poll->poll_id] as $ques)
                                                                    <div class="radio">
                                                                        <input class="magic-checkbox" id="checkbox-{{ $ques->id }}" type="checkbox" name="poll_checkbox[]" value="{{ $ques->id }}" >
                                                                        <label for="checkbox-{{ $ques->id }}">{{ $ques->name }}</label>
                                                                    </div>
                                                                @endforeach
                                                                <input type="hidden" name="vote_type" value="checkbox">
                                                            @else
                                                                @foreach($poll_options[$poll->poll_id] as $ques)
                                                                    <div class="radio">
                                                                        <input id="radio-{{ $ques->id }}" class="magic-radio" type="radio" name="poll_radio"  value="{{ $ques->id }}">
                                                                        <label for="radio-{{ $ques->id }}">{{ $ques->name }}</label>
                                                                    </div>
                                                                @endforeach
                                                                <input type="hidden" name="vote_type" value="radio">
                                                            @endif
                                                            <input type="hidden" name="user_id" value="{{ Auth::user()->id }}" >
                                                                <input type="hidden" name="_token" value="{{ csrf_token() }}" >
                                                                <input type="hidden" name="poll_id" value="{{ $poll->poll_id }}" >

                                                        </div>
                                                        <div style="text-align:center;">
                                                            <button type="button" data-url="{{ url('poll/vote') }}" data-user_id="" id="poll-submit" class="btn btn-danger">Vote</button>
                                                            <button type="button" data-url="{{ url('poll/vote') }}" data-user_id="" id="poll-results" class="btn btn-dark">Hide Results</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            @endforeach
                                                <div id="poll-chart"></div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                                    <!-- // Poll  -->

                            <!-- Download annual report -->
                            <a href="http://www.alliancefinance.lk/resources/117/Annual_Report_2015-16_CSE.pdf" download>
                                <div class="panel panel-info panel-colorful media middle pad-all">
                                    <div style="padding-bottom: 10px; text-align: center;" class="panel-heading">
                                        <h3 class="panel-title">Download Annual Report 2017 <i style="background-color: #fff; padding: 10px; border-radius: 20px; color: #03a9f4;" class="ti-arrow-down" aria-hidden="true"></i></h3>
                                    </div>
                                </div>
                            </a>
                            <!-- // Download annual report -->
            </div>
        </div>
    </div>
</div>
</div>

<!--Morris.js Sample [ SAMPLE ]-->
{{--<script src="{{ asset('template_assets/js/demo/morris-js.js') }}"></script>--}}


<script>
    var route_prefix = "{{  config('app.url') }}";
    var lfm_route = "{{  config('app.url') }}/laravel-filemanager";

    function defaultParametersPhoto(work_path) {
        return {
            working_dir: work_path,
            type: $('#type').val()
        };
    }

    function downloadPhoto(file_name, work_path) {
        // /shares/Media
        var data = defaultParametersPhoto(work_path);
        data['file'] = file_name;
        //alert(lfm_route + '/download?' + $.param(data)); return false;
        location.href = lfm_route + '/download?' + $.param(data);
    }

    bootbox.setDefaults({locale:lang['locale-bootbox']});

    function fileView(file_url, timestamp) {
        bootbox.dialog({
            title: '',
            message: $('<img>')
                    .addClass('img img-responsive center-block')
                    .attr('src', file_url + '?timestamp=' + timestamp),
            size: 'large',
            onEscape: true,
            backdrop: true
        });
    }
</script>
@endsection
