@extends('layouts.dashboard_app')

@section('title', '| Dashboard')

@section('content')

<!-- test -->

<!-- RIBBON -->
<div id="ribbon">

                    <span class="ribbon-button-alignment">
                        <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
                            <i class="fa fa-refresh"></i>
                        </span>
                    </span>

    <!-- breadcrumb -->
    <ol class="breadcrumb">
        <li>Home</li><li>Dashboard</li>
    </ol>
    <!-- end breadcrumb -->

    <!-- You can also add more buttons to the
    ribbon for further usability

    Example below:

    <span class="ribbon-button-alignment pull-right">
    <span id="search" class="btn btn-ribbon hidden-xs" data-title="search"><i class="fa-grid"></i> Change Grid</span>
    <span id="add" class="btn btn-ribbon hidden-xs" data-title="add"><i class="fa-plus"></i> Add</span>
    <span id="search" class="btn btn-ribbon" data-title="search"><i class="fa-search"></i> <span class="hidden-mobile">Search</span></span>
    </span> -->

</div>
<!-- END RIBBON -->

<!-- MAIN CONTENT -->
<div id="content">

    <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
            <h1 class="page-title txt-color-blueDark"><i class="fa-fw fa fa-home"></i> Dashboard <span>> My Dashboard</span></h1>
        </div>
        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
            <ul id="sparks" class="">
                <li class="sparks-info">
                    <h5> My Income <span class="txt-color-blue">$47,171</span></h5>
                    <div class="sparkline txt-color-blue hidden-mobile hidden-md hidden-sm">
                        1300, 1877, 2500, 2577, 2000, 2100, 3000, 2700, 3631, 2471, 2700, 3631, 2471
                    </div>
                </li>
                <li class="sparks-info">
                    <h5> Site Traffic <span class="txt-color-purple"><i class="fa fa-arrow-circle-up"></i>&nbsp;45%</span></h5>
                    <div class="sparkline txt-color-purple hidden-mobile hidden-md hidden-sm">
                        110,150,300,130,400,240,220,310,220,300, 270, 210
                    </div>
                </li>
                <li class="sparks-info">
                    <h5> Site Orders <span class="txt-color-greenDark"><i class="fa fa-shopping-cart"></i>&nbsp;2447</span></h5>
                    <div class="sparkline txt-color-greenDark hidden-mobile hidden-md hidden-sm">
                        110,150,300,130,400,240,220,310,220,300, 270, 210
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <!-- widget grid -->
    <section id="widget-grid" class="">

        <!-- row -->

        <div class="row">

            <!-- MAIN SLIDER -->
            <div class="">
                <div class="well">
                    <div id="myCarousel" class="carousel slide" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            @foreach($slider_images as $key=>$slider)
                                <li data-target="#myCarousel-2" data-slide-to="{{ $key }}"  class="@if($key == 0) active @endif"></li>
                            @endforeach
                        </ol>

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner">

                            @foreach($slider_images as $key=>$slider)
                                <div class="item @if($key == 0) active @endif ">
                                    <img src="{{ $slider['image'] }}" alt="...">
                                    <div class="carousel-caption caption-right">
                                         <h1>{{ $slider['title'] }}</h1>
                                        <p>
                                            {!! htmlspecialchars_decode($slider['description']) !!}
                                        </p>
                                        <img style="width: 150px; border-radius: 20px;" src="{{ $slider['user_profile_image'] }}">
                                        <h4>{{ $slider['created_at'] }}</h4>
                                        <h5>{{ $slider['user_name'] }}</h5>
                                    </div>
                                </div>
                            @endforeach
                        </div>

                        <!-- Left and right controls -->
                        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#myCarousel" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>


                    <script>
                        $('#myCarousel-1').carousel({
                            interval : 3000,
                            cycle : true
                        });
                    </script>
                </div>
            </div>
            <!-- END MAIN SLIDER -->

            <!-- NEWS Area -->
            <article  class="col-xs-12 col-sm-4 sortable-grid ui-sortable jarviswidget">


                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget" id="wid-id-3" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-custombutton="false" data-widget-collapsed="false" data-widget-sortable="false" role="widget">
                    <header role="heading">
                        <span class="widget-icon"> <i class="fa fa-twitter text-info"></i> </span>
                        <h2>News</h2>

                        <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span></header>

                    <!-- widget div-->
                    <div role="content">

                        <!-- widget edit box -->
                        <div class="jarviswidget-editbox">
                            <!-- This area used as dropdown edit box -->
                            <input class="form-control" type="text">
                        </div>
                        <!-- end widget edit box -->

                        <!-- widget content -->
                        <div class="widget-body no-padding">

                            <!-- this is what the user will see -->
                            <div class="chat-body custom-scroll" style="height: 599px !important;">
                                <ul>
                                    @foreach ($news_posts as $post)
                                        <li class="message margin-bottom-10">
                                            <img src="{{ $post['thumb_image'] }}" width="52px" alt="" class="img-circle">
                                            <div class="message-text">
                                                <a href="{{ route('posts.show', $post['id'] ) }}" class="username txt-color-blueDark">{{ $post['title'] }}</a>
														<span class="font-xs">
                                                            @php
                                                            $content = htmlspecialchars_decode($post['body']);
                                                            $content = preg_replace("/<img[^>]+\>/i", "", $content);
                                                            @endphp
                                                            {!! substr($content,0,80) !!}
														</span>
                                                <br/>
                                                <i class="fa fa-globe"> {{ $post['first_name'].' '.$post['last_name'] }}</i>  <i class="fa fa-clock-o"> {{ $post['active_time'] }}</i><br/>
                                                <i class="fa fa-heart"></i> {{ $news_post_like_count[$post['id']] }} <i class="fa fa-comment"></i> {{ $news_post_comment_count[$post['id']] }}
                                            </div>
                                        </li>
                                        @endforeach

                                    <li class="v-align-m text-align-center">
                                        <a href="javascript:void(0);" class="btn btn-primary no-margin btn-xs">Load more</a>
                                    </li>
                                </ul>

                            </div>

                        </div>
                        <!-- end widget content -->

                    </div>
                    <!-- end widget div -->

                </div>
                <!-- end widget -->

            </article>
            <!-- END News Area -->

            <!-- EVENTS -->
            <article class="col-xs-12 col-sm-3 sortable-grid ui-sortable">

                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget" id="wid-id-7" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-custombutton="false" data-widget-collapsed="false" data-widget-sortable="false" role="widget">
                    <header role="heading">
                        <span class="widget-icon"> <i class="fa fa-twitter text-info"></i> </span>
                        <h2>Events</h2>

                        <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span></header>

                    <!-- widget div-->
                    <div role="content">

                        <!-- widget edit box -->
                        <div class="jarviswidget-editbox">
                            <!-- This area used as dropdown edit box -->
                            <input class="form-control" type="text">
                        </div>
                        <!-- end widget edit box -->

                        <!-- widget content -->
                        <div class="widget-body no-padding">

                            <!-- this is what the user will see -->
                            <div class="chat-body custom-scroll" style="height: 599px !important;">
                                <ul>
                                    @foreach ($events as $event)
                                        <li class="message margin-bottom-10">
                                            <img src="{{ $event['event_image'] }}" width="52px" alt="" class="img-circle">
                                            <div class="message-text">
                                                <a href="javascript:void(0);" class="username txt-color-blueDark">{{ $event['name'] }}</a>
														<span class="font-xs">
                                                            {!! substr($event['description'],0,80) !!}
														</span>
                                                <br/>
                                                <i class="fa fa-globe"> {{ $event['first_name'].' '.$event['last_name'] }}</i>  <i class="fa fa-clock-o"> {{ $event['active_time'] }}</i><br/>
                                            </div>
                                        </li>
                                    @endforeach

                                    <li class="v-align-m text-align-center">
                                        <a href="javascript:void(0);" class="btn btn-primary no-margin btn-xs">Load more</a>
                                    </li>
                                </ul>
                            </div>

                        </div>
                        <!-- end widget content -->

                    </div>
                    <!-- end widget div -->

                </div>
                <!-- end widget -->

            </article>
            <!-- END EVENTS -->

            <!-- POLL -->
            <article class="col-xs-12 col-sm-3 sortable-grid ui-sortable">

                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget" id="wid-id-7" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-custombutton="false" data-widget-collapsed="false" data-widget-sortable="false" role="widget">
                    <header role="heading">
                        <span class="widget-icon"> <i class="fa fa-twitter text-info"></i> </span>
                        <h2>POll</h2>
                        <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span></header>
                   <div id="poll-chart"></div>
                    <!-- widget div-->
                    @foreach($polls  as $poll)
                        <div role="content">
                            <!-- widget edit box -->
                            <div class="jarviswidget-editbox">
                                <!-- This area used as dropdown edit box -->
                                <input class="form-control" type="text">
                            </div>
                            <!-- end widget edit box -->

                            <!-- widget content -->
                            <div class="widget-body no-padding">

                                <!-- this is what the user will see -->
                                <div class="chat-body custom-scroll" style="height: 599px !important;">
                                    <div id="poll-success" class="alert alert-success hidden">
                                        <strong>Success!</strong> Thank you for voting.
                                    </div>
                                    <div id="poll-warning" class="alert alert-warning hidden">
                                        <strong>Warning!</strong> Already voted this poll.
                                        <ul id="voted_options">
                                            <li></li>
                                        </ul>
                                    </div>
                                    <form action="" id="poll_from" method="post" >
                                    <h4>{{ $poll->title }}</h4>
                                    <p>{{ $poll->question }}</p>
                                    @if($poll->option_type == 'checkbox')
                                        @foreach($poll_options[$poll->poll_id] as $ques)
                                            <input id="checkbox-{{ $ques->id }}" type="checkbox" name="poll_checkbox[]" value="{{ $ques->id }}" >
                                            <label for="checkbox-{{ $ques->id }}">{{ $ques->name }}</label>
                                            <br/>
                                        @endforeach
                                        <input type="hidden" name="vote_type" value="checkbox">
                                    @else
                                        @foreach($poll_options[$poll->poll_id] as $ques)
                                            <input id="radio-{{ $ques->id }}" type="radio" name="poll_radio" value="{{ $ques->id }}" >
                                            <label for="radio-{{ $ques->id }}">{{ $ques->name }}</label>
                                            <br/>
                                        @endforeach
                                            <input type="hidden" name="vote_type" value="radio">
                                        @endif
                                        <input type="hidden" name="user_id" value="{{ Auth::user()->id }}" >
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}" >
                                        <input type="hidden" name="poll_id" value="{{ $poll->poll_id }}" >
                                    <button type="button" data-url="{{ url('poll/vote') }}" data-user_id="" id="poll-submit" class="btn btn-info">vote</button>
                                    <button type="button" data-url="{{ url('poll/vote') }}" data-user_id="" id="poll-results" class="btn btn-info">results</button>
                                    </form>
                                </div>

                            </div>
                            <!-- end widget content -->

                        </div>
                        @endforeach
                                <!-- end widget div -->



                </div>
                <!-- end widget -->

            </article>
            <!-- END POLL -->



            <article class="col-xs-12 col-sm-3 sortable-grid ui-sortable">
                <div class="well">
                    <a class="weatherwidget-io" href="https://forecast7.com/en/1d36103d87/singapore/" data-label_1="SINGAPORE" data-label_2="WEATHER" data-theme="marine" >SINGAPORE WEATHER</a>
                    <script>
                        !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src='https://weatherwidget.io/js/widget.min.js';fjs.parentNode.insertBefore(js,fjs);}}(document,'script','weatherwidget-io-js');
                    </script>
                </div>

                <div class="well">
                    <p>@php echo date('H:i A'); @endphp</p>
                    <p>@php echo date("D, d F Y"); @endphp</p>
                    <p><strong>{{ $user->country_name}}</strong> | {{ $user->office_name }}</p>
                </div>
                <div class="well">
                    <div id="myCarousel" class="carousel slide" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            @foreach($announcements as $key=>$announcement)
                                <li data-target="#myCarousel-2" data-slide-to="{{ $key }}"  class="@if($key == 0) active @endif"></li>
                            @endforeach
                        </ol>

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner">

                            @foreach($announcements as $key=>$announcement)
                                <div class="item @if($key == 0) active @endif ">
                                    <div class="carousel-caption caption-right">
                                        <h4>{{ $announcement->subject }}</h4>
                                        <p>
                                            {!! htmlspecialchars_decode($announcement->description) !!}
                                        </p>
                                    </div>
                                </div>
                            @endforeach
                        </div>

                        <!-- Left and right controls -->
                        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#myCarousel" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>


                    <script>
                        $('#myCarousel-2').carousel({
                            interval : 3000,
                            cycle : true
                        });
                    </script>
                </div>
            </article>

            <!-- Upcoming Birthday -->
            <article class="col-xs-12 col-sm-3 sortable-grid ui-sortable">

                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget" id="wid-id-7" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-togglebutton="false" data-widget-deletebutton="false" data-widget-fullscreenbutton="false" data-widget-custombutton="false" data-widget-collapsed="false" data-widget-sortable="false" role="widget">
                    <header role="heading">
                        <span class="widget-icon"> <i class="fa fa-twitter text-info"></i> </span>
                        <h2>Upcoming Birthday</h2>

                        <span class="jarviswidget-loader"><i class="fa fa-refresh fa-spin"></i></span></header>

                    <!-- widget div-->
                    <div role="content">

                        <!-- widget edit box -->
                        <div class="jarviswidget-editbox">
                            <!-- This area used as dropdown edit box -->
                            <input class="form-control" type="text">
                        </div>
                        <!-- end widget edit box -->

                        <!-- widget content -->
                        <div class="widget-body no-padding">

                            <!-- this is what the user will see -->
                            <div class="chat-body custom-scroll" style="height: 599px !important;">
                                <ul>
                                    @foreach ($upcoming_birthday as $birthday)
                                        <li class="message margin-bottom-10">
                                            <img src="{{ $birthday['profile_image'] }}" width="52px" alt="" class="img-circle">
                                            <div class="message-text">
                                                <a href="javascript:void(0);" class="username txt-color-blueDark">{{ $birthday['name']}} ({{ $birthday['age'] }})</a>
														<span class="font-xs">
                                                            {{ $birthday['birthday'] }}
														</span>
                                                <br/>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>

                        </div>
                        <!-- end widget content -->

                    </div>
                    <!-- end widget div -->
                </div>
                <!-- end widget -->
            </article>
            <!-- // Upcoming Birthday -->

            <div class="clearfix"></div>

            <!-- Photo gallery -->
            <article class="col-xs-12 col-sm-4 sortable-grid ui-sortable">
                <h4>Photo Gallery</h4>
                <div class="">
                    <table class="table table-responsive table-condensed table-striped hidden-xs table-list-view">
                        <thead>
                        <tr><th style="width:50%;">Item</th>
                            <th>Type</th>
                            <th>Modified</th>
                            <th>Action</th>
                        </tr></thead>
                        <tbody>
                        @foreach($photo_files  as $photo)
                            <tr>

                                <td>
                                    <img src="http://ezy.local/files/{{ $photo['file_path'].'/'.$photo['name'] }}" style="width: 80px;">
                                    <a class="file-item clickable" data-id="http://ezy.local/files/{{ $photo['file_path'].'/'.$photo['name'] }}" title=" {{ $photo['name'] }}">
                                        {{ $photo['name'] }}
                                    </a>
                                </td>
                                <td>image/jpeg</td>
                                <td>{{ $photo['date'] }}</td>
                                <td class="actions">
                                    <a href="javascript:downloadPhoto('{{ $photo['name'] }}', '{{ $photo['file_path'] }}')" title="Download">
                                        <i class="fa fa-download fa-fw"></i>
                                    </a>
                                    <a href="javascript:fileView('http://ezy.local/files/{{ $photo['file_path'].'/'.$photo['name'] }}', '1524198725')" title="Preview">
                                        <i class="fa fa-image fa-fw"></i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </article>

            <!-- Media gallery -->
            <article class="col-xs-12 col-sm-4 sortable-grid ui-sortable">
                <h4>Media Gallery</h4>
                <div class="">
                    <table class="table table-responsive table-condensed table-striped hidden-xs table-list-view">
                        <thead>
                        <tr><th style="width:50%;">Item</th>
                            <th>Type</th>
                            <th>Modified</th>
                            <th>Action</th>
                        </tr></thead>
                        <tbody>
                        @foreach($media_files  as $media)
                            <tr>

                                <td>
                                    <em><video controls="controls" width="100%"><source src="http://ezy.local/files{{ $media['file_path'].'/'.$media['name'] }}" type="video/mp4"></video></em>
                                    <a class="file-item clickable" data-id="{{ config('app.url').'/files/'.$media['file_path'].'/'.$media['name'] }}" title="{{ $media['name'] }}">
                                        {{ $media['name'] }}
                                    </a>
                                </td>
                                <td>image/jpeg</td>
                                <td>{{ $photo['date'] }}</td>
                                <td class="actions">
                                    <a href="javascript:downloadPhoto('{{ $media['name'] }}', '{{ $media['file_path'] }}')" title="Download">
                                        <i class="fa fa-download fa-fw"></i>
                                    </a>
                                    <!-- <a href="javascript:fileView('http://ezy.local/files{{ $media['file_path'].'/'.$media['name'] }}', '1524198725')" title="Preview">
                                        <i class="fa fa-image fa-fw"></i>
                                    </a> -->

                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </article>

            <!-- Documents gallery -->
            <article class="col-xs-12 col-sm-4 sortable-grid ui-sortable">
                <h4>Documents </h4>
                <div class="">
                    <table class="table table-responsive table-condensed table-striped hidden-xs table-list-view">
                        <thead>
                        <tr><th style="width:50%;">Item</th>
                            <th>Type</th>
                            <th>Modified</th>
                            <th>Action</th>
                        </tr></thead>
                        <tbody>
                        @foreach($documents_files  as $document)
                            <tr>

                                <td>
                                    <i class="fa fa-file"></i>
                                    <a class="file-item clickable" data-id="{{ config('app.url').'/files/'.$document['file_path'].'/'.$document['name'] }}" title="{{ $document['name'] }}">
                                        {{ $document['name'] }}
                                    </a>
                                </td>
                                <td>image/jpeg</td>
                                <td>{{ $photo['date'] }}</td>
                                <td class="actions">
                                    <a href="javascript:downloadPhoto('{{ $document['name'] }}', '{{ $document['file_path'] }}')" $document="Download">
                                        <i class="fa fa-download fa-fw"></i>
                                    </a>
                                    <!-- <a href="javascript:fileView('http://ezy.local/files{{ $document['file_path'].'/'.$document['name'] }}', '1524198725')" title="Preview">
                                        <i class="fa fa-image fa-fw"></i>
                                    </a> -->
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </article>

        </div>

        <!-- end row -->

    </section>
    <!-- end widget grid -->

</div>
<!-- END MAIN CONTENT -->

@endsection

@section('page-js')

<!-- PAGE RELATED PLUGIN(S) -->

<!-- Flot Chart Plugin: Flot Engine, Flot Resizer, Flot Tooltip -->
<script src="{{ asset('asset/js/plugin/flot/jquery.flot.cust.min.js') }}"></script>
<script src="{{ asset('asset/js/plugin/flot/jquery.flot.resize.min.js') }}"></script>
<script src="{{ asset('asset/js/plugin/flot/jquery.flot.time.min.js') }}"></script>
<script src="{{ asset('asset/js/plugin/flot/jquery.flot.tooltip.min.js') }}"></script>

<!-- Vector Maps Plugin: Vectormap engine, Vectormap language -->
<script src="{{ asset('asset/js/plugin/vectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('asset/js/plugin/vectormap/jquery-jvectormap-world-mill-en.js') }}"></script>

<!-- Full Calendar -->
<script src="{{ asset('asset/js/plugin/moment/moment.min.js') }}"></script>
<script src="{{ asset('asset/js/plugin/fullcalendar/fullcalendar.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/4.4.0/bootbox.js"></script>


<!-- Morris Chart Dependencies -->
<script src="{{ asset('asset/js/plugin/morris/raphael.min.js') }}"></script>
<script src="{{ asset('asset/js/plugin/morris/morris.min.js') }}"></script>

<script>
    $(document).ready(function() {

        // DO NOT REMOVE : GLOBAL FUNCTIONS!
        pageSetUp();



        /*
         * PAGE RELATED SCRIPTS
         */

        $(".js-status-update a").click(function() {
            var selText = $(this).text();
            var $this = $(this);
            $this.parents('.btn-group').find('.dropdown-toggle').html(selText + ' <span class="caret"></span>');
            $this.parents('.dropdown-menu').find('li').removeClass('active');
            $this.parent().addClass('active');
        });

        /*
         * TODO: add a way to add more todo's to list
         */

        // initialize sortable
        $(function() {
            $("#sortable1, #sortable2").sortable({
                handle : '.handle',
                connectWith : ".todo",
                update : countTasks
            }).disableSelection();
        });

        // check and uncheck
        $('.todo .checkbox > input[type="checkbox"]').click(function() {
            var $this = $(this).parent().parent().parent();

            if ($(this).prop('checked')) {
                $this.addClass("complete");

                // remove this if you want to undo a check list once checked
                //$(this).attr("disabled", true);
                $(this).parent().hide();

                // once clicked - add class, copy to memory then remove and add to sortable3
                $this.slideUp(500, function() {
                    $this.clone().prependTo("#sortable3").effect("highlight", {}, 800);
                    $this.remove();
                    countTasks();
                });
            } else {
                // insert undo code here...
            }

        })
        // count tasks
        function countTasks() {

            $('.todo-group-title').each(function() {
                var $this = $(this);
                $this.find(".num-of-tasks").text($this.next().find("li").size());
            });

        }

        /*
         * RUN PAGE GRAPHS
         */

        /* TAB 1: UPDATING CHART */
        // For the demo we use generated data, but normally it would be coming from the server

        var data = [], totalPoints = 200, $UpdatingChartColors = $("#updating-chart").css('color');

        function getRandomData() {
            if (data.length > 0)
                data = data.slice(1);

            // do a random walk
            while (data.length < totalPoints) {
                var prev = data.length > 0 ? data[data.length - 1] : 50;
                var y = prev + Math.random() * 10 - 5;
                if (y < 0)
                    y = 0;
                if (y > 100)
                    y = 100;
                data.push(y);
            }

            // zip the generated y values with the x values
            var res = [];
            for (var i = 0; i < data.length; ++i)
                res.push([i, data[i]])
            return res;
        }

        // setup control widget
        var updateInterval = 1500;
        $("#updating-chart").val(updateInterval).change(function() {

            var v = $(this).val();
            if (v && !isNaN(+v)) {
                updateInterval = +v;
                $(this).val("" + updateInterval);
            }

        });

        // setup plot
        var options = {
            yaxis : {
                min : 0,
                max : 100
            },
            xaxis : {
                min : 0,
                max : 100
            },
            colors : [$UpdatingChartColors],
            series : {
                lines : {
                    lineWidth : 1,
                    fill : true,
                    fillColor : {
                        colors : [{
                            opacity : 0.4
                        }, {
                            opacity : 0
                        }]
                    },
                    steps : false

                }
            }
        };

        var plot = $.plot($("#updating-chart"), [getRandomData()], options);

        /* live switch */
        $('input[type="checkbox"]#start_interval').click(function() {
            if ($(this).prop('checked')) {
                $on = true;
                updateInterval = 1500;
                update();
            } else {
                clearInterval(updateInterval);
                $on = false;
            }
        });

        function update() {
            if ($on == true) {
                plot.setData([getRandomData()]);
                plot.draw();
                setTimeout(update, updateInterval);

            } else {
                clearInterval(updateInterval)
            }

        }

        var $on = false;

        /*end updating chart*/

        /* TAB 2: Social Network  */

        $(function() {
            // jQuery Flot Chart
            var twitter = [[1, 27], [2, 34], [3, 51], [4, 48], [5, 55], [6, 65], [7, 61], [8, 70], [9, 65], [10, 75], [11, 57], [12, 59], [13, 62]], facebook = [[1, 25], [2, 31], [3, 45], [4, 37], [5, 38], [6, 40], [7, 47], [8, 55], [9, 43], [10, 50], [11, 47], [12, 39], [13, 47]], data = [{
                label : "Twitter",
                data : twitter,
                lines : {
                    show : true,
                    lineWidth : 1,
                    fill : true,
                    fillColor : {
                        colors : [{
                            opacity : 0.1
                        }, {
                            opacity : 0.13
                        }]
                    }
                },
                points : {
                    show : true
                }
            }, {
                label : "Facebook",
                data : facebook,
                lines : {
                    show : true,
                    lineWidth : 1,
                    fill : true,
                    fillColor : {
                        colors : [{
                            opacity : 0.1
                        }, {
                            opacity : 0.13
                        }]
                    }
                },
                points : {
                    show : true
                }
            }];

            var options = {
                grid : {
                    hoverable : true
                },
                colors : ["#568A89", "#3276B1"],
                tooltip : true,
                tooltipOpts : {
                    //content : "Value <b>$x</b> Value <span>$y</span>",
                    defaultTheme : false
                },
                xaxis : {
                    ticks : [[1, "JAN"], [2, "FEB"], [3, "MAR"], [4, "APR"], [5, "MAY"], [6, "JUN"], [7, "JUL"], [8, "AUG"], [9, "SEP"], [10, "OCT"], [11, "NOV"], [12, "DEC"], [13, "JAN+1"]]
                },
                yaxes : {

                }
            };

            var plot3 = $.plot($("#statsChart"), data, options);
        });

        // END TAB 2

        // TAB THREE GRAPH //
        /* TAB 3: Revenew  */

        $(function() {

            var trgt = [[1354586000000, 153], [1364587000000, 658], [1374588000000, 198], [1384589000000, 663], [1394590000000, 801], [1404591000000, 1080], [1414592000000, 353], [1424593000000, 749], [1434594000000, 523], [1444595000000, 258], [1454596000000, 688], [1464597000000, 364]], prft = [[1354586000000, 53], [1364587000000, 65], [1374588000000, 98], [1384589000000, 83], [1394590000000, 980], [1404591000000, 808], [1414592000000, 720], [1424593000000, 674], [1434594000000, 23], [1444595000000, 79], [1454596000000, 88], [1464597000000, 36]], sgnups = [[1354586000000, 647], [1364587000000, 435], [1374588000000, 784], [1384589000000, 346], [1394590000000, 487], [1404591000000, 463], [1414592000000, 479], [1424593000000, 236], [1434594000000, 843], [1444595000000, 657], [1454596000000, 241], [1464597000000, 341]], toggles = $("#rev-toggles"), target = $("#flotcontainer");

            var data = [{
                label : "Target Profit",
                data : trgt,
                bars : {
                    show : true,
                    align : "center",
                    barWidth : 30 * 30 * 60 * 1000 * 80
                }
            }, {
                label : "Actual Profit",
                data : prft,
                color : '#3276B1',
                lines : {
                    show : true,
                    lineWidth : 3
                },
                points : {
                    show : true
                }
            }, {
                label : "Actual Signups",
                data : sgnups,
                color : '#71843F',
                lines : {
                    show : true,
                    lineWidth : 1
                },
                points : {
                    show : true
                }
            }]

            var options = {
                grid : {
                    hoverable : true
                },
                tooltip : true,
                tooltipOpts : {
                    //content: '%x - %y',
                    //dateFormat: '%b %y',
                    defaultTheme : false
                },
                xaxis : {
                    mode : "time"
                },
                yaxes : {
                    tickFormatter : function(val, axis) {
                        return "$" + val;
                    },
                    max : 1200
                }

            };

            plot2 = null;

            function plotNow() {
                var d = [];
                toggles.find(':checkbox').each(function() {
                    if ($(this).is(':checked')) {
                        d.push(data[$(this).attr("name").substr(4, 1)]);
                    }
                });
                if (d.length > 0) {
                    if (plot2) {
                        plot2.setData(d);
                        plot2.draw();
                    } else {
                        plot2 = $.plot(target, d, options);
                    }
                }

            };

            toggles.find(':checkbox').on('change', function() {
                plotNow();
            });
            plotNow()

        });

        /*
         * VECTOR MAP
         */

        data_array = {
            "US" : 4977,
            "AU" : 4873,
            "IN" : 3671,
            "BR" : 2476,
            "TR" : 1476,
            "CN" : 146,
            "CA" : 134,
            "BD" : 100
        };

        $('#vector-map').vectorMap({
            map : 'world_mill_en',
            backgroundColor : '#fff',
            regionStyle : {
                initial : {
                    fill : '#c4c4c4'
                },
                hover : {
                    "fill-opacity" : 1
                }
            },
            series : {
                regions : [{
                    values : data_array,
                    scale : ['#85a8b6', '#4d7686'],
                    normalizeFunction : 'polynomial'
                }]
            },
            onRegionLabelShow : function(e, el, code) {
                if ( typeof data_array[code] == 'undefined') {
                    e.preventDefault();
                } else {
                    var countrylbl = data_array[code];
                    el.html(el.html() + ': ' + countrylbl + ' visits');
                }
            }
        });

        /*
         * FULL CALENDAR JS
         */

        if ($("#calendar").length) {
            var date = new Date();
            var d = date.getDate();
            var m = date.getMonth();
            var y = date.getFullYear();

            var calendar = $('#calendar').fullCalendar({

                editable : true,
                draggable : true,
                selectable : false,
                selectHelper : true,
                unselectAuto : false,
                disableResizing : false,
                height: "auto",

                header : {
                    left : 'title', //,today
                    center : 'prev, next, today',
                    right : 'month, agendaWeek, agenDay' //month, agendaDay,
                },

                select : function(start, end, allDay) {
                    var title = prompt('Event Title:');
                    if (title) {
                        calendar.fullCalendar('renderEvent', {
                                    title : title,
                                    start : start,
                                    end : end,
                                    allDay : allDay
                                }, true // make the event "stick"
                        );
                    }
                    calendar.fullCalendar('unselect');
                },

                events : [{
                    title : 'All Day Event',
                    start : new Date(y, m, 1),
                    description : 'long description',
                    className : ["event", "bg-color-greenLight"],
                    icon : 'fa-check'
                }, {
                    title : 'Long Event',
                    start : new Date(y, m, d - 5),
                    end : new Date(y, m, d - 2),
                    className : ["event", "bg-color-red"],
                    icon : 'fa-lock'
                }, {
                    id : 999,
                    title : 'Repeating Event',
                    start : new Date(y, m, d - 3, 16, 0),
                    allDay : false,
                    className : ["event", "bg-color-blue"],
                    icon : 'fa-clock-o'
                }, {
                    id : 999,
                    title : 'Repeating Event',
                    start : new Date(y, m, d + 4, 16, 0),
                    allDay : false,
                    className : ["event", "bg-color-blue"],
                    icon : 'fa-clock-o'
                }, {
                    title : 'Meeting',
                    start : new Date(y, m, d, 10, 30),
                    allDay : false,
                    className : ["event", "bg-color-darken"]
                }, {
                    title : 'Lunch',
                    start : new Date(y, m, d, 12, 0),
                    end : new Date(y, m, d, 14, 0),
                    allDay : false,
                    className : ["event", "bg-color-darken"]
                }, {
                    title : 'Birthday Party',
                    start : new Date(y, m, d + 1, 19, 0),
                    end : new Date(y, m, d + 1, 22, 30),
                    allDay : false,
                    className : ["event", "bg-color-darken"]
                }, {
                    title : 'Smartadmin Open Day',
                    start : new Date(y, m, 28),
                    end : new Date(y, m, 29),
                    className : ["event", "bg-color-darken"]
                }],


                eventRender : function(event, element, icon) {
                    if (!event.description == "") {
                        element.find('.fc-title').append("<br/><span class='ultra-light'>" + event.description + "</span>");
                    }
                    if (!event.icon == "") {
                        element.find('.fc-title').append("<i class='air air-top-right fa " + event.icon + " '></i>");
                    }
                }
            });

        };

        /* hide default buttons */
        $('.fc-toolbar .fc-right, .fc-toolbar .fc-center').hide();

        // calendar prev
        $('#calendar-buttons #btn-prev').click(function() {
            $('.fc-prev-button').click();
            return false;
        });

        // calendar next
        $('#calendar-buttons #btn-next').click(function() {
            $('.fc-next-button').click();
            return false;
        });

        // calendar today
        $('#calendar-buttons #btn-today').click(function() {
            $('.fc-button-today').click();
            return false;
        });

        // calendar month
        $('#mt').click(function() {
            $('#calendar').fullCalendar('changeView', 'month');
        });

        // calendar agenda week
        $('#ag').click(function() {
            $('#calendar').fullCalendar('changeView', 'agendaWeek');
        });

        // calendar agenda day
        $('#td').click(function() {
            $('#calendar').fullCalendar('changeView', 'agendaDay');
        });

        /*
         * CHAT
         */

        $.filter_input = $('#filter-chat-list');
        $.chat_users_container = $('#chat-container > .chat-list-body')
        $.chat_users = $('#chat-users')
        $.chat_list_btn = $('#chat-container > .chat-list-open-close');
        $.chat_body = $('#chat-body');

        /*
         * LIST FILTER (CHAT)
         */

        // custom css expression for a case-insensitive contains()
        jQuery.expr[':'].Contains = function(a, i, m) {
            return (a.textContent || a.innerText || "").toUpperCase().indexOf(m[3].toUpperCase()) >= 0;
        };

        function listFilter(list) {// header is any element, list is an unordered list
            // create and add the filter form to the header

            $.filter_input.change(function() {
                var filter = $(this).val();
                if (filter) {
                    // this finds all links in a list that contain the input,
                    // and hide the ones not containing the input while showing the ones that do
                    $.chat_users.find("a:not(:Contains(" + filter + "))").parent().slideUp();
                    $.chat_users.find("a:Contains(" + filter + ")").parent().slideDown();
                } else {
                    $.chat_users.find("li").slideDown();
                }
                return false;
            }).keyup(function() {
                // fire the above change event after every letter
                $(this).change();

            });

        }

        // on dom ready
        listFilter($.chat_users);

        // open chat list
        $.chat_list_btn.click(function() {
            $(this).parent('#chat-container').toggleClass('open');
        })

        $.chat_body.animate({
            scrollTop : $.chat_body[0].scrollHeight
        }, 500);

    });

</script>

<!-- Your GOOGLE ANALYTICS CODE Below -->
<script>
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-XXXXXXXX-X']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script');
        ga.type = 'text/javascript';
        ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(ga, s);
    })();



    var route_prefix = "{{  config('app.url') }}";
    var lfm_route = "{{  config('app.url') }}/laravel-filemanager";

    function defaultParametersPhoto(work_path) {
        return {
            working_dir: work_path,
            type: $('#type').val()
        };
    }

    function downloadPhoto(file_name, work_path) {
        // /shares/Media
        var data = defaultParametersPhoto(work_path);
        data['file'] = file_name;
        //alert(lfm_route + '/download?' + $.param(data)); return false;
        location.href = lfm_route + '/download?' + $.param(data);
    }

    bootbox.setDefaults({locale:lang['locale-bootbox']});

    function fileView(file_url, timestamp) {
        bootbox.dialog({
            title: '',
            message: $('<img>')
                    .addClass('img img-responsive center-block')
                    .attr('src', file_url + '?timestamp=' + timestamp),
            size: 'large',
            onEscape: true,
            backdrop: true
        });
    }

</script>
<style>
    .carousel-caption
    {
        bottom: inherit !important;
        color: #000000 !important;
    }
    .carousel-inner .item
    {
        height: 200px !important;
    }
</style>
@endsection