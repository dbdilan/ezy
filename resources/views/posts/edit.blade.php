
@extends('layouts.dashboard_app')

@section('title', '| Edit News')

@section('content')

        <!-- RIBBON -->
<div id="ribbon">

    <span class="ribbon-button-alignment">
        <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
            <i class="fa fa-refresh"></i>
        </span>
    </span>

    <!-- breadcrumb -->
    <ol class="breadcrumb">
        <li>Home</li>
        <li>News</li>
        <li>Edit</li>
    </ol>
    <!-- end breadcrumb -->

</div>
<!-- END RIBBON -->

<!-- MAIN CONTENT -->
<div id="content">

    <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
            <h1 class="page-title txt-color-blueDark">
                <i class="fa fa-users fa-fw "></i>
                News <span>> Edit </span>
            </h1>
        </div>
        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
            <button type="button" class="btn btn-primary pull-right" onclick="window.history.back()">< Back</button>
        </div>
    </div>

    <!-- widget grid -->
    <section id="widget-grid" class="">

        <!-- row -->
        <div class="row">

            <article class="col-sm-12 col-md-12 col-lg-6">

                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget" id="wid-id-1" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                        <h2>Edit a News </h2>

                    </header>

                    <!-- widget div-->
                    <div>

                        <!-- widget edit box -->
                        <div class="jarviswidget-editbox">
                            <!-- This area used as dropdown edit box -->

                        </div>
                        <!-- end widget edit box -->

                        <!-- widget content -->
                        <div class="widget-body">
                            @include ('errors.list')
                            {{ Form::model($post, array('route' => array('posts.update', $post->id), 'method' => 'PUT')) }}
                            <div class="form-group">
                                {{ Form::label('title', 'Title') }}
                                {{ Form::text('title', null, array('class' => 'form-control')) }}<br>
                                @if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <!-- Thumb Image -->
                            <div class="form-group">
                                <label>Thumb image</label>
                                <div class="input-group">
                                    <span class="input-group-btn">
                                        <a id="lfm" data-input="thumbnail-post-1" data-preview="holder-thumb-1"  class="btn btn-primary file-manager">
                                            <i class="fa fa-picture-o"></i> Choose
                                        </a>
                                    </span>
                                    <input id="thumbnail-post-1" class="form-control" value="{{ $post->thumb_image }}" type="text" name="thumb_image">
                                    @if ($errors->has('thumb_image'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('thumb_image') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <!-- // Thumb -->

                            <!-- Post type -->
                            <div class="form-group">
                                <label>Post Type</label>
                                <select class="form-control" name="post_type" id="post-type">
                                    <option value="">Select</option>
                                    <option @if($post->post_type == 'gallery') selected @endif value="gallery">Gallery</option>
                                    <option @if($post->post_type == 'album') selected @endif value="album">Album</option>
                                    {{--<option @if($post->post_type == 'video') selected @endif value="video">Video</option>--}}
                                    {{--<option @if($post->post_type == 'audio') selected @endif value="audio">Audio</option>--}}
                                    {{--<option @if($post->post_type == 'other') selected @endif value="other">Other</option>--}}
                                </select>
                                @if ($errors->has('post_type'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('post_type') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <!-- // Post Type -->

                            <!-- Album  -->
                            <div class="form-group hidden" id="album-div-box">
                                <label>Album (Select one image form album)</label>
                                <div class="input-group">
                                    <span class="input-group-btn">
                                        <a id="lfm-album" data-input="album-1" class="btn btn-primary">
                                            <i class="fa fa-picture-o"></i> Choose
                                        </a>
                                    </span>
                                    <input id="album-1" value="{{ $content_other.'/1.jpg' }}" class="form-control" type="text" name="album">
                                </div>
                            </div>
                            <!-- // Album -->

                            <!-- Galley Div -->
                            <div class="form-group hidden" id="gallery-div-box">
                                <label>Select Images</label>
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>File</th>
                                        <th>Image</th>
                                        <th>Remove</th>
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <button type="button" id="add-new-gallery-btn" class="btn btn-primary hidden" onclick="addNewGallery()"><i class="fa fa-plus"></i></button>
                                        </td>
                                    </tr>
                                    </tfoot>
                                    <tbody class="gallery-div">
                                    @if($post->post_type == 'gallery')
                                        @php $row_id = 1; @endphp
                                        @foreach($content_array as $key=>$image_item)
                                            <tr id="image-row{{ $row_id }}">
                                                <td style="width:99%" >
                                                    <div class="input-group">
                                                        <span class="input-group-btn">
                                                            <a id="lfm" data-input="thumbnail-{{ $row_id }}" data-preview="holder-{{ $row_id }}"  class="btn btn-primary file-manager">
                                                                <i class="fa fa-picture-o"></i> Choose
                                                            </a>
                                                        </span>
                                                        <input id="thumbnail-{{ $row_id }}" value="{{ $image_item }}" class="form-control" type="text" name="gallery_image[]">
                                                    </div>
                                                </td>
                                                <td class="gallery-thmb-img">
                                                    <img id="holder-{{ $row_id }}" src="{{ $image_item }}" style="max-height:50px;">
                                                </td>
                                                <td style="padding-left: 10px; " class="text-left">
                                                    {{--@if($key != 0)--}}
                                                    <button type="button" onclick="$('#image-row{{ $row_id }}, .tooltip').remove();" data-toggle="tooltip" title="Remove" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button>
                                                    {{--@endif--}}
                                                </td>

                                            </tr>
                                            @php $row_id++; @endphp
                                        @endforeach
                                        <tr id="image-row{{ $row_id }}">
                                            <td style="width:99%">
                                                <div class="input-group">
                                                    <span class="input-group-btn">
                                                        <a id="lfm" data-input="thumbnail-{{ $row_id }}" data-preview="holder-{{ $row_id }}"  class="btn btn-primary file-manager">
                                                            <i class="fa fa-picture-o"></i> Choose
                                                        </a>
                                                    </span>
                                                    <input id="thumbnail-{{ $row_id }}"  class="form-control" type="text" name="gallery_image[]">
                                                </div>
                                            </td>
                                            <td class="gallery-thmb-img">
                                                <img id="holder-{{ $row_id }}" style="max-height:50px;">
                                            </td>
                                            <td>
                                                <button type="button" onclick="$('#image-row{{ $row_id }}, .tooltip').remove();" data-toggle="tooltip" title="Remove" class="btn btn-danger hidden row-delete-btn-{{ $row_id }}"><i class="fa fa-minus-circle"></i></button>
                                            </td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                            <!-- // Galley Div -->

                            <!-- Video  -->
                            <div class="form-group hidden" id="video-div-box">
                                <label>Video</label>
                                <div class="input-group">
                                    <span class="input-group-btn">
                                        <a id="lfm-video" data-input="video-1" class="btn btn-primary">
                                            <i class="fa fa-picture-o"></i> Choose
                                        </a>
                                    </span>
                                    <input id="video-1" class="form-control" type="text" value="{{ $content_other }}" name="video">
                                </div>
                            </div>
                            <!-- // Video -->

                            <!-- Audio  -->
                            <div class="form-group hidden" id="audio-div-box">
                                <label>Audio</label>
                                <div class="input-group">
                                    <span class="input-group-btn">
                                        <a id="lfm-audio" data-input="audio-1" class="btn btn-primary">
                                            <i class="fa fa-picture-o"></i> Choose
                                        </a>
                                    </span>
                                    <input id="audio-1" class="form-control" type="text" value="{{ $content_other }}" name="audio">
                                </div>
                            </div>
                            <!-- // Audio -->

                            <!-- File  -->
                            <div class="form-group hidden" id="other-div-box">
                                <label>Other</label>
                                <div class="input-group">
                                    <span class="input-group-btn">
                                        <a id="lfm-other" data-input="other-1" class="btn btn-primary">
                                            <i class="fa fa-picture-o"></i> Choose
                                        </a>
                                    </span>
                                    <input id="other-1" class="form-control" type="text" value="{{ $content_other }}" name="other">
                                </div>
                            </div>
                            <!-- // File -->

                            <div class="form-group">
                                <label>Publish date</label>
                                <input type="text" name="publish_date" value="{{ $post->publish_date }}"  placeholder="Publish Date" class="datepicker form-control" data-dateformat='dd/mm/yy' required>
                                <input type="hidden" name="created_by" value="{{ $post->created_by }}">
                            </div>

                            <div class="form-group">
                                <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
                                <textarea name="description" class="form-control my-editor">{{ $post->body }}</textarea>
                                <script>
                                    var editor_config = {
                                        path_absolute : "/",
                                        selector: "textarea.my-editor",
                                        plugins: [
                                            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                                            "searchreplace wordcount visualblocks visualchars code fullscreen",
                                            "insertdatetime media nonbreaking save table contextmenu directionality",
                                            "emoticons template paste textcolor colorpicker textpattern"
                                        ],
                                        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
                                        relative_urls: false,
                                        file_browser_callback : function(field_name, url, type, win) {
                                            var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
                                            var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

                                            var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
                                            if (type == 'image') {
                                                cmsURL = cmsURL + "&type=Images";
                                            } else {
                                                cmsURL = cmsURL + "&type=Files";
                                            }

                                            tinyMCE.activeEditor.windowManager.open({
                                                file : cmsURL,
                                                title : 'Filemanager',
                                                width : x * 0.8,
                                                height : y * 0.8,
                                                resizable : "yes",
                                                close_previous : "no"
                                            });
                                        }
                                    };

                                    tinymce.init(editor_config);
                                </script>

                                @if ($errors->has('description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label>Privacy</label>
                                <select class="form-control" name="privacy" id="privacy">
                                    @foreach($privacy  as $privacy_item)
                                        <option @if($post->privacy == $privacy_item->code) selected @endif value="{{ $privacy_item->code }}">{{ $privacy_item->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div id="user-group-div" class="form-group hidden">
                                <label>User Groups</label>
                                <select style="width:100%" class="select2" name="group[]" multiple id="user-group">
                                    @foreach($user_groups  as $user_group)
                                        <option @if(in_array($user_group->group_id, $group_array)) selected @endif value="{{ $user_group->group_id }}">{{ $user_group->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            @role('admin')
                            <div id="office-div" class="form-group hidden">
                                <label>Location</label>
                                <select style="width:100%" class="select2" name="office[]" multiple id="office">
                                    @foreach($offices  as $office)
                                        <option @if(in_array($office->office_id, $office_array)) selected @endif value="{{ $office->office_id }}">{{ $office->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            @endrole

                            @can('News add to main slider')
                            <div class="form-group">
                                <input type="checkbox" @if($post->attach_to_main_slider == 1) checked @endif id="attach-to-main-slider" name="attach_to_main_slider" value="1">
                                <label for="attach-to-main-slider">Post add to main slider</label>
                            </div>
                            @endcan

                            <input type="hidden" value="{{ $post->id }}" name="post_id">
                            {{ Form::submit('Save', array('class' => 'btn btn-primary')) }}

                            {{ Form::close() }}
                        </div>
                        <!-- end widget content -->

                    </div>
                    <!-- end widget div -->

                </div>
                <!-- end widget -->

            </article>
            <!-- END COL -->
        </div>

    </section>
    <!-- end widget grid -->

</div>
<!-- END MAIN CONTENT -->

@endsection


@section('page-js')

    <script>

        //============================================  post type ==========================================================
        postType();
        $('#post-type').change(function () {
            postType();
        });

        function postType() {
            $('#gallery-div-box').addClass('hidden');
            $('#video-div-box').addClass('hidden');
            $('#audio-div-box').addClass('hidden');
            $('#other-div-box').addClass('hidden');
            $('#album-div-box').addClass('hidden');
            var post_type = $('#post-type').val();
            if(post_type == 'gallery')
            {
                $('#gallery-div-box').removeClass('hidden');
            }
            if(post_type == 'video')
            {
                $('#video-div-box').removeClass('hidden');
            }
            if(post_type == 'audio')
            {
                $('#audio-div-box').removeClass('hidden');
            }
            if(post_type == 'other')
            {
                $('#other-div-box').removeClass('hidden');
            }
            if(post_type == 'album')
            {
                $('#album-div-box').removeClass('hidden');
            }
        }
        //======================================== End  post type ==========================================================


        //======================================== Gallery Add =============================================================
        thumbChange();
        $("#thumbnail-{{ $row_id }}").bind("change paste keyup", function() {
            thumbChange();
        });

        function thumbChange() {
            var str_value = $('#thumbnail-{{ $row_id }}').val();
            var add_new_btn = $('#add-new-gallery-btn');
            var row_delete = $('.row-delete-btn-{{ $row_id }}');
            add_new_btn.addClass('hidden');
            if(str_value != '')
            {
                add_new_btn.removeClass('hidden');
                row_delete.removeClass('hidden');
            }
        }
        //======================================== End Gallery Add =========================================================


        @php $row_id++; @endphp

        var row_id = '{{ $row_id }}';
        function addNewGallery() {

            var html = '';
            html += '<tr id="image-row' + row_id  + '">';
            html += '<td style="width:99%">';
            html += '<div class="input-group">';
            html += '<span class="input-group-btn">';
            html += '<a id="lfm'+row_id+'" data-input="thumbnail-'+row_id+'" data-preview="holder-'+row_id+'"  class="btn btn-primary file-manager">';
            html += '<i class="fa fa-picture-o"></i> Choose';
            html += '</a>';
            html += '</span>';
            html += '<input id="thumbnail-'+row_id+'" class="form-control" type="text" name="gallery_image[]">';
            html += '</div>';
            html += '</td>';
            html += '<td class="gallery-thmb-img">';
            html += '<img id="holder-'+row_id+'" style="max-height:50px;">';
            html += '</td>';
            html += '<td style="padding-left: 10px; " class="text-left"><button type="button" onclick="$(\'#image-row' + row_id  + ', .tooltip\').remove();" data-toggle="tooltip" title="Remove" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
            html += '</tr>';

            row_id++;

            $('.gallery-div').append(html);
        }

        $("#post-form").validate();

        // DO NOT REMOVE : GLOBAL FUNCTIONS!
        $(document).ready(function() {
            pageSetUp();
        })

    </script>
@endsection