@extends('layouts.dashboard_app')
@section('content')

<div class="panel-heading">Page {{ $posts->currentPage() }} of {{ $posts->lastPage() }}</div>


@foreach ($posts as $post)
<h1>{{ $post->id. ' '. $post->title }}</h1>
    @endforeach
<div class="text-center">
    {!! $posts->links() !!}
</div>

    @endsection