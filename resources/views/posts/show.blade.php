@extends('layouts.dashboard_app')

@section('title', '| View Post')

@section('content')

        <!-- RIBBON -->
<div id="ribbon">

    <span class="ribbon-button-alignment">
        <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
            <i class="fa fa-refresh"></i>
        </span>
    </span>

    <!-- breadcrumb -->
    <ol class="breadcrumb">
        <li>Home</li><li>News</li>
    </ol>


</div>
<!-- END RIBBON -->



<!-- MAIN CONTENT -->
<div id="content">

    <!-- row -->
    <div class="row">

        <!-- col -->
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
            <h1 class="page-title txt-color-blueDark">
                <!-- PAGE HEADER -->
                <i class="fa-fw fa fa-home"></i> Home <span>>
                    News </span>
            </h1>
        </div>
        <!-- end col -->

        <!-- right side of the page with the sparkline graphs -->
        <!-- col -->
        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
            @can('News Create')
            <a href="{{ route('posts.create') }}" class="btn btn-primary pull-right">New Article</a>
            @endcan
        </div>
        <!-- end col -->

    </div>
    <!-- end row -->

    <div class="row">

        <div class="">
            <!-- Gallery post -->
            @if($post->post_type == 'gallery' && $content[$post->id] != '')
            @if(count($content[$post->id]) >  1 )
                    <!-- well -->
            <div class="well">
                <div id="myCarousel-2" class="carousel slide">
                    <ol class="carousel-indicators">
                        @foreach($content[$post->id] as $key=>$item_image)
                            @if($key == 0)
                                <li data-target="#myCarousel-2" data-slide-to="{{ $key }}" class="active"></li>
                            @else
                                <li data-target="#myCarousel-2" data-slide-to="{{ $key }}"></li>
                            @endif
                        @endforeach
                    </ol>
                    <div class="carousel-inner">
                        <!-- Slide 1 -->
                        @foreach($content[$post->id] as $key=>$item_image)
                            @if($key == 0)
                                <div class="item active">
                                    <img src="{{ $item_image }}" alt="">
                                </div>
                            @else
                                <div class="item">
                                    <img src="{{ $item_image }}" alt="">
                                </div>
                            @endif
                        @endforeach
                    </div>
                    <a class="left carousel-control" href="#myCarousel-2" data-slide="prev"> <span class="glyphicon glyphicon-chevron-left"></span> </a>
                    <a class="right carousel-control" href="#myCarousel-2" data-slide="next"> <span class="glyphicon glyphicon-chevron-right"></span> </a>
                </div>

            </div>
            <!-- end well-->
            @else
                <img src="{{ $content[$post->id][0] }}" class="img-responsive" alt="img">
                @endif
                @endif
                        <!-- // Gallery post -->

                <!-- Video post -->
                @if($post->post_type == 'video' && $content_other[$post->id] != '')
                    <em><video controls="controls" width="100%"><source src="{{ $content_other[$post->id] }}" type="video/mp4"></video></em>
                    @endif
                            <!-- // Video post -->

                    <!-- Audio post -->
                    @if($post->post_type == 'audio' && $content_other[$post->id] != '')
                        <em><audio src="{{ $content_other[$post->id] }}" controls="controls"></audio></em>
                        @endif
                                <!-- // Audio post -->


                        <!-- File post -->
                        @if($post->post_type == 'other' && $content_other[$post->id] != '')
                            <a target="_blank" href="{{ $content_other[$post->id] }}"><i class="fa fa-file fa-3x"></i></a>
                            @endif
                                    <!-- // File post -->
        </div>

        <h1>{{ $post->title }}</h1>


        <div class="">
            @if($post->post_type == 'album' && $album[$post->id] != '')
            @foreach($album[$post->id] as $item)
                <img src="{{ asset($item) }}" width="100px">
                @endforeach
                @endif

        </div>
        <hr>
        <p class="lead"> {!! htmlspecialchars_decode($post->body) !!}</p>
        <hr>

        <div class="panel panel-default">
            <div class="panel-body status">
                <!-- Level 1 comment area -->
                <ul class="comments" id="comments-one-{{ $post->id }}">
                    @foreach($post_comment[$post->id] as $comment)
                        <li id="li-comment-id-{{ $comment->post_comment_id }}">
                            <!-- user image -->
                            <img src="{{ $comment->profile_image }}" alt="{{ $comment->first_name }}" class="online">
                            <!-- // user image -->

                            <!-- user name -->
                            <span class="name">{{ $comment->first_name.' '.$comment->last_name }}</span>
                            <!-- // user name -->

                            <!-- Remove comment -->
                            @if(Auth::user()->id == $comment->user_id)
                            <button class="btn btn-sm btn-info pull-left post-comment-remove-btn" type="button" data-post_id="{{ $post->id }}" data-url="{{ url('post/comment/remove') }}" data-post_comment_id="{{ $comment->post_comment_id }}" >
                                <i class="fa fa-trash" onclick=""></i>
                            </button>
                            @endif
                            <!-- // Remove comment -->

                            <!-- user Comment area -->
                            @if($comment->image != '')
                                <br/>
                                <img src="{{ $comment->image }}" style="width: 100px; height: 50px; padding-left: 39px;">
                                <br/>
                                <br/>
                                <div class="clearfix"></div>
                            @endif
                            @if($comment->description != '')
                                <p style="margin-top: 13px;">{{ $comment->description }}</p>
                                @endif
                                        <!-- // user Comment area -->
                                <a href="javascript:void(0);" onclick="showCommentBox('{{ $comment->post_comment_id }}')" data-post_comment_id="{{ $comment->post_comment_id }}">
                                    <i class="fa fa-reply"></i>
                                    @if($level_2_post_comment[$post->id][$comment->post_comment_id])
                                        {{ count($level_2_post_comment[$post->id][$comment->post_comment_id]) }}
                                    @endif
                                    Replay
                                </a>

                                <!-- level 2 comment area -->
                                <ul class="level-2-comment-box hidden" id="level-2-comment-box-two-{{ $comment->post_comment_id }}">

                                    @if($level_2_post_comment[$post->id][$comment->post_comment_id])
                                        @foreach($level_2_post_comment[$post->id][$comment->post_comment_id] as $comment_level_2)
                                            <li id="li-comment-id-{{ $comment_level_2->post_comment_id }}">
                                                <!-- user image -->
                                                <img src="{{ $comment_level_2->profile_image }}" alt="{{ $comment_level_2->first_name }}" class="online">
                                                <!-- // user image -->

                                                <!-- user name -->
                                                <span class="name">{{ $comment_level_2->first_name.' '.$comment_level_2->last_name }}</span>
                                                <!-- // user name -->

                                                <!-- Remove comment -->
                                                @if(Auth::user()->id == $comment_level_2->user_id)
                                                <button class="btn btn-sm btn-info pull-left post-comment-remove-btn" type="button" data-post_id="{{ $post->id }}" data-url="{{ url('post/comment/remove') }}" data-post_comment_id="{{ $comment_level_2->post_comment_id }}" >
                                                    <i class="fa fa-trash" onclick=""></i>
                                                </button>
                                                @endif
                                                <!-- // Remove comment -->

                                                <!-- user Comment area -->
                                                @if($comment_level_2->image != '')
                                                    <br/>
                                                    <img src="{{ $comment_level_2->image }}" style="width: 100px; height: 50px; padding-left: 39px;">
                                                    <br/>
                                                    <br/>
                                                    <div class="clearfix"></div>
                                                @endif
                                                @if($comment_level_2->description != '')
                                                    <p style="margin-top: 13px;">{{ $comment_level_2->description }}</p>
                                                @endif
                                            </li>
                                            @endforeach
                                            @endif

                                                    <!-- level 2 add comment area -->
                                            <li>
                                                <form id="post-comment-form-two-{{ $comment->post_comment_id }}" method="post">

                                                    {{ csrf_field() }}
                                                    <img src="{{ Auth::user()->profile_image }}" alt="img" class="online">
                                                    <input type="text" name="description" id="description-two-{{ $comment->post_comment_id }}" class="form-control" placeholder="Post your comment...">
                                                    <br/>
                                                    <a data-input="comment-picture-two-{{ $comment->post_comment_id }}" class="btn btn-primary lfm-comment-picture">
                                                        <i class="fa fa-picture-o"></i>
                                                    </a>
                                                    <!-- <a data-input="comment-video-{{ $comment->post_comment_id }}" class="btn btn-primary lfm-comment-video">
                                                                    <i class="fa fa-camera"></i>
                                                                </a> -->
                                                    <button type="button" data-unique_id="two-{{ $comment->post_comment_id }}" data-form_id="post-comment-form-two-{{ $comment->post_comment_id }}" data-url="{{ url('post/comments') }}" data-remove_url="{{ url('/post/comment/remove') }}" class="btn btn-info btn-sm post-comment-btn" >Save</button>
                                                    <br/>
                                                    <input id="comment-picture-two-{{ $comment->post_comment_id }}" class="form-control" type="hidden" name="comment_picture">
                                                    <input id="comment-video-two-{{ $comment->post_comment_id }}" class="form-control" type="hidden" name="comment_video">
                                                    <input id="comment-level-two-{{ $comment->post_comment_id }}" value="2" name="level_id" type="hidden">
                                                    <input id="post-id-two-{{ $comment->post_comment_id }}" value="{{ $post->id }}" name="post_id" type="hidden">
                                                    <input type="hidden" id="parent-id-two-{{ $comment->post_comment_id }}" name="parent_id" value="{{ $comment->post_comment_id }}">

                                                </form>
                                            </li>
                                            <!-- // level 2 add comment area -->

                                </ul>
                                <!-- // level 2 comment area -->
                        </li>
                        @endforeach

                                <!-- Add new comment area -->
                        <li>
                            <form id="post-comment-form-{{ $post->id }}" method="post">

                                {{ csrf_field() }}
                                <img src="{{ Auth::user()->profile_image }}" alt="img" class="online">
                                <input type="text" name="description" id="description-one-{{ $post->id }}" class="form-control" placeholder="Post your comment...">
                                <br/>
                                <a data-input="comment-picture-one-{{ $post->id }}" class="btn btn-primary lfm-comment-picture">
                                    <i class="fa fa-picture-o"></i>
                                </a>
                                <!-- <a data-input="comment-video-{{ $post->id }}" class="btn btn-primary lfm-comment-video">
                                                    <i class="fa fa-camera"></i>
                                                </a> -->
                                <button type="button" data-unique_id="one-{{ $post->id }}" data-form_id="post-comment-form-{{ $post->id }}" data-url="{{ url('post/comments') }}" data-remove_url="{{ url('/post/comment/remove') }}" class="btn btn-info btn-sm post-comment-btn" >Save</button>
                                <br/>
                                <input id="comment-picture-one-{{ $post->id }}"  class="form-control" type="hidden" name="comment_picture">
                                <input id="comment-video-one-{{ $post->id }}" class="form-control" type="hidden" name="comment_video">
                                <input id="comment-level-one-{{ $post->id }}" value="1" name="level_id" type="hidden">
                                <input id="post-id-one-{{ $post->id }}" value="{{ $post->id }}" name="post_id" type="hidden">
                                <input type="hidden" id="parent-id-one-{{ $post->id }}" name="parent_id" value="0">

                            </form>
                        </li>
                        <!-- // Add new comment area -->
                </ul>
                <!-- // Level 1 comment area -->
            </div>
        </div>



        {!! Form::open(['method' => 'DELETE', 'route' => ['posts.destroy', $post->id] ]) !!}
        <a href="{{ url()->previous() }}" class="btn btn-primary">Back</a>
        @can('Edit Post')
        <a href="{{ route('posts.edit', $post->id) }}" class="btn btn-info" role="button">Edit</a>
        @endcan
        @can('Delete Post')
        {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
        @endcan
        {!! Form::close() !!}



    </div>
    {{ csrf_field() }}
</div>
<!-- END MAIN CONTENT -->


<style>
    .btn.btn-sm.btn-info.pull-left.post-comment-remove-btn {
        float: right !important;
        margin-top: -22px;
        background-color: #b40606;
        border-color: #950a0a;
    }
</style>

@endsection