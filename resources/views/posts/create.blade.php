@extends('layouts.new_dashboard_app')

@section('title', '| Create New News')

@section('content')

    <div id="content-container">
        <!--Page content-->
        <!--===================================================-->
        <div id="page-content">
            <div style="margin-top:20px;" class="row">
                <div class="col-md-12">

                    <div class="panel">
                        <!--Page Title-->
                        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->

                        <div id="page-title">
                            <h1 class="page-header text-overflow">Edit a News</h1>
                        </div>
                        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                        <!--End page title-->


                        <!--Breadcrumb-->
                        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                        <ol class="breadcrumb">
                            <li><a href="#"><i class="demo-pli-home"></i></a></li>
                            <li><a href="#">News</a></li>
                            <li class="active">Create</li>
                        </ol>
                        <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                        <!--End breadcrumb-->

                        <!--Block Styled Form -->
                        <!--===================================================-->
                        <form action="{{ route('posts.store') }}" method="post" id="post-form" class="form" >
                            {{ csrf_field() }}
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-sm-6 col-md-6 col-lg-6">
                                        <div class="form-group">
                                            <label class="control-label">Title</label>
                                            <input required type="text" name="title" value="{{ old('title') }}" class="form-control">
                                            @if ($errors->has('title'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('title') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-sm-12 col-md-6 col-lg-6">
                                        <div class="form-group">
                                            <label>Featured Image</label>
                                            <div class="input-group">
                                                    <span class="input-group-btn">
                                                        <a id="lfm" data-input="thumbnail-post-1"  data-preview="holder-thumb-1"  class="btn btn-primary file-manager">
                                                            <i class="fa fa-picture-o"></i> Choose
                                                        </a>
                                                    </span>
                                                <input id="thumbnail-post-1" required class="form-control" type="text" name="thumb_image">
                                                @if ($errors->has('thumb_image'))
                                                    <span class="help-block">
                                                <strong>{{ $errors->first('thumb_image') }}</strong>
                                            </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-6 col-md-6 col-lg-6">
                                        <div class="form-group">
                                            <ul class="dropdown-menu" role="menu">
                                                {{--<li><a href="javascript:rename('{{ $folder_name }}')"><i class="fa fa-edit fa-fw"></i> {{ Lang::get('laravel-filemanager::lfm.menu-rename') }}</a></li>--}}
                                                <li><a href="javascript:choose('Shared Files ')">Shared Files <i class="fa fa-check"></i> {{ Lang::get('laravel-filemanager::lfm.menu-choose') }}</a></li>
                                                {{--<li><a href="javascript:trash('{{ $folder_name }}')"><i class="fa fa-trash fa-fw"></i> {{ Lang::get('laravel-filemanager::lfm.menu-delete') }}</a></li>--}}
                                            </ul>
                                            <label>Post Type</label>
                                            <select required class="form-control" name="post_type" id="post-type">
                                                <option value="">Select</option>
                                                <option value="gallery">Gallery</option>
                                                <option value="album">Album</option>
                                                {{--<option value="video">Video</option>--}}
                                                {{--<option value="audio">Audio</option>--}}
                                                {{--<option value="other">Other</option>--}}
                                            </select>
                                            @if ($errors->has('post_type'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('post_type') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-12 col-md-6 col-lg-6">
                                        <!-- Gallery  -->
                                        <div class="form-group hidden" id="gallery-div-box">
                                            <table class="table table-striped">
                                                <thead>
                                                <tr>
                                                    <th>File</th>
                                                    <th>Image</th>
                                                    <th>Remove</th>
                                                </tr>
                                                </thead>
                                                <tfoot>
                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td style="text-align: center;">
                                                        <a class="btn btn-icon add-tooltip hidden" id="add-new-gallery-btn" data-toggle="tooltip" onclick="addNewGallery()"><i class="icon-2x pli-add"></i></a>
                                                    </td>
                                                </tr>
                                                </tfoot>
                                                <tbody class="gallery-div">
                                                <tr>
                                                    <td style="width:100%; padding-left:0px;">
                                                        <div class="input-group">
                                                            <span class="input-group-btn">
                                                                <a id="lfm" data-input="thumbnail-1" data-preview="holder-1"  class="btn btn-primary file-manager">
                                                                    <i class="fa fa-picture-o"></i> Choose
                                                                </a>
                                                            </span>
                                                            <input id="thumbnail-1"  class="form-control" type="text" name="gallery_image[]">
                                                        </div>
                                                    </td>
                                                    <td class="gallery-thmb-img">
                                                        <div style="overflow: hidden; width: 80px; height:30px;">
                                                            <img class="img-responsive" style="width:100%; height:auto; display: flex; justify-content: center; // align horizontal align-items: center;" id="holder-1">
                                                        </div>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- // Gallery -->

                                        <!-- Video  -->
                                        <div class="form-group hidden" id="video-div-box">
                                            <label>Video</label>
                                            <div class="input-group">
                                                <span class="input-group-btn">
                                                    <a id="lfm-video" data-input="video-1" class="btn btn-primary">
                                                        <i class="fa fa-picture-o"></i> Choose
                                                    </a>
                                                </span>
                                                <input id="video-1" class="form-control" type="text" name="video">
                                            </div>
                                        </div>
                                        <!-- // Video -->

                                        <!-- Audio  -->
                                        <div class="form-group hidden" id="audio-div-box">
                                            <label>Audio</label>
                                            <div class="input-group">
                                                <span class="input-group-btn">
                                                    <a id="lfm-audio" data-input="audio-1" class="btn btn-primary">
                                                        <i class="fa fa-picture-o"></i> Choose
                                                    </a>
                                                </span>
                                                <input id="audio-1" class="form-control" type="text" name="audio">
                                            </div>
                                        </div>
                                        <!-- // Audio -->

                                        <!-- File  -->
                                        <div class="form-group hidden" id="other-div-box">
                                            <label>Other</label>
                                            <div class="input-group">
                                                <span class="input-group-btn">
                                                    <a id="lfm-other" data-input="other-1" class="btn btn-primary">
                                                        <i class="fa fa-picture-o"></i> Choose
                                                    </a>
                                                </span>
                                                <input id="other-1" class="form-control" type="text" name="other">
                                            </div>
                                        </div>
                                        <!-- // File -->

                                        <!-- Album  -->
                                        <div class="form-group hidden" id="album-div-box">
                                            <label>Album (Select one image form album)</label>
                                            <div class="input-group">
                                                <span class="input-group-btn">
                                                    <a id="lfm-album" data-input="album-1" class="btn btn-primary">
                                                        <i class="fa fa-picture-o"></i> Choose
                                                    </a>
                                                </span>
                                                <input id="album-1" class="form-control" type="text" name="album">
                                            </div>
                                        </div>
                                        <!-- // Album -->

                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                        <div class="form-group">
                                            <label>Description</label>
                                            <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
                                            <textarea name="description" class="form-control my-editor"></textarea>
                                            <script>
                                                var editor_config = {
                                                    path_absolute : "/",
                                                    selector: "textarea.my-editor",
                                                    plugins: [
                                                        "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                                                        "searchreplace wordcount visualblocks visualchars code fullscreen",
                                                        "insertdatetime media nonbreaking save table contextmenu directionality",
                                                        "emoticons template paste textcolor colorpicker textpattern"
                                                    ],
                                                    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
                                                    relative_urls: false,
                                                    file_browser_callback : function(field_name, url, type, win) {
                                                        var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
                                                        var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

                                                        var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
                                                        if (type == 'image') {
                                                            cmsURL = cmsURL + "&type=Images";
                                                        } else {
                                                            cmsURL = cmsURL + "&type=Files";
                                                        }

                                                        tinyMCE.activeEditor.windowManager.open({
                                                            file : cmsURL,
                                                            title : 'Filemanager',
                                                            width : x * 0.8,
                                                            height : y * 0.8,
                                                            resizable : "yes",
                                                            close_previous : "no"
                                                        });
                                                    }
                                                };

                                                tinymce.init(editor_config);
                                            </script>
                                            @if ($errors->has('description'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('description') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-6 col-md-6 col-lg-6">
                                        <div class="form-group">
                                            <div style="width:100%;" class="input-group date">
                                                <label class="control-label">Publish Date</label>
                                                <input  type="text" name="publish_date" placeholder="Publish Date" class="form-control"  value="{!! date('Y-m-d') !!}" data-dateformat='yy-mm-dd' required>
                                                <input type="hidden" name="created_by" value="{{ Auth::user()->id }}" id="datepicker" class="datepicker form-control">
                                            </div>
                                            <small class="text-muted">Pickup a date</small>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-6 col-lg-6">
                                        <div class="form-group">
                                            <label>Privacy</label>
                                            <select class="form-control" required name="privacy" id="privacy">
                                                @foreach($privacy  as $privacy_item)
                                                    <option value="{{ $privacy_item->code }}">{{ $privacy_item->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-12 col-md-6 col-lg-6 col-md-offset-6 col-lg-offset-6">
                                        <div id="user-group-div" class="form-group hidden">
                                            <label style="width:100%;" for="id_label_multiple">
                                                <select multiple="multiple" style="width:100%" class="js-example-basic-multiple js-states form-control" name="group[]" id="user-group id_label_multiple">
                                                    @foreach($user_groups  as $user_group)
                                                        <option value="{{ $user_group->group_id }}">{{ $user_group->name }}</option>
                                                    @endforeach
                                                </select>
                                            </label>
                                        </div>

                                        @role('admin')
                                        <div id="office-div" class="form-group hidden">
                                            <label style="width:100%;" for="id_label_multiple">
                                                <select multiple="multiple" style="width:100%" class="js-example-basic-multiple js-states form-control" name="office[]"  id="office id_label_multiple">
                                                    @foreach($offices  as $office)
                                                        <option value="{{ $office->office_id }}">{{ $office->name }}</option>
                                                    @endforeach
                                                </select>
                                            </label>
                                        </div>
                                        @endrole
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-12 col-md-16 col-lg-6">
                                        @can('News add to main slider')
                                        <div class="checkbox form-group">
                                            <input id="demo-checkbox-1" class="magic-checkbox" type="checkbox" name="acceptTerms" data-bv-field="acceptTerms">
                                            <label for="demo-checkbox-1"> Post add to main slider</label>
                                        </div>
                                        @endcan
                                    </div>
                                </div>
                            </div>

                            <div class="panel-footer text-right">
                                <div class="form-group">
                                    {{ Form::submit('Create Post', array('class' => 'btn btn-success')) }}
                                </div>
                            </div>
                        </form>
                        <!--===================================================-->
                        <!--End Block Styled Form -->
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('page-js')

    <script>
        //============================================  post type ==========================================================
        $('#post-type').change(function () {
            postType();
        });

    function postType() {
        $('#gallery-div-box').addClass('hidden');
        $('#video-div-box').addClass('hidden');
        $('#album-div-box').addClass('hidden');
        $('#audio-div-box').addClass('hidden');
        $('#other-div-box').addClass('hidden');
        var post_type = $('#post-type').val();
        if(post_type == 'gallery')
        {
            $('#gallery-div-box').removeClass('hidden');
        }
        if(post_type == 'video')
        {
            $('#video-div-box').removeClass('hidden');
        }
        if(post_type == 'audio')
        {
            $('#audio-div-box').removeClass('hidden');
        }
        if(post_type == 'other')
        {
            $('#other-div-box').removeClass('hidden');
        }
        if(post_type == 'album')
        {
            $('#album-div-box').removeClass('hidden');
        }
    }
    //======================================== End  post type ==========================================================


        //======================================== Gallery Add =============================================================
        thumbChange();
        $("#thumbnail-1").bind("change paste keyup", function() {
            thumbChange();
        });

        function thumbChange() {
            var str_value = $('#thumbnail-1').val();
            var add_new_btn = $('#add-new-gallery-btn');
            add_new_btn.addClass('hidden');
            if(str_value != '')
            {
                add_new_btn.removeClass('hidden');
            }
        }
        //======================================== End Gallery Add =========================================================


        var row_id = '{{ $row_id }}';
        function addNewGallery() {

            var html = '';
            html += '<tr id="image-row' + row_id  + '">';
            html += '<td style="width:100%; padding-left: 0px;">';
            html += '<div class="input-group">';
            html += '<span class="input-group-btn">';
            html += '<a id="lfm'+row_id+'" data-input="thumbnail-'+row_id+'" data-preview="holder-'+row_id+'"  class="btn btn-primary file-manager">';
            html += '<i class="fa fa-picture-o"></i> Choose';
            html += '</a>';
            html += '</span>';
            html += '<input id="thumbnail-'+row_id+'" class="form-control" type="text" name="gallery_image[]">';
            html += '</div>';
            html += '</td>';
            html += '<td class="gallery-thmb-img">';
            html += '<div style="overflow: hidden; width: 80px; height:30px;">';
            html += '<img class="img-responsive" id="holder-'+row_id+'" style="width:100%; height:auto; display: flex; justify-content: center; align horizontal align-items: center;">';
            html += '</div>';
            html += '</td>';
            html += '<td style="padding-left: 10px; text-align: center;"><a class="btn btn-icon add-tooltip" id="add-new-gallery-btn" data-toggle="tooltip" onclick="$(\'#image-row' + row_id  + ', .tooltip\').remove();"><i class="icon-2x pli-close"></i></a></td>';
            html += '</tr>';

            row_id++;

            $('.gallery-div').append(html);
        }

        $("#post-form").validate();

        // DO NOT REMOVE : GLOBAL FUNCTIONS!
        $(document).ready(function() {
            pageSetUp();
        });

        function choose(item_name) {
            performLfmRequest('jsonitems', {working_dir: $('#working_dir').val() + '/' + item_name, type: 'Images'}, 'html')
                    .done(function (data) {
                        var response = JSON.parse(data);
                        var result_html = $(response.html);
                        var urls = [];
                        result_html.find('[data-url]').each(function(imageDiv){
                            urls.push($(this).attr('data-url'));
                        });
                        if (window.opener) {
                            window.opener.SetUrlsArray(urls);
                            window.close();
                        }
                    });
        }

    </script>

    <script>
        $(document).ready(function() {
            $('.js-example-basic-multiple').select2();
        });
    </script>

@endsection