@extends('layouts.dashboard_app')

@section('title', '| Users')

@section('content')
        <!-- RIBBON -->
<div id="ribbon">

    <span class="ribbon-button-alignment">
        <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
            <i class="fa fa-refresh"></i>
        </span>
    </span>

    <!-- breadcrumb -->
    <ol class="breadcrumb">
        <li>Home</li><li>Events</li><li> Calendar</li>
    </ol>
    <!-- end breadcrumb -->

    <!-- You can also add more buttons to the
    ribbon for further usability

    Example below:

    <span class="ribbon-button-alignment pull-right">
    <span id="search" class="btn btn-ribbon hidden-xs" data-title="search"><i class="fa-grid"></i> Change Grid</span>
    <span id="add" class="btn btn-ribbon hidden-xs" data-title="add"><i class="fa-plus"></i> Add</span>
    <span id="search" class="btn btn-ribbon" data-title="search"><i class="fa-search"></i> <span class="hidden-mobile">Search</span></span>
    </span> -->

</div>
<!-- END RIBBON -->

<!-- MAIN CONTENT -->
<div id="content">

    <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
            <h1 class="page-title txt-color-blueDark"><i class="fa fa-calendar fa-fw "></i>
                Calendar
                <span>>
                    Events
                </span>
            </h1>
        </div>
        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
            <!-- Back code here -->
            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                <button type="button" class="btn btn-primary pull-right" onclick="window.history.back()">< Back</button>
            </div>
        </div>
    </div>
    <!-- row -->

    <div class="row">

        @can('Events Edit')
        <div class="col-sm-12 col-md-12 col-lg-3">
            <!-- new widget -->
            <div class="jarviswidget jarviswidget-color-blueDark">
                <header>
                    <h2> Events </h2>
                </header>

                <!-- widget div-->
                <div>

                    <div class="widget-body">
                        <!-- content goes here -->

                        <form id="update-event-form" action="{{ url('events/edit-event') }}" method="post" >
                            {{ csrf_field() }}
                            <input type="hidden" name="event_id" value="{{ $event->event_id }}">
                            <fieldset>

                                <div class="form-group">
                                    <label>Select Event Icon</label>
                                    <div class="btn-group btn-group-sm btn-group-justified" data-toggle="buttons">
                                        <label class="btn btn-default  @if($event->icon == 'fa-info') active @endif">
                                            <input type="radio" name="iconselect" id="icon-1" value="fa-info" @if($event->icon == 'fa-info') checked @endif>
                                            <i class="fa fa-info text-muted"></i> </label>
                                        <label class="btn btn-default @if($event->icon == 'fa-warning') active @endif">
                                            <input type="radio" name="iconselect" id="icon-2" value="fa-warning" @if($event->icon == 'fa-warning') checked @endif>
                                            <i class="fa fa-warning text-muted"></i> </label>
                                        <label class="btn btn-default @if($event->icon == 'fa-check') active @endif">
                                            <input type="radio" name="iconselect" id="icon-3" value="fa-check" @if($event->icon == 'fa-check') checked @endif>
                                            <i class="fa fa-check text-muted"></i> </label>
                                        <label class="btn btn-default @if($event->icon == 'fa-user') active @endif">
                                            <input type="radio" name="iconselect" id="icon-4" value="fa-user" @if($event->icon == 'fa-user') checked @endif>
                                            <i class="fa fa-user text-muted"></i> </label>
                                        <label class="btn btn-default @if($event->icon == 'fa-lock') active @endif">
                                            <input type="radio" name="iconselect" id="icon-5" value="fa-lock" @if($event->icon == 'fa-lock') checked @endif>
                                            <i class="fa fa-lock text-muted"></i> </label>
                                            <label class="btn btn-default @if($event->icon == 'fa-clock-o') active @endif">
                                            <input type="radio" name="iconselect" id="icon-6" value="fa-clock-o" @if($event->icon == 'fa-clock-o') checked @endif>
                                            <i class="fa fa-clock-o text-muted"></i> </label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label>Event Title</label>
                                    <input class="form-control"  id="title" name="title" maxlength="40" type="text" placeholder="Event Title" value="{{ $event->name }}" required>
                                    @if ($errors->has('title'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('title') }}</strong>
                                            </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>Image</label>
                                    <div class="input-group">
                                                    <span class="input-group-btn">
                                                        <a id="lfm" data-input="thumbnail-1" data-preview="holder-1"  class="btn btn-primary file-manager">
                                                            <i class="fa fa-picture-o"></i> Choose
                                                        </a>
                                                    </span>
                                        <input id="thumbnail-1" value="{{ $event->event_image }}" class="form-control" type="text" name="event_image" required>
                                        @if ($errors->has('event_image'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('event_image') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label>Event Description</label>
                                    <textarea class="form-control" placeholder="Please be brief" rows="3" name="description" maxlength="500" id="description" required>{{ $event->description }}</textarea>
                                    @if ($errors->has('description'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('description') }}</strong>
                                            </span>
                                    @endif
                                    <p class="note">Maxlength is set to 500 characters</p>
                                </div>

                                <div class="form-group">
                                    <label>Start Date</label>
                                    <input class="datetimepicker form-control"  data-date="@php date('Y-m-d H:i:s') @endphp" value="{{ $event->start_date }}"  id="start_date" name="start_date" type="text" placeholder="Start date" required>
                                    @if ($errors->has('start_date'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('start_date') }}</strong>
                                            </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>End Date</label>
                                    <input class="datetimepicker form-control" id="end_date" data-date="@php date('Y-m-d H:i:s') @endphp" value="{{ $event->end_date }}"   name="end_date" type="text" placeholder="End date" required>
                                    @if ($errors->has('end_date'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('end_date') }}</strong>
                                            </span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label>Privacy</label>
                                    <select class="form-control" name="privacy" id="privacy">
                                        @foreach($privacy  as $privacy_item)
                                            <option @if($event->privacy == $privacy_item->code) selected @endif value="{{ $privacy_item->code }}">{{ $privacy_item->name }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div id="user-group-div" class="form-group hidden">
                                    <label>User Groups</label>
                                    <select style="width:100%" class="select2" name="group[]" multiple id="user-group">
                                        @foreach($user_groups  as $user_group)
                                            <option @if(in_array($user_group->group_id, $group_array)) selected @endif value="{{ $user_group->group_id }}">{{ $user_group->name }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                @role('admin')
                                <div id="office-div" class="form-group hidden">
                                    <label>Location</label>
                                    <select style="width:100%" class="select2" name="office[]" multiple id="office">
                                        @foreach($offices  as $office)
                                            <option @if(in_array($office->office_id, $office_array)) selected @endif value="{{ $office->office_id }}">{{ $office->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                @endrole

                                <div class="form-group">
                                    <label>Select Event Color</label>
                                    <div class="btn-group btn-group-justified btn-select-tick" data-toggle="buttons">
                                        <label class="btn bg-color-darken @if($event->color == 'bg-color-darken txt-color-white') active @endif">
                                            <input type="radio" name="priority" id="option1" value="bg-color-darken txt-color-white" @if($event->color == 'bg-color-darken txt-color-white') checked @endif>
                                            <i class="fa fa-check txt-color-white"></i> </label>
                                        <label class="btn bg-color-blue @if($event->color == 'bg-color-blue txt-color-white') active @endif">
                                            <input type="radio" name="priority" id="option2" value="bg-color-blue txt-color-white" @if($event->color == 'bg-color-blue txt-color-white') checked @endif>
                                            <i class="fa fa-check txt-color-white"></i> </label>
                                        <label class="btn bg-color-orange @if($event->color == 'bg-color-orange txt-color-white') active @endif">
                                            <input type="radio" name="priority" id="option3" value="bg-color-orange txt-color-white" @if($event->color == 'bg-color-orange txt-color-white') checked @endif>
                                            <i class="fa fa-check txt-color-white"></i> </label>
                                        <label class="btn bg-color-greenLight @if($event->color == 'bg-color-greenLight txt-color-white') active @endif">
                                            <input type="radio" name="priority" id="option4" value="bg-color-greenLight txt-color-white" @if($event->color == 'bg-color-greenLight txt-color-white') checked @endif>
                                            <i class="fa fa-check txt-color-white"></i> </label>
                                        <label class="btn bg-color-blueLight @if($event->color == 'bg-color-blueLight txt-color-white') active @endif">
                                            <input type="radio" name="priority" id="option5" value="bg-color-blueLight txt-color-white" @if($event->color == 'bg-color-blueLight txt-color-white') checked @endif>
                                            <i class="fa fa-check txt-color-white"></i> </label>
                                        <label class="btn bg-color-red @if($event->color == 'bg-color-red txt-color-white') active @endif">
                                            <input type="radio" name="priority" id="option6" value="bg-color-red txt-color-white" @if($event->color == 'bg-color-red txt-color-white') checked @endif>
                                            <i class="fa fa-check txt-color-white"></i> </label>
                                    </div>
                                </div>

                                @can('Events add to main slider')
                                <div class="form-group">
                                    <input type="checkbox" @if($event->attach_to_main_slider == 1) checked @endif id="attach-to-main-slider" name="attach_to_main_slider" value="1">
                                    <label for="attach-to-main-slider">Events add to main slider</label>
                                </div>
                                @endcan

                                <div class="form-group">
                                    <label>Status</label>
                                    <select class="form-control" name="status" id="status">
                                        <option value="1" @if($event->status == 1) selected @endif>Enable</option>
                                        <option value="0" @if($event->status == 2) selected @endif>Disable</option>
                                    </select>
                                </div>

                            </fieldset>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-12">
                                        <button  class="btn btn-default" type="submit" id="add-event" >
                                            Update Event
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>

                        <!-- end content -->
                    </div>

                </div>
                <!-- end widget div -->
            </div>
            <!-- end widget -->
        </div>
        @endcan

    </div>

    <!-- end row -->

</div>
<!-- END MAIN CONTENT -->
@endsection

@section('page-js')
        <!-- PAGE RELATED PLUGIN(S) -->
<script src="{{ asset('asset/js/plugin/moment/moment.min.js') }}"></script>
<script src="{{ asset('asset/js/plugin/fullcalendar/fullcalendar.min.js') }}"></script>

<script>

    privacyChange();
    // select privacy and change div
    $('#privacy').change(function () {
        privacyChange();
    });

    // form load and privacy sect to only me
    function privacyChange() {
        var privacy_code = $('#privacy').val();
        $('#user-group-div').addClass('hidden');
        $('#office-div').addClass('hidden');
        if(privacy_code == 'office')
        {
            $('#office-div').removeClass('hidden');
        }
        if(privacy_code == 'group')
        {
            $('#user-group-div').removeClass('hidden');
        }
    }



    $("#update-event-form").validate();

    $("#start_date").datetimepicker({
        format: 'yyyy-mm-dd hh:ii',
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
    });
    $("#end_date").datetimepicker({
        format: 'yyyy-mm-dd hh:ii',
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
    });

    // DO NOT REMOVE : GLOBAL FUNCTIONS!

    $(document).ready(function() {

        pageSetUp();


        "use strict";

        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();

        var hdr = {
            left: 'title',
            center: 'month,agendaWeek,agendaDay',
            right: 'prev,today,next'
        };



        /* initialize the calendar
         -----------------------------------------------------------------*/

        /* hide default buttons */
        $('.fc-right, .fc-center').hide();


        $('#calendar-buttons #btn-prev').click(function () {
            $('.fc-prev-button').click();
            return false;
        });

        $('#calendar-buttons #btn-next').click(function () {
            $('.fc-next-button').click();
            return false;
        });

        $('#calendar-buttons #btn-today').click(function () {
            $('.fc-today-button').click();
            return false;
        });

        $('#mt').click(function () {
            $('#calendar').fullCalendar('changeView', 'month');
        });

        $('#ag').click(function () {
            $('#calendar').fullCalendar('changeView', 'agendaWeek');
        });

        $('#td').click(function () {
            $('#calendar').fullCalendar('changeView', 'agendaDay');
        });

    })

</script>
@endsection