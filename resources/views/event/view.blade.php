@extends('layouts.dashboard_app')

@section('title', '| Users')


@section('content')
        <!-- RIBBON -->
<div id="ribbon">

    <span class="ribbon-button-alignment">
        <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
            <i class="fa fa-refresh"></i>
        </span>
    </span>

    <!-- breadcrumb -->
    <ol class="breadcrumb">
        <li>Home</li><li>View</li>
    </ol>
    <!-- end breadcrumb -->

    <!-- You can also add more buttons to the
    ribbon for further usability

    Example below:

    <span class="ribbon-button-alignment pull-right">
    <span id="search" class="btn btn-ribbon hidden-xs" data-title="search"><i class="fa-grid"></i> Change Grid</span>
    <span id="add" class="btn btn-ribbon hidden-xs" data-title="add"><i class="fa-plus"></i> Add</span>
    <span id="search" class="btn btn-ribbon" data-title="search"><i class="fa-search"></i> <span class="hidden-mobile">Search</span></span>
    </span> -->

</div>
<!-- END RIBBON -->

<!-- MAIN CONTENT -->
<div id="content">

    <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-4">
            <h1 class="page-title txt-color-blueDark"><i class="fa fa-calendar fa-fw "></i>
                Events
                <span>>
                    View
                </span>
            </h1>
        </div>
        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-8">
            <!-- Back code here -->
            @can('Events List')
            <a href="{{ url('events/list-event') }}" class="btn btn-primary pull-right">List Event</a>
            @endcan
        </div>
    </div>
    <!-- row -->

    <div class="row">

        @can('Events View')
        <div class="col-sm-12 col-md-12 col-lg-3">
            <!-- new widget -->
            <div class="jarviswidget jarviswidget-color-blueDark">
                <header>
                    <h2> Events </h2>
                </header>

                <!-- widget div-->
                <div>

                    <div class="widget-body">
                        <!-- content goes here -->

                        <div class="row">
                            <div class="col-md-4">
                                <img src="{{ $event->event_image }}" class="img-responsive" alt="img">
                                <ul class="list-inline padding-10">
                                    <li>
                                        <i class="fa fa-calendar"></i>
                                        <a href="javascript:void(0);"> {{ $event->start_date }} </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-8 padding-left-0">
                                <h3 class="margin-top-0"><a href="{{ url('events/edit/'.$event->event_id) }}"> {{ $event->name }}</a><br><small class="font-xs"><i>Published by <a href="javascript:void(0);">{{ $event->first_name.' '.$event->last_name }}</a></i></small></h3>
                                <p>
                                    <i class="fa {{ $event->icon }} text-muted"></i> </label> {{ $event->description }}
                                </p>
                                <a class="btn btn-warning" href="{{ url('events/edit/'.$event->event_id) }}"> Edit </a>
                            </div>
                        </div>

                        <!-- end content -->
                    </div>

                </div>
                <!-- end widget div -->
            </div>
            <!-- end widget -->
        </div>
        @endcan

    </div>

    <!-- end row -->

</div>
<!-- END MAIN CONTENT -->
@endsection

@section('page-js')
        <!-- PAGE RELATED PLUGIN(S) -->
<script src="{{ asset('asset/js/plugin/moment/moment.min.js') }}"></script>
<script src="{{ asset('asset/js/plugin/fullcalendar/fullcalendar.min.js') }}"></script>

<script>

    privacyChange();
    // select privacy and change div
    $('#privacy').change(function () {
        privacyChange();
    });

    // form load and privacy sect to only me
    function privacyChange() {
        var privacy_code = $('#privacy').val();
        $('#user-group-div').addClass('hidden');
        $('#office-div').addClass('hidden');
        if(privacy_code == 'office')
        {
            $('#office-div').removeClass('hidden');
        }
        if(privacy_code == 'group')
        {
            $('#user-group-div').removeClass('hidden');
        }
    }



    $("#add-event-form").validate();

    $("#start_date").datetimepicker({
        format: 'yyyy-mm-dd hh:ii',
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
    });
    $("#end_date").datetimepicker({
        format: 'yyyy-mm-dd hh:ii',
        todayBtn:  1,
        autoclose: 1,
        todayHighlight: 1,
    });

    // DO NOT REMOVE : GLOBAL FUNCTIONS!

    $(document).ready(function() {

        pageSetUp();


        "use strict";

        var date = new Date();
        var d = date.getDate();
        var m = date.getMonth();
        var y = date.getFullYear();

        var hdr = {
            left: 'title',
            center: 'month,agendaWeek,agendaDay',
            right: 'prev,today,next'
        };



        /* initialize the calendar
         -----------------------------------------------------------------*/

        /* hide default buttons */
        $('.fc-right, .fc-center').hide();


        $('#calendar-buttons #btn-prev').click(function () {
            $('.fc-prev-button').click();
            return false;
        });

        $('#calendar-buttons #btn-next').click(function () {
            $('.fc-next-button').click();
            return false;
        });

        $('#calendar-buttons #btn-today').click(function () {
            $('.fc-today-button').click();
            return false;
        });

        $('#mt').click(function () {
            $('#calendar').fullCalendar('changeView', 'month');
        });

        $('#ag').click(function () {
            $('#calendar').fullCalendar('changeView', 'agendaWeek');
        });

        $('#td').click(function () {
            $('#calendar').fullCalendar('changeView', 'agendaDay');
        });

    })

</script>
@endsection