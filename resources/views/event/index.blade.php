@extends('layouts.new_dashboard_app')

@section('title', '| Users')

@section('content')

<!--CONTENT CONTAINER-->
<!--===================================================-->
<div id="content-container">
    <!--Page content-->
    <!--===================================================-->
    <div id="page-content">

        <div style="margin-top: 10px;" class="panel">
            <div id="page-head">
                <!--Page Title-->
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <div id="page-title">
                    <h1 style="color: #4d627b;" class="page-header text-overflow">Add Events</h1>
                </div>
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <!--End page title-->

                <!--Breadcrumb-->
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <ol class="breadcrumb">
                    <li><a href="#"><i style="color: #4d627b;" class="demo-pli-home"></i></a></li>
                    <li><a style="color: #4d627b;" href="#">Calendar</a></li>
                    <li style="color: #4d627b;" class="active">Events</li>
                </ol>
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <!--End breadcrumb-->
            </div>

            <div class="panel-body">
                @can('Events Create')
                <div class="col-lg-3 col-md-3 col-sm-12">
                    <form id="add-event-form" action="{{ url('events/add-new-event') }}" method="post">
                        {{ csrf_field() }}
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="control-label">Event Title</label>
                                        <div class="btn-group btn-group-sm btn-group-justified" data-toggle="buttons">
                                            <label class="btn btn-default active">
                                                <input type="radio" name="iconselect" id="icon-1" value="fa-info" checked>
                                                <i style="transform:rotate(180deg);" class="pli-exclamation text-muted"></i> </label>
                                            <label class="btn btn-default">
                                                <input type="radio" name="iconselect" id="icon-2" value="fa-warning">
                                                <i class="pli-danger text-muted"></i> </label>
                                            <label class="btn btn-default">
                                                <input type="radio" name="iconselect" id="icon-3" value="fa-check">
                                                <i class="pli-yes text-muted"></i> </label>
                                            <label class="btn btn-default">
                                                <input type="radio" name="iconselect" id="icon-4" value="fa-user">
                                                <i class="pli-male-2 text-muted"></i> </label>
                                            <label class="btn btn-default">
                                                <input type="radio" name="iconselect" id="icon-5" value="fa-lock">
                                                <i class="pli-lock-3 text-muted"></i> </label>
                                            <label class="btn btn-default">
                                                <input type="radio" name="iconselect" id="icon-6" value="fa-clock-o">
                                                <i class="pli-clock text-muted"></i> </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="control-label">Event Title</label>
                                        <input class="form-control"  id="title" name="title" maxlength="40" type="text" placeholder="Event Title" required>
                                        @if ($errors->has('title'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('title') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="control-label">Image</label>
                                        <div class="input-group">
                                                    <span class="input-group-btn">
                                                        <a id="lfm" data-input="thumbnail-1" data-preview="holder-1"  class="btn btn-primary file-manager">
                                                            <i class="fa fa-picture-o"></i> Choose
                                                        </a>
                                                    </span>
                                            <input id="thumbnail-1" class="form-control" type="text" name="event_image" required>
                                            @if ($errors->has('event_image'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('event_image') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="control-label">Event Description</label>
                                        <textarea class="form-control" placeholder="Please be brief" rows="3" name="description" maxlength="500" id="description" required></textarea>
                                        @if ($errors->has('description'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('description') }}</strong>
                                            </span>
                                        @endif
                                        <p class="note">Maxlength is set to 500 characters</p>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group required">
                                        <label class="control-label">Start Date</label>
                                        <input class="datetimepicker form-control"  data-date="@php date('Y-m-d H:i:s') @endphp"  id="start_date" name="start_date" type="text" placeholder="Start date" required>
                                        @if ($errors->has('start_date'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('start_date') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="control-label">End Date</label>
                                        <input class="datetimepicker form-control" id="end_date" data-date="@php date('Y-m-d H:i:s') @endphp"   name="end_date" type="text" placeholder="End date" required>
                                        @if ($errors->has('end_date'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('end_date') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label>Privacy</label>
                                        <select class="form-control" name="privacy" id="privacy">
                                            @foreach($privacy  as $privacy_item)
                                                <option value="{{ $privacy_item->code }}">{{ $privacy_item->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <div id="user-group-div" class="form-group hidden">
                                        <label>User Groups</label>
                                        <select style="width:100%" class="select2" name="group[]" multiple id="user-group">
                                            @foreach($user_groups  as $user_group)
                                                <option value="{{ $user_group->group_id }}">{{ $user_group->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    @role('admin')
                                    <div id="office-div" class="form-group hidden">
                                        <label>Location</label>
                                        <select style="width:100%" class="select2" name="office[]" multiple id="office">
                                            @foreach($offices  as $office)
                                                <option value="{{ $office->office_id }}">{{ $office->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    @endrole
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <label class="control-label">Select Event Color</label>
                                        <div class="btn-group btn-group-justified btn-select-tick" data-toggle="buttons">
                                            <label class="btn bg-color-darken btn-dark active">
                                                <input type="radio" name="priority" id="option1" value="bg-color-darken txt-color-white" checked>
                                                <i class="fa fa-check txt-color-white"></i> </label>
                                            <label class="btn btn-info bg-color-blue">
                                                <input type="radio" name="priority" id="option2" value="bg-color-blue txt-color-white">
                                                <i class="fa fa-check txt-color-white"></i> </label>
                                            <label class="btn btn-warning bg-color-orange">
                                                <input type="radio" name="priority" id="option3" value="bg-color-orange txt-color-white">
                                                <i class="fa fa-check txt-color-white"></i> </label>
                                            <label class="btn btn-success bg-color-greenLight">
                                                <input type="radio" name="priority" id="option4" value="bg-color-greenLight txt-color-white">
                                                <i class="fa fa-check txt-color-white"></i> </label>
                                            <label class="btn btn-primary bg-color-blueLight">
                                                <input type="radio" name="priority" id="option5" value="bg-color-blueLight txt-color-white">
                                                <i class="fa fa-check txt-color-white"></i> </label>
                                            <label class="btn btn-danger bg-color-red">
                                                <input type="radio" name="priority" id="option6" value="bg-color-red txt-color-white">
                                                <i class="fa fa-check txt-color-white"></i> </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <input name="attach_to_main_slider" value="1" id="attach-to-main-slider" class="magic-checkbox" type="checkbox">
                                        <label for="attach-to-main-slider">Events add to main slider</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="panel-footer text-right">
                            <button type="submit" id="add-event" class="btn btn-success">Add Event</button>
                        </div>
                    </form>
                </div>
                @endcan

                @can('Events View')
                <div class="col-lg-9 col-md-9 col-sm-12">
                    <div id='demo-calendar'></div>
                </div>
                @endcan
            </div>
        </div>
    </div>
    <!--===================================================-->
    <!--End page content-->
</div>
<!--===================================================-->
<!--END CONTENT CONTAINER-->

@endsection

@section('page-js')

    <script>
        privacyChange();
        // select privacy and change div
        $('#privacy').change(function () {
            privacyChange();
        });

        // form load and privacy sect to only me
        function privacyChange() {
            var privacy_code = $('#privacy').val();
            $('#user-group-div').addClass('hidden');
            $('#office-div').addClass('hidden');
            if(privacy_code == 'office')
            {
                $('#office-div').removeClass('hidden');
            }
            if(privacy_code == 'group')
            {
                $('#user-group-div').removeClass('hidden');
            }
        }

        $("#add-event-form").validate();

        $("#start_date").datetimepicker({
            format: 'yyyy-mm-dd hh:ii',
            todayBtn:  1,
            autoclose: 1,
            todayHighlight: 1,
        });
        $("#end_date").datetimepicker({
            format: 'yyyy-mm-dd hh:ii',
            todayBtn:  1,
            autoclose: 1,
            todayHighlight: 1,
        });

        // DO NOT REMOVE : GLOBAL FUNCTIONS!

        $(document).ready(function() {

            pageSetUp();
            "use strict";
        })
    </script>

    <!--Full Calendar [ OPTIONAL ]-->
    <script src="{{ asset('template_assets/plugins/fullcalendar/lib/moment.min.js') }}"></script>
    <script src="{{ asset('template_assets/plugins/fullcalendar/lib/jquery-ui.custom.min.js') }}"></script>
    <script src="{{ asset('template_assets/plugins/fullcalendar/fullcalendar.min.js') }}"></script>

    <script>
        $(document).on('nifty.ready', function() {
            // Calendar
            // =================================================================
            // Require Full Calendar
            // -----------------------------------------------------------------
            // http://fullcalendar.io/
            // =================================================================

            // initialize the external events
            // -----------------------------------------------------------------
            $('#demo-external-events .fc-event').each(function() {
                // store data so the calendar knows to render an event upon drop
                $(this).data('event', {
                    title: $.trim($(this).text()), // use the element's text as the event title
                    stick: true, // maintain when user navigates (see docs on the renderEvent method)
                    className : $(this).data('class')
                });


                // make the event draggable using jQuery UI
                $(this).draggable({
                    zIndex: 99999,
                    revert: true,      // will cause the event to go back to its
                    revertDuration: 0  //  original position after the drag
                });
            });

            // Initialize the calendar
            // -----------------------------------------------------------------
            $('#demo-calendar').fullCalendar({
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay'
                },
                editable: true,
                droppable: true, // this allows things to be dropped onto the calendar
                drop: function() {
                    // is the "remove after drop" checkbox checked?
                    if ($('#drop-remove').is(':checked')) {
                        // if so, remove the element from the "Draggable Events" list
                        $(this).remove();
                    }
                },
                defaultDate: '2018-05-15',
                eventLimit: true, // allow "more" link when too many events
                events: [
                        @foreach($events as $event_array)
                        @foreach($event_array as $event)
                    {
                        title: '{{ $event->name }}',
                        start: '{{ $event->start_date }}',
                        end: '{{ $event->end_date }}',
                        description: '{{ substr($event->description, 0, 40) }}',
                        className: ["event", "{{ $event->color }}"],
                        icon: '{{ $event->icon }}',

                        @if(Auth::user()->id == $event->created_by)
                        url : "{{ url('events/view/'.$event->event_id) }}"
                        @endif
                    },
                    @endforeach
                    @endforeach
                ],
            });

            // Register form validation
            $("#add-event-form").validate();

        });
    </script>

    <script src="{{ asset('asset/js/script.js') }}"></script>

    <script>
        // START AND FINISH DATE
        $('.datepicker').datepicker({
            dateFormat: 'yy-mm-dd'
        });
        $('.datetimepicker').datetimepicker();

        //========================================================= LOGOUT FORM SUBMIT =====================================
        $('#logout-btn').click(function () {
            // ask verification
            $.SmartMessageBox({
                title: "<i class='fa fa-sign-out txt-color-orangeDark'></i> Logout <span class='txt-color-orangeDark'><strong>Rajith Malinda</strong></span> ?",
                content : "You can improve your security further after logging out by closing this opened browser",
                buttons : '[No][Yes]'
            }, function(ButtonPressed) {
                if (ButtonPressed === "Yes") {
                    $('#logout-form').submit();
                }
                if (ButtonPressed === "No") {
                    // no option code here
                }
            });
            e.preventDefault();
        });
        //========================================================= END LOGOUT FORM SUBMIT =================================
    </script>

@endsection