@extends('layouts.dashboard_app')

@section('title', '| Create New News')

@section('content')


        <!-- RIBBON -->
<div id="ribbon">

    <span class="ribbon-button-alignment">
        <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
            <i class="fa fa-refresh"></i>
        </span>
    </span>

    <!-- breadcrumb -->
    <ol class="breadcrumb">
        <li>Home</li>
        <li>Announcement</li>
        <li>Create</li>
    </ol>
    <!-- end breadcrumb -->

</div>
<!-- END RIBBON -->

<!-- MAIN CONTENT -->
<div id="content">

    <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
            <h1 class="page-title txt-color-blueDark">
                <i class="fa fa-users fa-fw "></i>
                Announcement <span>> Create </span>
            </h1>
        </div>
        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
            <button type="button" class="btn btn-primary pull-right" onclick="window.history.back()">< Back</button>
        </div>
    </div>

    <!-- widget grid -->
    <section id="widget-grid" class="">

        <!-- row -->
        <div class="row">

            <article class="col-sm-12 col-md-12 col-lg-6">

                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget" id="wid-id-1" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                        <h2>New Announcement Create </h2>

                    </header>

                    <!-- widget div-->
                    <div>

                        <!-- widget edit box -->
                        <div class="jarviswidget-editbox">
                            <!-- This area used as dropdown edit box -->

                        </div>
                        <!-- end widget edit box -->

                        <!-- widget content -->
                        <div class="widget-body">

                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <form action="{{ url('announcement/add-new') }}" method="post" id="announcement-form" class="form" >
                                {{ csrf_field() }}

                                        <!-- Title -->
                                <div class="form-group">
                                    <label>Subject</label>
                                    <input type="text" name="subject" class="form-control" required>
                                    @if ($errors->has('subject'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('subject') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <!-- // Title -->

                                <!-- Description -->
                                <div class="form-group">
                                    <label>Description</label>
                                    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
                                    <textarea name="description" class="form-control my-editor"></textarea>
                                    <script>
                                        var editor_config = {
                                            path_absolute : "/",
                                            selector: "textarea.my-editor",
                                            plugins: [
                                                "advlist autolink lists link image charmap print preview hr anchor pagebreak",
                                                "searchreplace wordcount visualblocks visualchars code fullscreen",
                                                "insertdatetime media nonbreaking save table contextmenu directionality",
                                                "emoticons template paste textcolor colorpicker textpattern"
                                            ],
                                            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
                                            relative_urls: false,
                                            file_browser_callback : function(field_name, url, type, win) {
                                                var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
                                                var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

                                                var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
                                                if (type == 'image') {
                                                    cmsURL = cmsURL + "&type=Images";
                                                } else {
                                                    cmsURL = cmsURL + "&type=Files";
                                                }

                                                tinyMCE.activeEditor.windowManager.open({
                                                    file : cmsURL,
                                                    title : 'Filemanager',
                                                    width : x * 0.8,
                                                    height : y * 0.8,
                                                    resizable : "yes",
                                                    close_previous : "no"
                                                });
                                            }
                                        };

                                        tinymce.init(editor_config);
                                    </script>
                                    @if ($errors->has('description'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <!-- Description -->

                                <!-- publish date -->
                                <div class="form-group">
                                    <label>Publish date</label>
                                    <input type="text" name="publish_date"  placeholder="Publish Date" class="datepicker form-control" value="{!! date('Y-m-d') !!}" data-dateformat='dd/mm/yy' required>
                                    <input type="hidden" name="created_by" value="{{ Auth::user()->id }}">
                                    @if ($errors->has('publish_date'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('publish_date') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <!-- // publish date -->

                                <div class="form-group">
                                    <label>Privacy</label>
                                    <select class="form-control" name="privacy" id="privacy">
                                        @foreach($privacy  as $privacy_item)
                                            <option value="{{ $privacy_item->code }}">{{ $privacy_item->name }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div id="user-group-div" class="form-group hidden">
                                    <label>User Groups</label>
                                    <select style="width:100%" class="select2" name="group[]" multiple id="user-group">
                                        @foreach($user_groups  as $user_group)
                                            <option value="{{ $user_group->group_id }}">{{ $user_group->name }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                @role('admin')
                                <div id="office-div" class="form-group hidden">
                                    <label>Location</label>
                                    <select style="width:100%" class="select2" name="office[]" multiple id="office">
                                        @foreach($offices  as $office)
                                            <option value="{{ $office->office_id }}">{{ $office->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                @endrole

                                <div class="form-group">
                                    <label>Status</label>
                                    <select class="form-control" name="status" id="status" required>
                                       <option value="">Select</option>
                                        <option value="1">Enable</option>
                                        <option value="0">Disable</option>
                                    </select>
                                    @if ($errors->has('status'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('status') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    {{ Form::submit('Create Post', array('class' => 'btn btn-success btn-lg btn-block')) }}
                                </div>

                            </form>
                        </div>
                        <!-- end widget content -->

                    </div>
                    <!-- end widget div -->

                </div>
                <!-- end widget -->

            </article>
            <!-- END COL -->
        </div>

    </section>
    <!-- end widget grid -->

</div>
<!-- END MAIN CONTENT -->

@endsection

@section('page-js')

    <script>

        $("#announcement-form").validate();

        // DO NOT REMOVE : GLOBAL FUNCTIONS!
        $(document).ready(function() {
            pageSetUp();
        })

    </script>
@endsection