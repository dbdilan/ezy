@extends('layouts.auth_app')

@section('content')

<link rel="stylesheet" media="screen" href="{{ asset('template_assets/css/par.css') }}">
<!-- particles.js container -->

<div id="container" class="cls-container">
    <div style="position: absolute;" id="particles-js"></div>
    <!-- BACKGROUND IMAGE -->
    <!--===================================================-->
    <div id="bg-overlay"></div>

    <!-- LOGIN FORM -->
    <!--===================================================-->

    

    <div class="cls-content">
        <div class="cls-content-sm panel">
            @if (session('msg'))
    <div class="alert alert-danger">
        {{ session('msg') }}
    </div>
    @endif
            <div class="panel-body">
                <div class="mar-ver pad-btm">
                    <center><img style="max-width: 35%;" class="img-responsive" src="{{ asset('asset/img/logo.png') }}" alt="EZY"/></center>
                    <h1 class="h3">Account Login</h1>
                    <p>Sign In to your account</p>
                </div>
                <form action="{{ route('login') }}" method="POST" id="login-form">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <input type="email" name="email" class="form-control" placeholder="Email" autofocus required>
                        @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <input type="password" name="password" class="form-control" placeholder="Password" required>
                        @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="checkbox pad-btm text-left">
                        <input id="demo-form-checkbox" class="magic-checkbox" type="checkbox" name="remember"  {{ old('remember') ? 'checked' : '' }} checked="">
                        <label for="demo-form-checkbox">Stay signed in</label>
                    </div>
                    <button class="btn btn-primary btn-lg btn-block" type="submit">Sign In</button>
                </form>
            </div>

            <div class="col-md-6">
                <a style="text-align: center;" href="{{ route('password.request') }}" class="btn-link">Forgot password ?</a>

            </div>
            <div class="col-md-6">
                <a style="text-align: center;" href="{{ url('/register') }}" class="btn-link">Create a new account</a>
            </div>
        </div>

        <!--===================================================-->

    </div>
    <!-- DEMO PURPOSE ONLY -->
    <!--===================================================-->
        <!--<div class="demo-bg">
            <div id="demo-bg-list">
                <div class="demo-loading"><i class="psi-repeat-2"></i></div>
                <img class="demo-chg-bg bg-trans active" src="{{ asset('template_assets/img/bg-img/thumbs/bg-trns.jpg') }}" alt="Background Image">
                <img class="demo-chg-bg" src="{{ asset('template_assets/img/bg-img/thumbs/bg-img-1.jpg') }}" alt="Background Image">
                <img class="demo-chg-bg" src="{{ asset('template_assets/img/bg-img/thumbs/bg-img-2.jpg') }}" alt="Background Image">
                <img class="demo-chg-bg" src="{{ asset('template_assets/img/bg-img/thumbs/bg-img-3.jpg') }}" alt="Background Image">
                <img class="demo-chg-bg" src="{{ asset('template_assets/img/bg-img/thumbs/bg-img-4.jpg') }}" alt="Background Image">
                <img class="demo-chg-bg" src="{{ asset('template_assets/img/bg-img/thumbs/bg-img-5.jpg') }}" alt="Background Image">
                <img class="demo-chg-bg" src="{{ asset('template_assets/img/bg-img/thumbs/bg-img-6.jpg') }}" alt="Background Image">
                <img class="demo-chg-bg" src="{{ asset('template_assets/img/bg-img/thumbs/bg-img-7.jpg') }}" alt="Background Image">
            </div>
        </div>-->
        <!--===================================================-->

    </div>

    <!--===================================================-->
    <!-- END OF CONTAINER -->
    @endsection
