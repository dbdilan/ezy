<!DOCTYPE html>
<html lang="en-us" id="extr-page">
<head>
    <meta charset="utf-8">
    <title>{{ config('app.name', 'EZY Intranet Application') }}</title>
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- #CSS Links -->
    <!-- Basic Styles -->
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('asset/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('asset/css/font-awesome.min.css') }}">

    <!--Nifty Premium Icon [ DEMONSTRATION ]-->
    <link href="{{ asset('template_assets/css/demo/nifty-demo-icons.min.css') }}" rel="stylesheet">

    <!--Nifty Stylesheet [ REQUIRED ]-->
    <link href="{{ asset('template_assets/css/nifty.css') }}" rel="stylesheet">

    <!--Pace - Page Load Progress Par [OPTIONAL]-->
    <link href="{{ asset('template_assets/plugins/pace/pace.min.css') }}" rel="stylesheet">
    <script src="{{ asset('template_assets/plugins/pace/pace.min..js') }}"></script>

    <!--Demo [ DEMONSTRATION ]-->
    <link href="{{ asset('template_assets/css/demo/nifty-demo.min.css') }}" rel="stylesheet">

    <!-- SmartAdmin Styles : Caution! DO NOT change the order -->
    <!--<link rel="stylesheet" type="text/css" media="screen" href="{{ asset('asset/css/smartadmin-production-plugins.min.css') }}">
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('asset/css/smartadmin-production.min.css') }}">
    <link rel="stylesheet" type="text/css" media="screen" href="{{ asset('asset/css/smartadmin-skins.min.css') }}">-->

    <!-- SmartAdmin RTL Support -->
    <!--<link rel="stylesheet" type="text/css" media="screen" href="{{ asset('asset/css/smartadmin-rtl.min.css') }}">-->

    <!-- We recommend you use "your_style.css" to override SmartAdmin
         specific styles this will also ensure you retrain your customization with each SmartAdmin update.
         <link rel="stylesheet" type="text/css" media="screen" href="css/your_style.css"> -->

         <!-- Demo purpose only: goes with demo.js, you can delete this css when designing your own WebApp -->
         <!--<link rel="stylesheet" type="text/css" media="screen" href="{{ asset('asset/css/demo.min.css') }}">-->

         <!-- #FAVICONS -->
         <link rel="shortcut icon" href="{{ asset('asset/img/favicon/favicon.ico') }}" type="image/x-icon">
         <link rel="icon" href="{{ asset('asset/img/favicon/favicon.ico') }}" type="image/x-icon">

         <!-- #GOOGLE FONT -->
         <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,300,400,700">


     </head>

     <body class="animated fadeInDown">

        <!-- Container Section -->
        @yield('content')
        <!-- // Container Section -->


        <!--================================================== -->

        <!-- PACE LOADER - turn this on if you want ajax loading to show (caution: uses lots of memory on iDevices)-->
        <script src="{{ asset('asset/js/plugin/pace/pace.min.js') }}"></script>

        <!-- Link to Google CDN's jQuery + jQueryUI; fall back to local -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script> if (!window.jQuery) { document.write('<script src="{{ asset('asset/js/libs/jquery-3.2.1.min.js') }}"><\/script>');} </script>

        <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
        <script> if (!window.jQuery.ui) { document.write('<script src="{{ asset('asset/js/libs/jquery-ui.min.js') }}"><\/script>');} </script>



        <!-- IMPORTANT: APP CONFIG -->
        <script src="{{ asset('asset/js/app.config.js') }}"></script>

<!-- JS TOUCH : include this plugin for mobile drag / drop touch events
    <script src="js/plugin/jquery-touch/jquery.ui.touch-punch.min.js"></script> -->

    <!-- BOOTSTRAP JS -->
    <script src="{{ asset('asset/js/bootstrap/bootstrap.min.js') }}"></script>

    <!-- JQUERY VALIDATE -->
    <script src="{{ asset('asset/js/plugin/jquery-validate/jquery.validate.min.js') }}"></script>

    <!-- JQUERY MASKED INPUT -->
    <script src="{{ asset('asset/js/plugin/masked-input/jquery.maskedinput.min.js') }}"></script>

    <!--NiftyJS [ RECOMMENDED ]-->
    <script src="{{ asset('template_assets/js/nifty.min.js') }}"></script>

    <!--Background Image [ DEMONSTRATION ]-->
    <script src="{{ asset('template_assets/js/demo/bg-images.js') }}"></script>



<!--[if IE 8]>

<h1>Your browser is out of date, please update your browser by going to www.microsoft.com/download</h1>

<![endif]-->

<!-- MAIN APP JS FILE -->
<script src="{{ asset('asset/js/app.min.js') }}"></script>

<script src="{{ asset('template_assets/js/app.js') }}"></script>

<script src="{{ asset('template_assets/js/particles.js') }}"></script>

<script>
    runAllForms();

    // Model i agree button
    $("#i-agree").click(function(){
        $this=$("#terms");
        if($this.checked) {
            $('#myModal').modal('toggle');
        } else {
            $this.prop('checked', true);
            $('#myModal').modal('toggle');
        }
    });

    $(function() {
        // Login  Validation
        $("#login-form").validate({
            // Rules for form validation
            rules : {
                email : {
                    required : true,
                    email : true
                },
                password : {
                    required : true,
                    minlength : 3,
                    maxlength : 20
                }
            },

            // Messages for form validation
            messages : {
                email : {
                    required : 'Please enter your email address',
                    email : 'Please enter a VALID email address'
                },
                password : {
                    required : 'Please enter your password'
                }
            },

            // Do not change code below
            errorPlacement : function(error, element) {
                error.insertAfter(element.parent());
            }
        });

        $.validator.addMethod('domaintype', function(value, element) {

            var allowedDomains = [ 'gmail.com' ];
            var domain = $("#email").val().split("@")[1];
            if ($.inArray(domain, allowedDomains) !== -1) {
        //acceptable, do nothing
        return true;
    } else{
        return false;
        //not acceptable, prevent submit event
    }

});

        // Register form validation
        $("#smart-form-register").validate({

            // Rules for form validation
            rules : {
                username : {
                    required : true
                },
                email : {
                    required : true,
                    email : true,
                    domaintype : true
                },
                password : {
                    required : true,
                    minlength : 3,
                    maxlength : 20
                },
                password_confirmation : {
                    required : true,
                    minlength : 3,
                    maxlength : 20,
                    equalTo : '#password'
                },
                firstname : {
                    required : true
                },
                lastname : {
                    required : true
                },
                gender : {
                    required : true
                },
                terms : {
                    required : true
                }
            },

            // Messages for form validation
            messages : {
                login : {
                    required : 'Please enter your login'
                },
                email : {
                    required : 'Please enter your email address',
                    email : 'Please enter a VALID email address',
                    domaintype : 'Please use your company email to register.'
                },
                password : {
                    required : 'Please enter your password'
                },
                password_confirmation : {
                    required : 'Please enter your password one more time',
                    equalTo : 'Please enter the same password as above'
                },
                firstname : {
                    required : 'Please select your first name'
                },
                lastname : {
                    required : 'Please select your last name'
                },
                gender : {
                    required : 'Please select your gender'
                },
                terms : {
                    required : 'You must agree with Terms and Conditions'
                }
            },

            // Ajax form submition
            submitHandler : function(form) {
                $(form).ajaxSubmit({
                    success : function() {
                        $("#smart-form-register").addClass('submited');
                    }
                });
            },

            // Do not change code below
            errorPlacement : function(error, element) {
                error.insertAfter(element.parent());
            }
        });
    });
</script>

</body>
</html>