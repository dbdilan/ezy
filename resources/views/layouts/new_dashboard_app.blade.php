<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Dashboard</title>

    <!--STYLESHEET-->
    <!--=================================================-->

    <!--Open Sans Font [ OPTIONAL ]-->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

    <!--Bootstrap Stylesheet [ REQUIRED ]-->
    <link href="{{ asset('template_assets/css/bootstrap.min.css') }}" rel="stylesheet">

    <!--Nifty Stylesheet [ REQUIRED ]-->
    <link href="{{ asset('template_assets/css/nifty.css') }}" rel="stylesheet">

    <!--Nifty Premium Icon [ DEMONSTRATION ]-->
    <link href="{{ asset('template_assets/css/demo/nifty-demo-icons.min.css') }}" rel="stylesheet">

    <!--Premium Line Icons [ OPTIONAL ]-->
    <link href="{{ asset('template_assets/css/line-icons/premium-line-icons.min.css') }}" rel="stylesheet">

    <!--Themify Icons [ OPTIONAL ]-->
    <link href="{{ asset('template_assets/plugins/themify-icons/themify-icons.min.css') }}" rel="stylesheet">
    <!--=================================================-->

    <!--Pace - Page Load Progress Par [OPTIONAL]-->
    <link href="{{ asset('template_assets/plugins/pace/pace.min.css') }}" rel="stylesheet">
    <script src="{{ asset('template_assets/plugins/pace/pace.min..js') }}"></script>

    <!--Demo [ DEMONSTRATION ]-->
    <link href="{{ asset('template_assets/css/demo/nifty-demo.min.css') }}" rel="stylesheet">
    <link href="{{ asset('template_assets/css/normalize.css') }}" rel="stylesheet">

    <!--Morris.js [ OPTIONAL ]-->
    <link href="{{ asset('template_assets/plugins/morris-js/morris.min.css') }}" rel="stylesheet">

    <style>
        @media (max-width: 959px) and (min-width: 600px) {
            #caro2{
                font-size: 10px;
            }
        }
    </style>
</head>

<!--TIPS-->
<!--You may remove all ID or Class names which contain "demo-", they are only used for demonstration. -->
<body>
<div id="container" class="effect aside-float aside-bright mainnav-lg">

    <!--NAVBAR-->
    <!--===================================================-->
    <header id="navbar">
        <div id="navbar-container" class="boxed">

            <!--Brand logo & name-->
            <!--================================-->
            <div class="navbar-header">
                <a href="index.html" class="navbar-brand">
                    <img src="{{ asset('template_assets/img/logo.png') }}" alt="Nifty Logo" class="brand-icon">
                    <div class="brand-title">
                        <span class="brand-text"></span>
                    </div>
                </a>
            </div>
            <!--================================-->
            <!--End brand logo & name-->

            <!--Navbar Dropdown-->
            <!--================================-->
            <div class="navbar-content">
                <!-- <ul class="nav navbar-top-links"> -->

                    <!-- <li class="tgl-menu-btn hidden-lg hidden-md">
                        <a class="mainnav-toggle" href="#">
                            <i class="demo-pli-list-view"></i>
                        </a>
                    </li>
                    <li class="tgl-menu-btn">
                        <i style="margin-top: 24px;" class="fa fa-search fa-1x hidden-xs" aria-hidden="true"></i>
                    </li> -->
                    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                    <!--End Navigation toogle button-->

                    <!--Search-->
                    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                 <!--    <li>
                        <div class="custom-search-form">
                            <label class="btn btn-trans" for="search-input" data-toggle="collapse" data-target="#nav-searchbox">

                            </label>

                            <form>
                                <div class="search-container collapse" id="nav-searchbox">
                                    <input id="search-input" type="text" class="form-control" placeholder="Type for search...">
                                </div>
                            </form>
                        </div>
                    </li> -->
                    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                    <!--End Search-->

                <!-- </ul> -->

                <ul class="nav navbar-top-links">
                    <li id="dropdown-user" class="dropdown">
                        <a href="#" data-toggle="dropdown" class="dropdown-toggle text-right">
                                <span class="ic-user pull-right">
                                    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                                    <!--You can use an image instead of an icon.-->
                                    <!--<img class="img-circle img-user media-object" src="img/profile-photos/1.png" alt="Profile Picture">-->
                                    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                                    <i class="demo-pli-male"></i>
                                </span>
                            <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                            <!--You can also display a user name in the navbar.-->
                            <!--<div class="username hidden-xs">Aaron Chavez</div>-->
                            <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                        </a>

                        <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right panel-default">
                            <ul class="head-list">
                                <li>
                                    <a href="{{ route('user-profile') }}"><i class="demo-pli-male icon-lg icon-fw"></i> Profile</a>
                                </li>

                                <li>
                                    <a href="#"><span class="label label-success pull-right">New</span><i class="demo-pli-gear icon-lg icon-fw"></i> Settings</a>
                                </li>
                                <li>
                                    <a href="#"><i class="demo-pli-computer-secure icon-lg icon-fw"></i> Lock screen</a>
                                </li>
                                <li>
                                    <a href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="demo-pli-unlock icon-lg icon-fw"></i> Logout</a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </div>
                    </li>
                    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                    <!--End user dropdown-->
                    <li>
                        <a href="#">
                            <i class="demo-pli-mail"></i>
                            <span class="badge badge-header badge-danger">21</span>
                        </a>
                    </li>
                    <!--Notification dropdown-->
                    <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                    <li class="dropdown">
                        <a href="#" data-toggle="dropdown" class="dropdown-toggle">
                            <i class="demo-pli-bell"></i>
                            <span class="badge badge-header badge-info">12</span>
                        </a>


                        <!--Notification dropdown menu-->
                        <div class="dropdown-menu dropdown-menu-md dropdown-menu-right">
                            <div class="nano scrollable">
                                <div class="nano-content">
                                    <ul class="head-list">
                                        <li>
                                            <a href="#" class="media add-tooltip" data-title="Used space : 95%" data-container="body" data-placement="bottom">
                                                <div class="media-left">
                                                    <i class="demo-pli-data-settings icon-2x text-main"></i>
                                                </div>
                                                <div class="media-body">
                                                    <p class="text-nowrap text-main text-semibold">HDD is full</p>
                                                    <div class="progress progress-sm mar-no">
                                                        <div style="width: 95%;" class="progress-bar progress-bar-danger">
                                                            <span class="sr-only">95% Complete</span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a class="media" href="#">
                                                <div class="media-left">
                                                    <i class="demo-pli-file-edit icon-2x"></i>
                                                </div>
                                                <div class="media-body">
                                                    <p class="mar-no text-nowrap text-main text-semibold">Write a news article</p>
                                                    <small>Last Update 8 hours ago</small>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a class="media" href="#">
                                                <span class="label label-info pull-right">New</span>
                                                <div class="media-left">
                                                    <i class="demo-pli-speech-bubble-7 icon-2x"></i>
                                                </div>
                                                <div class="media-body">
                                                    <p class="mar-no text-nowrap text-main text-semibold">Comment Sorting</p>
                                                    <small>Last Update 8 hours ago</small>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a class="media" href="#">
                                                <div class="media-left">
                                                    <i class="demo-pli-add-user-star icon-2x"></i>
                                                </div>
                                                <div class="media-body">
                                                    <p class="mar-no text-nowrap text-main text-semibold">New User Registered</p>
                                                    <small>4 minutes ago</small>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a class="media" href="#">
                                                <div class="media-left">
                                                    <img class="img-circle img-sm" alt="Profile Picture" src="{{ asset('template_assets/img/profile-photos/9.png') }}">
                                                </div>
                                                <div class="media-body">
                                                    <p class="mar-no text-nowrap text-main text-semibold">Lucy sent you a message</p>
                                                    <small>30 minutes ago</small>
                                                </div>
                                            </a>
                                        </li>
                                        <li>
                                            <a class="media" href="#">
                                                <div class="media-left">
                                                    <img class="img-circle img-sm" alt="Profile Picture" src="{{ asset('template_assets/"img/profile-photos/3.png') }}">
                                                </div>
                                                <div class="media-body">
                                                    <p class="mar-no text-nowrap text-main text-semibold">Jackson sent you a message</p>
                                                    <small>40 minutes ago</small>
                                                </div>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <!--Dropdown footer-->
                            <div class="pad-all bord-top">
                                <a href="#" class="btn-link text-main box-block">
                                    <i class="pci-chevron chevron-right pull-right"></i>Show All Notifications
                                </a>
                            </div>
                        </div>
                    </li>

                    <li>
                        <a href="#" class="aside-toggle">
                            <i class="demo-pli-dot-vertical"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </header>

    <div class="boxed">

        @yield('content')

        <aside id="aside-container">
            <div id="aside">
                <div class="nano">
                    <div class="nano-content">

                        <!--Nav tabs-->
                        <!--================================-->
                        <ul class="nav nav-tabs nav-justified">
                            <li class="active">
                                <a href="#demo-asd-tab-1" data-toggle="tab">
                                    <i class="demo-pli-speech-bubble-7 icon-lg"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#demo-asd-tab-2" data-toggle="tab">
                                    <i class="demo-pli-information icon-lg icon-fw"></i> Report
                                </a>
                            </li>
                            <li>
                                <a href="#demo-asd-tab-3" data-toggle="tab">
                                    <i class="demo-pli-wrench icon-lg icon-fw"></i> Settings
                                </a>
                            </li>
                        </ul>
                        <!--================================-->
                        <!--End nav tabs-->

                        <!-- Tabs Content -->
                        <!--================================-->
                        <div class="tab-content">

                            <!--First tab (Contact list)-->
                            <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                            <div class="tab-pane fade in active" id="demo-asd-tab-1">
                                <p class="pad-all text-main text-sm text-uppercase text-bold">
                                    <span class="pull-right badge badge-warning">3</span> Family
                                </p>

                                <!--Family-->
                                <div class="list-group bg-trans">
                                    <a href="#" class="list-group-item">
                                        <div class="media-left pos-rel">
                                            <img class="img-circle img-xs" src="{{ asset('template_assets/img/profile-photos/2.png') }}" alt="Profile Picture">
                                            <i class="badge badge-success badge-stat badge-icon pull-left"></i>
                                        </div>
                                        <div class="media-body">
                                            <p class="mar-no text-main">Stephen Tran</p>
                                            <small class="text-muteds">Availabe</small>
                                        </div>
                                    </a>
                                    <a href="#" class="list-group-item">
                                        <div class="media-left pos-rel">
                                            <img class="img-circle img-xs" src="{{ asset('template_assets/img/profile-photos/7.png') }}" alt="Profile Picture">
                                        </div>
                                        <div class="media-body">
                                            <p class="mar-no text-main">Brittany Meyer</p>
                                            <small class="text-muteds">I think so</small>
                                        </div>
                                    </a>
                                    <a href="#" class="list-group-item">
                                        <div class="media-left pos-rel">
                                            <img class="img-circle img-xs" src="{{ asset('template_assets/img/profile-photos/1.png') }}" alt="Profile Picture">
                                            <i class="badge badge-info badge-stat badge-icon pull-left"></i>
                                        </div>
                                        <div class="media-body">
                                            <p class="mar-no text-main">Jack George</p>
                                            <small class="text-muteds">Last Seen 2 hours ago</small>
                                        </div>
                                    </a>
                                    <a href="#" class="list-group-item">
                                        <div class="media-left pos-rel">
                                            <img class="img-circle img-xs" src="{{ asset('template_assets/img/profile-photos/4.png') }}" alt="Profile Picture">
                                        </div>
                                        <div class="media-body">
                                            <p class="mar-no text-main">Donald Brown</p>
                                            <small class="text-muteds">Lorem ipsum dolor sit amet.</small>
                                        </div>
                                    </a>
                                    <a href="#" class="list-group-item">
                                        <div class="media-left pos-rel">
                                            <img class="img-circle img-xs" src="{{ asset('template_assets/img/profile-photos/8.png') }}" alt="Profile Picture">
                                            <i class="badge badge-warning badge-stat badge-icon pull-left"></i>
                                        </div>
                                        <div class="media-body">
                                            <p class="mar-no text-main">Betty Murphy</p>
                                            <small class="text-muteds">Idle</small>
                                        </div>
                                    </a>
                                    <a href="#" class="list-group-item">
                                        <div class="media-left pos-rel">
                                            <img class="img-circle img-xs" src="{{ asset('template_assets/img/profile-photos/9.png') }}" alt="Profile Picture">
                                            <i class="badge badge-danger badge-stat badge-icon pull-left"></i>
                                        </div>
                                        <div class="media-body">
                                            <p class="mar-no text-main">Samantha Reid</p>
                                            <small class="text-muteds">Offline</small>
                                        </div>
                                    </a>
                                </div>

                                <hr>

                                <p class="pad-all text-main text-sm text-uppercase text-bold">
                                    <span class="pull-right badge badge-success">Offline</span> Friends
                                </p>

                                <!--Works-->
                                <div class="list-group bg-trans">
                                    <a href="#" class="list-group-item">
                                        <span class="badge badge-purple badge-icon badge-fw pull-left"></span> Joey K. Greyson
                                    </a>
                                    <a href="#" class="list-group-item">
                                        <span class="badge badge-info badge-icon badge-fw pull-left"></span> Andrea Branden
                                    </a>
                                    <a href="#" class="list-group-item">
                                        <span class="badge badge-success badge-icon badge-fw pull-left"></span> Johny Juan
                                    </a>
                                    <a href="#" class="list-group-item">
                                        <span class="badge badge-danger badge-icon badge-fw pull-left"></span> Susan Sun
                                    </a>
                                </div>

                                <hr>

                                <p class="pad-all text-main text-sm text-uppercase text-bold">News</p>

                                <div class="pad-hor">
                                    <p>Lorem ipsum dolor sit amet, consectetuer
                                        <a data-title="45%" class="add-tooltip text-semibold text-main" href="#">adipiscing elit</a>, sed diam nonummy nibh. Lorem ipsum dolor sit amet.
                                    </p>
                                    <small><em>Last Update : Des 12, 2014</em></small>
                                </div>
                            </div>
                            <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                            <!--End first tab (Contact list)-->


                            <!--Second tab (Custom layout)-->
                            <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                            <div class="tab-pane fade" id="demo-asd-tab-2">

                                <!--Monthly billing-->
                                <div class="pad-all">
                                    <p class="pad-ver text-main text-sm text-uppercase text-bold">Billing &amp; reports</p>
                                    <p>Get <strong class="text-main">$5.00</strong> off your next bill by making sure your full payment reaches us before August 5, 2018.</p>
                                </div>
                                <hr class="new-section-xs">
                                <div class="pad-all">
                                    <span class="pad-ver text-main text-sm text-uppercase text-bold">Amount Due On</span>
                                    <p class="text-sm">August 17, 2018</p>
                                    <p class="text-2x text-thin text-main">$83.09</p>
                                    <button class="btn btn-block btn-success mar-top">Pay Now</button>
                                </div>

                                <hr>

                                <p class="pad-all text-main text-sm text-uppercase text-bold">Additional Actions</p>

                                <!--Simple Menu-->
                                <div class="list-group bg-trans">
                                    <a href="#" class="list-group-item"><i class="demo-pli-information icon-lg icon-fw"></i> Service Information</a>
                                    <a href="#" class="list-group-item"><i class="demo-pli-mine icon-lg icon-fw"></i> Usage Profile</a>
                                    <a href="#" class="list-group-item"><span class="label label-info pull-right">New</span><i class="demo-pli-credit-card-2 icon-lg icon-fw"></i> Payment Options</a>
                                    <a href="#" class="list-group-item"><i class="demo-pli-support icon-lg icon-fw"></i> Message Center</a>
                                </div>


                                <hr>

                                <div class="text-center">
                                    <div><i class="demo-pli-old-telephone icon-3x"></i></div>
                                    Questions?
                                    <p class="text-lg text-semibold text-main"> (415) 234-53454 </p>
                                    <small><em>We are here 24/7</em></small>
                                </div>
                            </div>
                            <!--End second tab (Custom layout)-->
                            <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->


                            <!--Third tab (Settings)-->
                            <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                            <div class="tab-pane fade" id="demo-asd-tab-3">
                                <ul class="list-group bg-trans">
                                    <li class="pad-top list-header">
                                        <p class="text-main text-sm text-uppercase text-bold mar-no">Account Settings</p>
                                    </li>
                                    <li class="list-group-item">
                                        <div class="pull-right">
                                            <input class="toggle-switch" id="demo-switch-1" type="checkbox" checked>
                                            <label for="demo-switch-1"></label>
                                        </div>
                                        <p class="mar-no text-main">Show my personal status</p>
                                        <small class="text-muted">Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</small>
                                    </li>
                                    <li class="list-group-item">
                                        <div class="pull-right">
                                            <input class="toggle-switch" id="demo-switch-2" type="checkbox" checked>
                                            <label for="demo-switch-2"></label>
                                        </div>
                                        <p class="mar-no text-main">Show offline contact</p>
                                        <small class="text-muted">Aenean commodo ligula eget dolor. Aenean massa.</small>
                                    </li>
                                    <li class="list-group-item">
                                        <div class="pull-right">
                                            <input class="toggle-switch" id="demo-switch-3" type="checkbox">
                                            <label for="demo-switch-3"></label>
                                        </div>
                                        <p class="mar-no text-main">Invisible mode </p>
                                        <small class="text-muted">Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </small>
                                    </li>
                                </ul>

                                <hr>

                                <ul class="list-group pad-btm bg-trans">
                                    <li class="list-header"><p class="text-main text-sm text-uppercase text-bold mar-no">Public Settings</p></li>
                                    <li class="list-group-item">
                                        <div class="pull-right">
                                            <input class="toggle-switch" id="demo-switch-4" type="checkbox" checked>
                                            <label for="demo-switch-4"></label>
                                        </div>
                                        Online status
                                    </li>
                                    <li class="list-group-item">
                                        <div class="pull-right">
                                            <input class="toggle-switch" id="demo-switch-5" type="checkbox" checked>
                                            <label for="demo-switch-5"></label>
                                        </div>
                                        Show offline contact
                                    </li>
                                    <li class="list-group-item">
                                        <div class="pull-right">
                                            <input class="toggle-switch" id="demo-switch-6" type="checkbox" checked>
                                            <label for="demo-switch-6"></label>
                                        </div>
                                        Show my device icon
                                    </li>
                                </ul>

                                <hr>

                                <p class="pad-hor text-main text-sm text-uppercase text-bold mar-no">Task Progress</p>
                                <div class="pad-all">
                                    <p class="text-main">Upgrade Progress</p>
                                    <div class="progress progress-sm">
                                        <div class="progress-bar progress-bar-success" style="width: 15%;"><span class="sr-only">15%</span></div>
                                    </div>
                                    <small>15% Completed</small>
                                </div>
                                <div class="pad-hor">
                                    <p class="text-main">Database</p>
                                    <div class="progress progress-sm">
                                        <div class="progress-bar progress-bar-danger" style="width: 75%;"><span class="sr-only">75%</span></div>
                                    </div>
                                    <small>17/23 Database</small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </aside>
        <!--===================================================-->
        <!--END ASIDE-->


        <!--MAIN NAVIGATION-->
        <!--===================================================-->
        <nav id="mainnav-container">
            <div id="mainnav">
                <div id="mainnav-menu-wrap">
                    <div class="nano">
                        <div class="nano-content">
                            <div id="mainnav-shortcut" class="hidden">
                                <ul class="list-unstyled shortcut-wrap">
                                    <li class="col-xs-3" data-content="My Profile">
                                        <a class="shortcut-grid" href="#">
                                            <div class="icon-wrap icon-wrap-sm icon-circle bg-mint">
                                                <i class="demo-pli-male"></i>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="col-xs-3" data-content="Messages">
                                        <a class="shortcut-grid" href="#">
                                            <div class="icon-wrap icon-wrap-sm icon-circle bg-warning">
                                                <i class="demo-pli-speech-bubble-3"></i>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="col-xs-3" data-content="Activity">
                                        <a class="shortcut-grid" href="#">
                                            <div class="icon-wrap icon-wrap-sm icon-circle bg-success">
                                                <i class="demo-pli-thunder"></i>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="col-xs-3" data-content="Lock Screen">
                                        <a class="shortcut-grid" href="#">
                                            <div class="icon-wrap icon-wrap-sm icon-circle bg-purple">
                                                <i class="demo-pli-lock-2"></i>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <!--================================-->
                            <!--End shortcut buttons-->

                            <ul style="margin-top:10px;" id="mainnav-menu" class="list-group">
                                <li class="active-sub">
                                    <a href="{{ url('/dashboard') }}">
                                        <i class="demo-pli-home"></i>
                                        <span class="menu-title">Home</span>
                                    </a>
                                </li>

                                <li>
                                    <a href="{{ url('/users') }}">
                                        <i class="demo-pli-male-female" aria-hidden="true"></i>
                                        <span class="menu-title">Peoples</span>
                                    </a>
                                </li>

                                <li>
                                    <a href="#">
                                        <i class="pli-newspaper" aria-hidden="true"></i>
                                        <span class="menu-title">News</span>
                                        <i class="arrow"></i>
                                    </a>

                                    <!--Submenu-->
                                    <ul class="collapse">
                                        <li><a href="{{ url('/home') }}">News</a></li>
                                        <li><a href="{{ url('/posts/create') }}">Create News</a></li>
                                        <li><a href="{{ url('/announcement/list') }}">Announcement</a></li>
                                    </ul>
                                </li>

                                <li>
                                    <a href="{{ url('/events') }}">
                                        <i class="pli-calendar-4" aria-hidden="true"></i>
                                        <span class="menu-title">Events</span>
                                    </a>
                                </li>

                                <li>
                                    <a href="{{ url('/documentation/list') }}">
                                        <i class="pli-folder-open" aria-hidden="true"></i>
                                        <span class="menu-title">Documents</span>
                                    </a>
                                </li>

                              <!--   <li>
                                    <a href="{{ url('/home') }}">
                                        <i class="demo-pli-split-vertical-2"></i>
                                        <span class="menu-title">Projects</span>
                                    </a>
                                </li> -->

                                <li>
                                    <a href="{{ url('/home') }}">
                                        <i class="pli-play-music" aria-hidden="true"></i>
                                        <span class="menu-title">Media</span>
                                    </a>
                                </li>

                                <li>
                                    <a href="{{ url('/email-signature') }}">
                                        <i class="pli-mail" aria-hidden="true"></i>
                                        <span class="menu-title">Email Signature</span>
                                    </a>
                                </li>

                               <!--  <li>
                                    <a href="{{ url('/home') }}">
                                        <i class="pli-headphone" aria-hidden="true"></i>
                                        <span class="menu-title">Contact Us</span>
                                    </a>
                                </li> -->
								
								<li>
                                    <a href="#">
                                        <i class="demo-pli-tactic"></i>
                                        <span class="menu-title">Setting</span>
                                        <i class="arrow"></i>
                                    </a>

                                    <!--Submenu-->
                                    <ul class="collapse">
                                        <li>
                                            <a href="#">User<i class="arrow"></i></a>

                                            <!--Submenu-->
                                            <ul class="collapse">
                                                <li><a href="{{ url('/users') }}">User List</a></li>
                                                <li><a href="{{ url('/roles') }}">User Role</a></li>
                                            </ul>
                                        </li>
                                        <li>
                                            <a href="#">Dashboard<i class="arrow"></i></a>

                                            <!--Submenu-->
                                            <ul class="collapse">
                                                <li><a href="{{ url('/setting/dashboard') }}">Slider</a></li>
                                                <li>
                                                    <a href="#">News<i class="arrow"></i></a>
                                                    <!--Submenu-->
                                                    <ul class="collapse" style="padding-left: 20px;">
                                                        <li><a href="{{ url('/posts/create') }}">Create News</a></li>
                                                        <li><a href="{{ url('/home') }}">News List</a></li>
                                                    </ul>
                                                </li>
                                                <li>
                                                    <a href="#">Events</a>
                                                    <!--Submenu-->
                                                    <ul class="collapse" style="padding-left: 20px;">
                                                        <li><a href="{{ url('/events/list-event') }}">Events List</a></li>
                                                        <li><a href="{{ url('/posts/create') }}">Calender</a></li>
                                                    </ul>
                                                </li>
                                                <li><a href="{{ url('/announcement/list') }}">Announcements</a></li>
                                                <li>
                                                    <a href="#">Poll<i class="arrow"></i></a>
                                                    <!--Submenu-->
                                                    <ul class="collapse" style="padding-left: 20px;">
                                                        <li><a href="{{ url('/poll/list') }}">Poll List</a></li>
                                                        <li><a href="{{ url('/poll/create') }}">Create Poll</a></li>
                                                    </ul>
                                                </li>
                                                <li><a href="{{ url('/documentation/list/') }}">Media</a></li>
                                                <li><a href="{{ url('/documentation/list/') }}">Documents</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    </div>

    <!--<footer id="footer">
        <div class="show-fixed pad-rgt pull-right">
            You have <a href="#" class="text-main"><span class="badge badge-danger">3</span> pending action.</a>
        </div>

        <p class="pad-lft">&#0169; 2017 Your Company</p>

    </footer>-->


    <!-- FOOTER -->
    <!--===================================================-->

    <!--===================================================-->
    <!-- END FOOTER -->


    <!-- SCROLL PAGE BUTTON -->
    <!--===================================================-->
    <button class="scroll-top btn">
        <i class="pci-chevron chevron-up"></i>
    </button>
    <!--===================================================-->



</div>
<!--===================================================-->
<!-- END OF CONTAINER -->





<!--JAVASCRIPT-->
<!--=================================================-->

<!--jQuery [ REQUIRED ]-->
<script src="{{ asset('template_assets/js/jquery.min.js') }}"></script>

<script src="{{ asset('template_assets/js/normalize.js') }}"></script>


<!--BootstrapJS [ RECOMMENDED ]-->
<script src="{{ asset('template_assets/js/bootstrap.min.js') }}"></script>


<!--NiftyJS [ RECOMMENDED ]-->
<script src="{{ asset('template_assets/js/nifty.min.js') }}"></script>




<!--=================================================-->

<!--Demo script [ DEMONSTRATION ]-->
<script src="{{ asset('template_assets/js/demo/nifty-demo.min.js') }}"></script>


<!--Flot Chart [ OPTIONAL ]-->
<script src="{{ asset('template_assets/plugins/flot-charts/jquery.flot.min.js') }}"></script>
<script src="{{ asset('template_assets/plugins/flot-charts/jquery.flot.resize.min.js') }}"></script>
<script src="{{ asset('template_assets/plugins/flot-charts/jquery.flot.tooltip.min.js') }}"></script>


<!--Sparkline [ OPTIONAL ]-->
<script src="{{ asset('template_assets/plugins/sparkline/jquery.sparkline.min.js') }}"></script>


<!--Specify page [ SAMPLE ]-->
<script src="{{ asset('template_assets/js/demo/dashboard.js') }}"></script>

<!--Flot Sample [ SAMPLE ]-->
<script src="{{ asset('template_assets/js/demo/flot-charts.js') }}"></script>

<script src="/vendor/laravel-filemanager/js/lfm.js"></script>


<!-- Morris Chart Dependencies -->
<script src="{{ asset('asset/js/plugin/morris/raphael.min.js') }}"></script>
<script src="{{ asset('asset/js/plugin/morris/morris.min.js') }}"></script>



@yield('page-js')

<script src="{{ asset('asset/js/script.js') }}"></script>


<script>

    //======================================================= File Manager Event =======================================
    $('#lfm').filemanager('image');
    $('#lfm-video').filemanager('file');
//    $('#lfm-audio').filemanager('file');
//    $('#lfm-other').filemanager('file');
//    $('.file-manager').filemanager('image');
//    $('.lfm-comment-picture').filemanager('image');
//    $('.lfm-comment-video').filemanager('file');

//    $(document).on('click', '.file-manager' , function(e){
//        localStorage.setItem('target_input', $(this).data('input'));
//        localStorage.setItem('target_preview', $(this).data('preview'));
//        window.open('/laravel-filemanager?type=image', 'FileManager', 'width=900,height=600');
//        return false;
//    });

    $(document).on('click', '.file-manager-video-or-audio' , function(e){
        localStorage.setItem('target_input', $(this).data('input'));
        localStorage.setItem('target_preview', $(this).data('preview'));
        window.open('/laravel-filemanager?type=file', 'FileManager', 'width=900,height=600');
        return false;
    });

    //======================================================= END File Manager Event ===================================

    //======================================================= Privacy changing events ==================================
    privacyChange();
    // select privacy and change div
    $('#privacy').change(function () {
        privacyChange();
    });

    // form load and privacy sect to only me
    function privacyChange() {
        var privacy_code = $('#privacy').val();
        $('#user-group-div').addClass('hidden');
        $('#office-div').addClass('hidden');
        if(privacy_code == 'office')
        {
            $('#office-div').removeClass('hidden');
        }
        if(privacy_code == 'group')
        {
            $('#user-group-div').removeClass('hidden');
        }
    }
    //======================================================= END Privacy changing events ==============================

    //======================================================= Delete Notification Request ==============================
    $('.delete-btn').click(function () {

        var form_id  = $(this).attr("data-form");
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be  able to recover this imaginary record!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
                $('#'+form_id).submit();
            } else {
                // else code here ! // swal("Your imaginary file is safe!");
            }
        });
    });
    //======================================================= END Delete Notification Request ==========================

</script>

<!-- ======================================================= FLASH MESSAGE ========================================== -->
@if(session()->has('delete_success_message'))
    <script>
        //Success Message for log user
        swal("SuccessFull!, {{ session()->get('delete_success_message') }} ", {icon: "success"});
    </script>
@endif

@if(session()->has('success_message'))
    <script>
        //Success Message for log user
        swal("SuccessFull!, {{ session()->get('success_message') }} ", {icon: "success"});
    </script>
    @endif
            <!-- ======================================================= END FLASH MESSAGE ========================================== -->

    <script>

        // START AND FINISH DATE
        $('.datepicker').datepicker({
            dateFormat: 'yy-mm-dd'
        });
        $('.datetimepicker').datetimepicker();

        //========================================================= LOGOUT FORM SUBMIT =====================================
        $('#logout-btn').click(function () {
            // ask verification
            $.SmartMessageBox({
                title: "<i class='fa fa-sign-out txt-color-orangeDark'></i> Logout <span class='txt-color-orangeDark'><strong>{{ Auth::user()->name }}</strong></span> ?",
                content : "You can improve your security further after logging out by closing this opened browser",
                buttons : '[No][Yes]'
            }, function(ButtonPressed) {
                if (ButtonPressed === "Yes") {
                    $('#logout-form').submit();
                }
                if (ButtonPressed === "No") {
                    // no option code here
                }
            });
            e.preventDefault();
        });
        //========================================================= END LOGOUT FORM SUBMIT =================================

    </script>


</body>
</html>
