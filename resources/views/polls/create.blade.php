@extends('layouts.dashboard_app')

@section('title', '| Create New News')

@section('content')


        <!-- RIBBON -->
<div id="ribbon">

    <span class="ribbon-button-alignment">
        <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
            <i class="fa fa-refresh"></i>
        </span>
    </span>

    <!-- breadcrumb -->
    <ol class="breadcrumb">
        <li>Home</li>
        <li>Poll</li>
        <li>Create</li>
    </ol>
    <!-- end breadcrumb -->

</div>
<!-- END RIBBON -->

<!-- MAIN CONTENT -->
<div id="content">

    <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
            <h1 class="page-title txt-color-blueDark">
                <i class="fa fa-users fa-fw "></i>
                Poll <span>> Create </span>
            </h1>
        </div>
        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
            @can('Poll List')
            <a href="{{ url('poll/list') }}" class="btn btn-primary pull-right">Poll List</a>
            @endcan
            <button type="button" class="btn btn-primary pull-right" onclick="window.history.back()">< Back</button>
        </div>
    </div>

    <!-- widget grid -->
    <section id="widget-grid" class="">

        <!-- row -->
        <div class="row">

            <article class="col-sm-12 col-md-12 col-lg-6">

                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget" id="wid-id-1" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                        <h2>Poll Create </h2>

                    </header>

                    <!-- widget div-->
                    <div>

                        <!-- widget edit box -->
                        <div class="jarviswidget-editbox">
                            <!-- This area used as dropdown edit box -->

                        </div>
                        <!-- end widget edit box -->

                        <!-- widget content -->
                        <div class="widget-body">

                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <form action="{{ url('poll/add-new-poll')  }}" method="post" id="poll-form" class="form" >
                                {{ csrf_field() }}

                                        <!-- Title -->
                                <div class="form-group">
                                    <label>Title</label>
                                    <input type="text" name="title" class="form-control" required>
                                    @if ($errors->has('title'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <!-- // Title -->

                                <!-- Description -->
                                <div class="form-group">
                                    <label>Description</label>
                                    <textarea name="body" class="form-control my-editor" required></textarea>
                                    @if ($errors->has('body'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('body') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <!-- Description -->

                                <!-- Options -->
                                <div class="form-group">
                                    <label>Options</label>
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <th>Question</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody id="option-body">
                                        <tr id="option-row-1">
                                            <td>
                                                <input name="options[]" class="form-control" placeholder="Option" type="text">
                                            </td>
                                            <td style="padding-left: 10px; " class="text-left">
                                                <button type="button" onclick="$('#option-row-1, .tooltip').remove();" data-toggle="tooltip" title="Remove" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button>
                                            </td>
                                        </tr>
                                        </tbody>
                                        <tfoot>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <button type="button" id="add-new-gallery-btn" class="btn btn-primary" onclick="createNewOption()"><i class="fa fa-plus"></i></button>
                                            </td>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                                <!-- // Options -->

                                <!-- Option Type -->
                                <div class="form-group">
                                    <label>Poll Type</label>
                                    <select class="form-control" name="option_type">
                                        <option value="radio">Single select</option>
                                        <option value="checkbox">Multiple select</option>
                                    </select>
                                </div>
                                <!-- // Option Type -->


                                <!-- publish date -->
                                <div class="form-group">
                                    <label>Vote Start Date</label>
                                    <input type="text" name="publish_date"  placeholder="Publish Date" class="datepicker form-control" value="{!! date('Y-m-d') !!}" data-dateformat='dd/mm/yy' required>
                                    <input type="hidden" name="created_by" value="{{ Auth::user()->id }}">
                                    @if ($errors->has('publish_date'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('publish_date') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <!-- // publish date -->

                                <!-- close date -->
                                <div class="form-group">
                                    <label>Vote Close date</label>
                                    <input type="text" name="close_date"  placeholder="Close Date" class="datepicker form-control" value="{!! date('Y-m-d') !!}" data-dateformat='dd/mm/yy' required>
                                    @if ($errors->has('close_date'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('close_date') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <!-- // close date -->

                                <div class="form-group">
                                    <label>Privacy</label>
                                    <select class="form-control" name="privacy" id="privacy">
                                        @foreach($privacy  as $privacy_item)
                                            <option value="{{ $privacy_item->code }}">{{ $privacy_item->name }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div id="user-group-div" class="form-group hidden">
                                    <label>User Groups</label>
                                    <select style="width:100%" class="select2" name="group[]" multiple id="user-group">
                                        @foreach($user_groups  as $user_group)
                                            <option value="{{ $user_group->group_id }}">{{ $user_group->name }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                @role('admin')
                                <div id="office-div" class="form-group hidden">
                                    <label>Location</label>
                                    <select style="width:100%" class="select2" name="office[]" multiple id="office">
                                        @foreach($offices  as $office)
                                            <option value="{{ $office->office_id }}">{{ $office->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                @endrole

                                <div class="form-group">
                                    <label>Status</label>
                                    <select class="form-control" name="status">
                                        <option  value="1" >Enable</option>
                                        <option selected value="0" >Disable</option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    {{ Form::submit('Create Poll', array('class' => 'btn btn-success btn-lg btn-block')) }}
                                </div>

                            </form>
                        </div>
                        <!-- end widget content -->

                    </div>
                    <!-- end widget div -->

                </div>
                <!-- end widget -->

            </article>
            <!-- END COL -->
        </div>

    </section>
    <!-- end widget grid -->

</div>
<!-- END MAIN CONTENT -->

@endsection

@section('page-js')

    <script>

        $("#poll-form").validate();

        // Option create
        var row_id = 2;
        function createNewOption() {

            var html = '';
            html += '<tr id="option-row-'+ row_id +'">';
            html += '<td><input type="text" name="options[]" class="form-control" placeholder="Option"></td>';
            html += '<td style="padding-left: 10px; " class="text-left"><button type="button" onclick="$(\'#option-row-' + row_id  + ', .tooltip\').remove();" data-toggle="tooltip" title="Remove" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
            html += '</tr>';

            $('#option-body').append(html);

            row_id++;
        }

        // DO NOT REMOVE : GLOBAL FUNCTIONS!
        $(document).ready(function() {
            pageSetUp();
        })

    </script>
@endsection