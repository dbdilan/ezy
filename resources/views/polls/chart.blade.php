<div id="donut-graph" class="chart no-padding"></div>

<script>

    // donut
    if ($('#donut-graph').length) {
        Morris.Donut({
            element : 'donut-graph',
            data : [
                    @foreach($votes_array as $votes)
                {
                    value : '{{ $votes['votes_count'] }}',
                    label : '{{ $votes['name'] }}'
                },
                    @endforeach

            ],
            colors: [
                '#ec407a',
                '#03a9f4',
                '#d8dfe2'
            ],
            formatter : function(x) {
                return x + ""
            }
        });
    }
    /**
     * formatter : function(x) {
                return x + ""
            }
     */
</script>