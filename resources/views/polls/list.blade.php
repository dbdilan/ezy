@extends('layouts.dashboard_app')

@section('title', '| Create New News')

@section('content')


        <!-- RIBBON -->
<div id="ribbon">

    <span class="ribbon-button-alignment">
        <span id="refresh" class="btn btn-ribbon" data-action="resetWidgets" data-title="refresh"  rel="tooltip" data-placement="bottom" data-original-title="<i class='text-warning fa fa-warning'></i> Warning! This will reset all your widget settings." data-html="true">
            <i class="fa fa-refresh"></i>
        </span>
    </span>

    <!-- breadcrumb -->
    <ol class="breadcrumb">
        <li>Home</li>
        <li>Poll</li>
        <li>List</li>
    </ol>
    <!-- end breadcrumb -->

</div>
<!-- END RIBBON -->

<!-- MAIN CONTENT -->
<div id="content">

    <div class="row">
        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
            <h1 class="page-title txt-color-blueDark">
                <i class="fa fa-users fa-fw "></i>
                Poll <span>> List </span>
            </h1>
        </div>
        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
            <button type="button" class="btn btn-primary pull-right" onclick="window.history.back()">< Back</button>
        </div>
    </div>

    <!-- widget grid -->
    <section id="widget-grid" class="">

        <!-- row -->
        <div class="row">

            <article class="col-sm-12 col-md-12 col-lg-9">

                <!-- Widget ID (each widget will need unique ID)-->
                <div class="jarviswidget" id="wid-id-1" data-widget-colorbutton="false" data-widget-editbutton="false" data-widget-custombutton="false">
                    <header>
                        <span class="widget-icon"> <i class="fa fa-edit"></i> </span>
                        <h2>Poll Create </h2>

                    </header>

                    <!-- widget div-->
                    <div>

                        <!-- widget edit box -->
                        <div class="jarviswidget-editbox">
                            <!-- This area used as dropdown edit box -->

                        </div>
                        <!-- end widget edit box -->

                        <!-- widget content -->
                        <div class="widget-body">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Title</th>
                                    <th>Description</th>
                                    <th>Created by</th>
                                    <th>Publish date</th>
                                    <th>Close date</th>
                                    <th>Status</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($polls as $key=>$poll)
                                    <tr>
                                        <td>{!! $key+1 !!}</td>
                                        <td>{{ $poll->title }}</td>
                                        <td>{{ $poll->question }}</td>
                                        <td>{{ $poll->first_name.' '.$poll->last_name }}</td>
                                        <td>{{ $poll->publish_date }}</td>
                                        <td>{{ $poll->close_date }}</td>
                                        <td>@if($poll->status == 1) Enable @else Disable @endif</td>
                                        <td>
                                            @can('Poll Edit')
                                            <a class="btn btn-info" href="{{ url('poll/view/'.$poll->poll_id) }}">Edit</a>
                                            @endcan

                                            @if($polls_vote_count[$poll->poll_id] < 1)
                                                @can('Poll Delete')
                                                {!! Form::open(['method' => 'GET', 'url' => '/poll/destroy/'.$poll->poll_id, 'id' => 'form-poll-'.$poll->poll_id, 'class' => 'delete-form' ]) !!}
                                                <button type="button" data-form="form-poll-{{ $poll->poll_id  }}" data-validate-url="{{ url('/poll/delete-validate/'. $poll->poll_id) }}" class="btn btn-danger delete-btn">Delete</button>
                                                {!! Form::close() !!}
                                                @endcan
                                            @else
                                                <a class="admin-view-results btn btn-info"  data-url="{{ url('/poll/results/'.$poll->poll_id) }}">Results</a>
                                            @endif

                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <!-- end widget content -->

                    </div>
                    <!-- end widget div -->

                </div>
                <!-- end widget -->

            </article>
            <!-- END COL -->

            <article class="col-lg-3">
                <h3>Results Chart view</h3>
                <div id="admin-poll-view-results">

                </div>
            </article>
        </div>

    </section>
    <!-- end widget grid -->

</div>
<!-- END MAIN CONTENT -->

@endsection

@section('page-js')

        <!-- Morris Chart Dependencies -->
<script src="{{ asset('asset/js/plugin/morris/raphael.min.js') }}"></script>
<script src="{{ asset('asset/js/plugin/morris/morris.min.js') }}"></script>

<script>

    $("#poll-form").validate();

    // Option create
    var row_id = 2;
    function createNewOption() {

        var html = '';
        html += '<tr id="option-row-'+ row_id +'">';
        html += '<td><input type="text" name="options[]" class="form-control" placeholder="Option"></td>';
        html += '<td style="padding-left: 10px; " class="text-left"><button type="button" onclick="$(\'#option-row-' + row_id  + ', .tooltip\').remove();" data-toggle="tooltip" title="Remove" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
        html += '</tr>';

        $('#option-body').append(html);

        row_id++;
    }

    // DO NOT REMOVE : GLOBAL FUNCTIONS!
    $(document).ready(function() {
        pageSetUp();
    })

</script>
@endsection