<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

 Route::get('/dashboard/new', function () {
     return view('dashboard.new');
 });



Auth::routes();

Route::get('/home', 'PostController@index');
Route::post('/test_user', 'PostController@testUser');
//
Route::get('/', 'PostController@login')->name('login-post');

Route::resource('users', 'UserController');

Route::resource('roles', 'RoleController');

Route::resource('permissions', 'PermissionController');

Route::resource('posts', 'PostController');


// Dashboard views
Route::get('/dashboard', 'DashboardController@index')->name('dashboard');


// Users Controller
Route::get('/members', 'UserController@members')->name('members');
Route::post('/user/add-follows', 'UserController@userAddFollows')->name('add-follows');
Route::post('/user/add-connections', 'UserController@userAddConnections')->name('add-connections');
Route::post('/user/upload-profile-photo', 'UserController@uploadProfilePhoto')->name('upload-profile-photo');
Route::post('/user/upload-cover-photo', 'UserController@uploadCoverPhoto')->name('upload-cover-photo');
Route::post('/user/change-password', 'UserController@changePassword')->name('change-password');
Route::post('/user/update-user-profile', 'UserController@updateUserProfile')->name('update-user-profile');


//Events
Route::get('/events', 'EventController@index')->name('events');
Route::post('/events/add-new-event', 'EventController@addNewEvent')->name('add-new-event');
Route::get('/events/edit/{id}', 'EventController@editEvent')->name('edit-event');
Route::post('/events/edit-event', 'EventController@editPostEvent')->name('edit-post-event');
Route::get('/events/list-event', 'EventController@getList')->name('list-event');
Route::get('/events/destroy/{id}', 'EventController@destroy')->name('destroy-event');


// Post Controller
Route::post('/post/add-post-like', 'PostController@addPostLike')->name('add-post-like');
Route::post('/post/comments', 'PostController@comments')->name('add-comments');
Route::post('/post/comment/remove', 'PostController@commentsRemove')->name('remove-comments');


// Poll Controller
Route::get('/poll/create', 'PollController@create')->name('poll-create');
Route::get('/poll/list', 'PollController@getList')->name('poll-list');
Route::get('/poll/edit/{id}', 'PollController@pollEdit')->name('poll-edit');
Route::get('/poll/view/{id}', 'PollController@pollView')->name('poll-view');
Route::post('/poll/edit-post', 'PollController@pollEditPost')->name('poll-edit-post');
Route::post('/poll/add-new-poll', 'PollController@addNewPoll')->name('add-new-poll');
Route::post('/poll/vote', 'PollController@addPollVote')->name('add-poll-vote');
Route::get('/poll/destroy/{id}', 'PollController@destroy')->name('poll-destroy');
Route::get('/poll/results/{poll_id}', 'PollController@viewPollResults')->name('poll-results');
Route::get('/poll/delete-validate/{poll_id}', 'PollController@checkPollHasVote')->name('delete-validate');

// Documentation
Route::get('documentation/list','DocumentationController@listOfDocumentation')->name('documentation-list');


//Announcement
Route::get('announcement/create', 'AnnouncementController@create')->name('announcement-create');
Route::post('announcement/add-new', 'AnnouncementController@addNewAnnouncement')->name('announcement-add-new');
Route::get('announcement/list', 'AnnouncementController@getList')->name('announcement-get-list');
Route::get('announcement/edit/{id}', 'AnnouncementController@editAnnouncement')->name('announcement-edit');
Route::post('announcement/edit-post', 'AnnouncementController@editPostAnnouncement')->name('announcement-edit-post');

//Setting
Route::get('setting/dashboard', 'SettingController@dashboard')->name('setting-dashboard');
Route::post('setting/dashboard/save', 'SettingController@dashboardSave')->name('setting-dashboard-save');
