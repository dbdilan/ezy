@if((sizeof($files) > 0) || (sizeof($directories) > 0))
        <!--<table class="table table-responsive table-condensed table-striped hidden-xs table-list-view">
    <thead>
    <th style='width:50%;'>{{ Lang::get('laravel-filemanager::lfm.title-item') }}</th>
    <th>{{ Lang::get('laravel-filemanager::lfm.title-size') }}</th>
    <th>{{ Lang::get('laravel-filemanager::lfm.title-type') }}</th>
    <th>{{ Lang::get('laravel-filemanager::lfm.title-modified') }}</th>
    <th>{{ Lang::get('laravel-filemanager::lfm.title-action') }}</th>
    </thead>
    <tbody>
    @foreach($items as $item)
        <tr>
          <td>
            <i class="fa {{ $item->icon }}"></i>
          <a class="{{ $item->is_file ? 'file' : 'folder'}}-item clickable" data-id="{{ $item->is_file ? $item->url : $item->path }}" title="{{$item->name}}">
            {{ str_limit($item->name, $limit = 40, $end = '...') }}
        </a>
      </td>
      <td>{{ $item->size }}</td>
        <td>{{ $item->type }}</td>
        <td>{{ $item->time }}</td>
        <td class="actions">
          @if($item->is_file)
        <a href="javascript:download('{{ $item->name }}')" title="{{ Lang::get('laravel-filemanager::lfm.menu-download') }}">
              <i class="fa fa-download fa-fw"></i>
            </a>
            @if($item->thumb)
        <a href="javascript:fileView('{{ $item->url }}', '{{ $item->updated }}')" title="{{ Lang::get('laravel-filemanager::lfm.menu-view') }}">
                <i class="fa fa-image fa-fw"></i>
              </a>
              <a href="javascript:cropImage('{{ $item->name }}')" title="{{ Lang::get('laravel-filemanager::lfm.menu-crop') }}">
                <i class="fa fa-crop fa-fw"></i>
              </a>
              <a href="javascript:resizeImage('{{ $item->name }}')" title="{{ Lang::get('laravel-filemanager::lfm.menu-resize') }}">
                <i class="fa fa-arrows fa-fw"></i>
              </a>
            @endif
@endif
        <a href="javascript:rename('{{ $item->name }}')" title="{{ Lang::get('laravel-filemanager::lfm.menu-rename') }}">
            <i class="fa fa-edit fa-fw"></i>
          </a>
          <a href="javascript:trash('{{ $item->name }}')" title="{{ Lang::get('laravel-filemanager::lfm.menu-delete') }}">
            <i class="fa fa-trash fa-fw"></i>
          </a>
        </td>
      </tr>
    @endforeach
        </tbody>
      </table>-->


<ul id="demo-mail-list" class="file-list hidden-xs">
  <!-- {{ print_r($items) }} -->
  @foreach($items as $item)
    <li>
      <div class="btn-file-toolbar pull-right">
        @if($item->is_file)
          <a class="btn btn-icon add-tooltip" data-original-title="Delete" data-toggle="tooltip" href="javascript:download('{{ $item->name }}')" title="{{ Lang::get('laravel-filemanager::lfm.menu-download') }}"><i class="icon-2x demo-pli-download-from-cloud text-info"></i></a>
          @if($item->thumb)
            <a class="btn btn-icon add-tooltip" data-original-title="File View" data-toggle="tooltip" href="javascript:fileView('{{ $item->url }}', '{{ $item->updated }}')" title="{{ Lang::get('laravel-filemanager::lfm.menu-view') }}"><i class="icon-2x pli-preview"></i></a>

          @if((strpos($item->url, 'share') !== false) OR (strpos($item->path, 'share') !== false))
          @can('Role View')
            <a class="btn btn-icon add-tooltip" data-original-title="Crop Image" data-toggle="tooltip" href="javascript:cropImage('{{ $item->name }}')" title="{{ Lang::get('laravel-filemanager::lfm.menu-crop') }}"><i class="icon-2x pli-crop"></i></a>
          @endcan

        @else  
            <a class="btn btn-icon add-tooltip" data-original-title="Crop Image" data-toggle="tooltip" href="javascript:cropImage('{{ $item->name }}')" title="{{ Lang::get('laravel-filemanager::lfm.menu-crop') }}"><i class="icon-2x pli-crop"></i></a>
        @endif

        @if((strpos($item->url, 'share') !== false) OR (strpos($item->path, 'share') !== false))
          @can('Role View')
            <a class="btn btn-icon add-tooltip" data-original-title="Resize Image" data-toggle="tooltip" href="javascript:resizeImage('{{ $item->name }}')" title="{{ Lang::get('laravel-filemanager::lfm.menu-resize') }}"><i class="icon-2x pli-resize"></i></a>
          @endcan

        @else  

            <a class="btn btn-icon add-tooltip" data-original-title="Resize Image" data-toggle="tooltip" href="javascript:resizeImage('{{ $item->name }}')" title="{{ Lang::get('laravel-filemanager::lfm.menu-resize') }}"><i class="icon-2x pli-resize"></i></a>
        @endif

          @endif
        @endif


        @if((strpos($item->url, 'share') !== false) OR (strpos($item->path, 'share') !== false))
          @can('Role View')
        <a class="btn btn-icon add-tooltip" data-original-title="Rename" data-toggle="tooltip" href="javascript:rename('{{ $item->name }}')" title="{{ Lang::get('laravel-filemanager::lfm.menu-rename') }}"><i class="icon-2x pli-file-edit"></i></a>
          @endcan

        @else  
        <a class="btn btn-icon add-tooltip" data-original-title="Rename" data-toggle="tooltip" href="javascript:rename('{{ $item->name }}')" title="{{ Lang::get('laravel-filemanager::lfm.menu-rename') }}"><i class="icon-2x pli-file-edit"></i></a>
        @endif


       @if((strpos($item->url, 'share') !== false) OR (strpos($item->path, 'share') !== false))
          @can('Role View')
          <a class="btn btn-icon add-tooltip" data-original-title="Delete" data-toggle="tooltip" href="javascript:trash('{{ $item->name }}')" title="{{ Lang::get('laravel-filemanager::lfm.menu-delete') }}"><i class="icon-2x demo-pli-recycling"></i></a>
          @endcan

        @else
              <a class="btn btn-icon add-tooltip" data-original-title="Delete" data-toggle="tooltip" href="javascript:trash('{{ $item->name }}')" title="{{ Lang::get('laravel-filemanager::lfm.menu-delete') }}"><i class="icon-2x demo-pli-recycling"></i></a>
        @endif

      </div>
      <div class="file-attach-icon"></div>
      <a class="{{ $item->is_file ? 'file' : 'folder'}}-item clickable file-details" data-id="{{ $item->is_file ? $item->url : $item->path }}" title="{{$item->name}}">
        <div class="media-block">
          <div class="media-left"><i class="fa {{ $item->icon }}"></i></div>
          <div class="media-body">
            <p class="file-name" style="margin-bottom:0px;">{{ str_limit($item->name, $limit = 40, $end = '...') }}</p>
            <small>{{ $item->time }} | {{ $item->size }} | {{ $item->type }}</small>
          </div>
        </div>
      </a>
    </li>
  @endforeach
</ul>


<table class="table visible-xs">
  <tbody>
  @foreach($items as $item)
    <tr>
      <td>
        <div class="media" style="height: 70px;">
          <div class="media-left">
            <div style="border:none;" class="square {{ $item->is_file ? 'file' : 'folder'}}-item clickable"  data-id="{{ $item->is_file ? $item->url : $item->path }}">
              @if($item->thumb)
                <img src="{{ $item->thumb }}">
              @else
                <i class="fa {{ $item->icon }} fa-5x"></i>
              @endif
            </div>
          </div>
          <div class="media-body" style="padding-top: 10px;">
            <div class="media-heading">
              <p>
                <a class="{{ $item->is_file ? 'file' : 'folder'}}-item clickable" data-id="{{ $item->is_file ? $item->url : $item->path }}">
                  {{ str_limit($item->name, $limit = 20, $end = '...') }}
                </a>
                &nbsp;&nbsp;
                {{-- <a href="javascript:rename('{{ $item->name }}')">
                  <i class="fa fa-edit"></i>
                </a> --}}
              </p>
            </div>
            <p style="color: #aaa;font-weight: 400">{{ $item->time }}</p>
          </div>
        </div>
      </td>
    </tr>
  @endforeach
  </tbody>
</table>

@else
  <p>{{ trans('laravel-filemanager::lfm.message-empty') }}</p>
@endif