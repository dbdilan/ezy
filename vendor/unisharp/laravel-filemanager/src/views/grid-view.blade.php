@if((sizeof($files) > 0) || (sizeof($directories) > 0))
        
  <!-- -->
<div class="row">
  @foreach($items as $item)
  <div style="margin-top:10px;" class="col-xs-6 col-sm-4 col-md-3 col-lg-2 img-row">
    <?php $item_name = $item->name; ?>
    <?php $thumb_src = $item->thumb; ?>
    <?php $item_path = $item->is_file ? $item->url : $item->path; ?>

    <div style="border: none;" class="square clickable {{ $item->is_file ? '' : 'folder-item' }}" data-id="{{ $item_path }}"
           @if($item->is_file && $thumb_src) onclick="useFile('{{ $item_path }}', '{{ $item->updated }}')"
           @elseif($item->is_file) onclick="download('{{ $item_name }}')" @endif >
      @if($thumb_src)
      <img src="{{ $thumb_src }}">
      @else
      <i class="fa {{ $item->icon }} fa-5x"></i>
      @endif
    </div>

      <!--<div class="btn-file-toolbar">
        @if($item->is_file)
          <a class="btn btn-icon add-tooltip" data-original-title="Delete" data-toggle="tooltip" href="javascript:download('{{ $item->name }}')" title="{{ Lang::get('laravel-filemanager::lfm.menu-download') }}"><i class="demo-pli-download-from-cloud"></i></a>
          @if($item->thumb)
            <a class="btn btn-icon add-tooltip" data-original-title="File View" data-toggle="tooltip" href="javascript:fileView('{{ $item->url }}', '{{ $item->updated }}')" title="{{ Lang::get('laravel-filemanager::lfm.menu-view') }}"><i class="pli-preview"></i></a>
            <a class="btn btn-icon add-tooltip" data-original-title="Crop Image" data-toggle="tooltip" href="javascript:cropImage('{{ $item->name }}')" title="{{ Lang::get('laravel-filemanager::lfm.menu-crop') }}"><i class="pli-crop"></i></a>
            <a class="btn btn-icon add-tooltip" data-original-title="Resize Image" data-toggle="tooltip" href="javascript:resizeImage('{{ $item->name }}')" title="{{ Lang::get('laravel-filemanager::lfm.menu-resize') }}"><i class="pli-resize"></i></a>
          @endif
        @endif
        <a class="btn btn-icon add-tooltip" data-original-title="Rename" data-toggle="tooltip" href="javascript:rename('{{ $item->name }}')" title="{{ Lang::get('laravel-filemanager::lfm.menu-rename') }}"><i class="pli-file-edit"></i></a>
        <a class="btn btn-icon add-tooltip" data-original-title="Delete" data-toggle="tooltip" href="javascript:trash('{{ $item->name }}')" title="{{ Lang::get('laravel-filemanager::lfm.menu-delete') }}"><i class="demo-pli-recycling"></i></a>
      </div>-->

    <div class="caption text-center">
      <div class="btn-group">
        <button type="button" data-id="{{ $item_path }}"
                class="item_name btn btn-default btn-xs {{ $item->is_file ? '' : 'folder-item'}}"
                @if($item->is_file && $thumb_src) onclick="useFile('{{ $item_path }}', '{{ $item->updated }}')"
                @elseif($item->is_file) onclick="download('{{ $item_name }}')" @endif >
          {{ $item_name }}
        </button>
        <button type="button" class="btn btn-default dropdown-toggle btn-xs" data-toggle="dropdown" aria-expanded="false">
          <span class="caret"></span>
          <span class="sr-only">Toggle Dropdown</span>
        </button>
        <ul style="width:100%;" class="dropdown-menu" role="menu">
          <li><a href="javascript:rename('{{ $item_name }}')"><i class="pli-file-edit"></i> {{ Lang::get('laravel-filemanager::lfm.menu-rename') }}</a></li>
          @if($item->is_file)
          <li><a href="javascript:download('{{ $item_name }}')"><i class="demo-pli-download-from-cloud"></i> {{ Lang::get('laravel-filemanager::lfm.menu-download') }}</a></li>
          <li class="divider"></li>
          @if($thumb_src)
          <li><a href="javascript:fileView('{{ $item_path }}', '{{ $item->updated }}')"><i class="pli-preview"></i> {{ Lang::get('laravel-filemanager::lfm.menu-view') }}</a></li>
          <li><a href="javascript:resizeImage('{{ $item_name }}')"><i class="pli-resize"></i> {{ Lang::get('laravel-filemanager::lfm.menu-resize') }}</a></li>
          <li><a href="javascript:cropImage('{{ $item_name }}')"><i class="pli-crop"></i> {{ Lang::get('laravel-filemanager::lfm.menu-crop') }}</a></li>
          <li class="divider"></li>
          @endif
          @endif
          <li><a href="javascript:trash('{{ $item_name }}')"><i class="demo-pli-recycling"></i> {{ Lang::get('laravel-filemanager::lfm.menu-delete') }}</a></li>
        </ul>
      </div>
    </div>

  </div>
  @endforeach
</div>
@else
<p>{{ Lang::get('laravel-filemanager::lfm.message-empty') }}</p>
@endif
