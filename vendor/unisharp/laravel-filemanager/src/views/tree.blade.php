<p class="pad-hor mar-top text-main text-bold text-sm text-uppercase">Personal</p>
<div class="list-group bg-trans pad-btm bord-btm">
    @foreach($root_folders as $root_folder)

        <a class="clickable folder-item list-group-item" data-id="{{ $root_folder->path }}">
            <i class="pli-folder-bookmark text-info icon-lg icon-fw"></i> {{ $root_folder->name }}
        </a>

        @foreach($root_folder->children as $directory)

            <a style="margin-left: 10px;" class="clickable folder-item list-group-item" data-id="{{ $directory->path }}">
                <i class="text-info icon-lg icon-fw demo-pli-folder "></i> {{ $directory->name }}
            </a>

        @endforeach
        @if($root_folder->has_next)
            <hr>
            <p class="pad-hor mar-top text-main text-bold text-sm text-uppercase">Shared</p>
        @endif
    @endforeach
</div>
